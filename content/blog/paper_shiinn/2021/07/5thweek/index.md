---
title: 論文試飲メモ(2021年07月第5週)
date: "2021-08-01T04:00:00.000Z"
description: "2021年7月第5週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Fully gapped superconductivity with preserved time reversal symmetry innoncentrosymmetric LaPdIn** [arXiv:2107.09895 [cond-mat.supr-con]](https://arxiv.org/abs/2107.09895)

  六方晶系非正方晶化合物であるLaPdInの超伝導の調査. 

  ![2107.09895](/kohgasa/2107.09895.jpg)

- **Spin-wave growth via Shapiro resonances in a spinor Bose-Einstein condensate** [arXiv:2107.09924 [cond-mat.quant-gas]](https://arxiv.org/abs/2107.09924)

  2次Zeeman結合によって周期的に駆動されるスピン1のBose-Einstein凝縮体におけるShapiro吸収についての調査. 

  ![2107.09924](/kohgasa/2107.09924.jpg)

- **Dynamics of two ferromagnetic insulators coupled by superconducting spin current** [arXiv:2107.09959 [cond-mat.supr-con]](https://arxiv.org/abs/2107.09959)

  2つの強磁性体に挟まれた従来型超伝導体がどのようにコヒーレント平衡スピン流を維持することができるのかを示している. 

  ![2107.09959](/kohgasa/2107.09959.jpg)

- **Controllable enhancement of $p$-wave superconductivity via magnetic couplingto a conventional superconductor** [arXiv:2107.10263 [cond-mat.supr-con]](https://arxiv.org/abs/2107.10263)

  非従来型超伝導体の$T_{\text{c}}$ブーストは強磁性層における磁化の方向を通じて変調させることができること, $T_{\text{c}}$の上昇は外部磁場によるZeeman効果を用いて達成されうることを示している. 

  ![2107.10263](/kohgasa/2107.10263.jpg)

- **Prevalence of tilted stripes in La1.88Sr0.12CuO4 and the importance of t′in the Hamiltonian** [arXiv:2107.10264 [cond-mat.str-el]](https://arxiv.org/abs/2107.10264)

  高分解能中性子散乱を用いて, $x=0.12$のLSCOが低温で二つの共存している相を持ち, 一つは静的なスピン縞を, もう一つは揺らぎのあるスピン縞を持ち, どちらの層も同じ傾き角を持っていることを見出している. 

  ![2107.10264](/kohgasa/2107.10264.jpg)

- **Parametric longitudinal coupling between a high-impedance superconductingresonator and a semiconductor quantum dot singlet-triplet spin qubit** [arXiv:2107.10269 [cond-mat.mes-hall]](https://arxiv.org/abs/2107.10269)

  スピンキュビットと共振器の間の縦波相互作用に基づいたスピン-フォノン結合を実現し, 高インピーダンス超伝導共振器に対して一重項-三重項キュビットを結合させることで共振周波数付近で期待される縦波結合が起こることを示している. 

  ![2107.10269](/kohgasa/2107.10269.jpg)

- **Evolution of transport properties in FeSe thin flakes with thickness approaching thetwo-dimensional limit** [arXiv:2107.10481 [cond-mat.supr-con]](https://arxiv.org/abs/2107.10481)

  剥離を通してバルクの単結晶から二層までの厚さを持つFeSeの超伝導性とネマティシティの発展についての包括的な研究を提示している. 

  ![2107.10481](/kohgasa/2107.10481.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
- [Tunisian president dismisses government, sparking jubilation on streets](https://www.theguardian.com/world/2021/jul/26/tunisian-president-dismisses-government-sparking-street-celebrations)
- [Investigation into the Surfside condo building collapse won't begin while the site remains a crime scene, expert says](https://edition.cnn.com/2021/07/19/us/miami-dade-building-collapse-monday/index.html)
- [Video shows salmon injured by unlivable water temperatures after heatwave](https://www.theguardian.com/us-news/2021/jul/27/salmon-boiled-alive-pacific-north-west-heatwave-video)
- [Dell pulls energy-hungry gaming PCs in six US states after failing efficiency rules](https://www.theguardian.com/games/2021/jul/29/dell-pulls-energy-hungry-gaming-pcs-in-six-us-states-after-failing-efficiency-rules)
- [Over 5m people in UK had parcels lost or stolen last year, says Citizens Advice](https://www.theguardian.com/business/2021/jul/30/over-5m-people-in-uk-had-parcels-lost-or-stolen-last-year-says-citizens-advice)
- [Greenland: enough ice melted on single day to cover Florida in two inches of water](https://www.theguardian.com/environment/2021/jul/30/greenland-ice-sheet-florida-water-climate-crisis)
- [The Observer view on the Royal Navy’s operation in the South China Sea](https://www.theguardian.com/commentisfree/2021/aug/01/observer-view-royal-navy-south-china-sea)