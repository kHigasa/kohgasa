---
title: 論文試飲メモ(2021年07月第1週)
date: "2021-07-04T04:00:00.000Z"
description: "2021年7月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Unitary $p$-wave Fermi gas in one dimension** [arXiv:2106.12909 [cond-mat.quant-gas]](https://arxiv.org/abs/2106.12909)

  $p$波Feshbach共鳴付近の1次元2成分超低温Fermi気体の従う普遍的な状態方程式を多体$T$行列法とヴィリアル展開を用いて与えている. 

  ![2106.12909](/kohgasa/2106.12909.jpg)

- **Theory of Multi-Orbital Topological Superconductivityin Transition Metal Dichalcogenides** [arXiv:2106.13075 [cond-mat.supr-con]](https://arxiv.org/abs/2106.13075)

  軌道内項と軌道間項の両方を含むオンサイトペアリングポテンシャルを仮定して, 遷移金属ヂカルコゲナイド（TMD）単層膜における可能な超伝導状態を研究, 系の面に関する鏡映対称性が破れているとき, この種のペアリングが時間反転不変のノーダルでフルにギャップの開いたトポロジカル相を含んだ普通でない超伝導性を生じさせることが見出されている. また多軌道繰り込み群の手法を用いて, これらの相が局所Coulomb斥力とHund則結合, フォノンの媒介する相互作用間の相互影響から引き起こされることが示されている. 

  ![2106.13075](/kohgasa/2106.13075.jpg)

- **Low-dimensional antiferromagnetic fluctuations in the heavy-fermion paramagneticladder UTe2** [arXiv:2106.13087 [cond-mat.str-el]](https://arxiv.org/abs/2106.13087)

  重いフェルミオン常磁性UTe$_2$の単結晶に対して, 超伝導転移温度より上の温度で中性子散乱実験を行い, 波数ベクトル$\bm{k}_1=(0,0.57,0)$で反磁性的揺らぎがあることが示されている. 

  ![2106.13087](/kohgasa/2106.13087.jpg)

- **Quantum-tailored machine-learning characterization of a superconducting qubit** [arXiv:2106.13126 [quant-ph]](https://arxiv.org/abs/2106.13126)

  超伝導キュビットのダイナミクスを特徴づけ, パラメータを学習するための機械学習アプローチの設計に量子力学の特徴を取り入れている. 

  ![2106.13126](/kohgasa/2106.13126.jpg)

- **Synthesis and Characterization of Ca-Substituted Infinite-Layer Nickelate Crystals** [arXiv:2106.13171 [cond-mat.supr-con]](https://arxiv.org/abs/2106.13171)

  高圧力法を用いて, 連続して無限層相La$_{1-x}$Ca$_x$NiO$_{2+\delta}$になる, ペロヴスカイトニッケレートLa$_{1-x}$Ca$_x$NiO$_3$の高品質な単結晶を合成し, その物性を調べている. 

  ![2106.13171](/kohgasa/2106.13171.jpg)

- **Chiral Quantum Network with Giant Atoms** [arXiv:2106.13187 [quant-ph]](https://arxiv.org/abs/2106.13187)

  超伝導量子回路において, しばしばフェライトサーキュレータを用いて実現されるカイラルルーティング量子情報は大抵損失が多く強磁場が必要であるが, それらの問題を克服するためにフォトニック結晶導波管 (PCW) と相互作用する巨大な原子を用いての実現を示している. 

  ![2106.13187](/kohgasa/2106.13187.jpg)

- **Acoustic-phonon-mediated superconductivity in rhombohedral trilayer graphene** [arXiv:2106.13231 [cond-mat.supr-con]](https://arxiv.org/abs/2106.13231)

  菱面体3層グラフェンにおいて電子-音響フォノン結合を調査し, $s-$波スピン一重項と$f-$波スピン三重項対が同じ$T_{\text{c}}$を生み, 他の対は無視できるほどの$T_{\text{c}}$しか生まないことを示している. 

  ![2106.13231](/kohgasa/2106.13231.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
- [US strikes hit Iran-backed militia facilities in Iraq and Syria](https://www.theguardian.com/world/2021/jun/28/us-strikes-hit-iran-backed-militia-facilities-in-iraq-and-syria)
- [New weight-loss tool prevents mouth from opening more than 2mm](https://www.theguardian.com/lifeandstyle/2021/jun/28/new-weight-loss-tool-dentalslim-diet-control)
- [Supreme Court allows coronavirus eviction moratorium to remain in place](https://edition.cnn.com/2021/06/29/politics/supreme-court-eviction-moratorium-cdc/index.html)
- [Tim Berners-Lee’s NFT of world wide web source code sold for $5.4m](https://www.theguardian.com/technology/2021/jun/30/world-wide-web-nft-sold)
- [US halts all federal executions amid review of capital punishment](https://www.theguardian.com/world/2021/jul/01/us-federal-executions-halted-attorney-general)
- [‘A scourge of the Earth’: grasshopper swarms overwhelm US west](https://www.theguardian.com/environment/2021/jul/04/grasshopper-swarms-us-west-drought)
- [Massachusetts police standoff with heavily armed men ends in 11 arrests](https://edition.cnn.com/2021/07/03/us/shelter-in-place-wakefield-reading-massachusetts/index.html)