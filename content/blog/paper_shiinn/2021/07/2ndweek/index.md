---
title: 論文試飲メモ(2021年07月第2週)
date: "2021-07-11T04:00:00.000Z"
description: "2021年7月第2週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Quantum magnetism and topological superconductivity in Yu-Shiba-Rusinov chains** [arXiv:2107.00031 [cond-mat.mes-hall]](https://arxiv.org/abs/2107.00031)

  Yu-Shiba-Rusinov (YSR) 鎖が量子磁性と相関電子ダイナミクスの将来有望なプラットフォームであることを示している. 

  ![2107.00031](/kohgasa/2107.00031.jpg)

- **Higher angular momentum pairings in inter-orbital shadowed-triplet superconductors: Application to Sr2RuO4** [arXiv:2107.00047 [cond-mat.supr-con]](https://arxiv.org/abs/2107.00047)

  より高い角運動量を持つ対がスピン三重項対の形を生じさせるのかを明らかにするために, 相関した金属のスピン軌道結合と電子的分散間の相互作用を調べ, Sr$_2$RuO$_4$について複数のスピン軌道結合のパラメータを変調させて異なった対状態間の競合を決定している. 

  ![2107.00047](/kohgasa/2107.00047.jpg)

- **p-wave superconductivity and the axi-planar phase of triple-point fermions** [arXiv:2107.00171 [cond-mat.supr-con]](https://arxiv.org/abs/2107.00171)

  反対のカイラリティの2つの擬スピン$s=1$低エネルギーFermionの反転, 回転対称性を持つ系における弱結合超伝導を考え, もたらされる$p-$波秩序変数についてGinzburg-Landauの自由エネルギーを計算, この場合にそれが最小化され, 結果として過剰な縮退と時間反転対称性を破る "axi-planar" $p-$波に類似した秩序状態が得られることを示している. 

- **Full orbital decomposition of Yu-Shiba-Rusinov states based on first principles** [arXiv:2107.00237 [cond-mat.supr-con]](https://arxiv.org/abs/2107.00237)

  Bogoliubov-de Gennes (BdG) 方程式を実装し, 磁性不純物があるときに存在するYu-Shiba-Rusinov (YSR) 状態に異なった軌道がどのような影響をもたらすかを示している. 

  ![2107.00237](/kohgasa/2107.00237.jpg)

- **Charge density wave in single-layer Pb/Ge(111) driven by Pb-substrate exchangeinteraction** [arXiv:2107.00257 [cond-mat.str-el]](https://arxiv.org/abs/2107.00257)

  第一原理計算を行うことによってPbと基板間の非局所交換が系に$3\times 3$電荷密度波を駆動することを示している. 

  ![2107.00257](/kohgasa/2107.00257.jpg)

- **Anisotropic Upper Critical Field, Seebeck and Nernst Coefficient in Nb0.20Bi2Se3Topological Superconductor** [arXiv:2107.00432 [cond-mat.supr-con]](https://arxiv.org/abs/2107.00432)

  NbドープされたBi$_2$Se$_3$トポロジカル超伝導体の磁気輸送と熱電効果を調べ, 角度依存磁気抵抗の調査から上部臨界磁場における異方性を明らかにしている. 

  ![2107.00432](/kohgasa/2107.00432.jpg)

- **Ground-State Cooling of a Mechanical Oscillator via a HybridElectro-Optomechanical System** [arXiv:2107.00510 [quant-ph]](https://arxiv.org/abs/2107.00510)

  超伝導キュビットと空洞場に同時に結合させることにより, 力学的共振器の基底状態冷却を実現する方法を提案している. 

  ![2107.00510](/kohgasa/2107.00510.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
- [Number of NHS mental health beds down by 25% since 2010, analysis shows](https://www.theguardian.com/society/2021/jul/05/number-of-nhs-mental-health-beds-down-by-25-since-2010-analysis-shows)
- [Maricopa County supervisor on rejecting calls from Trump allies: 'Whatever needed to be said, needed to be said in a courtroom'](https://edition.cnn.com/2021/07/05/politics/clint-hickman-trump-giuliani-election-calls-maricopa-county-cnntv/index.html)
- [Xi Jinping urges countries to confront ‘technology blockades’ in swipe at US](https://www.theguardian.com/world/2021/jul/07/xi-jinping-urges-countries-to-confront-technology-blockades-in-swipe-at-us)
- [A 12-foot Burmese python has been loose in a Louisiana mall store for days](https://edition.cnn.com/2021/07/07/us/python-missing-louisiana-trnd/index.html)
- [Pfizer sees waning immunity from its Covid-19 vaccine, says developing new booster](https://edition.cnn.com/2021/07/08/health/pfizer-waning-immunity-bn/index.html)
- [An Alabama teen raised $39,000 for kids with cancer by cutting off his beloved 19-inch Afro](https://edition.cnn.com/2021/07/10/us/teenage-19-inch-afro-kids-cancer-kieran-mose-trnd/index.html)
- [In California’s interior, there’s no escape from the desperate heat: ‘Why are we even here?’](https://www.theguardian.com/us-news/2021/jul/10/california-central-valley-extreme-heat-race)