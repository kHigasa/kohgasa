---
title: 論文試飲メモ(2021年07月第4週)
date: "2021-07-25T04:00:00.000Z"
description: "2021年7月第4週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Anapole superconductivity from PT-symmetric mixed-parity interband pairing** [arXiv:2107.07096 [cond-mat.supr-con]](https://arxiv.org/abs/2107.07096)

  時間反転対称性の破れた混合パリティ超伝導状態は一般的に非ユニタリバンド内対形成により, 異常反対称Bogoliubovスペクトルを表すことを示し, そのスペクトルに対する必要条件を与えている. 更に, 反対称Bogoliubov準粒子が超伝導状態の実効的なアナポールモーメントをもたらし, ゼロ磁場下で非一様Fulde-Ferrell-Larkin-Ovchinnikov状態を安定化させることを示している. 

  ![2107.07096](/kohgasa/2107.07096.jpg)

- **Continuous Real-Time Detection of Quasiparticle Trapping** [arXiv:2107.07125 [cond-mat.mes-hall]](https://arxiv.org/abs/2107.07125)

  伝送線共振器に組み込まれた2ジャンクションアルミニウムナノブリッジ超伝導量子干渉装置に基づいた準粒子トラッピング検出器の特性を明らかにしている. 

  ![2107.07125](/kohgasa/2107.07125.jpg)

- **Multipolar nematic state of nonmagnetic FeSe based on the DFT+U method** [arXiv:2107.07244 [cond-mat.str-el]](https://arxiv.org/abs/2107.07244)

  FeSeの非磁性常伝導状態における電子状態の$U$依存性を調べ, ネマティック状態に対する完全多極子解析を行って, $U$が増加するにつれて, 常伝導状態はネマティック基底状態の出現前にFermi面のトポロジカルな変化を経験することが見出されている. その結果もたらされるネマティック基底状態は各Feサイトに$E$表示における反磁性六極子と$B_2$表示における強磁性多極子を持っている多極子状態となっている. 

  ![2107.07244](/kohgasa/2107.07244.jpg)

- **London model of dual color superconductor** [arXiv:2107.07251 [hep-ph]](https://arxiv.org/abs/2107.07251)

  超伝導におけるMeissner効果のLondon現象論の論理に従って, chromo-electric場の短距離の振る舞いの起源を説明し, London双色超伝導電流が強く相互作用するクォーク-グルーオンプラズマの液滴における殆ど完全な流動性をもたらすことを示唆している. 

- **Implementing efficient selective quantum process tomography of superconducting quantum gates on the IBM quantum processor** [arXiv:2107.07462 [quant-ph]](https://arxiv.org/abs/2107.07462)

  実験的な複雑さを減らすために, 標準的な選択量子過程トモグラフィー (SQPT) を数学的に最定式化している. 

- **Colossal orbital-Edelstein effect in non-centrosymmetric superconductors** [arXiv:2107.07476 [cond-mat.supr-con]](https://arxiv.org/abs/2107.07476)

  反転対称性の破れた超伝導体において, 超流動によって誘発される軌道磁化がスピンによるものよりも大きさのオーダーが1以上大きく, これが巨大な磁気-電気効果をもたらしていることを見出している. 

  ![2107.07476](/kohgasa/2107.07476.jpg)

- **Transient Higgs oscillations and high-order nonlinear light-Higgs coupling in terahertz-wave driven NbN superconductor** [arXiv:2107.07488 [cond-mat.supr-con]](https://arxiv.org/abs/2107.07488)

  強テラヘルツ波へのNbN薄膜の非線型光学応答を調べ, $T_{\text{c}}$に向かって温度が上がるにつれて周波数においてソフトにさせる新たな一時的振動を観測, Higgs一時的振動と同定している. 

  ![2107.07488](/kohgasa/2107.07488.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
- [Two Athletes Have Tested Positive For COVID-19 Inside The Olympic Village](https://www.npr.org/2021/07/18/1017606827/two-athletes-have-tested-positive-for-covid-19-inside-the-olympic-village)
- [First US Capitol rioter convicted of a felony gets 8 months in prison after DOJ says stiffer sentence could stop future attacks](https://edition.cnn.com/2021/07/19/politics/capitol-riot-felony-paul-hodgkins/index.html)
- [Typhoon In-fa strengthening while on track to impact Japan, Taiwan and China](https://edition.cnn.com/2021/07/20/weather/typhoon-cempaka-in-fa-flood-china-taiwan-japan/index.html)
- [West Virginia's anti-trans sports ban temporarily blocked by federal judge](https://edition.cnn.com/2021/07/21/politics/west-virginia-trans-sports-ban-blocked/index.html)
- [Hospitalized Covid-19 patients are asking for the vaccine -- when it's too late, two health care workers say](https://edition.cnn.com/2021/07/22/health/hospitalized-patients-covid-19-vaccine/index.html)
- [Stonehenge may be next UK site to lose world heritage status](https://www.theguardian.com/uk-news/2021/jul/23/stonehenge-may-be-next-uk-site-to-lose-world-heritage-status)
- [Roses out, olives in: the new English garden in a time of climate crisis](https://www.theguardian.com/lifeandstyle/2021/jul/25/roses-out-olives-in-the-new-english-garden-in-a-time-of-climate-crisis)