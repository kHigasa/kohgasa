---
title: 論文試飲メモ(2021年07月第3週)
date: "2021-07-18T04:00:00.000Z"
description: "2021年7月第3週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Superconducting Magnets** [arXiv:2107.03177 [cond-mat.supr-con]](https://arxiv.org/abs/2107.03177)

  超伝導マグネット. 

  ![2107.03177](/kohgasa/2107.03177.jpg)

- **Splitting of the superfluid transition in $_3$He by nematic aerogels.** [arXiv:2107.03343 [cond-mat.supr-con]](https://arxiv.org/abs/2107.03343)

  表題通り. 

- **Superconductivity from charge order in cuprates** [arXiv:2107.03421 [cond-mat.supr-con]](https://arxiv.org/abs/2107.03421)

  La$_2$CuO$_4$にドープすることにより得られる214種のクプラート族のレヴュー. 電荷ストライプ秩序がバルク超伝導と競合する傾向にあるが, 絡み合った秩序のような見方に移ってきているとのこと. 

  ![2107.03421](/kohgasa/2107.03421.jpg)

- **Non-Abelian monopoles in the multiterminal Josephson effect** [arXiv:2107.03435 [cond-mat.supr-con]](https://arxiv.org/abs/2107.03435)

  対称性制約散乱行列法を用いることによって, 複数ターミナルJosephson接合におけるAndreev束縛状態のスペクトル特性を解析し, 超伝導相の合成5次元空間におけるAndreev束縛状態の交差が第二クラスのChern数によって特徴づけられるトポロジカル電荷を持つ非Abelian $SU(2)$単極子を支持している可能性のあることを見出している. 

  ![2107.03435](/kohgasa/2107.03435.jpg)

- **Triplet superconductivity from non-local Coulomb repulsion in Sn/Si(111)** [arXiv:2107.03482 [cond-mat.supr-con]](https://arxiv.org/abs/2107.03482)

  ホールのドープされたSn/Si(111)における超伝導の不安定性の本質について調査し, 相互作用の拡張されたHubbard性が三重項対形成を生むのに重要であることを見出している. 

  ![2107.03482](/kohgasa/2107.03482.jpg)

- **Breakdown of induced p±ip pairing in a superconductor-semiconductor hybrid** [arXiv:2107.03695 [cond-mat.supr-con]](https://arxiv.org/abs/2107.03695)

  2次元Al-InAs混合系において誘起される超伝導を調べるために回路量子電磁気構造を用い, 磁場によって駆動される超流動密度の強い抑圧と強められた散逸を観測している. 

  ![2107.03695](/kohgasa/2107.03695.jpg)

- **Translation symmetry-enriched toric code insulator** [arXiv:2107.04030 [cond-mat.str-el]](https://arxiv.org/abs/2107.04030)

  並進対称性により強調されたトリックコードトポロジカル秩序を持つ2次元電子絶縁体の状態を3流体結合ワイヤモデルを用いて, アニオンスペクトルと結び目統計を調べている. 

  ![2107.04030](/kohgasa/2107.04030.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
- [Thousands march in Cuba in rare mass protests amid economic crisis](https://www.theguardian.com/global-development/2021/jul/12/thousands-march-in-cuba-in-rare-mass-protests-amid-economic-crisis)
- [Biden administration says states can restart pandemic unemployment benefits as lawsuits mount](https://edition.cnn.com/2021/07/12/politics/biden-administration-restart-pandemic-unemployment-benefits-lawsuits/index.html)
- [Feds arrest 5 family members from Texas in new US Capitol riot case](https://edition.cnn.com/2021/07/13/politics/munn-family-texas-us-capitol/index.html)
- ['They're not going to f**king succeed': Top generals feared Trump would attempt a coup after election, according to new book](https://edition.cnn.com/2021/07/14/politics/donald-trump-election-coup-new-book-excerpt/index.html)
- [Minimum wage workers can't afford rent anywhere in America](https://edition.cnn.com/2021/07/15/homes/rent-affordability-minimum-wage/index.html)
- ['Hollywood Ripper' serial killer sentenced to death for the murders of 2 women and attempted murder of another ](https://edition.cnn.com/2021/07/17/us/hollywood-ripper-death-sentence/index.html)
- [A woman mowing the lawn at a Canadian airstrip is struck and killed by a small plane making a landing](https://edition.cnn.com/2021/07/06/americas/canada-fatal-plane-accident-trnd/index.html)