---
title: 論文試飲メモ(2021年03月第4週前半)
date: "2021-03-24T04:00:00.000Z"
description: "2021年3月第4週前半に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

30稿分. 基本的には主張と手法をざっくり. 

- **Counting and Sampling Perfect Matchings in Regular Expanding Non-Bipartite Graphs** [arXiv:2103.08683 [cs.DS]](https://arxiv.org/abs/2103.08683)

$d$-正則強拡張(非2部)グラフ(2$n$個の頂点を有する)における完全マッチング数に対するほぼ完全マッチングな数の比が$n$の多項式であることが示されている. さらにそのようなグラフが少なくとも$\Omega(d)^n$個の完全マッチングを持つことを示している. 

![2103.08683](/kohgasa/2103.08683.jpg)

- **Space efficient quantum algorithms for mode, min-entropy and k-distinctness** [arXiv:2103.09033 [quant-ph]](https://arxiv.org/abs/2103.09033)

ブラックボックスとして与えられた量子回路の出力分散のモードがある閾値よりも大きいかどうかを決定するアルゴリズムで, 空間の複雑さが分散の領域の大きさにおいて対数関数的になるものを打ち立てている. これにより, $k$-識別やそのギャップ版, 分散の最小エントロピー推定を再考することが可能になる. 

![2103.09033](/kohgasa/2103.09033.jpg)

- **A Polynomial-Time Algorithm for Special Cases of the Unbounded Subset-Sum Problem** [arXiv:2103.09080 [cs.DS]](https://arxiv.org/abs/2103.09080)

無限部分和問題(USSP)を解く際, 入力セットが特別な場合には, 多項式時間で実行可能であるようなアルゴリズムを2つ提示. 

![2103.09080](/kohgasa/2103.09080.jpg)

- **On Linear Solution of “Cherry Pickup II”. Max Weight of Two Disjoint Paths in Node-Weighted Gridlike DAG** [arXiv:2103.09362 [cs.DS]](https://arxiv.org/abs/2103.09362)

Cherry Pickup II(CP2)問題は最小降下経路和(MFPS)問題の複数エージェント版であり, ここではCP2問題の線形解法を2つのvertex-disjoint経路の重みの和の最大値を発見する方法として提案している. さらにこの解法をより広い動きのルールを持ったCP2問題に拡張している. 

![2103.09362](/kohgasa/2103.09362.jpg)

- **Vertex Deletion Parameterized by Elimination Distance and Even Less** [arXiv:2103.09715 [cs.DS]](https://arxiv.org/abs/2103.09715)

頂点削除問題において解の大きさと木の幅を任意に小さくすることのできるパラメータ化のためのFPTアルゴリズムを提示. 

![2103.09715](/kohgasa/2103.09715.jpg)

- **On Guillotine Separable Packings for the Two-dimensional Geometric Knapsack Problem** [arXiv:2103.09735 [cs.DS]](https://arxiv.org/abs/2103.09735)

ギロチンカット制約の下での幾何的ナップサック問題に対する近似アルゴリズムの研究. $90$度回転を許す/許さない場合に対する多項式時間の$(1+\epsilon)$-近似アルゴリズムを提示し, ここで全ての入力数値データは$n$において多項式に境界付けられていると想定している. 

![2103.09735](/kohgasa/2103.09735.jpg)

- **Experimental requirements for entangled two-photon spectroscopy** [arXiv:2103.10079 [quant-ph]](https://arxiv.org/abs/2103.10079)

高い分解能を持つエンタングルした光子のスペクトルの形状を実現する実験的セットアップの提示, 非線形結晶において周波数生成の和を検知して性質を評価, 古典的な光と効率を比較している. 

![2103.10079](/kohgasa/2103.10079.jpg)

- **Semi-Quantum Inspired Lightweight Mediated Quantum Key Distribution with Limited Resource and Untrusted TP** [arXiv:2103.10119 [quant-ph]](https://arxiv.org/abs/2103.10119)

ハイコストであるBell状態の生成・測定をなくし, 限られたリソースの下での半量子軽量量子鍵配送を提示. このプロトコルでは, 信頼されないサードパーティ(TP)が1つの光子に関連した量子操作を実行し, 参加者は2つの量子操作を実行するのみである. 

![2103.10119](/kohgasa/2103.10119.jpg)

- **Experimental SWAP test of infinite dimensional quantum states** [arXiv:2103.10219 [quant-ph]](https://arxiv.org/abs/2103.10219)

トラップされた$^{171}\mathrm{Yb}^+$イオンの系における2つの運動状態の重なりを測定するSWAPテストを実験的に行っている. この測定をFock状態やコヒーレント状態, スクイーズド真空状態, 猫の状態に対して行い, 汎用性を示している. また, 混合状態の純度を測定することによって, SWAPテストの応用を強調している. 

![2103.10219](/kohgasa/2103.10219.jpg)

- **Non-metric interaction rules in models of active matter** [arXiv:2103.10239 [cond-mat.soft]](https://arxiv.org/abs/2103.10239)

非計量相互作用を与えたアクティヴマターモデルの調査. 

![2103.10239](/kohgasa/2103.10239.jpg)

- **Domain Generalization using Ensemble Learning** [arXiv:2103.10257 [cs.LG]](https://arxiv.org/abs/2103.10257)

モデル全体の予測をより汎化させるために, ある1つのインプットソースで訓練された土台の深層学習モデルの上にアンサンブルモデルを構築(5つ構築されている), 正確性向上. 

![2103.10257](/kohgasa/2103.10257.jpg)

- **Soft mode theory of ferroelectric phase transitions in the low-temperature phase** [arXiv:2103.10262 [cond-mat.mtr]](https://arxiv.org/abs/2103.10262)

フォノンソフト化と不安定性の観点から低温層で起こることを説明することにより, 強誘電変位型相転移のソフトモード理論を提示. また, 強く非調和的なフォノン減衰を持つ物質に対する一般化されたLyddane-Sachs-Teller(LST)方程式を新たな方法で導いている. 

![2103.10262](/kohgasa/2103.10262.jpg)

- **Comment on: “Crossover of Charge Fluctuations across the Strange Metal Phase Diagram”** [arXiv:2103.10268 [cond-mat.supr-con]](https://arxiv.org/abs/2103.10268)

反射における電子エネルギー損失分光器(R-EELS)の実験結果の解釈が誤っているという指摘. その誤りは実験の運動量のスケールが不正確であることより説明され, さらにLindhard模型を使えばもっと多くの実験結果が説明される. 

![2103.10268](/kohgasa/2103.10268.jpg)

- **Requirement Engineering Challenges for AI-intense Systems Development** [arXiv:2103.10270 [cs.LG]](https://arxiv.org/abs/2103.10270)

  内容=タイトル. 具体的な*challenges*は
  1. 文脈上の定義と要求
  1. データ属性と要求
  1. パフォーマンスの定義とモニタリング
  1. システムの容認と成功に対する人間の影響 . 

- **TOP: Backdoor Detection in Neural Networks via Transferability of Perturbation** [arXiv:2103.10274 [cs.LG]](https://arxiv.org/abs/2103.10274)

バックドアを検知するために訓練された深層ニューラルネットワーク(DNNs)のモデルでは, 敵の画像から画像への摂動が綺麗なデータによるモデルよりも改変されたデータによるモデルの方が容易く転移することを同定. 

![2103.10274](/kohgasa/2103.10274.jpg)

- **Quantum-enhanced radiometry via approximate quantum error correction** [arXiv:2103.10281 [quant-ph]](https://arxiv.org/abs/2103.10281)

Boson的なモードの巨大なHilbert空間を探索し, 近似的量子誤り訂正(QEC)と量子ジャンプ追跡手法を開発することによって, 異なったエンコーディングを持つBoson的プローブ内センシングにおける量子向上を実験的に示している. 

![2103.10281](/kohgasa/2103.10281.jpg)

- **Analytical Energy Formalism and Kinetic Effects of Grain Boundary: A Case Study of Graphene** [arXiv:2103.10283 [cond-mat]](https://arxiv.org/abs/2103.10283)

グラフェンについて粒子境界の角度空間におけるエネルギー関数の解析. また速度効果と粒子境界角の関係も見出している. 

![2103.10283](/kohgasa/2103.10283.jpg)

- **MODELING THE SECOND PLAYER IN DISTRIBUTIONALLY ROBUST OPTIMIZATION** [arXiv:2103.10282 [cs.LG]](https://arxiv.org/abs/2103.10282)

分布的ロバスト最適化(DRO)問題をより大きなスケールの生成モデルの勾配に基づいた最適化へ適用可能にするKL成約内部最大化の緩和を提示し, ハイパーパラメータ探索の指針を与えるための選択ヒューリスティクスモデルを開発している. 

![2103.10282](/kohgasa/2103.10282.jpg)

- **Integrated Decision and Control: Towards Interpretable and Efficient Driving Intelligence** [arXiv:2103.10290 [cs.LG]](https://arxiv.org/abs/2103.10290)

  運転タスクを
  1. 複数経路計画 : 静的な制約のみを考慮して複数の経路を生成
  1. 最適追跡 : 動的な障害を考慮して最適な経路を追跡
  の2つの階層的に構造化されたタスクに分解し, 自動運転に対する解釈可能で効率的な決定と制御の枠組みを提案. さらに候補となる経路のそれぞれに対する制約付き最適制御問題(OCP)を理論的に定式化し, それらを個々に最適化, 最も良い追跡機能を示すものを選択. オフライン訓練とオンライン応用の枠組みによって重い計算を除去するための近似的制約付きOCPソルバーとしての強化学習(RL)アルゴリズムを提案. 

![2103.10290](/kohgasa/2103.10290.jpg)

- **Neural tensor contractions and the expressive power of deep neural quantum states** [arXiv:2103.10293 [quant-ph]](https://arxiv.org/abs/2103.10293)

一般的なテンソルネットワークと深層フィードフォワード人口ニューラルネットワークとの直接の関係の調査, 効率的にテンソル縮約を実行し, 一般的に適用される非線形活性化関数を用いるニューラルネットワーク層の構築. 多体量子状態の文脈では, この構築されたネットワークより, ニューラルネットワーク状態が厳密に実用的に使用可能な変分テンソルネットワークと同等かそれ以上の表現力を持つことが示されている. 

![2103.10293](/kohgasa/2103.10293.jpg)

- **Learning to Schedule Heuristics in Branch-and-Bound** [arXiv:2103.10294 [cs.LG]](https://arxiv.org/abs/2103.10294)

初の混合整数計画法(MIP)の厳密なソルバーにおけるスケジューリングヒューリスティクスに対するデータ駆動フレームワークを提案. 基本的なヒューリスティクスの性能を記述しているデータから学ぶことで, 最小コストで多くの解をまとめて見つける個々の問題特有のヒューリスティクスのスケジュールを得る. また問題の形式的な記述を与え, そのようなスケジュールを計算するための効率的なアルゴリズムを提案. 

![2103.10294](/kohgasa/2103.10294.jpg)

- **Thermal motion of skyrmion arrays in granular films** [arXiv:2103.10307 [cond-mat.mes-hall]](https://arxiv.org/abs/2103.10307)

粒子境界とスキルミオンの相互作用を調査. ラグタイムが長いとき, スキルミオンの拡散係数が減少し, ラグタイムが短いとき, 粒子境界との相互作用によってスキルミオンにジャイロトロピックな運動が誘起され拡散係数を強めている. 

![2103.10307](/kohgasa/2103.10307.jpg)

- **Faster quantum-inspired algorithms for solving linear systems** [arXiv:2103.10309 [quant-ph]](https://arxiv.org/abs/2103.10309)

量子線形ソルバーによって使用されるQRAMに類似したモデルにおける線形システムを解くために古典的なアルゴリズムを改良し, 乱雑化されたKaczmarz法と乱雑化された座標降下法に基づいた2つのアルゴリズムを提示. 

![2103.10309](/kohgasa/2103.10309.jpg)

- **Plasmonically enhanced spectrally selective narrowband MWIR and LWIR light detection based on hybrid nanopatterned graphene and phase changing vanadium oxide heterostructure operating close to room temperature** [arXiv:2103.10311 [cond-mat.mes-hall]](https://arxiv.org/abs/2103.10311)

ナノパターン化されたグラフェン(NPG)と二酸化ヴァナディウム($\mathcal{VO}_2$)により作られるハイブリッドヘテロ構造からなる, 中波長赤外(MWIR)と長波長赤外(LWIR)領域において動作する超高感度な中赤外(mid-IR)の光検出器のモデルを提示. 

![2103.10311](/kohgasa/2103.10311.jpg)

- **Motivation and Cognitive Abilities as Mediators between Polygenic Scores and Psychopathology in Children** [medrxiv:2020.06.08.20123877](https://www.medrxiv.org/content/10.1101/2020.06.08.20123877)

MDDとADHD多遺伝子スコア(PS)がP-因子に関連していることの発見. MDD PSと精神病理学の関連は罰則敏感性と認知能力によって, またADHD PSとの関連は報酬敏感性と認知能力によって部分的に調停される. 特に罰則敏感性の調停役割は感情的内面化に, 報酬敏感性の調停役割行動的外面化と精神病理学の神経発達段階に特有である. 

![2020.06.08.20123877](/kohgasa/2020.06.08.20123877.jpg)

- **Copy number variant detection with low-coverage whole-genome sequencing is a viable alternative to the traditional array-CGH** [medrxiv:2020.09.07.20183665](https://doi.org/10.1101/2020.09.07.20183665)

GenomeScreenという低適用範囲の全ゲノム配列に対する次世代配列(NGS)に基づいたコピー数多型(CNV)検知手法を提案し, この手法の理論的な適用限界を決定, それを拡張したシリコン内研究と遺伝子型のわかっている実際の患者のサンプルを用いて確かめている. また, 現在使われているミクロ配列に基づいた方法(aCGH)と比較して, パフォーマンスの向上を確かめている. 

![2020.09.07.20183665](/kohgasa/2020.09.07.20183665.jpg)

- **DeepCOVID: An Operational Deep Learning-driven Framework for Explainable Real-time COVID-19 Forecasting** [medrxiv:2020.09.28.20203109](https://doi.org/10.1101/2020.09.28.20203109)

DeepCOVIDという即時COVID-19予想に対して設計された深層学習フレームワークの提案. 疎なデータとノイジーな混成データ信号をうまく扱い, とくに短期的予想で優位性を持つ. 

![2020.09.28.20203109](/kohgasa/2020.09.28.20203109.jpg)

- **Predicting the Clinical Management of Skin Lesions using Deep Learning** [medrxiv:2020.11.02.20223941](https://doi.org/10.1101/2020.11.02.20223941)

診断を予測せずに直接に臨床管理の決定を患者の皮膚疾患の臨床画像に基づいて探索する手法を提示. 

![2020.11.02.20223941](/kohgasa/2020.11.02.20223941.jpg)

- **Correcting B0 inhomogeneity-induced distortions in whole-body diffusion MRI of bone** [medrxiv:2020.11.22.20236547](https://doi.org/10.1101/2020.11.22.20236547)

骨の空間変位を推定し, 歪み修正拡散強調MRI(DWI)の画像がより正確に根本的な解剖学を反映するかどうかを調査している. 

![2020.11.22.20236547](/kohgasa/2020.11.22.20236547.jpg)

- **Changes in symptomatology, re-infection and transmissibility associated with SARS-CoV-2 variant B.1.1.7: an ecological study** [medrxiv:2021.01.28.21250680](https://doi.org/10.1101/2021.01.28.21250680)

SARS-CoV-2のB.1.1.7変異種の地域人口と報告された症状, 疾病経過, 再感染率, 伝達性の関係を調べるための生態学的調査. いずれも目立った関連はなく, 現状の処置(ワクチン含め)が有効とのこと. 

![2021.01.28.21250680](/kohgasa/2021.01.28.21250680.jpg)
