---
title: 論文試飲メモ(2021年03月第3週前半)
date: "2021-03-17T04:00:00.000Z"
description: "2021年3月第3週前半に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

6稿分. 基本的には主張と手法をざっくり. 

- **Observation of quantum domain melting and its simulation with a quantum computer** [arXiv:2103.07343 [quant-ph]](https://arxiv.org/abs/2103.07343)

量子系をシミュレートするために量子コンピュータを用いるFeynmanの着想の直接の実現のして, 電子再構成ダイナミクスと領域溶解の調査を行い, 類似を探っている. また, STMにより測定した電子的領域再構成ダイナミクスの時間発展と量子領域溶解シミュレーションにおけるエンタングルした相関電子集団内領域の時間発展を比較している. 

![2103.07343](/kohgasa/2103.07343.jpg)

- **Investigation of the dependence of noise characteristics of SPAD on the gate parameters in sine-wave gated single-photon detectors** [arXiv:2103.07363 [quant-ph]](https://arxiv.org/abs/2103.07363)

量子鍵配送(QKD)のために使われる, 波長$1500\ \mathrm{nm}$に対する正弦波ゲート(SWG)単一光子探知機(SPD)を開発し調査. また, SPDのノイズ特性へのゲートパラメータの影響を調査. ある量子効率(QE)の値を固定したときに, ゲート電圧を増加させると暗計数率(DCR)が減少することを発見. 

![2103.07363](/kohgasa/2103.07363.jpg)

- **Boolean Hierarchical Tucker Networks on Quantum Annealers** [arXiv:2103.07399 [quant-ph]](https://arxiv.org/abs/2103.07399)

Boolの階層的タッカーネットワーク(BHTN)と呼ばれるBoolのテンソル分解の1種を計算して, D-Wave System社の量子アニーリング装置D-Wave 2000Qの能力を調査. 

![2103.07399](/kohgasa/2103.07399.jpg)

- **Subradiant-to-Subradiant Phase Transition in the Bad Cavity Laser** [arXiv:2103.07402 [quant-ph]](https://arxiv.org/abs/2103.07402)

悪性キャビティレーザーにおける定常状態の超放射の開始が定常状態にある副放射の散逸的な相転移に先導されることを示し, この相転移の特徴付け・物理的解釈を与えている. 

![2103.07402](/kohgasa/2103.07402.jpg)

- **Constant-Depth Circuits for Dynamic Simulations of Materials on Quantum Computers** [arXiv:2103.07429 [quant-ph]](https://arxiv.org/abs/2103.07429)

1次元Heisenberg模型から導かれる特別化したHamiltonian集合に対して, 多くの量子物質の動的シミュレーションの時間ステップを持つ深さ一定の回路を作成する手法の提示, シミュレーションの実行. 

![2103.07429](/kohgasa/2103.07429.jpg)

- **Symmetry-resolved entanglement detection using partial transpose moments** [arXiv:2103.07443 [quant-ph]](https://arxiv.org/abs/2103.07443)

混合状態におけるエンタングルメントを探知するための実験的に利用可能な条件の順序付けられた集合を提案, 対称性を利用し探知能力を向上させている. 

![2103.07443](/kohgasa/2103.07443.jpg)
