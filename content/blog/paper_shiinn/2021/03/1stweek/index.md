---
title: 論文試飲メモ(2021年03月第1週)
date: "2021-03-07T04:00:00.000Z"
description: "2021年3月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Analysis of Compton profile through information theory in H-like atoms inside impenetrable sphere** [arXiv:2102.13576 [quant-ph]](https://arxiv.org/abs/2102.13576)

閉じ込められた水素原子に対するComptonプロファイルの解析とそれを用いた情報理論におけるShannonエントロピーとOnicescuエネルギーの導出. 

![2102.13576](/kohgasa/2102.13576.jpg)

- **A Reconfigurable Quantum Local Area Network Over Deployed Fiber** [arXiv:2102.13596 [quant-ph]](https://arxiv.org/abs/2102.13596)

適応性のある帯域プロヴィジョニングと既製の制御系を持つ同時分散遠隔探知機を組み合わせて, キャンパス内の3つの建物に, 動的にかつシームレスに遠隔で再構成可能である最低損失の電気通信輸送の窓において8つの独立したエンタングルメントチャネルを持つLANを実装. これを用いると, セットアップにおいていかなる要素も付け加えたり削除したりすることなしに様々な構成と帯域の割当を行うことができる. 

1秒あたりのエンタングルしたビット数の観点からリンクのスループットを測定することによって, それぞれの論理結合の質を検証し, どのくらい異なったプロヴィジョニングの状況がこれらのレートを修正するのかを示している. また, 具体的にネットワークが量子プロトコルをサポートすることを示すために, 全てのリンクに対して遠隔量子状態生成(RSP)を確認している. 

![2102.13596](/kohgasa/2102.13596.jpg)

- **Second law of thermodynamics for quantum correlations** [arXiv:2102.13606 [quant-ph]](https://arxiv.org/abs/2102.13606)

量子または古典的相関がエルゴトロピーにおいて果たす役割に着目した解析. また, エルゴトロピーと2つの部分から成る系における量子相互情報の間の直接的な関係を証明. さらに, エルゴトロピーの量子相互情報と束縛エルゴトロピーの双方に対する関係を表す方程式を導出. これらの一般的な結果を有限温度の熱浴に結合された特定のキュビットにおいて説明し解析している. 

![2102.13606](/kohgasa/2102.13606.jpg)

- **Randomness Amplification under Simulated $\mathcal{PT}$-symmetric Evolution** [arXiv:2102.13630 [quant-ph]](https://arxiv.org/abs/2102.13630)

開放量子系の枠組みで$\mathcal{PT}$-対称な量子シミュレーションを考え, もし基底となるHilbert空間が実であるならば, BobはAliceの側から入力されたいかなる情報も受け取れないことを示している. この発見により, 
1. 系は複素場にわたるHilbert空間に関連している
1. 本物のランダムネスが弱い乱数生成器源から生成され得る暗号記法のプリミティヴやランダムネス増幅を編み出すことにおける直接の応用になる

という結果を得るため, 後者に関する実証をしている. 

![2102.13630](/kohgasa/2102.13630.jpg)

- **Area-law entangled eigenstates from nullspaces of local Hamiltonians** [arXiv:2102.13633 [quant-ph]](https://arxiv.org/abs/2102.13633)

大規模クラスのスピン鎖における指数関数的に縮退している零空間の構造を探索し, そのような零空間の存在を保証するある対称性を持つ2サイトのHamiltonianの広範なクラスに対する行列積状態(MPS)のゼロモードを解析的に構築. またより一般のHamiltonianにおけるゼロモードを調査するために, 数値計算のアルゴリズムを用いてエンタングルメントエントロピーに関して秩序付けられた零空間における基底を構築し, 一般のHamiltonianにおける面積則エンタングルメント拡大に従うために示される最も低い割合でエンタングルしたゼロモードの概念を定義. 

これらより, 指数関数的に縮退した零空間を持つ全ての局所Hamiltonianが, エンタングルメントエントロピーの面積則拡大と量子多体傷跡(QMBS)への本質的な道筋の確立, 弱いエルゴード性の破れを持つゼロモードを特徴づけると予想. 

![2102.13633](/kohgasa/2102.13633.jpg)

- **Angle Locking of a Levitating Diamond using Spin-Diamagnetism** [arXiv:2102.13637 [quant-ph]](https://arxiv.org/abs/2102.13637)

窒素-空孔(NV)中心の反交差している基底状態の準位(GSLAC)の後に, ドープされたダイアモンドが強い磁気異方性を持つ物質として振る舞い, 反磁性に相転移することを示し, そのスピン反磁性を用いて, 周囲条件で印加された磁場に沿ってトラップされたミクロダイアモンドの方向を限定. 

![2102.13637](/kohgasa/2102.13637.jpg)

- **Generating high-order quantum exceptional points** [arXiv:2102.13646 [quant-ph]](https://arxiv.org/abs/2102.13646)

系のLiouvillian超演算子の高次のモーメントを量子化することによって, 新たな非エルミートHamiltonian(NHHs)の記述を得る. このマッピングを応用して, 散逸系において高次の量子例外点(EPs)が作られることと, コヒーレンスとスペクトル函数によってそれが調査可能であることを実証している. 

![2102.13646](/kohgasa/2102.13646.jpg)

#### 読み物
- [私は一ヶ月後、この牛を殺します。〜私がヴィーガンにならないと決めるまで〜](https://locafra.com/2021/02/27/anatanoketsudan/)
- [旧帝大の助教は普段何をしているのか](https://anond.hatelabo.jp/20210228234557)

#### ヴィデオ
- *M-1グランプリ2004*

#### 世の中
- [子「YouTuberになる」父「YouTubeが突然サービスを終了した途端に路頭に迷うことになるんだぞ」子「こんなに流行ってるサービスをやめることなんてないよ」父親の次の一言を考えてくれませんか？](https://qr.ae/pNQS57)
- [何故日本では、アメリカと違って、用なしで非生産的な社員をクビにしないですか？](https://qr.ae/pNQQI9)
- [もし、人が嘘をつかなくなると、世界はどうなると思いますか？](https://qr.ae/pNdyUP)
- [人間の脳について、一番びっくりする意外な事実は何ですか？](https://qr.ae/pNdCQR)
- [「子供を許可なく勝手に産んで、勝手に不幸にすることが許される時点で、人間って例外なく悪人だ。」この主張に反論できますか？](https://qr.ae/pNdOzu)
- [「産んでくれなんて頼んでない」という子供の主張に反論できますか？](https://qr.ae/pNdmkN)
- [無償の愛は世の中に存在すると思いますか？](https://qr.ae/pNd0fE)

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr"><a href="https://twitter.com/hashtag/%E6%8B%A1%E6%95%A3%E5%B8%8C%E6%9C%9B?src=hash&amp;ref_src=twsrc%5Etfw">#拡散希望</a><br>トランプが世界を救うという陰謀論があるのは知っているけれど、その内容がわからんという人へ<br>Qアノンと化してしまった母親の話をまとめたものです<br>想像以上にやばくて震えています<br>参考にどうぞ <a href="https://t.co/8m2fju3ZBf">pic.twitter.com/8m2fju3ZBf</a></p>&mdash; 詩秘 恵瑠 (@sh1p1_ele) <a href="https://twitter.com/sh1p1_ele/status/1365919490907668486?ref_src=twsrc%5Etfw">February 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">日本郵政さんどうやって入れたの？<br>怖いんだけど。 <a href="https://t.co/paiSWTlJA7">pic.twitter.com/paiSWTlJA7</a></p>&mdash; だいき@ (@DAIKIShome10) <a href="https://twitter.com/DAIKIShome10/status/1365702757835923456?ref_src=twsrc%5Etfw">February 27, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">近所の公園に子供連れてきたらこんなの貼ってあって目を疑った。児童遊園で昼間に子供遊ばしたらあかんのか？ <a href="https://t.co/fyYplCMswG">pic.twitter.com/fyYplCMswG</a></p>&mdash; 安岡寺👀おめめ (@fcomeme) <a href="https://twitter.com/fcomeme/status/1365848785595760641?ref_src=twsrc%5Etfw">February 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">山田内閣広報官辞職。<br>この業界に身を置いてる人はわかるはず。土曜日までは飄々とその職を続けようとしていた人が、日曜日にいきなり態度を変えて月曜朝イチで辞職意向が伝わるケース。そう、それは週刊誌がこの問題で更に続報ネタをもっていて記事掲載の電話が日曜夜に彼女にいったということだ。</p>&mdash; 大濱﨑 卓真 (@oohamazaki) <a href="https://twitter.com/oohamazaki/status/1366175933401505792?ref_src=twsrc%5Etfw">February 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">自分に学歴コンプレックスがあるから、この子は東大に行かせたい。だから○○小を受験させるの！<br>って突然真顔で同じ園のママさんにカムアウトされたとき<br><br>何も考えず「ならママが東大院受ければ良いのでは？」と答えちゃったことがあって、私にコミュニケーションのセンスが全くないことが証明された</p>&mdash; mumum (@mumum43041047) <a href="https://twitter.com/mumum43041047/status/1365633171358801924?ref_src=twsrc%5Etfw">February 27, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">妹よ高校卒業おめでとう <a href="https://t.co/npve5AvsWK">pic.twitter.com/npve5AvsWK</a></p>&mdash; つくね (@merompans) <a href="https://twitter.com/merompans/status/1366235944395808768?ref_src=twsrc%5Etfw">March 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">匿名でチャットできるサイトで知らない人と話してたんだけど、変なプレイさせられた。 <a href="https://t.co/Rmm4u7eqa9">pic.twitter.com/Rmm4u7eqa9</a></p>&mdash; 盟神探湯 (@F9DMRK) <a href="https://twitter.com/F9DMRK/status/1367035706158108675?ref_src=twsrc%5Etfw">March 3, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
