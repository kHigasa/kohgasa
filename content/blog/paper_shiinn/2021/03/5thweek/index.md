---
title: 論文試飲メモ(2021年03月第5週)
date: "2021-03-31T04:00:00.000Z"
description: "2021年3月第5週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

6稿分. 基本的には主張と手法をざっくり. 

- **Limitations on Uncloneable Encryption and Simultaneous One-Way-to-Hiding** [arXiv:2103.14510 [quant-ph]](https://arxiv.org/abs/2103.14510)

  古典的なメッセージに対するクローン不可能な量子暗号化方式を, 情報理論的な設定に注目して研究, これらの方式の構造と安全性に対する幾つかの限界を与えている. 

  ![2103.14510](/kohgasa/2103.14510.jpg)

- **Testing identity of collections of quantum states: sample complexity analysis** [arXiv:2103.14511 [quant-ph]](https://arxiv.org/abs/2103.14511)

  濃度$N$の$d$-次元量子状態の集合に対して, 状態間の平均二乗Hilbert-Schmidt距離を推定することによって同一性のテストを得, サンプル計算量が$\mathcal{O}(\sqrt{N}d/\epsilon^2)$であり, 定数まで最適化されることが示されている. 

  ![2103.14511](/kohgasa/2103.14511.jpg)

- **Non-Markovianity boosts the efficiency of bio-molecular switches** [arXiv:2103.14534 [quant-ph]](https://arxiv.org/abs/2103.14534)

  非マルコフ性がナノスケールでの量子熱力学において有用な資源たり得るという主張. 光学異性化の産出率が非マルコフ性の効果を含めれば以前よりも増加するとのこと. 

  ![2103.14534](/kohgasa/2103.14534.jpg)

- **Modulation leakage vulnerability in continuous-variable quantum key distribution** [arXiv:2103.14567 [quant-ph]](https://arxiv.org/abs/2103.14567)

  単一端バンドエンコーディング形式を実装するために, 光学同位相で直角位相の変調器を用いている系において, 変調漏洩脆弱性を報告し, 理論的にこの脆弱性のインパクトを解析している. また, この脆弱性に対する対抗策として, 信頼されたノイズを付加することの有効性が調べられている. 

  ![2103.14567](/kohgasa/2103.14567.jpg)

- **Extracting Bayesian networks from multiple copies of a quantum system** [arXiv:2103.14570 [quant-ph]](https://arxiv.org/abs/2103.14570)

  事後選択に結び付けられた複合量子系の独立な複製の局所測定に基づいたBayesianネットワークの複数時間の経路確率を決定する一般的な枠組みをしている. また, このプロトコルが非射影測定に対応していることを示している. 
  
  ![2103.14570](/kohgasa/2103.14570.jpg)

- **A model for optimizing quantum key distribution with continuous-wave pumped entangled-photon sources** [arXiv:2103.14639 [quant-ph]](https://arxiv.org/abs/2103.14639)

  一時的に一様な対形成確率を持つ量子鍵配送(QKD)に対する根本的な機構を解析し, 安全な鍵の最大割合を達成するための最適なトレードオフを計算するための単純だが正確なモデルを開発. 与えられた損失と検知時間の分解能に対する光源の明るさの最適化戦略を求めている. 

  ![2103.14639](/kohgasa/2103.14639.jpg)
