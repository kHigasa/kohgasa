---
title: 論文試飲メモ(2021年03月第3週後半)
date: "2021-03-21T04:00:00.000Z"
description: "2021年3月第3週後半に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

8稿分. 基本的には主張と手法をざっくり. 

- **Entanglement renormalization for quantum fields with boundaries and defects** [arXiv:2103.07463 [quant-ph]](https://arxiv.org/abs/2103.07463)

連続マルチスケールエンタングルメント繰り込み群(cMERA)を等角の境界と欠損に制限して一般化. $1+1$次元のBoson cMERAを一般化の実行例として示している. 

![2103.07463](/kohgasa/2103.07463.jpg)

- **Molecular spin qudits for quantum simulation of light-matter interactions** [arXiv:2103.09706 [quant-ph]](https://arxiv.org/abs/2103.09706)

分子スピンキュディットが物質と強く相互作用しているフォトン場の量子ダイナミクスをシミュレートするための理想的なプラットフォームを提供することを示している. この分子量子シミュレータは, ただマクロ波パルスのみによって制御された単純なスピン1/2の2量体とスピンSを持つ遷移金属イオンによって実現される. 

![2103.09706](/kohgasa/2103.09706.jpg)

- **Faster Coherent Quantum Algorithms for Phase, Energy, and Amplitude Estimation** [arXiv:2103.09717 [quant-ph]](https://arxiv.org/abs/2103.09717)

位相とエネルギー, 確率振幅推定に対するブロックエンコーディング技術を用いた著しい量子アルゴリズムの提案. 

![2103.09717](/kohgasa/2103.09717.jpg)

- **Maximally efficient quantum thermal machines fuelled by nonequilibrium steady states** [arXiv:2103.09723 [quant-ph]](https://arxiv.org/abs/2103.09723)

非平衡定常状態によって駆動される2段階の量子熱機関の効率と出力を解析し, 最適化されたものを計算. 

![2103.09723](/kohgasa/2103.09723.jpg)

- **Low overhead universality and quantum supremacy using only Z-control** [arXiv:2103.09753 [quant-ph]](https://arxiv.org/abs/2103.09753)

一様で一定の外場がX方向にかかっているときに, Z方向に対角で制御可能なHamiltonianを適用することによって定義された変動Z(VZ)量子計算モデルを対象とし, $\mathcal{O}(1)$のオーバーヘッドのある普遍ゲートセットを作ることで, 1次元においてでさえ, それがユニバーサリティを持つことを示している. また, この普遍ゲートセットで量子回路を表し, $\mathcal{O}(n)$の深さにおける量子超越性を達成. 

![2103.09753](/kohgasa/2103.09753.jpg)

- **Quantum Illumination with a Parametrically Amplified Idler** [arXiv:2103.09757 [quant-ph]](https://arxiv.org/abs/2103.09757)

2つのモードを持つスクイーズド真空のアイドラーモードに位相感応増幅をかけることで, 位相感応でない交差相関が生まれることと, パラメトリックに増幅されたアイドラーを持つ量子照明は古典状態の機構よりも低いエラー率を持つことが示されている. 

![2103.09757](/kohgasa/2103.09757.jpg)

- **Spectral Properties of Confining Superexponential Potentials** [arXiv:2103.09765 [quant-ph]](https://arxiv.org/abs/2103.09765)

超指数ポテンシャル内に閉じ込められた粒子のスペクトルの性質を調査. 

![2103.09765](/kohgasa/2103.09765.jpg)

- **Reality as a Vector in Hilbert Space** [arXiv:2103.09780 [quant-ph]](https://arxiv.org/abs/2103.09780)

世界の根本的なオントロジーがSchr$\text{\"{o}}$dinger方程式により発展するHilbert空間におけるベクトルから成るという超極端な立場を養護する御意見. 

ぴよ. 

#### 読み物
- [[特別講義]論理(1)共通テスト「論理」の整理と演習](https://note.com/kumotaka/n/nf4038880a900)

#### ヴィデオ
- *The Da Vinci Code*

#### 世の中
- [今まで経験した最大のカルチャーショックはなんですか？](https://qr.ae/pN0BkV)
- [お寿司屋さんの力量を量るには何をファーストオーダーすればいいのでしょうか？](https://qr.ae/pNfpMI)
- [ほとんどの人が聞いたことがない歴史の中でも最も邪悪な人は誰ですか？](https://qr.ae/pNfpM4)
- [嘘が書いてある写真はありますか？](https://qr.ae/pNfF5T)
- [寿司屋の職人の技量がわかる寿司ネタと、新鮮さがわかる寿司ネタは違いますか？](https://qr.ae/pNfjEw)
- [何故ホームレスの人達は肉体労働や皿洗いの仕事を見つけて働かないのですか？](https://qr.ae/pGXIjr)
- [天皇ってただの世襲なのになぜ敬わなければいけないんですか？](https://qr.ae/pGXCrY)
<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">なんやこの会話 <a href="https://t.co/KnHxnhhjAf">pic.twitter.com/KnHxnhhjAf</a></p>&mdash; 仮想NISHI (@Nishi8maru) <a href="https://twitter.com/Nishi8maru/status/1371280920351674371?ref_src=twsrc%5Etfw">March 15, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">高校で無期停学になった時これ書いて出したらメチャクチャ怒られて退学になった。 <a href="https://t.co/w11jTilpka">pic.twitter.com/w11jTilpka</a></p>&mdash; 喪2 (@moni_fukusazu) <a href="https://twitter.com/moni_fukusazu/status/1371507936267239428?ref_src=twsrc%5Etfw">March 15, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">昨晩、元心理学専攻の韓国の方と話していて、アメリカ留学時に「あなたの人生を一番幸せにしてくれるものは」と授業で初めに当てられて「achievements」と即答したところ、「So Asian」的な反応があり、後続が「family」「love」とか答え初めて、競争社会の傷痕を感じたという話が面白かった。</p>&mdash; KAMEDA Akihiro (@cm3) <a href="https://twitter.com/cm3/status/1371491233097420801?ref_src=twsrc%5Etfw">March 15, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">1日14時間勉強して東大早慶私立医学部に合格したときは4年後まさか自分が勘当をくらい友達からも絶縁され大学を中退し精神疾患を抱えて引きこもった挙句青木ヶ原で首吊って失敗し結局底辺宿に住みながら格安ソープランドでボーイをして日払いで命を繋ぐ生活を送る事になるなんて思ってもみなかったのだ</p>&mdash; 東大中退したアライさん(とうイさん) (@akaikebasutei) <a href="https://twitter.com/akaikebasutei/status/1372776154696871939?ref_src=twsrc%5Etfw">March 19, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">あーーーーー今の地震で毎分0リットルになってしまった・・・うちの温泉管完全に死んだな。 <a href="https://t.co/yAdPhRJJnE">pic.twitter.com/yAdPhRJJnE</a></p>&mdash; 和田真尋(ほりえや旅館非公式) (@horieyakazunari) <a href="https://twitter.com/horieyakazunari/status/1373211260225875968?ref_src=twsrc%5Etfw">March 20, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<iframe width="560" height="315" src="https://www.youtube.com/embed/deNChl69--Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>