---
title: 論文試飲メモ(2021年03月第2週)
date: "2021-03-14T04:00:00.000Z"
description: "2021年3月第2週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Enumerating all bilocal Clifford distillation protocols through symmetry reduction** [arXiv:2103.03669 [quant-ph]](https://arxiv.org/abs/2103.03669)

  1. 商用のデスクトップコンピュータを使って, $n=5$までのBell対角状態に対する全てのプロトコルを列挙し, 最適化するための道具を導入. 
  1. 入力状態の対称性を利用することによって, $n=8$までのWerner状態の複製に対する全てのプロトコルを発見. 

2に対して, 高信頼性の担保される回路を提示. 

![2103.03669](/kohgasa/2103.03669.jpg)

- **Experimental hierarchy and optimal robustness of quantum correlations of two-qubit states with controllable white noise** [arXiv:2103.03691 [quant-ph]](https://arxiv.org/abs/2103.03691)

  実験的に準備された制御可能なホワイトノイズを持つ2キュビットWernerライクな状態上の量子相関の階層構造を示している. また, 実験的にWerner状態とそれらの一般化されたもの(GWSs)を生成, GWSsがWerner状態と比べて階層構造の根本的に新しい側面を明らかにしている, 
  1. 最大限でなく, 適度にエンタングルした状態がBell状態よりもホワイトノイズに対してより頑健になり得る
  1. 普通のWerner状態に対しては不可能な, Bell不等式を侵害しない2つの測定シナリオにおいて, 操作可能となるようなGWSsが存在する. 

![2103.03691](/kohgasa/2103.03691.jpg)

- **Quantum Zeno and Anti-Zeno probes of noise correlations in photon polarisation** [arXiv:2103.03698 [quant-ph]](https://arxiv.org/abs/2103.03698)

分極測定の手段によって光子を探索することで, ノイズスペクトルとそれに対応する時間的相関を調査し, 正(負)の時間相関によって特徴づけられるノイズがが量子Zeno効果(QZE)(反量子Zeno効果(AZE))を生じさせることを示している. 

![2103.03698](/kohgasa/2103.03698.jpg)

- **Destructive Controlled-Phase Gate Using Linear Optics** [arXiv:2103.03711 [quant-ph]](https://arxiv.org/abs/2103.03711)

1つの非線形符号ゲートのみを使用したControled-Phaseゲートの実装を提案. 

![2103.03711](/kohgasa/2103.03711.jpg)

- **QUANTUM ALGORITHM FOR THE NAVIER-STOKES EQUATIONS** [arXiv:2103.03804 [quant-ph]](https://arxiv.org/abs/2103.03804)

2次元(2D)Navier-Stokes方程式(NSE)を解くための量子アルゴリズムを提示, Qiskitで実装. 

量子回路は, Encoding, Collision, Propagation, Macros, Boundaryの5ステップから成り, 各実装が図示されている. 

![2103.03804](/kohgasa/2103.03804.jpg)

- **Certificates of quantum many-body properties assisted by machine learning** [arXiv:2103.03830 [quant-ph]](https://arxiv.org/abs/2103.03830)

最適化の問題に対して, 限られた計算資源を利用して最良な境界を見つけるために, 強化学習を緩和技術と組み合わせた手法を提案, 多体量子系の基底状態のエネルギーを求める問題にこれを応用し実行可能性を示している. 

![2103.03830](/kohgasa/2103.03830.jpg)

- **Quantum control of a nanoparticle optically levitated in cryogenic free space** [arXiv:2103.03853 [quant-ph]](https://arxiv.org/abs/2103.03853)

極低温においてフェムトグラムの誘電粒子を光学的に浮揚. 

![2103.03853](/kohgasa/2103.03853.jpg)

#### 読み物
- [目隠しチェスは健康に悪影響を与えるのか？](https://chess.zukeran.org/2017/12/20/blindfold-chess-consider-harmful/)

#### ヴィデオ
- *American Sniper*

#### 世の中
- [良い独裁者は存在しましたか？](https://qr.ae/pNLvZL)
- [大学の授業で受けたどんなことが社会人生活で役立っていますか？](https://qr.ae/pNL4Fi)
- [社会主義がそんなにひどいものなら、何故ロシアはそう多くの科学者を生み出せたのですか？](https://qr.ae/pNLVGa)
- [「こんなことが有り得るのか！」と思わせる写真はどちらですか？](https://qr.ae/pNLE2j)
- [今まで海外で差別を受けた時、どんな対応で相手を黙らせますか？](https://qr.ae/pN0Ifw)
- [デザインと機能が両立しているプロダクトを教えて頂けませんか？ジャンル不問です。](https://qr.ae/pN0N7V)
- [かつて男子は家庭科を受けなかったのに、プロの料理人はほとんど男性なのは何故ですか？](https://qr.ae/pN0Cai)
- [どんな人達を見ると同じ人間として恥ずかしいと思いますか？](https://qr.ae/pN0CaB)
- [炭火焼きは炭の種類によって味も変わってきますか？](https://qr.ae/pN0Wij)

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">入社3年目ぐらい時、同期の子が震えた声で「ちょっと…聞いてくれる？」と言って来たので身構えながら話を聞いたら「Tさん(50代のおじさん社員)が作った折れ線グラフが更新できなくてなんでかなあってクリックしてみたらね、全部…全部図形で作ってあったの。枠も線も点も、全部…」私も震えた</p>&mdash; すりごま🐾 (@surigoma2012) <a href="https://twitter.com/surigoma2012/status/1368888790136188928?ref_src=twsrc%5Etfw">March 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>