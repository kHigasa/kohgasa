---
title: 論文試飲メモ(2021年03月第4週後半)
date: "2021-03-28T04:00:00.000Z"
description: "2021年3月第4週後半に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

8稿分. 基本的には主張と手法をざっくり. 

- **Lorentz Quantum Computer** [arXiv:2103.10315 [quant-ph]](https://arxiv.org/abs/2103.10315)

  通常のキュビットに加えて双曲ビットと呼ばれる付加的なビットを持つLorentz量子力学に基づいた計算機の理論的なモデルが提案されている. このモデルに対する探索アルゴリズムを設計し, 通常の量子コンピュータに対するGroverの探索アルゴリズムよりも速く実行されている. 

![2103.10315](/kohgasa/2103.10315.jpg)

- **Circuit quantum electrodynamics (cQED) with modular quasi-lumped models** [arXiv:2103.10344 [quant-ph]](https://arxiv.org/abs/2103.10344)

  相互作用している量子情報処理システムのHamiltonianを抽出するために必要とされる, 物理回路の量子電気力学をそれらのより微妙な繰り込み効果まで含んでモデル化することのできる, 正確で広く応用可能なモジュール化された方法を実現する手法を提示. 量子デバイスをコンパクトな塊上のセルに分割し, 個々にシミュレートし, 併せた複合系を単純なサブシステムの集合と対間相互作用に簡約して写すというもの. 

![2103.10344](/kohgasa/2103.10344.jpg)

- **Comments on the Weyl-Wigner calculus for lattice models** [arXiv:2103.10351 [quant-ph]](https://arxiv.org/abs/2103.10351)

  最近文献において登場している所謂 "格子モデルに対する正確なWeyl-Wigner(W-W)計算" に対する批判的指摘, 
  - 離散格子モデルに対するコンパクトな連続運動量空間の使用は有限の場という側面を欠いているので非物理的である
  - 全単射のFourier変換を定式化できていない
  - 2つの離散格子サイトを表現するキュビットの量子物理を扱えていない. 

- **Visualizing Kraus operators for dephasing noise during application of the $\sqrt{\text{SWAP}}$ quantum gate** [arXiv:2103.10375 [quant-ph]](https://arxiv.org/abs/2103.10375)

  ディフェージングノイズが存在するときの$\sqrt{\text{SWAP}}$量子ゲートと最適化されたエンタングル作用を考え, 理想的な場合のノイズに対して最適化されたKraus演算子を導出, 各Kraus演算子の時間発展を3次元Euclid空間でグラフ化している. 

![2103.10375](/kohgasa/2103.10375.jpg)

- **Localizing genuine multimode entanglement: Asymmetric gains via non-Gaussianity** [arXiv:2103.10388 [quant-ph]](https://arxiv.org/abs/2103.10388)

  測定に基づいた量子相関と真の複数部分からなるエンタングルメントという2つの概念を統合し, 非Gauss測定がGauss測定に比べてより真の複数部分からなるエンタングルメントを集中させることができることを示している. また, 非Gauss測定による非Gauss状態に対して, 4つのスクイーズド真空状態のモードは1番目と3番目, 2番目と4番目のモードの交換に関して順列対称性を持つが, 光子を加えた場合には1つのモードを測定することによって対称性が破れ, 一方で光子を取り除いた場合には対称性が保たれることを示している. 

![2103.10388](/kohgasa/2103.10388.jpg)

- **Bounding the detection efficiency threshold in Bell tests using multiple copies of the two-qubit maximally entangled state** [arXiv:2103.10413 [quant-ph]](https://arxiv.org/abs/2103.10413)

  2つのキュビットの最大エンタングル状態の複数の複製と対応するキュビット部分空間で作用する局所Pauli測定を使って, Bell非局所性を確かめるために検出器の臨界効率を調査し, その上界をより制限している. 

![2103.10413](/kohgasa/2103.10413.jpg)

- **Steering between outputs of a beamsplitter using Gaussian states of light with excess noise** [arXiv:2103.10418 [quant-ph]](https://arxiv.org/abs/2103.10418)

  スクイーズドGauss光と真空がスクイージングの量と発生状態の純粋さの関数としてビームスプリッター内で混合しているとき, 出力モード間の量子ステアリングに対する条件を調査. Gauss測定に対して, 十分強いスクイージングでは, ステアリングが発生状態の純粋さに関わらず可能であることを発見. 

![2103.10418](/kohgasa/2103.10418.jpg)

- **Engineering spin-spin interactions with optical tweezers in trapped ions** [arXiv:2103.10425 [quant-ph]](https://arxiv.org/abs/2103.10425)

  捕獲されたイオン結晶の音波スペクトルを操作するために光ピンセットを用い, 現状のセットアップで利用可能な冪乗則の相互作用を超えて, イオンキュビットの相互作用と結合性が調節できることを示している. また実際に1と2次元結晶における標的のスピン間相互作用パターンを生成して実験的な実現可能性を示している. 

![2103.10425](/kohgasa/2103.10425.jpg)

#### 読み物
- [“quad”は「4」なのに何故“quadratic”は「2次」なのか](https://tasusu.hatenablog.com/entry/2014/10/22/214211)
- [任期切れた助教はどこにいくの](https://anond.hatelabo.jp/20180326180513)

#### ヴィデオ
- *Angels & Demons*

#### 世の中
- [日本にいる中国からの留学生の方に質問。日本で、「あ、これは中国と同じくらいおいしい」と思う中華料理の店はありましたか？やっぱり全然違う？](https://qr.ae/pGX9M0)
- [あなたが人生や世の中を見て「これはシステムだなぁ…これはまさしくシステムだ！」と感じることは何でしょうか？](https://qr.ae/pGX9uB)
- [添加物反対派の信じやすいフェイクニュースをご存知ですか？自己回答有ります。](https://qr.ae/pGX95K)
- [子どものころ、自分の家庭が「裕福」か「裕福じゃない」と感じたのはどんな瞬間ですか？](https://qr.ae/pG7NCZ)
- [日本はなぜアメリカに負けたのに英語にならなかったのですか？公用語が英語になった方が都合がいいと思いますが。](https://qr.ae/pG7C1a)
- [職場でハブられていて辛いです。 (ランチ、遊びに誘われない等) 「自分は仕事をしにきているんだ」と割り切って輪の外で励んでいますが、最近仕事中に涙を流してしまいました。もう限界なのでしょうか？](https://qr.ae/pG7Cpi)
- [好き嫌いが多く、ご飯を残す子供に「アフリカには食べられない子供もいるんだから食べなさい。」と言ったら、「アフリカの子供たちも嫌いな食べ物は食べないと思うよ。」と返事されました。この後、何て返しますか？](https://qr.ae/pG7zM2)
- [友人が『渡辺和子の「置かれた場所で咲きなさい」って言葉が大嫌い。』と言っていました。共感ができる人はいますか？](https://qr.ae/pGRR5f)
- [ゴキブリを部屋に侵入させない、科学的に有効な方法はありますか？](https://qr.ae/pGRRwt)

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">41歳にして初めて知ったんやけど、カステラの裏についてる紙みたいなやつ、ほんまに紙やったんやな。<br><br>いままで食べてた。</p>&mdash; 尿あこまん㊙️ (@akomankabu) <a href="https://twitter.com/akomankabu/status/1373438959829745668?ref_src=twsrc%5Etfw">March 21, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">全盲になったら、世界が何色になるかって、ご存知ですか？<br><br>私は正直、全盲になる前は真っ暗闇を想像していた。けれど、実際になってみたら真っ白な世界。これは人によって違うらしい。（ピンクや青色の人もいるとか。）<br><br>障害のある世界って、実は皆さんの想像とは違う世界なんです。。</p>&mdash; 浅井純子 (@nofkOzrKtKUViTE) <a href="https://twitter.com/nofkOzrKtKUViTE/status/1373489478262693889?ref_src=twsrc%5Etfw">March 21, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">職場の雰囲気が悪すぎて地震が起きても誰も反応しなかった</p>&mdash; ٰ (@gohan__0) <a href="https://twitter.com/gohan__0/status/1373202765342679041?ref_src=twsrc%5Etfw">March 20, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">長女小1の作文、長いけど読んでやってほしい。<br>親の僕は爆笑した。 <a href="https://t.co/oT34BckFFJ">pic.twitter.com/oT34BckFFJ</a></p>&mdash; ぐでちちwith7y♀2y♀ (@gude_chichi) <a href="https://twitter.com/gude_chichi/status/1374223858685808641?ref_src=twsrc%5Etfw">March 23, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">知り合いのオタクが彼女と別れました。当然、オタクをしすぎたことが理由です。<br>急遽ライブが入ってデートの予定をドタキャンしてしまうとかなら、全然わかるのですが、<br>彼の場合は「ライブの前日は感情がブレる」っていう理由で彼女からのLINEに一切返信をしなかったとのことなので本当に狂人だと思う</p>&mdash; くま林 (@kumakumarinko01) <a href="https://twitter.com/kumakumarinko01/status/1375588503543640070?ref_src=twsrc%5Etfw">March 26, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>