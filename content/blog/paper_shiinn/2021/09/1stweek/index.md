---
title: 論文試飲メモ(2021年09月第1週)
date: "2021-09-05T04:00:00.000Z"
description: "2021年9月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Quantum fluctuations of charge order induce phonon softening in a superconducting cuprate** [arXiv:2108.11425 [cond-mat.supr-con]](https://arxiv.org/abs/2108.11425)

  高分解共鳴非弾性X線散乱を用いてLa$_{2-x}$Sr$_x$CuO$_4$(LSCO)における短距離電荷秩序と量子揺らぎのスペクトル特性を示し, ダイアグラム形式を通して計算を実行することで, 電荷相関がフォノンの幾つかの枝をかなりソフト化することを発見している. 

  ![2108.11425](/kohgasa/2108.11425.jpg)

- **Theory for doping trends in titanium oxypnictide superconductors** [arXiv:2108.11588 [cond-mat.supr-con]](https://arxiv.org/abs/2108.11588)

  Ba$_{1-x}$Na$_x$Ti$_2$Sb$_2$Oについて, ドーピング道に沿った電子構造とFermi面を決定するために実験的な結晶構造のデータを用い, 電荷ドーピングがどのように超伝導転移温度を制御するのか調査している. 

  ![2108.11588](/kohgasa/2108.11588.jpg)

- **Fermi surface instabilities in electronic Raman scattering of the metallic kagomelattice CsV3Sb5** [arXiv:2108.11690 [cond-mat.str-el]](https://arxiv.org/abs/2108.11690)

  分極分解電子Raman分光法を用いて, 二次元金属カゴメ化合物$A$V$_3$Sb$_5$($A=$K,Rb,Cs)の準弾性ピーク(QEP)を観測し, その温度依存性が電荷密度相内のコヒーレントin-plane電荷相関の急峻な増加を明らかにしている. 

  ![2108.11690](/kohgasa/2108.11690.jpg)

- **Microscopic theory of the current-voltage characteristics of Josephson tunnel junctions** [arXiv:2108.11712 [cond-mat.supr-con]](https://arxiv.org/abs/2108.11712)

  DC電流バイアスのかかったJosephsonトンネル接合の電流-電圧(I-V)特性の解析的形式を導出している. 

  ![2108.11712](/kohgasa/2108.11712.jpg)

- **A silver(II) route to unconventional superconductivity** [arXiv:2108.12167 [cond-mat.supr-con]](https://arxiv.org/abs/2108.12167)

  第一原理電子構造計算によりAgF$_2$とクプラートの間の類似が明らかとなり, 計算されたスピン帯磁率により磁気的不安定性が実験的に観測された反強磁性転移と一致することが, 揺らぎ交換近似における線形化されたEliashberg理論により光学電子ドーピングしたバルクのAgF$_2$に対する非従来型一重項超伝導対が示されている. 

  ![2108.12167](/kohgasa/2108.12167.jpg)

- **Role of Topology and Symmetry for the Edge Currents of a 2D Superconductor** [arXiv:2108.12337 [cond-mat.supr-con]](https://arxiv.org/abs/2108.12337)

  二次元Rashba系を用いて, 対称性がAltland-ZirnbauerクラスDIIIとDの超伝導体のオーダーパラメータ混合とエッジ特性にもたらす影響を解析している. ストリップ幾何において現れるバルク超伝導特性とエッジ流を解析するために, Ginzbulg-Landauと微視的なモデルの両方を使い, トポロジカル状態に相転移するときでさえ, エッジ流がバルクトポロジーに独立で連続的に発達するトポロジカルエッジ状態に連関していることが見出されている. 

  ![2108.12337](/kohgasa/2108.12337.jpg)

- **Inherited topological superconductivity in two-dimensional Dirac semimetals** [arXiv:2108.12416 [cond-mat.supr-con]](https://arxiv.org/abs/2108.12416)

  谷内, 副格子内対形成と谷間, 副格子間対形成の場合に, Bogoliubov-de Gennes(BdG)準粒子スペクトルにおける点ノードが一次元回転数によって保護されていることを見出している. 

  ![2108.12416](/kohgasa/2108.12416.jpg)

#### 読み物
- [「GPSの相対論的補正の数値を押さえておきたい」を再掲](https://teenaka.at.webry.info/202108/article_39.html)

#### ヴィデオ
- *TENET*

#### 世の中
- [Google and Apple's next regulatory headaches are looming across the Pacific](https://edition.cnn.com/2021/08/30/tech/google-apple-south-korea-apps-intl-hnk/index.html)
- [Australian imports of ivermectin increase tenfold, prompting warning from TGA](https://www.theguardian.com/world/2021/aug/30/australian-imports-of-ivermectin-increase-10-fold-prompting-warning-from-tga)
- [Bali bombings case begins at Guantánamo, with Indonesian and two Malaysians on trial for murder](https://www.theguardian.com/us-news/2021/aug/31/bali-bombings-case-begins-at-guantanamo-with-indonesian-and-two-malaysians-on-trial-for)
- [Ofgem launches £450m fund to help homes and businesses go green](https://www.theguardian.com/business/2021/aug/31/ofgem-launches-450m-fund-to-help-homes-and-businesses-go-green)
- [Texas legislature gives final approval to sweeping voting restrictions bill](https://www.theguardian.com/us-news/2021/aug/31/texas-legislature-voting-restrictions-bill-final-approval)
- [Judge conditionally approves plan to dissolve OxyContin maker Purdue Pharma](https://edition.cnn.com/2021/09/01/business/purdue-sackler-opioid-bankruptcy-ruling/index.html)
- [The Gun Maker Being Sued for the Sandy Hook Shooting Wants School Records for Some of the Children Who Died](https://time.com/6094804/sandy-hook-lawsuit-school-records/)