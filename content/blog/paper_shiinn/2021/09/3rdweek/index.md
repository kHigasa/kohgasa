---
title: 論文試飲メモ(2021年09月第3週)
date: "2021-09-19T04:00:00.000Z"
description: "2021年9月第3週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Geometric superinductance qubits:Controlling phase delocalization across a single Josephson junction** [arXiv:2109.02864 [cond-mat.supr-con]](https://arxiv.org/abs/2109.02864)

  ![2109.02864](/kohgasa/2109.02864.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
