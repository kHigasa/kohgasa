---
title: 論文試飲メモ(2021年09月第2週)
date: "2021-09-12T04:00:00.000Z"
description: "2021年9月第2週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Spectroscopy of electron-phonon interaction of superconducting point contacts: experimental aspects** [arXiv:2109.00806 [cond-mat.supr-con]](https://arxiv.org/abs/2109.00806)

  常伝導状態における電流-電圧曲線の二階導関数が電子-フォノン相互作用関数に比例する弾道接触とフォノンの特異点も*weakly pronounced*もない不均一接触の両方について系統的に説明できる結果を得ている. 

  ![2109.00806](/kohgasa/2109.00806.jpg)

- **Supercurrent-induced long-range triplet correlations and controllable Josephson effect in superconductor/ferromagnet hybrids with extrinsic SOC** [arXiv:2109.00966 [cond-mat.supr-con]](https://arxiv.org/abs/2109.00966)

  長距離三重項相関(LRTC)が外的不純物スピン-軌道結合(SOC)のある超伝導体/強磁性体(S/F)における超流動によって生成され操作されると予測し, その構造をS/F二層とS/F/S Josephson接合に対して調べている. 

- **Superconducting THz sources with 12%power efficiency** [arXiv:2109.00986 [cond-mat.supr-con]](https://arxiv.org/abs/2109.00986)

  $~4\ \mathrm{THz}$の領域で$12$ %の高放射パワー効率を達成した高温超伝導体Bi$_2$Sr$_2$CaCu$_2$O$_{8+\delta}$のウィスカー結晶を用いたTHz装置の試作品を提示している. 

  ![2109.00986](/kohgasa/2109.00986.jpg)

- **Rectification in a Eu-chalcogenide-based superconducting diode** [arXiv:2109.01061 [cond-mat.supr-con]](https://arxiv.org/abs/2109.01061)

  超伝導Alと常伝導Cu間のEuS薄膜によって生成された強スピンフィルタリングとスプリッティングに基づいた超伝導スピントロニックトンネルダイオードを提案している. 

  ![2109.01061](/kohgasa/2109.01061.jpg)

- **Phenomenological theory of magnetic 90◦helical state** [arXiv:2109.01096 [cond-mat.str-el]](https://arxiv.org/abs/2109.01096)

  外部磁場中の隣接スピン間の90度回転角を持つ磁気的螺旋状態に対する相図を研究している. 

  ![2109.01096](/kohgasa/2109.01096.jpg)

- **Magnon heat transport in a 2D Mott insulator** [arXiv:2109.01119 [cond-mat.str-el]](https://arxiv.org/abs/2109.01119)

  絶縁しているクプラートからの熱伝導率における異常が反強磁性秩序と二次元Mott絶縁体におけるマグノンに帰せられるかどうかという問題に光を当てるため, Monte Carlo法と最大エントロピー解析接続を用いてhalf-filled二次元単一バンドHubbardモデルの熱伝導率$\kappa$と比熱$c_{\text{v}}$を調べている. 

  ![2109.01119](/kohgasa/2109.01119.jpg)

- **Superconductivity in the vicinity of an isospin-polarized state in a cubic Dirac band** [arXiv:2109.01133 [cond-mat.supr-con]](https://arxiv.org/abs/2109.01133)

  菱面体三層グラフェンにおけるアイソスピン分極状態近傍のソフト臨界揺らぎを起源とするCooper対形成の理論を提示している. 

  ![2109.01133](/kohgasa/2109.01133.jpg)

#### 読み物

#### ヴィデオ
- *The Shawshank Redemption*

#### 世の中
- [A Florida Gunman Killed 4, Including A Mother Who Was Still Cradling Her Baby](https://www.npr.org/2021/09/05/1034474532/a-florida-gunman-killed-4-including-a-mother-who-was-still-cradling-her-baby)
- [More global aid goes to fossil fuel projects than tackling dirty air – study](https://www.theguardian.com/environment/2021/sep/07/more-global-aid-goes-to-fossil-fuel-projects-than-tackling-dirty-air-study-pollution)
- [Thousands of kilometres from anywhere lies Point Nemo, a watery grave where space stations go to die](https://www.theguardian.com/science/2021/sep/04/thousands-of-kilometres-from-anywhere-lies-point-nemo-a-watery-grave-where-space-stations-go-to-die)
- [Biden administration tells ex-Trump officials to resign from military academy advisory boards or be dismissed](https://edition.cnn.com/2021/09/08/politics/trump-appointees-biden-boards/index.html)
- [Justice Department Sues Texas Over New Abortion Ban](https://www.npr.org/2021/09/09/1035467999/justice-department-sues-texas-over-new-abortion-ban)
- [Twin tropical storms threaten flooding, mudslides in Taiwan and Vietnam](https://edition.cnn.com/2021/09/10/asia/vietnam-taiwan-typhoon-chanthu-conson-intl-hnk/index.html)
- [Soaring inflation and energy costs are forcing China to sell some of its precious oil supply](https://edition.cnn.com/2021/09/10/economy/china-oil-prices-reserves-intl-hnk/index.html)