---
title: 論文試飲メモ(2021年06月第3週)
date: "2021-06-20T04:00:00.000Z"
description: "2021年6月第3週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Geometric superinductance qubits:Controlling phase delocalization across a single Josephson junction** [arXiv:2106.02864 [quant-ph]](https://arxiv.org/abs/2106.02864)

  単一の途切れていないアルミニウムワイヤによって形成された線形の超インダクタを用いて直接にフラクソニウムを実装し, 同じ回路から得られるあらゆる種類のキュビットを提示している. 

- **Out-of-plane Seebeck coefficient of the cuprate La1.6−xNd0.4SrxCuO4across thepseudogap critical point: particle-hole asymmetry and Fermi surface transformation** [arXiv:2106.05959 [cond-mat.str-el]](https://arxiv.org/abs/2106.05959)

  銅酸化物超伝導体La$_{1.6-x}$Nd$_{0.4}$Sr$_x$CuO$_4$の$ab$面と$c$軸におけるSeebeck効果を測定し, ドーピング$p^*=0.23$を境に散乱が大きく変化したとは考えられず, 面外Seebeck係数における変化はFermi面の変化に起因するものであると結論付けられている. 

  ![2106.05959](/kohgasa/2106.05959.jpg)

- **Resistance of 2D superconducting films** [arXiv:2106.05980 [cond-mat.supr-con]](https://arxiv.org/abs/2106.05980)

  ゼロK付近で$W$の幅のストリップを持つ超伝導薄膜の抵抗$R$を, 渦列のトンネリングが効いてくる領域について考え, $\mathrm{ln}R\sim-gW/\xi$で与えられることを見出している. 
 
  ![2106.05980](/kohgasa/2106.05980.jpg)

- **A practical guide for building superconducting quantum devices** [arXiv:2106.06173 [quant-ph]](https://arxiv.org/abs/2106.06173)

  題目通り. 

  ![2106.06173](/kohgasa/2106.06173.jpg)

- **Superconductivity and Quantum Oscillationsin Single Crystals of the Compensated Semimetal CaSb2** [arXiv:2106.06175 [cond-mat.supr-con]](https://arxiv.org/abs/2106.06175)

  CaSb$_2$の超伝導状態を調べ, Cooper対が適度に弱く結合していることがわかり, 低温での非飽和磁気抵抗と電子-正孔補償は密度汎関数理論 (DFT) 計算と一致してノード線の特徴を示している. また, 小さなFermi面と一致するde Haas-van Alphen (dHvA) 振動を観測している. 

  ![2106.06175](/kohgasa/2106.06175.jpg)

- **The role of quantum coherence in energy uctuations** [arXiv:2106.06461 [quant-ph]](https://arxiv.org/abs/2106.06461)

  開放量子系のエネルギー揺らぎにおける量子コヒーレンスの役割を調べるために, 終点測定法というプロトコルを導入し, 始状態の発展の後だけ実行されるエネルギー測定の関数としてエネルギー変化の統計を取れるようにしている. 

  ![2106.06461](/kohgasa/2106.06461.jpg)

- **Charge-Density-Wave-Induced Peak-Dip-Hump Structure and Flat Band in the KagomeSuperconductor CsV3Sb5** [arXiv:2106.06497 [cond-mat.supr-con]](https://arxiv.org/abs/2106.06497)

  高分解角度光電子分光器を用いてCDW状態にあるCsB$_3$Sb$_5$の電子的性質を調べ, $\overline{K}$付近のスペクトルが2つの分離した分散の枝に関連したピーク-ディップ-ハンプ構造を示し, それが等方的なCDWギャップ開放を示していることを見出している. 

  ![2106.06497](/kohgasa/2106.06497.jpg)

#### 読み物


#### ヴィデオ


#### 世の中
- [Israel's new prime minister is sworn in, ending Netanyahu's 12-year grip on power](https://edition.cnn.com/2021/06/13/middleeast/israel-knesset-vote-prime-minister-intl/index.html)
- [1 dead, 2 injured after dispute over mask at Georgia grocery store, sheriff says](https://edition.cnn.com/2021/06/14/us/georgia-deputy-shooting-decatur/index.html)
- [Senate unanimously passes a bill making Juneteenth a federal holiday](https://edition.cnn.com/2021/06/15/politics/juneteenth-federal-holiday-senate-vote/index.html)
- [Hong Kong police arrest editor-in-chief of Apple Daily newspaper in morning raids](https://www.theguardian.com/world/2021/jun/17/hong-kong-police-arrest-editor-in-chief-of-apple-daily-newspaper-in-morning-raids)
- [First astronauts blast off for China’s new space station](https://www.theguardian.com/world/2021/jun/17/first-astronauts-blast-off-for-china-new-space-station-tiangong)
- [Utah school omits teen with Down’s syndrome from cheer team photo](https://www.theguardian.com/us-news/2021/jun/18/utah-teen-student-cheerleading-shoreline-junior-high)
- [Arizona police shoot suspect who struck multiple cyclists with vehicle](https://edition.cnn.com/2021/06/19/us/arizona-suspect-shot-struck-cyclists/index.html)