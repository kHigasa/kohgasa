---
title: 論文試飲メモ(2021年06月第4週)
date: "2021-06-27T04:00:00.000Z"
description: "2021年6月第4週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Emission of particles from a parametrically driven condensate in a 1D lattice** [arXiv:2106.09604 [quant-gas]](https://arxiv.org/abs/2106.09604)

  1次元の深い井戸にトラップされたBose-Einstein凝縮体からの粒子放出を計算し, 対放出だけでなく1粒子放出があることを見出し, その線形応答による放出率を定式化している. 

  ![2106.09604](/kohgasa/2106.09604.jpg)

- **Molecular beam epitaxy of single-crystalline bixbyite (In1-xGax)2O3films(x≤0.18): Structural properties and consequences of compositionalinhomogeneity** [arXiv:2106.09612 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2106.09612)

  > he heteroepitaxial growth of single-crystalline bixbyite (In1-xGax)2O3films on (111)-orientedyttria-stabilized zirconia substrates using plasma-assisted molecular beam epitaxy under various growth conditions

  での物性調べ. 

  ![2106.09612](/kohgasa/2106.09612.jpg)

- **Band topology of pseudo-Hermitian phases through tensor Berry connections andquantum metric** [arXiv:2106.09648 [cond-mat.mes-hall]](https://arxiv.org/abs/2106.09648)

  2, 3次元における擬エルミート相が$q$変形行列を作ることにより形成されることを示し, それを2次元Chern絶縁体等のよく知られたエルミートトポロジカル相の擬エルミート版について調べている. 

  ![2106.09648](/kohgasa/2106.09648.jpg)

- **Prethermal Dynamical Localization and the Emergence of Chaos in a Kicked Interacting Quantum Gas** [arXiv:2106.09698 [cond-mat.quant-gas]](https://arxiv.org/abs/2106.09698)

  パルスされた光格子におけるBose-Einstein凝縮帯を使った相互作用を変調するキックされた量子ロータを実験的に実現し, エコータイプの時間反転実験により可逆性を壊すのに相互作用が役割を果たしていることを明らかにしている. 

  ![2106.09698](/kohgasa/2106.09698.jpg)

- **Intertwined states at finite temperatures in the Hubbard model** [arXiv:2106.09704 [cond-mat.str-el]](https://arxiv.org/abs/2106.09704)

  バイアスのない数値的に厳密なブルートフォースの有限温度量子Monte Carlo法を用いたHubbardモデルの解析の結果を示している. 

  ![2106.09704](/kohgasa/2106.09704.jpg)

- **Quantum Gases in Optical Boxes** [arXiv:2106.09716 [cond-mat.quant-gas]](https://arxiv.org/abs/2106.09716)

  光学箱中の量子ガスについてのレヴュー. 

  ![2106.09716](/kohgasa/2106.09716.jpg)

- **Higher-order topological insulators from3Qcharge bond orders on hexagonal lattices:A hint to kagome metals** [arXiv:2106.09717 [cond-mat.str-el]](https://arxiv.org/abs/2106.09717)

  カゴメ金属へのヒントとなるような, 六方格子での$3Q$秩序において現れる境界現象を議論. 

  ![2106.09717](/kohgasa/2106.09717.jpg)

#### 読み物
- K.Onnes: Leiden Comun. 120b, 122b, 124c (1991). 

#### ヴィデオ


#### 世の中
- [Witness describes aftermath of horrific crash that left 9 children and 1 adult dead in Alabama](https://edition.cnn.com/2021/06/20/us/alabama-crash-10-killed-girls-ranch/index.html)
- [Palm Jumeirah, Dubai's iconic man-made islands, turns 20](https://edition.cnn.com/travel/article/dubai-palm-jumeirah-20-years-spc-intl/index.html)
- [Moscow sees hottest June day for 120 years with more to come](https://www.theguardian.com/world/2021/jun/23/moscow-sees-hottest-june-day-for-120-years-with-more-to-come)
- [Scientist says early coronavirus samples were deleted from NIH database](https://edition.cnn.com/2021/06/23/health/coronavirus-sequences-database-scientist/index.html)
- [Rudy Giuliani suspended from practicing law in New York state](https://edition.cnn.com/2021/06/24/politics/rudy-giuliani-suspended-law/index.html)
- [It came out of the sky: US releases highly anticipated UFO report](https://www.theguardian.com/us-news/2021/jun/25/ufos-us-government-report)
- [A customer left a $16,000 tip after ordering some hot dogs, chips and a few drinks](https://edition.cnn.com/2021/06/26/us/customer-16k-tip-new-hampshire-trnd/index.html)