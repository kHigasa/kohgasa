---
title: 論文試飲メモ(2021年06月第2週)
date: "2021-06-13T04:00:00.000Z"
description: "2021年6月第2週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **The BCS Energy Gap at High Density** [arXiv:2106.02028 [math-ph]](https://arxiv.org/abs/2106.02028)

  高密度極限でのBCSエネルギーギャップの漸近形を導出し, エネルギーギャップと臨界温度の比のユニバーサリティを示している. 

  ![2106.02028](/kohgasa/2106.02028.jpg)

- **Evidence for interplay between pseudogap and orthorhombicity in underdopedYBa2Cu3Oyfrom ultrasound measurements** [arXiv:2106.02055 [cond-mat.supr-con]](https://arxiv.org/abs/2106.02055)

  超音波測定を用いて, ドープされていないYBCO単結晶を調べ, 再隣接Cupper間の結合を変調させることにより調節可能な局所的なエネルギースケールによって擬ギャップが制御されることが示唆されている. 

  ![2106.02055](/kohgasa/2106.02055.jpg)

- **Correlated insulators, semimetals, and superconductivity in twisted trilayer graphene** [arXiv:2106.02063 [cond-mat.str-el]](https://arxiv.org/abs/2106.02063)

  鏡面対称なねじれた3層グラフェンに対して, 解析的, 数値的手法を組み合わせてマジック角に近い部分における相互作用の影響を調べ, 平坦バンドのすべての整数フィリング摩擦$\nu$での漸近的に正確な相関多体基底状態を同定している. 

  ![2106.02063](/kohgasa/2106.02063.jpg)

- **Strong-coupling corrections to hard domain walls in superfluid3He-B** [arXiv:2106.02065 [cond-mat.supr-con]](https://arxiv.org/abs/2106.02065)

  空間依存のGinzburg-Landau方程式の自己無撞着解を求めることにより, ドメインウォールの幅と界面張力に対する強結合補正の効果を調べ, ドメインウォールの形成はい結合よりも強い結合の方がエネルギー的に有利であることを発見している. 

  ![2106.02065](/kohgasa/2106.02065.jpg)

- **Topological superconducting domain walls in magnetic Weyl semimetals** [arXiv:2106.02215 [cond-mat.supr-con]](https://arxiv.org/abs/2106.02215)

  s波超伝導対形成ポテンシャルがあるときのトポロジカル超伝導状態について調べ, 化学ポテンシャルを調節することによってカイラルMajoranaモード又はゼロエネルギーMajorana束縛状態がドメインウォールの端に局在するようなトポロジカル状態を得ることができることを示している. 

  ![2106.02215](/kohgasa/2106.02215.jpg)

- **The absence of superconductivity in the next-to-leading order Ginzburg-Landaufunctional for Bardeen-Cooper-Schrieffer superconductor** [arXiv:2106.02631 [cond-mat.supr-con]](https://arxiv.org/abs/2106.02631)

  Gor'kovよりもう1次だけ低次を考え, 一般化されたGinzburg-Landauの自由エネルギーを得, 微視的なモデルをさらによく近似することを目指したが, 得られた拡張されたGinzburg-Landau汎関数は超伝導状態をサポートしていないことを証明している. 

- **Superconducting on-chip spectrometer for mesoscopic quantum systems** [arXiv:2106.02632 [cond-mat.mes-hall]](https://arxiv.org/abs/2106.02632)

  バイアス電圧を印加された超伝導量子干渉装置に基づいたミリ波帯でよく機能するオンチップの吸収分光器の提案. 

  ![2106.02632](/kohgasa/2106.02632.jpg)

#### 読み物
- [Born−Oppenheimer近似と断熱近似](https://home.hiroshima-u.ac.jp/kyam/pages/results/monograph/Ref23_B-O.pdf)

#### ヴィデオ


#### 世の中
- [US oil hits $70 for the first time in nearly three years](https://edition.cnn.com/2021/06/06/investing/us-oil-prices/index.html)
- [24,000-year-old organisms found frozen in Siberia can still reproduce](https://www.theguardian.com/science/2021/jun/07/24000-year-old-organisms-found-frozen-in-siberia-can-still-reproduce)
- [Senate passes massive bipartisan bill to combat China's growing economic influence](https://edition.cnn.com/2021/06/08/politics/bipartisan-bill-vote-china-competitiveness/index.html)
- [Developer Abandons Keystone XL Pipeline Project, Ending Decade-Long Battle](https://www.npr.org/2021/06/09/1004908006/developer-abandons-keystone-xl-pipeline-project-ending-decade-long-battle)
- [At least 8 people hospitalized after a car plowed into a crowd at a Texas racetrack](https://edition.cnn.com/2021/06/14/us/texas-racetrack-car-crash-into-crowd/index.html)
- [Joe Biden to use Nato summit to atone for damage of Trump years](https://www.theguardian.com/world/2021/jun/14/joe-biden-to-use-nato-summit-to-atone-for-damage-of-trump-years)
- [How Taiwan’s struggle for Covid vaccines is inflaming tensions with China](https://www.theguardian.com/world/2021/jun/14/how-taiwan-struggle-for-covid-vaccines-is-inflaming-tensions-with-china)