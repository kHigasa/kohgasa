---
title: 論文試飲メモ(2021年06月第1週)
date: "2021-06-06T04:00:00.000Z"
description: "2021年6月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Strong and almost strong modes of Floquet spin chains in Krylov subspaces** [arXiv:2105.13246 [cond-mat.str-el]](https://arxiv.org/abs/2105.13246)

  2つのKrylov部分空間において得られる有効単一粒子モードが議論され, Krylov鎖のトポロジカル的性質が境界での$0$と$\pi$モードの安定性を保証することが強調されている. 

  ![2105.13246](/kohgasa/2105.13246.jpg)

- **Iron phthalocyanine on Au(111) is a “non-Landau” Fermi liquid** [arXiv:2105.13248 [cond-mat.str-el]](https://arxiv.org/abs/2105.13248)

  タイトル通りの主張. 

  ![2105.13248](/kohgasa/2105.13248.jpg)

- **Local density of states and scattering rates across the many-body localizationtransition** [arXiv:2105.13249 [cond-mat.dis-nn]](https://arxiv.org/abs/2105.13249)

  多体局在化 (MBL) 系に対する1粒子Green関数, 局所状態密度, 自己エネルギーを調べ, 散乱率の典型的な値がMBL遷移に対する非局在化を追跡するのに使われることを示している. 

  ![2105.13249](/kohgasa/2105.13249.jpg)

- **Entanglement and ground-state statistics of free bosons** [arXiv:2105.13269 [cond-mat.quant-gas]](https://arxiv.org/abs/2105.13269)

  基底状態にある格子上の自由ボソンに対するエンタングルメントエントロピーの対数的振る舞いを粒子数ゆらぎの体積則とどのように調和させるかを明らかにしている. 

  ![2105.13269](/kohgasa/2105.13269.jpg)

- **Polarization-dependent excitons and plasmon activity in nodal-line semimetal ZrSiS** [arXiv:2105.13285 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.13285)

  バルクのZrSiSノード線半金属の光学特性を理論的に解析. 

  ![2105.13285](/kohgasa/2105.13285.jpg)

- **Anomalous phase separation and hidden coarsening of super-clustersin the Falicov-Kimball model** [arXiv:2105.13304 [cond-mat.str-el]](https://arxiv.org/abs/2105.13304)

  Falicov-Kimballモデルにおいて電荷チェッカーボードクラスターの成長が隠れた対称性の破れに関連して領域粗大化と競合するという得意な相分離を生じさせることを明らかにしている. 

  ![2105.13304](/kohgasa/2105.13304.jpg)

- **Entanglement Domain Walls in Monitored Quantum Circuitsand the Directed Polymer in a Random Environment** [arXiv:2105.13352 [cond-mat.stat-mech]](https://arxiv.org/abs/2105.13352)

  ランダムユニタリーなダイナミクスを持ち, 測定値が散らばっている1次元のハイブリッド回路に対して, 効果的な統計力学モデルへの解析的マッピングとハイブリッドクリフォード回路上での大規模な数値シミュレーションを組み合わせ, 体積法則相の普遍的なエンタングルメント特性が, "ランダム環境下の有向性ポリマー" (DPRE) に相当する流動的なエンタングルメント領域壁によって定量的に記述できることを示している. 

  ![2105.13352](/kohgasa/2105.13352.jpg)

#### 読み物
- [バルク測定で見る強相関電子系における対称性の破れ :異方的超伝導から電子ネマティック状態まで(超伝導,重い電子系若手秋の学校,講義ノート)](https://repository.kulib.kyoto-u.ac.jp/dspace/bitstream/2433/169658/1/KJ00007730704.pdf)

#### ヴィデオ


#### 世の中
- [Catholics question why Boris Johnson was able to marry in church](https://www.theguardian.com/politics/2021/may/30/boris-johnson-carrie-symmonds-married-catcholic-church)
- [A 7-year-old boy in Florida swam for an hour to get help for his dad and sister who were stranded in a river](https://edition.cnn.com/2021/05/31/us/florida-seven-year-old-saves-family-stranded-in-river-trnd/index.html)
- [Zero daily Covid deaths announced in UK](https://www.theguardian.com/world/2021/jun/01/zero-daily-covid-deaths-announced-in-uk)
- [Trump shuts down his blog after less than a month](https://edition.cnn.com/2021/06/02/media/trump-blog-shut-down/index.html)
- [Five additional coffins found at 1921 Tulsa massacre search site](https://edition.cnn.com/2021/06/04/us/tulsa-race-massacre-additional-coffins-found/index.html)
- [Atlanta police say they found more than 170 pounds of marijuana in suitcases from an arriving flight](https://edition.cnn.com/2021/06/04/us/atlanta-police-marijuana-flight-seattle/index.html)
- [Hundreds of former leaders urge G7 to vaccinate poor against Covid-19](https://edition.cnn.com/2021/06/07/world/world-leaders-g7-covid-vaccine-intl-hnk/index.html)