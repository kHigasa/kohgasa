---
title: 論文試飲メモ(2021年04月第5週)
date: "2021-05-02T04:00:00.000Z"
description: "2021年4月第5週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **An End-to-End Computer Vision Methodology for Quantitative Metallography** [arXiv:2104.11159 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2104.11159)

  合金における不純物の異常度合いを自動的に定量化する異常検知に対するAIモデルの提示. 

  ![2104.11159](/kohgasa/2104.11159.jpg)

- **Phonon dressing of a facilitated one-dimensional Rydberg lattice gas** [arXiv:2104.11160 [cond-mat.quant-gas]](https://arxiv.org/abs/2104.11160)

  促進条件下における1次元Rydberg格子気体のダイナミクスの解析. フォノンによるドレッシングがあるときRydberg励起群の時間発展に関するHamiltonianのスペクトルを解析し, Rydberg励起と格子振動間の相互作用がある始状態の高速緩和を妨げるゆっくり減衰する束縛状態の出現をもたらすことを示している. 

  ![2104.11160](/kohgasa/2104.11160.jpg)

- **Mobile Majorana zero-modes in two-channel Kondo insulators** [arXiv:2104.11173 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2104.11173)

  Kondo絶縁体の1/4充填の位置にある2チャネルKondo不純物の秩序格子を考え, この状態におけるトポロジカル欠陥がMajoranaゼロモード, またはより複雑なパラフェルミオンを有することを示している. 

  ![2104.11173](/kohgasa/2104.11173.jpg)

- **Intrinsic Spin Susceptibility and Pseudogap-like Behavior in Infinite-Layer LaNiO2** [arXiv:2104.11187 [cond-mat.supr-con]](https://arxiv.org/abs/2104.11187)

  $^{\text{139}}$Laに対して核磁気共鳴(NMR)実験を行い, 磁気秩序がないことと, 時間依存Knightシフトとスピン-格子緩和率において擬ギャップ的振る舞いが観測されている. 

  ![2104.11187](/kohgasa/2104.11187.jpg)

- **Regular and in-plane skyrmions and antiskyrmions from boundary instabilities** [arXiv:2104.11194 [cond-mat.mes-hall]](https://arxiv.org/abs/2104.11194)

  磁場と電流パルスを用いたスキルミオンと反スキルミオンの生成の理論を定式化し, Dzyaloshinskii-Moriya相互作用 (DMI) を持つ系の異種DMI境界と同じくエッジにもトポロジカル欠陥が生成されうることを示している. スキルミオンと反スキルミオンのHall効果によって, 電流が境界からトポロジカル欠陥を追い出し, エッジにトポロジカル欠陥を生成している. 

  ![2104.11194](/kohgasa/2104.11194.jpg)

- **Dynamical Backaction Magnomechanics** [arXiv:2104.11218 [cond-mat.mes-hall]](https://arxiv.org/abs/2104.11218)

  球状磁性試料の機械的振動に対するマグノンによる動的バックアクションの影響を調べている. 

  ![2104.11218](/kohgasa/2104.11218.jpg)

- **Probability and Irreversibility in Modern Statistical Mechanics: Classical and Quantum** [arXiv:2104.11223 [cond-mat.stat-mech]](https://arxiv.org/abs/2104.11223)

  統計力学の確率概念が量子力学の確率に還元されることを実証. 

  へ? 

  ![2104.11223](/kohgasa/2104.11223.jpg)

#### 読み物


#### ヴィデオ


#### CVE
- [CVE-2021-30496](https://nvd.nist.gov/vuln/detail/CVE-2021-30496)

  - NIST: NVD
  - Base Score:  5.7 MEDIUM
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:N/A:H

- [CVE-2021-29474](https://nvd.nist.gov/vuln/detail/CVE-2021-29474)

  - CNA:  GitHub, Inc.
  - Base Score:  4.7 MEDIUM
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:N

- [CVE-2021-29476](https://nvd.nist.gov/vuln/detail/CVE-2021-29476)

  - CNA:  GitHub, Inc.
  - Base Score:  9.8 CRITICAL
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H

- [CVE-2021-29483](https://nvd.nist.gov/vuln/detail/CVE-2021-29483)

  - CNA:  GitHub, Inc.
  - Base Score:  9.4 CRITICAL
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:H

- [CVE-2021-29484](https://nvd.nist.gov/vuln/detail/CVE-2021-29484)

  - CNA:  GitHub, Inc.
  - Base Score:  6.8 MEDIUM
  - Vector:  CVSS:3.1/AV:N/AC:H/PR:N/UI:R/S:U/C:H/I:H/A:N

- [CVE-2021-21547](https://nvd.nist.gov/vuln/detail/CVE-2021-21547)

  - CNA:  Dell
  - Base Score:  6.4 MEDIUM
  - Vector:  CVSS:3.1/AV:L/AC:H/PR:H/UI:N/S:U/C:H/I:H/A:H

- [CVE-2021-20086](https://nvd.nist.gov/vuln/detail/CVE-2021-20086)

  - NIST: NVD
  - Base Score:  9.8 CRITICAL
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H

#### 世の中
- [Fully vaccinated Americans will be able to vacation in the EU this summer, European Commission president tells New York Times](https://edition.cnn.com/2021/04/25/us/europe-vaccinated-americans-summer-eu-vacation/index.html)
- [UK book sales soared in 2020 despite pandemic](https://www.theguardian.com/books/2021/apr/27/uk-book-sales-soared-in-2020-despite-pandemic)
- [CDC issues new outdoor mask guidance for fully vaccinated people](https://edition.cnn.com/2021/04/27/health/cdc-mask-guidance-fully-vaccinated-bn/index.html)
- [Hong Kong has fined a journalist for ticking a box. That shows the city's media freedoms are in jeopardy](https://edition.cnn.com/2021/04/28/media/hong-kong-media-bao-choy-intl-hnk/index.html)
- [Biden stops in Georgia to sell sweeping economic proposals after prime-time speech](https://edition.cnn.com/2021/04/29/politics/biden-georgia-economic-proposals/index.html)
- [WHO Film Festival: Starring Matchsticks As Burnt Out Health Workers](https://www.npr.org/sections/goatsandsoda/2021/05/01/987669552/who-film-festival-starring-matchsticks-as-burnt-out-health-workers)
- [Utah GOP vote to censure Mitt Romney fails while senator is booed at convention](https://edition.cnn.com/2021/05/01/politics/mitt-romney-utah-gop-trump/index.html)

- [目に止まりましたか！何でもいいので豆知識を教えてくださいませんか。](https://qr.ae/pGNBC2)
- [とても示唆的な写真はありますか？](https://qr.ae/pGNBFu)
- [トップレベルの学生の良い学習習慣は何ですか？](https://qr.ae/pGGNPU)
- [理解するには2度見なければならない、素晴らしい絵は何ですか？](https://qr.ae/pGGHYN)
- [教養を感じさせる馬鹿な話はありませんか？](https://qr.ae/pGG6GK)
- [科学者たちが、まだ説明できない出来事は何ですか？](https://qr.ae/pGhG3O)
- [ピタゴラスによる不思議な発明品があれば教えていただけませんか？](https://qr.ae/pGhYRs)