---
title: 論文試飲メモ(2021年04月第3週)
date: "2021-04-18T04:00:00.000Z"
description: "2021年4月第3週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Deterministic spatial search using alternating quantum walks** [arXiv:2104.03806 [quant-ph]](https://arxiv.org/abs/2104.03806)

  Grover拡張演算子が相互依存なネットワークのクラスに対する一連の連続時間量子ウォークに取り替えられる空間探索アルゴリズムの性能を検証している. 

  ![2104.03806](/kohgasa/2104.03806.jpg)

- **Fidelity based purity and coherence for quantum states** [arXiv:2104.03844 [quant-ph]](https://arxiv.org/abs/2104.03844)

  忠実度に基づいた純粋さとコヒーレンスモノトーンの正確な計量を提案し, それらの間の関係を確立, また純粋さに対する弱測定の役割を研究. 

  ![2104.03844](/kohgasa/2104.03844.jpg)

- **Robust Interior Point Method for Quantum Key Distribution Rate Computation** [arXiv:2104.03847 [quant-ph]](https://arxiv.org/abs/2104.03847)

  量子鍵配送(QKD)の鍵レート計算問題に対する凸型非線形半正定値計画(SDP)モデルの安定な最定式化を行い, 効率的で正確なアルゴリズムの作成に応用. 

  ![2104.03847](/kohgasa/2104.03847.jpg)

- **A Model of Spinfoam Coupled with an Environment** [arXiv:2104.03849 [quant-ph]](https://arxiv.org/abs/2104.03849)

  スピンフォームに対する開放量子系の理論を定式化しており, 主に量子重力場の還元ダイナミクスを計算するために実効的なLindblad等式を導出することを狙いとしている. 

  ![2104.03849](/kohgasa/2104.03849.jpg)

- **Measurement of the Casimir Force between 0.2 and8μm: Experimental Procedures and Comparison with Theory** [arXiv:2104.03857 [quant-ph]](https://arxiv.org/abs/2104.03857)

  微小ねじれ振動子を用いて, Auコーティングされたサファイア球とAuコーティングされた深いシリコン溝の上部・下部の間に働く差分Casimir力の測定結果を示している. 

  ![2104.03857](/kohgasa/2104.03857.jpg)

- **Investigation of the dead time duration and active reset influence on the afterpulse probability of InGaAs/InP SPAD based SPDs** [arXiv:2104.03919 [quant-ph]](https://arxiv.org/abs/2104.03919)

  InGaAs/InP SPADのアフターパルス確率に対するデッドタイムの持続時間とアクティヴリセットの影響の調査(殆どタイトルママ). 

  ![2104.03919](/kohgasa/2104.03919.jpg)

- **Energy-independent optical $S_0NN$ potential from Marchenko equation** [arXiv:2104.03939 [quant-ph]](https://arxiv.org/abs/2104.03939)

  Marchenko理論に基づいた量子散乱理論の逆問題を解くための新たな代数的方法を, 分離可能な形式におけるMarchenko方程式のカーネル展開に対する三角波の集合を適用することによって提示. また, この方法に基づいて散乱データから光学ポテンシャルの再構築を行う数値的アルゴリズムが提示されている. 

  ![2104.03939](/kohgasa/2104.03939.jpg)

#### 読み物
- [真空技術超入門](https://www.jstage.jst.go.jp/article/jvsj2/58/8/58_15-LC-021/_pdf)

#### ヴィデオ


#### CVE
- [CVE-2020-36318](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36318)

  Rust <1.49.0の標準ライブラリVecDequeのmake_contiguous関数が, ある状況で同じ要素を1回以上ポップしてしまうバグにより, use-after-freeやダブルフリーの可能性. 

- [CVE-2021-30044](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30044)

  [Remote Clinic](https://github.com/remoteclinic/RemoteClinic) 2.0でスタッフ登録ファイルの姓名フィールドにおけるXSS脆弱性. 

- [CVE-2021-29370](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29370)

  Thanos-Soft社のAndroid 1.2.0における[Cheetah Browser](https://play.google.com/store/apps/details?id=thanos.soft.com.browser.cheetah&hl=en_SG)にUXSS脆弱性. 

- [CVE-2021-30487](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30487)

  [Zulip Server](https://github.com/zulip/zulip) 3.x <3.4のトピック移動APIにおいて, 組織の管理者がメッセージを同じZulipでホストされている他の組織のストリームに移動させることが可能になっていた. 

- [CVE-2021-29450](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29450)

  [WordPress](https://github.com/WordPress/wordpress-develop) 4.7-5.7においてcontributorの権限があれば, エディターの1つのブロックを用いて, パスワードで保護された投稿やページを晒すことが可能となっていた. 

- [CVE-2020-36195](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-36195)
- [CVE-2021-3493](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-3493)

#### 世の中
- [Officer Near Minneapolis Kills Motorist, and a Crowd Confronts the Police](https://www.nytimes.com/2021/04/11/us/brooklyn-center-minnesota-police-shooting.html)
- [One person dead, officer injured after shooting at a high school in Knoxville, Tennessee](https://edition.cnn.com/2021/04/12/us/knoxville-school-shooting/index.html)
- [Berlin police investigate private club over alleged party amid lockdown](https://edition.cnn.com/2021/04/13/europe/coronavirus-party-soho-house-berlin-intl-scli/index.html)
- [Liz Cheney says she won't support Trump if he runs in 2024](https://edition.cnn.com/2021/04/14/politics/liz-cheney-donald-trump-2024/index.html)
- [Researchers now have an estimate for just how many T. rex once roamed Earth](https://edition.cnn.com/2021/04/15/world/t-rex-billions-intl-scli-scn/index.html)
- [US and China commit to cooperating on climate crisis](https://www.theguardian.com/environment/2021/apr/18/us-and-china-commit-to-cooperating-on-climate-crisis)
- [Family of three contract Covid from infected neighbours in hotel quarantine in Sydney](https://www.theguardian.com/australia-news/2021/apr/18/family-of-three-contract-covid-from-infected-neighbours-in-hotel-quarantine-in-sydney)

- [ランダムな面白い事実を知っていますか？](https://qr.ae/pGTWAN)
- [自分が無知だと気づいたのはいつですか？](https://qr.ae/pGTqoo)
- [人々が知っておくべきものはなんですか？](https://qr.ae/pGp1yC)
- [「なんで怒られているかわかるか？」これになんて返しますか？](https://qr.ae/pGp2dh)
- [人類に大きく貢献しているのにもかかわらず無名のままとなっている人にはどのような人がいますか？](https://qr.ae/pGp2LE)
- [事実は解明されているのに、ほとんどの人が知らないものは何ですか？](https://qr.ae/pGpgFh)
- [一見して百を知ることができるような写真はありますか？](https://qr.ae/pGI7EE)