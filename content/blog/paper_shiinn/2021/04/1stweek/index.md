---
title: 論文試飲メモ(2021年04月第1週)
date: "2021-04-04T04:00:00.000Z"
description: "2021年4月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

8稿分. 基本的には主張と手法をざっくり. 

- **How well can we guess the outcome of measurements of non-commuting observables?** [arXiv:2103.16338 [quant-ph]](https://arxiv.org/abs/2103.16338)

  過去の測定値を推測する際, Heisenbergの不確定性原理の与える限界は破られることを主張. 

  ![2103.16338](/kohgasa/2103.16338.jpg)

- **Geometry of Banach spaces: a new route towards Position Based Cryptography** [arXiv:2103.16357 [quant-ph]](https://arxiv.org/abs/2103.16357)

  位置に基づいた量子暗号化(PBQC)を幾何関数解析と量子ゲームとの関連から研究. PBQCプロトコルのセキュリティを傷つけるために, 攻撃者が共有しなければならないエンタングルメントの最適量を見積もることを目的に, 位置検証(PV)プロトコルを提案し, 攻撃可能となるエンタングルメントの領域をBanach空間の幾何的解析から限定している. 

  ![2103.16357](/kohgasa/2103.16357.jpg)

- **Theory and experiment for resource-efficient joint weak-measurement** [arXiv:2103.16389 [quant-ph]](https://arxiv.org/abs/2103.16389)

  測定における利用可能な内部自由度(DOF)の制限を強調するために, 読み出しシステムとしてただ1つのDOFを使って2つの非可換なオブザーバブルの合同弱測定を実行するための技術を提示し, 光子分極の密度行列の直接測定に応用している. 

  ![2103.16389](/kohgasa/2103.16389.jpg)

- **A Note About Claw Function With a Small Range** [arXiv:2103.16390 [quant-ph]](https://arxiv.org/abs/2103.16390)

  爪探知問題の量子問い合わせ計算量が$2\leq k< n$のとき$\Omega(n^{1/2}k^{1/6})$と$\mathcal{O}(n^{1/2+\epsilon}k^{1/4})$の間であることを示している. 

  ![2103.16390](/kohgasa/2103.16390.jpg)

- **Experimental statistical signature of many-body quantum interference** [arXiv:2103.16418 [quant-ph]](https://arxiv.org/abs/2103.16418)

  真の多体量子干渉を複数モード量子ディヴァイスの出力の統計的シグネチャを利用するプロトコルを通して実験的に同定. さらに, 機械学習の記述を用いて, どのように実験から得られた解析データが, アプリオリに知ることのできないそれらのシグネチャを見つけるための最適な特徴量を発見するのに役立つのかを示している. 

  ![2103.16418](/kohgasa/2103.16418.jpg)

- **Dissipative Topological Phase Transition with Strong System-Environment Coupling** [arXiv:2103.16445 [quant-ph]](https://arxiv.org/abs/2103.16445)

  電磁気環境に結合したトポロジカルエミッタ配列の研究. 保護されたトポロジカル相において, 弱い光子-エミッタ結合に対しては, エッジ状態は環境によって誘引される散逸の影響を被るが, 一方強い結合の場合は散逸の影響を受けない. 

  ![2103.16445](/kohgasa/2103.16445.jpg)

- **C-band single photons from a trapped ion via two-stage frequency conversion** [arXiv:2103.16450 [quant-ph]](https://arxiv.org/abs/2103.16450)

  捕獲されたイオンから放出される可視光領域の光子の周波数を変換して通信用C帯の波長を持つ光子を得ている. 

  ![2103.16450](/kohgasa/2103.16450.jpg)

- **Importance Sampling Scheme for the Stochastic Simulation of Quantum Spin Dynamics** [arXiv:2103.16468 [quant-ph]](https://arxiv.org/abs/2103.16468)

  量子スピン系を古典的な確率過程へマッピングする手法に基づいて, 量子スピンダイナミクスにシミュレーションに対する重点サンプリング手法を開発. これにより, 確率量における揺らぎの時間的成長を減らすことができ, 直接サンプリングに比べて実行可能な時間とシステムサイズが増加することを実証している. 

  ![2103.16468](/kohgasa/2103.16468.jpg)

#### 読み物
- [Bogoliubov-de Gennes方程式の導出](http://park.itc.u-tokyo.ac.jp/kato-yusuke-lab/nagai/note_100206_BdG.pdf)

#### ヴィデオ
*Inferno*

#### CVE
- [CVE-2021-29272](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29272)

  [bluemonday](https://github.com/microcosm-cc/bluemonday) <=1.0.4の正規表現の処理におけるXSS脆弱性. 
- [CVE-2021-29424](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29424)

  Perlによる幾つかのIPアドレスモジュールにおけるパブリックIPアドレスとプライベートサブネットの外部識別可能性. 
- [CVE-2020-24995](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-24995)

  [FFmpeg](https://ffmpeg.org/) 3.1.2のヒープ&スタックバッファオーバーフロー. 
- [CVE-2021-29349](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29349)

  [Mahara](https://mahara.org/) 20.10におけるCSRF脆弱性. 受信箱のメールが第三者によって全て削除可能. 
- [CVE-2021-21421](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-21421)

  Node.jsの[Etsy](https://www.etsy.com/)用REST APIクライアント[node-etsy-client](https://github.com/creharmony/node-etsy-client) <=0.2.0がエラーを返すときにAPIキーの値も一緒に返しちゃってた✨
- [CVE-2021-30074](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30074)

  [docsify](https://github.com/docsifyjs/docsify) 4.12.1のXSS脆弱性. 典型. 
- [CVE-2021-30127](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30127)

  [TerraMaster F2-210](https://www.terra-master.com/jp/f2-210.html)は管理サーバにTCPポート8181を通してアクセス可能にするためにUPnPを用いているが, これはローカルネットワークからのみアクセス可能であるという製品の文言と矛盾している. 

#### 世の中
- [US fossil-fuel companies took billions in tax breaks – and then laid off thousands](https://www.theguardian.com/us-news/2021/apr/02/fossil-fuel-companies-billions-tax-breaks-workers)
- [Capitol Police officer killed, another injured after suspect rams car into police barrier outside building](https://edition.cnn.com/2021/04/02/politics/us-capitol-incident/index.html)
- [High-Profile Figures In Jordan Arrested For 'Security Reasons'](https://www.npr.org/2021/04/03/984166356/high-profile-figures-arrested-for-security-reasons-in-jordan)

- [日本人は人前で話す時「えー」や「あのー」を連発しますが、英語ネイティブは急な質問にもテンポよく止まらずに答えます。頭の回転の違いですか？](https://qr.ae/pGRK4J)
- [日本人は同調圧力に負けるな。自己主張をしろ。と言う人がいますが日本人が同調を重んじるのは文化ではないでしょうか？そして米国の自己主張しろという姿勢もある意味同調圧力では？](https://qr.ae/pGRKMY)
- [日本の帝国主義による被害を受けた周辺国に対して「日本はすでに謝罪と賠償を済ませている」という人がいますが、その人自身は全く謝罪するつもりはないですよね？その辺りがドイツとの違いだと思います。](https://qr.ae/pGRKbJ)
- [お金持ちになるというのはどんな感じがするものですか？](https://qr.ae/pGRuLE)
- [どうして虐められたら、仕返しをしてはいけないのでしょうか？](https://qr.ae/pGRVkw)
- [何故 日本はニューヨークタイムス紙等にもはや独裁国家なのではと書かれる様な国になってしまったのでしょうか？](https://qr.ae/pG8TsC)
- [匿名でも結構です。日本に住んでいる外国人の方に質問します。日本という国に一度行ってやりたいことはありますか？](https://qr.ae/pG8vQz)
- [ネイティブスピーカーがカチンとくる、日本人の英語は何ですか？](https://qr.ae/pG8MDZ)
- [容姿差別を法律で禁止すべきという意見についてついてどう思われますか？](https://qr.ae/pG8ZuX)
- [レストラン、特にラーメン屋の広告では、どうして皆さん腕を組んでいるのでしょうか？](https://qr.ae/pG8Zum)
- [容赦なさすぎるだろって感じたのはいつですか？](https://qr.ae/pG8ZuQ)

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">鬱病やった時に無理矢理アウトレットモール連れて行かれた私見て <a href="https://t.co/KXu8a5fZEH">pic.twitter.com/KXu8a5fZEH</a></p>&mdash; かす崎 (@erikai4545) <a href="https://twitter.com/erikai4545/status/1376156844037599233?ref_src=twsrc%5Etfw">March 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">【速報】新採用さん、出勤30分でトイレに行くと言いそのまま帰る</p>&mdash; ふとん (@MUiMi_zzz) <a href="https://twitter.com/MUiMi_zzz/status/1377428486302756865?ref_src=twsrc%5Etfw">April 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">新しく審議委員になった野口氏、英語の経歴には中央大学PhDが書かれている一方、日本語の経歴では中央大学も博士号も登場せず、あくまでも東大博士課程単位取得退学になっていますね。海外ではPhDが偉くて日本では博士課程中退でも最終学歴東大の方がウケるという文化の違いをよく分かっていますね <a href="https://t.co/LyucKFQT3u">pic.twitter.com/LyucKFQT3u</a></p>&mdash; Shen (@shenmacro) <a href="https://twitter.com/shenmacro/status/1377983569394470913?ref_src=twsrc%5Etfw">April 2, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>