---
title: 論文試飲メモ(2021年04月第2週前半)
date: "2021-04-07T04:00:00.000Z"
description: "2021年4月第2週前半に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

12稿分. 基本的には主張と手法をざっくり. 

- **Bench-top Cooling of a Microwave Mode using an Optically Pumped Spin Refrigerator** [arXiv:2104.00268 [quant-ph]](https://arxiv.org/abs/2104.00268)

  $p$-ターフェニルの結晶内にドープされた光励起しているペンタセン分子のスピン分極した三重項状態間の相互作用により, 室温で$1.45\ \mathrm{GHz}$のマイクロ波モードから熱的光子を一時的に除去. 

  ![2104.00268](/kohgasa/2104.00268.jpg)

- **Correlational Resource Theory of Catalytic Quantum Randomness under Conservation Law** [arXiv:2104.00300 [quant-ph]](https://arxiv.org/abs/2104.00300)

  量子ランダムネスの触媒に関する理論における触媒の資源理論を打ち立て, 定義した触媒エントロピー増大を基に, ランダムネスが抽出されればランダムネスの源がその分たしかに消費されていることを示している. また, タイトルにあるように保存則の下にあるシステムにこれを応用している. 

  応用のとこよくわからない. 

  ![2104.00300](/kohgasa/2104.00300.jpg)

- **No-slip boundary conditions for electron hydrodynamics and the thermal Casimir pressure** [arXiv:2104.00334 [quant-ph]](https://arxiv.org/abs/2104.00334)

  THz領域と遠赤外線領域における電磁波に対する修正反射係数を導出. 

  ![2104.00334](/kohgasa/2104.00334.jpg)

- **Glassy quantum dynamics of disordered Ising spins** [arXiv:2104.00349 [quant-ph]](https://arxiv.org/abs/2104.00349)

  冪乗の相互作用と位置の無秩序を持つ量子Isingモデルにおいて, 大域的な磁化が伸縮力に対して指数的に減衰することを数値的に発見. また, 系のサイズが有限で十分強い無秩序を持つときガラスのような振る舞いを維持することを数値的に明らかにしている. 

  ![2104.00349](/kohgasa/2104.00349.jpg)

- **What Is the Generalized Representation of Dirac Equation in Two Dimensions?** [arXiv:2104.00388 [quant-ph]](https://arxiv.org/abs/2104.00388)

  $2+1$次元に対するDirac方程式($2\times 2$ Dirac行列)の一般形を求めている. 

  ![2104.00388](/kohgasa/2104.00388.jpg)

- **High-Speed Tunable Microcavities Coupled to Rare-Earth Quantum Emitters** [arXiv:2104.00389 [quant-ph]](https://arxiv.org/abs/2104.00389)

  電子光学的に調整可能なニオブ酸リチウム(LN)内に単一のレアアースイオン(REI)量子エミッターを取り入れて, $5\ \mathrm{\mu m}$のスイッチングスピードで$160\ \mathrm{GHz}$の周波数範囲にかけてREIに結合したLNマイクロキャヴィティの制御を行っている. この動的制御により, 短い時定数を持つREIsのPucell強度の変調が可能となり, これを用いて, LNキャヴィティ内の単一$\mathrm{Yb}^{3+}$イオンを検知できたことを示している. 

  ![2104.00389](/kohgasa/2104.00389.jpg)

- **Model Selection for Time Series Forecasting: Empirical Analysis of Different Estimators** [arXiv:2104.00584 [stat.ML]](https://arxiv.org/abs/2104.00584)

  こういうの面白くない. 

  ![2104.00584](/kohgasa/2104.00584.jpg)

- **NeRF-VAE: A Geometry Aware 3D Scene Generative Model** [arXiv:2104.00587 [stat.ML]](https://arxiv.org/abs/2104.00587)

  ニューラル輝度場(NeRF)を通して幾何的構造を取り入れた変分オートエンコーダ(VAE)(つまり改良された3Dシーン生成モデル), NeRF-VAEの提案. 幾何的構造を取り入れたことにより, 少ないインプット画像と1回の訓練で3Dシーンを推測しレンダリングできるようになった. 

  ![2104.00587](/kohgasa/2104.00587.jpg)

- **Overscreening and Underscreening in Solid-Electrolyte Grain Boundary Space-Charge Layers** [arXiv:2104.00623 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2104.00623)

  3次元Coulomb格子気体の運動Monte Carloシミュレーションを用いて, 非希薄固体における粒子境界空間電荷領域(の層平均欠陥数密度)がoverscreening(減衰振動的な空間電荷プロファイル)とunderscreening(対応するDebye長より長く, 欠陥間相互作用の強度の増加に伴って増加する減衰長)という, 本来濃縮電解溶液においてみられる現象が示されている. 

  ![2104.00623](/kohgasa/2104.00623.jpg)

- **Regularized target encoding outperforms traditional methods in supervised machine learning with high cardinality features** [arXiv:2104.00629 [stat.ML]](https://arxiv.org/abs/2104.00629)

  タイトルまま. 

  ![2104.00629](/kohgasa/2104.00629.jpg)

- **Thermodynamics of droplets undergoing liquid-liquid phase separation** [arXiv:2104.00651 [cond-mat.soft]](https://arxiv.org/abs/2104.00651)

  二元混合物で一方の体積が平坦な界面を形成するのに最低限必要な量より少ないようなものの熱力学を考え, 平均場理論により表面張力が支配するポリマー混合物の平衡相が1つの巨視的水滴を形成することを示している. 弾性相互作用が表面張力を繰り込み, ゲル-ポリマー混合物の相分離を遅らせ, 水滴相を安定化させる, とのこと. 

  ![2104.00651](/kohgasa/2104.00651.jpg)

- **Half and quarter metals in rhombohedral trilayer graphene** [arXiv:2104.00653 [cond-mat.mes-hall]](https://arxiv.org/abs/2104.00653)

  菱面体三層グラフェンが電子系の自発磁化によって特徴づけられた強磁性的状態を持つことが示されている. 

  本論文で肝要な半金属と1/4金属に関する相転移の部分はわからなかった...

  ![2104.00653](/kohgasa/2104.00653.jpg)
