---
title: 論文試飲メモ(2021年04月第4週)
date: "2021-04-25T04:00:00.000Z"
description: "2021年4月第4週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Decoherence of Coupled Flip-Flop Qubits Due to Charge Noise** [arXiv:2104.07485 [quant-ph]](https://arxiv.org/abs/2104.07485)

  単一フリップフロップキュビットと2つの双極結合キュビットに対して電荷ノイズのデコヒーレンス効果を調べている. 単一フリップフロップキュビットはスイートスポットで電荷ノイズに対して極めて強いが, 電荷の励起状態とキュビットの準位が近接しているためにキュビット操作のフィデリティを大きく損なっていることが報告されている. この元凶をキュビットのHilbert空間からの漏れに同定し, このデコヒーレンスを緩和するための他のバイアスの調査を行っている. 

  ![2104.07485](/kohgasa/2104.07485.jpg)

- **Realignment separability criterion assisted with filtrationfor detecting continuous-variable entanglement** [arXiv:2104.07510 [quant-ph]](https://arxiv.org/abs/2104.07510)

  連続変数のエンタングルメントを検知するのに適し, 物理的に実装可能な再配置分離可能性基準の弱形式を導入. 次にGaussian状態に注目してノイズのない増幅/減衰に基づいた濾過過程を導入し, エンタングルメント検知感度を向上させている. 

  ![2104.07510](/kohgasa/2104.07510.jpg)

- **Further Compactifying Linear Optical Unitaries** [arXiv:2104.07561 [quant-ph]](https://arxiv.org/abs/2104.07561)

  対称的に配置したMach Zehnder干渉計と回路の深度を増やさない程度に少数の外的な位相シフターを用いてClementsの方法を実現. 

  ![2104.07561](/kohgasa/2104.07561.jpg)

- **Quantum gravimetry in the same internal state using composite light Raman pulses** [arXiv:2104.07601 [quant-ph]](https://arxiv.org/abs/2104.07601)

  Raman型の複合光パルスを用いた原子重量測定シークエンスを導入. 変調過程により持ち込まれる予期せぬ周波数を特徴づける3つの摂動パラメータに関して干渉縞の解析的表現を得, 干渉縞の明度を向上させる特別なRabi振動数を得ている. 

  ![2104.07601](/kohgasa/2104.07601.jpg)

- **Scaling of temporal entanglement in proximity to integrability** [arXiv:2104.07607 [quant-ph]](https://arxiv.org/abs/2104.07607)

  積分可能なFloquetモデルの属に対するFeynmann-Vernon影響行列 (IM) を解析的に計算し, 代数的に減衰する相関を持つSchwinger-Keldysh積分路上のBardeen-Cooper-Schriefferライクな波動関数を見出している. またそのIMは全てのパラメータの値に対して, 面積則の即時エンタングルメントスケーリングを示すことを実証し, IMのエンタングルメントパターンは系の相図を表していることがわかっている. 

  ![2104.07607](/kohgasa/2104.07607.jpg)

- **Attainable and usable coherence in X states over Markovian and non-Markovian channels** [arXiv:2104.07648 [quant-ph]](https://arxiv.org/abs/2104.07648)

  2キュビット$X$状態に対するMarkovianと非Markovianチャンネルに関する量子コヒーレンスの資源理論的測定間の関係を厳密に調査. 

  ![2104.07648](/kohgasa/2104.07648.jpg)

- **Entanglement detection of two-qubit Werner states** [arXiv:2104.07653 [quant-ph]](https://arxiv.org/abs/2104.07653)

  測定枠組みとして対称的情報完全POVMを用いた2キュビットWerner状態によって表現された光子対のエンタングルメント検知に対するフレームワークを導入. 

  ![2104.07653](/kohgasa/2104.07653.jpg)

#### 読み物
- [量子測定の原理とその問題点](https://as2.c.u-tokyo.ac.jp/archive/MathSci469(2002).pdf)

#### ヴィデオ


#### CVE
- [CVE-2021-23381](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-23381)

  - CNA:  Snyk
  - Base Score:  7.3 HIGH
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L

- [CVE-2021-28451](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-28451)

  Microsoft Excel Remote Code Execution Vulnerability
  - NIST: NVD
  - Base Score:  7.8 HIGH
  - Vector:  CVSS:3.1/AV:L/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:H

- [CVE-2021-29462](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29462)

  DNS rebinding attacks
  - CNA:  GitHub, Inc.
  - Base Score:  7.6 HIGH
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:H/A:L

- [CVE-2021-29466](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-29466)

  - CNA:  GitHub, Inc.
  - Base Score:  6.5 MEDIUM
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N

- [CVE-2017-8414](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-8414)

  - NIST: NVD
  - Base Score:  7.8 HIGH
  - Vector:  CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H

- [CVE-2021-2202](https://nvd.nist.gov/vuln/detail/CVE-2021-2202)

  - CNA:  Oracle
  - Base Score:  6.5 MEDIUM
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H

- [CVE-2021-21643](https://nvd.nist.gov/vuln/detail/CVE-2021-21643)

  - NIST: NVD
  - Base Score:  6.5 MEDIUM
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N

#### 世の中
- [Three people are dead after apparent 'domestic situation' in Austin, Texas](https://edition.cnn.com/2021/04/18/us/austin-shooting-three-dead/index.html)
- [Apple and Parler agreement could restore rightwing platform to App Store](https://www.theguardian.com/technology/2021/apr/19/parler-apple-app-store-capitol-attack)
- [Netflix's subscriber growth slows as vaccinations ramp up and rivals gain strength](https://edition.cnn.com/2021/04/20/media/netflix-earnings/index.html)
- [Faulty Takata Air Bag Blamed For January Death In South Carolina](https://www.npr.org/2021/04/21/989695390/faulty-takata-air-bag-blamed-for-january-death-in-south-carolina)
- [Senate overwhelmingly passes anti-Asian hate crimes bill](https://edition.cnn.com/2021/04/22/politics/senate-vote-hate-crimes-bill/index.html)
- [Group of Texas students disciplined after they pretended to auction Black classmates in social media group chat](https://edition.cnn.com/2021/04/23/us/aledo-texas-students-racist-snapchat-group-chat-trnd/index.html)
- [Biden becomes first US president to recognise Armenian genocide](https://www.theguardian.com/us-news/2021/apr/24/joe-biden-armenian-genocide-recognition)

- [これまでの歴史の中で、バタフライ・エフェクトやカオス理論を最も端的に示していると思われる出来事は何ですか？](https://qr.ae/pGIrQO)
- [自分の人生を無駄にしているように思えて仕方がありません。20代前半の学生ですが、友達もほとんどいません。この状況を変えるにはどうすればいいのでしょうか？](https://qr.ae/pGI5wo)
- [最近読んだもので、シェアする価値があると思ったものは何ですか？](https://qr.ae/pGIVVJ)
- [日本人はなぜ広い家に住みたがらないですか？](https://qr.ae/pGILev)
- [誰でも簡単に怒らせる方法を教えて頂けますか？](https://qr.ae/pGNn6G)
- [人生について考えさせるような写真はありますか？](https://qr.ae/pGNk3O)
- [地球上でみられる、珍しい自然現象にはどのようなものがありますか？](https://qr.ae/pGNxWL)

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">これは永久保存版ですわ <a href="https://t.co/Go5pkENqNr">pic.twitter.com/Go5pkENqNr</a></p>&mdash; ボヘミアン (@hide_luxe) <a href="https://twitter.com/hide_luxe/status/1385797797778128899?ref_src=twsrc%5Etfw">April 24, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 