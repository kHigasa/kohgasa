---
title: 論文試飲メモ(2021年04月第2週後半)
date: "2021-04-11T04:00:00.000Z"
description: "2021年4月第2週後半に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

4稿分. 基本的には主張と手法をざっくり. 

- **Device-independent quantum key distribution based on Bell inequalities with more than two inputs and two outputs** [arXiv:2104.00413 [quant-ph]](https://arxiv.org/abs/2104.00413)

  2つ以上のインプットとアウトプットを持つBell不等式に立脚した2つのDI-QKDプロトコルを提示している. 最大エンタングル状態かつ完全干渉明度のときに, 3つのインプットと4つのインプットを持つBell不等式に基づいたプロトコルが各2つのインプットとアウトプットを持つそれよりも僅かに低い検知効率を要求することを示している. 

  ![2104.00413](/kohgasa/2104.00413.jpg)

- **Quantification of Wigner Negativity Remotely Generated via Einstein-Podolsky-Rosen Steering** [arXiv:2104.00451 [quant-ph]](https://arxiv.org/abs/2104.00451)

  複数部分から成る系において生成されたWigner負性の共有可能性を調査し, Wigner負性という量は自由に異なったモード感で分配させられないということを示している. さらに光子の放出に対して, 遠隔で生成されたWigner負性を定量化するための一般的な方法を提供し, この方法を用いてGaussianステアビリティと生成されたWigner負性の量との間に一切の量的関係がないことを発見している. 

  ![2104.00451](/kohgasa/2104.00451.jpg)

- **Experimental estimation of the quantum Fisher information from randomized measurements** [arXiv:2104.00519 [quant-ph]](https://arxiv.org/abs/2104.00519)

  量子Fisher情報(QFI)が乱雑化された測定を通してどのように推定されるのかということを調査, 実験的に
    1. ダイアモンドにおける窒素空孔中心スピン
    1. 超伝導量子コンピュータによって提供される4キュビット状態
  という2つのプラットフォームを用いて乱雑化された測定を検証. さらに複数部分から成るエンタングルメントを推定する際に, 量子状態トモグラフィーと比べて乱雑化された測定が有利な点を調べるために, 多体スピン系の数値的研究を行っている. 

  ![2104.00519](/kohgasa/2104.00519.jpg)

- **Learning to measure: adaptive informationally complete POVMs for near-term quantum algorithms** [arXiv:2104.00569 [quant-ph]](https://arxiv.org/abs/2104.00569)

  たくさんの測定を必要とするという変分量子アルゴリズムの問題に取り組むために, 変分測定法を導入. 関連したコスト函数の推定における統計揺らぎを最小化するためにオンザフライ方式で情報的に完全なPOVM(positive operator valued measurement)sを最適化するアルゴリズムを提示. また, 数値シミュレーションにおいて分子Hamiltonianの基底状態のエネルギーを計算するためにこれを変分量子固有値ソルバー(VQE)と組み合わせて使用している. 

  ![2104.00569](/kohgasa/2104.00569.jpg)

#### 読み物
- [量子測定理論入門](https://www.math.kyoto-u.ac.jp/~kfujiwara/sendai/ozawa.handout.pdf)

#### ヴィデオ
- SUITS

#### CVE
- [CVE-2021-21529](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-21529)

  Dell System Update(DSU) <=1.9のDoS攻撃に対する脆弱性. 

- [CVE-2021-20308](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-20308)

  [HTMLDOC](https://github.com/michaelrsweet/htmldoc) <=1.9.11の整数オーバーフロー脆弱性. 

- [CVE-2021-30178](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30178)

  [Linux kernel](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/) 5.11.11のファイル`arch/x86/kvm/hyperv.c`内関数`synic_get`にNULLポインタ参照があった. 

- [CVE-2021-30457](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30457)

  Rustの[id-map](https://github.com/andrewhickman/id-map)クレートにおけるダブルフリーのバグ. 

- [CVE-2021-3448](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-3448)

  [Dnsmasq](https://thekelleys.org.uk/gitweb/?p=dnsmasq.git;a=summary) <=2.85が特定のサーバーに対して固定ポートを用いていたために, DNSキャッシュポイズニング攻撃に利用される可能性が指摘されている. 

- [CVE-2021-30480](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-30480)

  09/04/21 WindowsとmacOS版[Zoom Chat](https://zoom.us/feature/messaging)(Zoomのchat機能ではない!)において, 攻撃者がユーザーの操作なく任意のコードを実行できる脆弱性を発見. 

- [CVE-2021-20020](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-20020)

  [SonicWall](https://www.sonicwall.com/) Global Management System(GMS) 9.3で認証されていない第三者がコマンド実行可能になっていた. -> pwn. 

#### 世の中
- [Ethiopia is fighting 'difficult and tiresome' guerrilla war in Tigray, says PM](https://www.theguardian.com/world/2021/apr/04/ethiopias-pm-says-military-fighting-difficult-and-tiresome-guerilla-war)
- [Putin signs law allowing him to run for two more terms as Russian President](https://edition.cnn.com/2021/04/05/europe/putin-russia-presidential-term-intl-hnk/index.html)
- [As Biden Pushes Major Rail Investments, Amtrak's 2035 Map Has People Talking](https://www.npr.org/2021/04/06/984464351/as-biden-pushes-major-rail-investments-rail-amtraks-2035-map-has-people-talking)
- [Body found in Epping Forest identified as Richard Okorogheye](https://www.theguardian.com/uk-news/2021/apr/07/body-found-in-epping-forest-identified-as-richard-okorogheye)
- [500 million LinkedIn users' data is for sale on a hacker site](https://edition.cnn.com/2021/04/08/tech/linkedin-data-scraped-hacker-site/index.html)
- [As the US races to vaccinate the country, J&J vaccine distribution will slow down 84% next week](https://edition.cnn.com/2021/04/09/health/jj-vaccine-slowdown/index.html)
- [BBC flooded with complaints over coverage of Prince Philip’s death](https://www.theguardian.com/media/2021/apr/10/bbc-flooded-with-complaints-over-prince-philip-coverage)

- [あんたそれはに人間としてやってはいかんよ、と思える画像やエピソードをあげてくれますか？](https://qr.ae/pG8E0V)
- [グレタ・トゥンベリさんが「もう一つのノーベル平和賞」といわれる「ライト・ライブリフッド賞」を受賞することに対して、なぜこれほどの反発・反感があるのでしょうか？](https://qr.ae/pG8j8o)
- [ほとんどの人は知らないけれど、あなたは知っているショックな事実は何ですか？](https://qr.ae/pG1hH0)
- [未だにアナログで呆れることはありますか？](https://qr.ae/pG1hoZ)
- [天皇陛下とのエピソードをお持ちでしょうか？](https://qr.ae/pG1Prs)
- [世界の見方を根底から変えてしまうようなことを何か教えて頂けませんか？](https://qr.ae/pG1E1Q)
- [無知が招いた恐ろしい失敗のエピソードはありますか？](https://qr.ae/pGT4Bm)