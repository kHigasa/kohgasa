---
title: 論文試飲メモ(2021年05月第3週)
date: "2021-05-23T04:00:00.000Z"
description: "2021年5月第3週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **The essential role of magnetic frustration in the phase diagrams of doped cobaltites** [arXiv:2105.06402 [cond-mat.str-el]](https://arxiv.org/abs/2105.06402)

  ドープされたペロヴスカイトコバルタイトの乱れた古典スピンモデルのMonte-Carloシミュレーションに磁気フラストレーションの効果を取り入れることで磁気相図を再現している. 

  ![2105.06402](/kohgasa/2105.06402.jpg)

- **Glassy dynamics of sticky hard spheres beyond the mode-coupling regime** [arXiv:2105.06417 [cond-mat.soft]](https://arxiv.org/abs/2105.06417)

  一般化されたモード結合理論 (GMCT) と呼ばれる改善された第一原理に基づく理論を粘着質硬球に適用し, ガラス転移のダイナミクスをより的確に予測. 

  ![2105.06417](/kohgasa/2105.06417.jpg)

- **Collective dynamics of domain walls: an antiferromagnetic spin texture in an opticalcavity** [arXiv:2105.06430 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2105.06430)

  反強磁性磁気光学系における領域壁の動力学がどのようにDzyaloshinskii-Moriya相互作用 (DMI) に直接関連した独自の特徴を示すのかを調査し, DMIの存在により磁気-光結合を許さない幾何学的形状における空孔光子を持つスピン相互作用が可能になり, DMIのない場合には周波数の変調が起こるという結果が得られている. 

  ![2105.06430](/kohgasa/2105.06430.jpg)

- **Frustrated network of indirect exchange paths between tetrahedrally coordinated Coin Ba2CoO4** [arXiv:2105.06437 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2105.06437)

  Ba$_2$CoO$_4$の電磁気相互作用を調査し, Co($d$)-O($p$)混成状態と$0.4\ \mu_{\text{B}}$のO上に誘起されたスピンモーメントが酸素-正孔状態を特徴づける役割を果たすことを示している. 

  ![2105.06437](/kohgasa/2105.06437.jpg)

- **Bicircular light tuning of magnetic symmetry and topology in Dirac semimetal Cd3As2** [arXiv:2105.06439 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.06439)

  二重円偏光 (BCL) を用いたFloquet工学が物質の磁気対称性とトポロジーを制御する汎用的な方法であることを示している. 

  ![2105.06439](/kohgasa/2105.06439.jpg)

- **AtomECS: Simulate laser cooling and magneto-optical traps** [arXiv:2105.06447 [cond-mat.quant-gas]](https://arxiv.org/abs/2105.06447)

  レーザー冷却や磁気光学トラップをシミュレートするソフトウェアの紹介. 

- **Partially-separated Majorana modes in a disordered medium** [arXiv:2105.06469 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.06469)

  半導体-超伝導体ヘテロ構造におけるMajoranaゼロモードに対する最近の実験から示唆されることに注目し, 乱れがある系におけるMajorana物理の明らかな特徴としてのゼロバイアス微分コンダクタンスの量子化を批判的に検討. 乱れの強さがMajorana物理に対する重要な指標となっているという主張. 

#### 読み物


#### ヴィデオ


#### 世の中
- [UK ‘faces labour shortage’ as Covid and Brexit fuel exodus of overseas workers](https://www.theguardian.com/business/2021/may/17/uk-faces-labour-shortage-as-covid-and-brexit-fuel-exodus-of-overseas-workers)
- [India variant will be dominant UK Covid strain ‘in next few days’](https://www.theguardian.com/world/2021/may/17/uk-cases-of-indian-covid-variant-almost-doubled-in-four-days-says-hancock)
- [Any amount of alcohol consumption harmful to the brain, finds study](https://www.theguardian.com/society/2021/may/18/any-amount-of-alcohol-consumption-harmful-to-the-brain-finds-study)
- [An 11-year-old girl fought off a knife-wielding man who tried to kidnap her at Florida school bus stop, sheriff says](https://edition.cnn.com/2021/05/19/us/florida-kidnapping-attempt-video-trnd/index.html)
- [A Fungus Is Pushing Cicada Sex Into Hyperdrive And Leaving Them Dismembered](https://www.npr.org/2021/05/18/997998920/the-fungus-thats-making-cicadas-sex-crazy)
- [Extreme weather kills 21 ultra-marathon runners in China](https://edition.cnn.com/2021/05/22/china/china-runners-deal-intl-hnk/index.html)
- [Lawmakers see promise in police reform negotiations as Biden plans meeting with George Floyd's family](https://edition.cnn.com/2021/05/23/politics/joe-biden-george-floyd-police-reform/index.html)