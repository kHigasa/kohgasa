---
title: 論文試飲メモ(2021年05月第4週)
date: "2021-05-30T04:00:00.000Z"
description: "2021年5月第4週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Entanglement Entropy of Non-Hermitian Free Fermions** [arXiv:2105.09793 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.09793)

  相関行列の手法を用いて並進対称性を持つ非エルミート自由フェルミオンモデルのエンタングルメント特性を調べ, エンタングルメントエントロピーは1次元系, 2次元系共に面積則に対して対数的な補正を持つことを示している. 

  ![2105.09793](/kohgasa/2105.09793.jpg)

- **Spontaneous Formation of Star-Shaped Surface Patterns in a Driven Bose-Einstein Condensate** [arXiv:2105.09794 [cond-mat.quant-gas]](https://arxiv.org/abs/2105.09794)

  Feshbach共鳴付近の散乱長を変調させることによる励起が引き起こす流動的なBose-Einstein凝縮体における自発的に形成される星型の表面パターンを観測している. 

  ![2105.09794](/kohgasa/2105.09794.jpg)

- **Spin dynamics of the quantum dipolar magnet Yb3Ga5O12in an external field** [arXiv:2105.09817 [cond-mat.str-el]](https://arxiv.org/abs/2105.09817)

  Yb$_3$Ga$_5$O$_{12}$の常磁性相における帯磁率や比熱の測定により, 量子双極子磁性体であることが示唆されている. 

  ![2105.09817](/kohgasa/2105.09817.jpg)

- **Excitation Spectrum and Superfluid Gap of an Ultracold Fermi Gas** [arXiv:2105.09820 [cond-mat.quant-gas]](https://arxiv.org/abs/2105.09820)

  Bragg顕微鏡を用いて強く相互作用している極低温Fermi気体の角度分解低エネルギー励起のスペクトルと超流動ギャップの発達の様子を調べている. 

  ![2105.09820](/kohgasa/2105.09820.jpg)

- **Overscreening–free electron–phonon interaction in realistic materials** [arXiv:2105.09823 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2105.09823)

  電子-フォノン相互作用の修正形式を導出し, その正当性を検証している. 

  ![2105.09823](/kohgasa/2105.09823.jpg)

- **Electron–phonon interaction without overscreening: a strategy for first–principlesmodelling** [arXiv:2105.09828 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2105.09828)

  遮蔽誤差をなくしたフォノン-電子自己エネルギーの理論的, 数値的計算手法を導入している. 

  ![2105.09828](/kohgasa/2105.09828.jpg)

- **Thermal-Field Electron Emission from Three-DimensionalTopological Semimetals** [arXiv:2105.09874 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.09874)

  Dirac/Weylノードを有する3次元トポロジカル半金属による電子の熱場放出を記述するモデルの構築. 

  ![2105.09874](/kohgasa/2105.09874.jpg)

#### 読み物


#### ヴィデオ


#### 世の中
- [Wuhan lab staff sought hospital care before COVID-19 outbreak disclosed - WSJ](https://www.reuters.com/business/healthcare-pharmaceuticals/wuhan-lab-staff-sought-hospital-care-before-covid-19-outbreak-disclosed-wsj-2021-05-23/)
- [Over 500 Democratic staffers urge Joe Biden to ‘hold Israel accountable’](https://www.theguardian.com/us-news/2021/may/24/joe-biden-israel-palestine-letter-democratic-staffers)
- [As Anger Toward Belarus Mounts, Recall the 2013 Forced Landing of Bolivia's Plane to Find Snowden](https://greenwald.substack.com/p/as-anger-toward-belarus-mounts-recall)
- ['Enough': Biden calls for action on guns in wake of San Jose shooting](https://edition.cnn.com/2021/05/26/politics/biden-san-jose-shooting/index.html)
- [‘They fired at everyone’: peril of Pakistani villagers protesting giant luxury estate](https://www.theguardian.com/global-development/2021/may/28/they-fired-at-everyone-peril-of-pakistani-villagers-protesting-giant-luxury-estate)
- [Man charged with conspiracy to murder over shooting of British BLM activist Sasha Johnson](https://edition.cnn.com/2021/05/28/uk/london-blm-activist-shooting-charge-intl-hnk/index.html)
- [The incredible rise of Samoa's first female Prime Minister-elect, and the man still standing in her way](https://edition.cnn.com/2021/05/29/asia/samoa-prime-minister-intl-hnk-dst/index.html)