---
title: 論文試飲メモ(2021年05月第1週)
date: "2021-05-09T04:00:00.000Z"
description: "2021年5月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Like-charge attraction at short distances in a charge-asymmetric two-dimensional two-component plasma: Exact results** [arXiv:2104.14451 [cond-mat.soft]](https://arxiv.org/abs/2104.14451)

  正と負で違う量だけ帯電した点電荷からなる2次元2成分電荷非対称プラズマに浸された2つのゲスト電荷の間にはたらく短距離実効ポテンシャルを正確に決定している. 

  ![2104.14451](/kohgasa/2104.14451.jpg)

- **First principles studies of the surface andopto-electronic properties of ultra-thint-Se** [arXiv:2104.14455 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2104.14455)

  感光材料になるアモルファス相Seの性質を理解するために, その基底状態である三方晶Se(t-Se)の薄膜の$(10\overline{1}0)$面に対する密度汎関数理論(DFT)に基づいた系統的調査を行っている. 

  ![2104.14455](/kohgasa/2104.14455.jpg)

- **Majorana bound states in semiconducting nanostructures** [arXiv:2104.14459 [cond-mat.mes-hall]](https://arxiv.org/abs/2104.14459)

  半導体ナノ構造において生じるMajorana束縛状態 (MBSs) へのイントロ. 

  ![2104.14459](/kohgasa/2104.14459.jpg)

- **Double-dome superconductivity under pressure in the V-based Kagome metalsAV3Sb5(A= Rb and K)** [arXiv:2104.14487 [cond-mat.supr-con]](https://arxiv.org/abs/2104.14487)

  Vベース超伝導体$A$V$_3$Sb$_5$ ($A$=Rb and K) の高圧下における抵抗測定. 

  ![2104.14487](/kohgasa/2104.14487.jpg)

- **Feynman-Vernon influence functional approach to quantum transport in interactingnanojunctions: An analytical hierarchical study** [arXiv:2104.14497 [cond-mat.mes-hall]](https://arxiv.org/abs/2104.14497)

  還元系のダイナミクスの実時間経路積分の形式化に基づいた相互作用しているナノ接合における電荷輸送に対する非摂動で形式的に厳密な手法を提示し, 厳密に解ける共鳴準位モデル(RLM)と単一不純物Andersonモデル(SIAM)にそれを適用している. 

  ![2104.14497](/kohgasa/2104.14497.jpg)

- **Transient Fluctuation–Induced Forcesin Driven Electrolytes after an Electric Field Quench** [arXiv:2104.14498 [cond-mat.soft]](https://arxiv.org/abs/2104.14498)

  電解質に電場クエンチをかけた後, 電荷と密度の長距離相関による力が時間的に減衰し定常状態の値に向かうことを示している. 

  ![2104.14498](/kohgasa/2104.14498.jpg)

- **Soft Mode in the Dynamics of Over-realizable On-line Learning for Soft CommitteeMachines** [arXiv:2104.14546 [cond-mat.dis-nn]](https://arxiv.org/abs/2104.14546)

  過剰実現可能な場合における2層ソフトコミッティーマシンのオンライン学習に対する完璧な学習へのアプローチは, 汎化誤差が冪乗の振る舞いを示すときに起こるという発見. 

  ![2104.14546](/kohgasa/2104.14546.jpg)

#### 読み物


#### ヴィデオ


#### CVE
- [CVE-2021-31540](https://nvd.nist.gov/vuln/detail/CVE-2021-31540)
  - NIST: NVD
  - Base Score:  7.1 HIGH
  - Vector:  CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:N

- [CVE-2021-31646](https://nvd.nist.gov/vuln/detail/CVE-2021-31646)
  - NIST: NVD
  - Base Score:  9.8 CRITICAL
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H

- [CVE-2021-29478](https://nvd.nist.gov/vuln/detail/CVE-2021-29478)
  - CNA:  GitHub, Inc.
  - Base Score:  7.5 HIGH
  - Vector:  CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:H/I:H/A:H

- [CVE-2021-29489](https://nvd.nist.gov/vuln/detail/CVE-2021-29489)
  - CNA:  GitHub, Inc.
  - Base Score:  7.6 HIGH
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:H/A:L

- [CVE-2021-29482](https://nvd.nist.gov/vuln/detail/CVE-2021-29482)
  - CNA:  GitHub, Inc.
  - Base Score:  7.5 HIGH
  - Vector:  CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H

- [CVE-2021-22330](https://nvd.nist.gov/vuln/detail/CVE-2021-22330)
  - NIST: NVD
  - Base Score:  6.5 MEDIUM
  - Vector:  CVSS:3.1/AV:A/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H

- [CVE-2021-1448](https://nvd.nist.gov/vuln/detail/CVE-2021-1448)
  - NIST: NVD
  - Base Score:  7.8 HIGH
  - Vector:  CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H

#### 世の中
- [German police bust child sex abuse imagery network with 400,000 users](https://edition.cnn.com/2021/05/03/europe/germany-child-sex-abuse-imagery-ring-intl/index.html)
- [‘Out-of-control’ Chinese rocket tumbling to Earth](https://www.theguardian.com/science/2021/may/04/out-of-control-chinese-rocket-tumbling-to-earth)
- [France threatens to cut off power to Jersey in post-Brexit fishing row](https://www.theguardian.com/uk-news/2021/may/04/france-threatens-to-cut-off-power-to-jersey-in-post-brexit-fishing-row)
- [National Guard soldier charged for storming the US Capitol](https://edition.cnn.com/2021/05/05/politics/wisconsin-national-guard-us-capitol/index.html)
- [Former Maldives president treated for wounds after blast outside home](https://jp.reuters.com/article/us-maldives-blast/former-maldives-president-treated-for-wounds-after-blast-outside-home-idUSKBN2CN21A)
- [Police have a fresh lead in the disappearance of a 4-year-old girl in 2003 after a TikTok interview with a woman in Mexico](https://edition.cnn.com/2021/05/07/us/tiktok-video-missing-girl-case/index.html)
- [4 Years After an Execution, a Different Man’s DNA Is Found on the Murder Weapon](https://www.nytimes.com/2021/05/07/us/ledell-lee-dna-testing-arkansas.html)

- [高校で学んだことで一番印象に残っていることは何ですか？](https://qr.ae/pGhaLf)
- [日本という国家の切なさを感じる写真はありますか？](https://qr.ae/pGhlQE)
- [なぜ/どのようにして人は陰謀論に夢中になり、それと正反対の圧倒的な証拠に直面しても、その考えを手放さないのでしょうか？](https://qr.ae/pGt8ZF)
- [日本人は他国の人とどういう違いがありますか？](https://qr.ae/pGt2da)
- [英語のリスニング力を、短期間で高める方法はありますか？](https://qr.ae/pGtgC6)
- [「マーガリンにはトランス脂肪酸が含まれているから、健康上よろしくない」と聞きます。しかしスーパーには、マーガリンが溢れています。どちらを信用すれば良いのでしょうか？](https://qr.ae/pGtmCq)
- [日本社会は幼稚ではありませんか？欧米諸国の方が倫理観が高く合理的な判断ができているように感じます。](https://qr.ae/pGnR7i)