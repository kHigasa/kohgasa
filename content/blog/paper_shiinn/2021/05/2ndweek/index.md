---
title: 論文試飲メモ(2021年05月第2週)
date: "2021-05-16T04:00:00.000Z"
description: "2021年5月第2週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Visualization of optical polarization transfer tophotoelectron spin vector emitted from the spin-orbit coupled surface state** [arXiv:2105.02749 [cond-mat.mtrl-sci]](https://arxiv.org/abs/2105.02749)

  レーザー角度スピン分解光電子分光を用いてBi$_2$Se$_3$におけるスピン-軌道結合表面状態の光電子放出において生じるスピン干渉を調査: レーザー光の分極に対する光電子のスピンベクトルを調べている. 

  ![2105.02749](/kohgasa/2105.02749.jpg)

- **Minigap suppression in S(N/F)S junctions** [arXiv:2105.02765 [cond-mat.supr-con]](https://arxiv.org/abs/2105.02765)

  通常金属 (N) と強磁性体 (F) の2層からなる弱いリンクのJosephson接合系において, 2層の本質的な非均質性から生じる対分離機構がスピン状態密度における少ギャップを抑制させることが示されている. 

  ![2105.02765](/kohgasa/2105.02765.jpg)

- **Local and non-local quantum transport due to Andreev bound statesin finite Rashba nanowires with superconducting and normal sections** [arXiv:2105.02791 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.02791)

  超伝導層によって部分的にのみ覆われたRashbaナノワイヤの常伝導部に形成されるAndreev束縛状態 (ABSs) を解析し, 有限サイズの非トポロジカルナノワイヤに対してその両端に相関したゼロバイアスピーク (ZBP) が存在することが明らかにされている. 

  ![2105.02791](/kohgasa/2105.02791.jpg)

- **Probing Robust Majorana Signatures by Crossed Andreev Reflectionwith a Quantum Dot** [arXiv:2105.02830 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.02830)

  通常金属とs波超伝導体, Majorana Yジャンクションの3つのリード部分に結合された量子ドットからなるMajoranaゼロモードのロバストな特徴を証明するための3端子構造を提案. 

  ![2105.02830](/kohgasa/2105.02830.jpg)

- **Effective Landé factors for an electrostatically defined quantum point contact insilicene** [arXiv:2105.02843 [cond-mat.mes-hall]](https://arxiv.org/abs/2105.02843)

  分裂ゲートの電場によってシリセン内で定義された量子点接触に対するトランスコンダクタンスと実効的Lande$g^*$因子の調査. 

  ![2105.02843](/kohgasa/2105.02843.jpg)

- **Chirality transitions in a system of active flatspinners** [arXiv:2105.02850 [cond-mat.soft]](https://arxiv.org/abs/2105.02850)

  上方への乱流によって流動している円盤型ローターの2次元ダイナミクスを調べ, 平均運動エネルギーの増加につれて, 系のカイラリティが正から負へ複雑な状態を経て発展することが示されている. 

  ![2105.02850](/kohgasa/2105.02850.jpg)

- **A continuum model reproducing the multiple frequencycrossovers in acoustic attenuation in glasses** [arXiv:2105.02864 [cond-mat.soft]](https://arxiv.org/abs/2105.02864)

  アモルファス物質において原子スケールとマクロスケールを橋渡しでき, 温度の影響も含めてGHzからTHz領域の$\omega^2-\omega^4-\omega^2$依存性を持つ音波減衰をよく再現できる粘弾性流体に対する連続体力学モデルを提示. 

  ![2105.02864](/kohgasa/2105.02864.jpg)

#### 読み物


#### ヴィデオ


#### CVE
- [CVE-2020-7731](https://nvd.nist.gov/vuln/detail/CVE-2020-7731)
- [CVE-2020-19113](https://nvd.nist.gov/vuln/detail/CVE-2020-19113)
- [CVE-2021-32099](https://nvd.nist.gov/vuln/detail/CVE-2021-32099)
- [CVE-2021-20266](https://nvd.nist.gov/vuln/detail/CVE-2021-20266)
- [CVE-2020-29444](https://nvd.nist.gov/vuln/detail/CVE-2020-29444)
- [CVE-2021-20538](https://nvd.nist.gov/vuln/detail/CVE-2021-20538)
- [CVE-2021-32399](https://nvd.nist.gov/vuln/detail/CVE-2021-32399)

#### 世の中
- [Google faces $5 billion lawsuit in U.S. for tracking 'private' internet use](https://www.reuters.com/article/us-alphabet-google-privacy-lawsuit-idUSKBN23933H)
- [US FDA authorizes Pfizer's Covid-19 vaccine for use in people ages 12 to 15](https://edition.cnn.com/2021/05/10/health/pfizer-vaccine-eua-12-15-teens/index.html)
- [US millionaire CEOs saw 29% pay raise while workers’ pay fell, report finds](https://www.theguardian.com/business/2021/may/11/us-millionaire-ceos-saw-29-pay-raises-while-workers-had-decreases-report-says)
- [‘All I saw was fire’: rockets fracture the sense of safety in Tel Aviv](https://www.theguardian.com/world/2021/may/12/all-i-saw-was-fire-rockets-fracture-the-sense-of-safety-in-tel-aviv)
- [Ohio Lottery to Give 5 People $1 Million Each to Encourage Vaccination](https://www.nytimes.com/2021/05/12/us/ohio-lottery-coronavirus-vaccine.html)
- [Seawater being used to put out fire on a cargo ship wreckage off the Georgia coast](https://edition.cnn.com/2021/05/14/us/golden-ray-wreckage-fire/index.html)
- [China lands unmanned spacecraft on Mars for first time](https://www.theguardian.com/science/2021/may/15/china-lands-unmanned-spacecraft-on-mars)

- [気の毒に思う国はどこですか？](https://qr.ae/pGnr9o)
- [日本人の英語力は非常に低いですが、今後、英語力は他のアジア諸国に追いつくことはあるのでしょうか？](https://qr.ae/pGnwyt)
- [ヴィーガン「肉を食べるなとは言いません、まずは家畜が殺されているという現実をその眼で何度も見てください、そうすれば答えは出るはずです」あなたは、何と返事をしますか？](https://qr.ae/pGnx27)
- [日本という国家の切なさを感じる写真はありますか？](https://qr.ae/pGnxYt)
- [人間の脳について、一番びっくりする意外な事実は何ですか？](https://qr.ae/pGvr5U)
- [価値観の違いについて語ってくれる漫画はありますか？](https://qr.ae/pGvueO)
- [何をしても悲しいときはどうしますか？](https://qr.ae/pGvuVv)