---
title: 論文試飲メモ(2021年08月第4週)
date: "2021-08-30T04:00:00.000Z"
description: "2021年8月第4週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Microscopic investigation of superconducting properties of a strongly coupled superconductor IrGe via μSR** [arXiv:2108.07982 [cond-mat.supr-con]](https://arxiv.org/abs/2108.07982)

  磁化, 交流輸送, 比熱, ミューオンスピン回転/緩和($\mu$SR)測定を用いて, 強結合超伝導体IrGeの超伝導基底状態を詳細に調査している. 

  ![2108.07982](/kohgasa/2108.07982.jpg)

- **Charge Order and Fluctuations in Bi2Sr2−xLax CuO6+δ Revealed by 63,65Cu-Nuclear Magnetic Resonance** [arXiv:2108.08150 [cond-mat.supr-con]](https://arxiv.org/abs/2108.08150)

  電荷密度波(CDW)の開始温度$T_{\text{CDW}}$が擬ギャップ温度$T^*$とともに上昇することが見出されている. 

  ![2108.08150](/kohgasa/2108.08150.jpg)

- **Half-vortices, confinement transition and chiral Josephson effect in superconducting Weyl/Diracsemimetals** [arXiv:2108.08182 [cond-mat.supr-con]](https://arxiv.org/abs/2108.08182)

  主題の超伝導がXYモデルの拡張として実現することを議論している. 

  ![2108.08182](/kohgasa/2108.08182.jpg)

- **Simulating gauge theories with variational quantum eigensolvers in superconducting microwave cavities** [arXiv:2108.08248 [quant-ph]](https://arxiv.org/abs/2108.08248)

  タイトルママ. 

  ![2108.08248](/kohgasa/2108.08248.jpg)

- **Switching Current Distributions in Josephson Junctions at Low TemperaturesResulting From Noise Enhanced Thermal Activation** [arXiv:2108.08256 [cond-mat.supr-con]](https://arxiv.org/abs/2108.08256)

  室温中の機器に入る雑音のエネルギーが極小さな比率で試料に到達し, Josephson接合の電流スイッチングバイアスの温度依存性の観測を再現するのに十分な回避率の向上をもたらすことを示している. 

  ?

  ![2108.08256](/kohgasa/2108.08256.jpg)

- **Enhancement and Discontinuity of Effective Mass through the First-Order Metamagnetic Transition in UTe2** [arXiv:2108.07982 [cond-mat.supr-con]](https://arxiv.org/abs/2108.07982)

  非従来型スピン一重項超伝導体UTe$_2$におけるメタ磁性転移が直交$b$-軸に沿ってかけられた場に対する磁化と温度の同時測定によって調査され, reentrant超伝導(RSC)が一次メタ磁性転移磁場$H_{\text{m}}$の上下で探知されている. 

  ![2108.07982](/kohgasa/2108.07982.jpg)

- **Renormalization group analysis of Dirac fermions with random mass** [arXiv:2108.08526 [cond-mat.dis-nn]](https://arxiv.org/abs/2108.08526)

  クラスDに属する二次元無秩序超伝導体の量子三重臨界点を特徴づけるために, $\varepsilon$-展開の観点からランダム質量を持つ二次元Dirac Fermionに対する二ループ繰り込み群解析を実行している. 

  ![2108.08526](/kohgasa/2108.08526.jpg)

#### 読み物
- [令和元年（あ）第１８４３号殺人，窃盗，住居侵入，会社法違反被告事件](https://www.courts.go.jp/app/files/hanrei_jp/889/089889_hanrei.pdf)
- [平成30年(し)第332号 再審開始決定に対する即時抗告の決定に対する 特別抗告事件](https://www.courts.go.jp/app/files/hanrei_jp/920/089920_hanrei.pdf)
- [令和２年（あ）第９６号殺人，殺人未遂，傷害被告事件](https://www.courts.go.jp/app/files/hanrei_jp/989/089989_hanrei.pdf)
- [平成３０年（あ）第１３８１号わいせつ電磁的記録記録媒体陳列，公然わいせつ被告事件](https://www.courts.go.jp/app/files/hanrei_jp/995/089995_hanrei.pdf)
- [平成３０年（あ）第１０号不正競争防止法違反被告事件](https://www.courts.go.jp/app/files/hanrei_jp/062/090062_hanrei.pdf)
- [平成30年(あ)第1270号 強盗殺人被告事件](https://www.courts.go.jp/app/files/hanrei_jp/238/090238_hanrei.pdf)
- [平成30年(し)第76号 再審請求棄却決定に対する即時抗告棄却決定に対する特別抗告事件](https://www.courts.go.jp/app/files/hanrei_jp/266/090266_hanrei.pdf)

#### ヴィデオ
- *Snowden*

#### 世の中
- [First Afghan evacuees arrive in Germany in one of the largest airlift operations in history](https://edition.cnn.com/2021/08/21/world/evacuations-afghanistan-taliban-ramstein-intl/index.html)
- [Nasa delays ISS spacewalk due to astronaut’s medical issue](https://www.theguardian.com/science/2021/aug/24/nasa-delays-iss-spacewalk-due-to-astronauts-medical-issue)
- [New Zealand won’t ‘throw in towel’ on Covid-zero strategy despite rising infections](https://www.theguardian.com/world/2021/aug/25/new-zealand-wont-throw-in-towel-on-covid-zero-strategy-despite-rising-infections)
- [LED streetlights decimating moth numbers in England](https://www.theguardian.com/environment/2021/aug/25/led-streetlights-moth-england-eco-friendly-sodium-insect-decline)
- [Apple agrees to let iPhone apps email users about payment options](https://www.theguardian.com/technology/2021/aug/27/apple-agrees-to-let-iphone-apps-email-users-about-payment-options)
- [Scientists discover ‘world’s northernmost island’ off Greenland’s coast](https://www.theguardian.com/world/2021/aug/28/scientists-discover-worlds-northernmost-island-off-greenlands-coast)
- [Floating wind turbines could open up vast ocean tracts for renewable power](https://www.theguardian.com/environment/2021/aug/29/floating-wind-turbines-ocean-renewable-power)