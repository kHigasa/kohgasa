---
title: 論文試飲メモ(2021年08月第3週)
date: "2021-08-16T04:00:00.000Z"
description: "2021年8月第3週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Fabrication and surface treatment of electron-beam evaporated niobiumfor low-loss coplanar waveguide resonators** [arXiv:2108.05354 [cond-mat.supr-con]](https://arxiv.org/abs/2108.05354)

  超高真空状態に置かれた低損失電子ビーム蒸着されたニオブ化薄膜の特性を評価している. 

  ![2108.05354](/kohgasa/2108.05354.jpg)

- **Topologically enabled superconductivity** [arXiv:2108.05360 [cond-mat.supr-con]](https://arxiv.org/abs/2108.05360)

  動的なインスタントンに付随している零モードが超伝導を可能にするだけでなくある場合にはより一層強固にすることを示している. 

  ![2108.05360](/kohgasa/2108.05360.jpg)

- **Higher-Order Topological Superconductivity in Twisted Bilayer Graphene** [arXiv:2108.05373 [cond-mat.supr-con]](https://arxiv.org/abs/2108.05373)

  捻れた二層グラフェンにスピン一重項又は三重項超伝導体を導入すると高次のトポロジカル超伝導を誘起することを示している. 

  ![2108.05373](/kohgasa/2108.05373.jpg)

- **Ising superconductivity in monolayer niobium dichalcogenide alloys** [arXiv:2108.05426 [cond-mat.supr-con]](https://arxiv.org/abs/2108.05426)

  第一原理計算を用いて, NbS$_x$Se$_{2-x}$におけるFermi準位での状態密度と近接磁化がNbSe$_2$やNbS$_2$と比較され, 前者が転移温度を下げる寄与をするのに対し, 後者が転移温度を上げるのに寄与することを示し, 又, Se空孔が磁気的対破壊欠損のように振る舞いNbSe$_2$における多大な集中を生むであろうことを示している. 

  ![2108.05426](/kohgasa/2108.05426.jpg)

- **Matrix-pairing states in the alkaline Fe-selenide superconductors: exotic Josephson junctions** [arXiv:2108.05442 [cond-mat.supr-con]](https://arxiv.org/abs/2108.05442)

  少なくとも一方のリードには$s\tau_3$状態という二軌道対形成状態の超伝導状態を持つJosephson接合を研究し, その接合の仕方により異なった束縛状態を得ている. 

  ![2108.05442](/kohgasa/2108.05442.jpg)

- **Theory of Topological Superconductivity in Doped IV-VI Semiconductors** [arXiv:2108.05780 [cond-mat.supr-con]](https://arxiv.org/abs/2108.05780)

  ドープされたABタイプIV-VI半導体におけるポテンシャル非従来型超伝導体を再隣接までの相互作用を持つ最小有効モデルに基づいて研究している. 

  ![2108.05780](/kohgasa/2108.05780.jpg)

- **Semiconductor Epitaxy in Superconducting Templates** [arXiv:2108.05878 [cond-mat.supr-con]](https://arxiv.org/abs/2108.05878)

  下図のようなセットアップでInAsの成長を実証している. 任意の結晶面でシャープな接合面が出来る様. 

  ![2108.05878](/kohgasa/2108.05878.jpg)

#### 読み物
- [令和２年（あ）第３４３号  準強姦被告事件](https://www.courts.go.jp/app/files/hanrei_jp/293/090293_hanrei.pdf)
- [令和３年（医へ）第５号  入院を継続すべきことを確認する旨の決定に対する抗告棄却決定に対する再抗告事件](https://www.courts.go.jp/app/files/hanrei_jp/381/090381_hanrei.pdf)
- [令和３年（あ）第５４号強盗致傷，犯人隠避教唆，犯人蔵匿教唆被告事件](https://www.courts.go.jp/app/files/hanrei_jp/383/090383_hanrei.pdf)
- [令和２年（あ）第１５２８号詐欺被告事件](https://www.courts.go.jp/app/files/hanrei_jp/434/090434_hanrei.pdf)
- [令和２年（あ）第９１９号 常習特殊窃盗被告事件](https://www.courts.go.jp/app/files/hanrei_jp/455/090455_hanrei.pdf)
- [平成３０年（あ）第１８４６号 薬事法違反被告事件](https://www.courts.go.jp/app/files/hanrei_jp/456/090456_hanrei.pdf)
- [令和２年（あ）第１７６３号 覚醒剤取締法違反，大麻取締法違反，医薬品，医療機器等の品質，有効性及び安全性の確保等に関する法律違反被告事件](https://www.courts.go.jp/app/files/hanrei_jp/502/090502_hanrei.pdf)

#### ヴィデオ

#### 世の中
- [US could soon hit more than 200,000 new coronavirus cases per day, NIH director warns](https://edition.cnn.com/2021/08/15/health/us-coronavirus-sunday/index.html)
- [Millions of UK homes could be heated with hydrogen by 2030](https://www.theguardian.com/environment/2021/aug/17/uk-homes-low-carbon-hydrogen-economy-jobs)
- [Swedish mountain shrinks by two metres in a year as glacier melts](https://www.theguardian.com/world/2021/aug/18/swedish-mountain-shrinks-by-two-metres-in-a-year-as-glacier-melts)
- [US cannot ensure safe passage to Kabul airport, embassy says](https://edition.cnn.com/2021/08/18/politics/afghanistan-airport-safe-passage/index.html)
- [Teachers In Washington State Must Get Fully Vaccinated — Or They Could Be Fired](https://www.npr.org/sections/back-to-school-live-updates/2021/08/19/1029282381/teachers-in-washington-state-must-get-vaccinated-or-they-could-be-fired)
- [New Englanders brace for what could be the first hurricane in 30 years](https://edition.cnn.com/2021/08/21/weather/tropical-storm-henri-saturday/index.html)
- [At Least 8 Killed And Dozens Missing In Tennessee Flooding](https://www.npr.org/2021/08/21/1030056420/8-killed-dozens-missing-in-tennessee-flood)