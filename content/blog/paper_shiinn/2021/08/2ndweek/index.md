---
title: 論文試飲メモ(2021年08月第2週)
date: "2021-08-15T04:00:00.000Z"
description: "2021年8月第2週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Characterisation of spatial charge sensitivity in a multi-mode superconducting qubit** [arXiv:2108.02105 [quant-ph]](https://arxiv.org/abs/2108.02105)

  二つのトランスモンライクなモードを有する超伝導キュビットにおける電荷感応性を特徴づけ, Ramsey干渉計を用いて四つの電荷パリティ構成に対する感応性を観測している. さらにそのような複数モードキュビットにおける電荷感応性の理論を与えている. 

  ![2108.02105](/kohgasa/2108.02105.jpg)

- **Pattern-dependent proximity effect and Majorana edge modein one-dimensional quasicrystals** [arXiv:2108.02212 [cond-mat.supr-con]](https://arxiv.org/abs/2108.02212)

  準周期的量子ワイヤに対する超伝導近接効果を探索し, どのように準周期的パターンがMajoranaモードの安定性に影響するのかを議論している. 

  ![2108.02212](/kohgasa/2108.02212.jpg)

- **Extracting the Transport Channel Transmissions in Scanning Tunneling Microscopy usingthe Superconducting Excess Current** [arXiv:2108.02250 [cond-mat.supr-con]](https://arxiv.org/abs/2108.02250)

  走査型トンネル顕微鏡における原子スケールの接合の輸送チャネル伝送を連続的に抽出するために, 超伝導過剰電流を用いた方法を実装している. 

  ![2108.02250](/kohgasa/2108.02250.jpg)

- **Magic angle and plasmon mode engineering in twisted trilayer graphene with pressure** [arXiv:2108.02450 [cond-mat.mes-hall]](https://arxiv.org/abs/2108.02450)

  捻れた三層グラフェン(tTLG)の電子相関を垂直圧力を通して変調させることができる可能性を探索し, プラズモン的な性質が零圧力マジック角と印加された圧力下のマジック角両方を持つ平坦バンドtTLGにおいて調査されている. 

  ![2108.02450](/kohgasa/2108.02450.jpg)

- **Polarizing electron spins with a superconducting flux qubit** [arXiv:2108.02463 [quant-ph]](https://arxiv.org/abs/2108.02463)

  電子スピンを能動的に分極させるための超伝導磁束キュビットを作ることを提案している. 

  ![2108.02463](/kohgasa/2108.02463.jpg)

- **Yu-Shiba-Rusinov multiplets and clusters of multiorbital adatoms in superconducting substrates: Subgap Green’s function approach** [arXiv:2108.02546 [cond-mat.mes-hall]](https://arxiv.org/abs/2108.02546)

  $s$-波対称性を持つ超伝導基板における古典的磁気モーメントを有する不純物の集団に対するYu-Shiba-Rusinov状態の全ての特徴を議論し, サブギャップGreen関数を計算して不純物の多軌道構造の効果と結晶場分裂の効果を考えている. 

  ![2108.02546](/kohgasa/2108.02546.jpg)

- **Automated discovery of autonomous quantum error correction schemes** [arXiv:2108.02766 [quant-ph]](https://arxiv.org/abs/2108.02766)

  ある物理系の記述を与えられた自律量子誤り訂正符号を発見することに対する隣接最適化に基づいた計算機的手法を開発し, 実証している. 

  ![2108.02766](/kohgasa/2108.02766.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
- [Police launch investigation into Alibaba sexual assault allegations](https://www.theguardian.com/business/2021/aug/09/police-launch-investigation-into-alibaba-sexual-assault-allegations)
- [Fake COVID-19 Vaccination Cards Worry College Officials](https://time.com/6088778/fake-covid-19-vaccination-cards-college/)
- [Nurse in Germany suspected of replacing Covid vaccines with saline solution](https://www.theguardian.com/world/2021/aug/11/nurse-in-germany-suspected-of-replacing-covid-vaccines-with-saline-solution)
- [Shell to pay $111m over decades-old oil spills in Nigeria](https://www.theguardian.com/business/2021/aug/12/shell-to-pay-111m-over-decades-old-oil-spills-in-nigeria)
- [UK dog owners warned about thieves staking out parks and luring puppies](https://www.theguardian.com/uk-news/2021/aug/14/uk-dog-owners-warned-about-thieves-staking-out-parks-and-luring-puppies)
- [Asteroid Bennu now has a greater chance of hitting Earth through 2300, but still slim](https://edition.cnn.com/2021/08/11/world/nasa-osiris-rex-impact-study-scn/index.html)
- [At least 20 people killed after fuel tank explodes in Lebanon](https://edition.cnn.com/2021/08/15/middleeast/lebanon-explosion-intl-hnk/index.html)