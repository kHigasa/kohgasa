---
title: 論文試飲メモ(2021年08月第1週)
date: "2021-08-08T04:00:00.000Z"
description: "2021年8月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Trade off-Free Entanglement Stabilization in a Superconducting Qutrit-Qubit System** [arXiv:2107.13579 [quant-ph]](https://arxiv.org/abs/2107.13579)

  ターゲット状態への移行に対する純粋に散逸的なチャネルを作り, 第二励起状態にあるトランズモンと熱浴共振器を強いパラメトリック相互作用によって結合を媒介することで, 回路QEDプラットフォーム上で実現されたキュートリット-キュビット系におけるトレードオフのないBell状態安定化を達成するプロトコルを実証している. 

  ![2107.13579](/kohgasa/2107.13579.jpg)

- **Resonant pair-exchange scattering and BCS-BEC crossover in a systemcomposed of dispersive and heavy incipient bands: a Feshbach analogy** [arXiv:2107.13805 [cond-mat.supr-con]](https://arxiv.org/abs/2107.13805)

  大きく異なる質量を持つ二バンド系がBCS-BECクロスオーバーによって強調されるような新しい対特性をもたらす共鳴対散乱を持つことを理論的に示している. 

  ![2107.13805](/kohgasa/2107.13805.jpg)

- **Superconducting triplet pairings and anisotropic tunneling-magnetoresistance effectsin ferromagnet/superconductor/ferromagnet double-barrier junctions** [arXiv:2107.13818 [cond-mat.supr-con]](https://arxiv.org/abs/2107.13818)

  一般化されたBlonder–Tinkham–Klapwijkの手法から始めて, 強磁性体/超伝導体/強磁性体スピン弁接合におけるトンネリングコンダクタンスに対する界面Rashba-Dresselhaus結合を調べ, それによりトンネリング磁気抵抗特性を調査している. 

  ![2107.13818](/kohgasa/2107.13818.jpg)

- **Superconductivity in the bilayer Hubbard model: Are two Fermi surfaces better thanone?** [arXiv:2107.13996 [cond-mat.str-el]](https://arxiv.org/abs/2107.13996)

  対形成におけるincipient バンドの役割について明らかにするために, 非摂動量子Monte Carlo (QMC) 動的クラスター近似 (DCA) を用いて二層Hubbardモデルの対相関を調べ, モデルが大きな$t_{\perp}$極限で頑強な$s_{\pm}$対相関を持ち, 一つのバンドがincipientになるにつれてそれが強くなることを見出している. しかし, これは対場帯磁率の抑圧により$T_{\text{c}}$は上がらないことも見出している. 

  ![2107.13996](/kohgasa/2107.13996.jpg)

- **Drastic effect of weak interaction near special points in semiclassical multiterminal superconducting nanostructures** [arXiv:2107.14105 [cond-mat.mes-hall]](https://arxiv.org/abs/2107.14105)

  複数の超伝導端子に接続された半古典的超伝導ナノ構造において相互作用モデルを構築し普遍的で一般的な作用を導出して,特別点の極近傍におけるスペクトルを超伝導位相の量子揺らぎに現れる弱い相互作用が劇的に変化させることを示している. 

  ![2107.14105](/kohgasa/2107.14105.jpg)

- **Stabilizing volume-law entangled states of fermions and qubits using local dissipation** [arXiv:2107.14121 [quant-ph]](https://arxiv.org/abs/2107.14121)

  1次元フェルミオン, キュビット格子系の体積則エンタングル状態の散逸的準備と安定化に対する一般的な手法の解析. 

  ![2107.14121](/kohgasa/2107.14121.jpg)

- **Character of Doped Holes in Nd1−xSrxNiO2** [arXiv:2107.14148 [cond-mat.str-el]](https://arxiv.org/abs/2107.14148)

  層化されたニッケレートにおける電荷分布を調査し, $d-p$Coulomb相互作用$U_{dp}$によってZhang-Rice一重項が抑制される一方で, 局所三重項の振幅が強められることが見出されている. 

  ![2107.14148](/kohgasa/2107.14148.jpg)

#### 読み物

#### ヴィデオ

#### 世の中
- [China tests millions as Nanjing airport outbreak sees Covid cases surge](https://www.theguardian.com/world/2021/aug/02/china-tests-millions-as-nanjing-airport-outbreak-sees-covid-cases-surge)
- [Officer who responded to January 6 attack is third to die by suicide](https://edition.cnn.com/2021/08/02/politics/dc-metropolitan-police-officer-suicide-january-6-capitol-riot/index.html)
- [The Taliban is advancing in Afghanistan. Could Biden change his mind on US withdrawal?](https://edition.cnn.com/2021/08/03/world/meanwhile-in-america-august-3-intl/index.html)
- [Australian mathematician discovers applied geometry engraved on 3,700-year-old tablet](https://www.theguardian.com/science/2021/aug/05/australian-mathematician-discovers-applied-geometry-engraved-on-3700-year-old-tablet)
- [Climate crisis: Scientists spot warning signs of Gulf Stream collapse](https://www.theguardian.com/environment/2021/aug/05/climate-crisis-scientists-spot-warning-signs-of-gulf-stream-collapse)
- [UK government spends more than £163,000 on union flags in two years](https://www.theguardian.com/politics/2021/aug/06/uk-government-spends-union-flags-two-years-boris-johnson)
- [Trump will run for president in 2024, Sean Spicer claims](https://www.theguardian.com/us-news/2021/aug/07/trump-run-president-2024-sean-spicer)