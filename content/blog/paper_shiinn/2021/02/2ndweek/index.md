---
title: 論文試飲メモ(2021年02月第2週)
date: "2021-02-14T09:00:00.000Z"
description: "2021年2月第2週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Dissipative stabilization of squeezing beyond $3\ \mathrm{dB}$ in a microwave mode** [arXiv:2102.02863 [quant-ph]](https://arxiv.org/abs/2102.02863)

$3\ \mathrm{dB}$を超える熱浴工学スクイージングが純粋に電磁気的な内部共振器モード, すなわち超伝導量子キュビットにおけるマイクロ波周波数のモードに対して達成されるということを実験的に示している. 

手法としては, 測定された出力モードから共振器の状態を推測する代わりに, 回路QEDを用いその場でスクイーズド内部共振器マイクロ波モードのWigner断層撮影をを行っている. その内部共振器スクイージング因子は$3\ \mathrm{dB}$制限を大幅に超え, 少なくとも$(6.7\pm 0.2)\ \mathrm{dB}$にまで達している. また光子数統計を調査することによってスクイーズド状態の非古典性を調査し, スクイージングの散逸発展の完全な力学を注意深く研究するためにこの断層撮影の方法を用い, これよりその場でGaussian状態を安定させ, 操作し, 特徴づけるような面白いプラットフォームが提示される. この安定化技術は単純なスクイーズド状態を超えて, 回路QEDにおいて実現される大きな非線形性を利用することにより, 猫状態や格子状態のように他の様々な連続状態へ拡張される. 

![2102.02863](/kohgasa/2102.02863.jpg)

- **Improving the variational quantum eigensolver using variational adiabatic quantum computing** [arXiv:2102.02875 [quant-ph]](https://arxiv.org/abs/2102.02875)

変分量子固有値ソルバー(VQE)の欠点
1. 大きな問題にスケールするのに難ありかもしれない
1. 対象が非凸なので大域的ではなく局所的最小値に落ちてしまい正確な固有値の推定を制限する

を主張し改善するために変分断熱量子計算(VAQC)を用い, VAQCがVQEを初期化するための良い初期回路パラメータを発見することを示している. それを示す中でVAQCを他の幾つかの最適化手法(確率勾配降下法やNakanishi-Fujii-Todo法等)と組み合わせて使うことで, ただVQEを使うよりもより正確な解を得られることも示されている. 

- **Identical Particles in Quantum Mechanics: Against the Received View** [arXiv:2102.02894 [quant-ph]](https://arxiv.org/abs/2102.02894)

同種で全てが同じ状態にある同一量子力学的粒子系は1つの量子物体系としてみなされるべきであるという主張. 同一の量子力学的粒子系が区別できて, 各量子力学的粒子が個々の性質を有するような状況が沢山あるとのこと. 

一方査読者側は, 同一の量子力学的粒子系は個々の性質を持たない前もって区別されない物体系であると主張している. 

> That is, we will consider a collection of “identical particles that are in exactly the same state” as one object, not consisting of individual parts; this quantum object is identified by the state and its occupation number. Under certain circumstances it will happen, though, that distinguishing characteristics are created (analogously to what may happen in bank transfers) and that the notion of an individual quantum particle becomes applicable.

反駁可能. 

- **Superconducting triplet pairing in Al/Al2 O3 /Ni/Ga junctions** [arXiv:2102.03083 [quant-ph]](https://arxiv.org/abs/2102.03083)

超伝導磁気的$\text{Al/AL}_2\text{O}_3\text{/Ni/Ga}$接合のコンダクタンスを体系的に調査し, 現実の理論的シミュレーションと組み合わせて$\text{Ni/Ga}$の二分子膜の存在による三重項対の生成を実験により示している. 

ここで$\text{Ni}$-$\text{Ga}$の膜の厚さの比に強く依存したコンダクタンスのピーク特性を観察. この比は$\text{Ni/Ga}$の界面での実効的なスピン-軌道結合の強さを制御し, 三重項対を調節するためのかなり効率が良い. 印加された面内の磁場はコンダクタンススペクトルにあまり大きな影響を与えなかったので, 超伝導三重項対がこれら特有の性質を支配しているということがはっきりとわかる. さらに, $\text{Ga}$における常磁性的なMeissnner効果も検知し, 奇周波数の超伝導三重項状態が存在の証拠を見出している. 

![2102.03083](/kohgasa/2102.03083.jpg)

- **Voltage staircase in a current-biased quantum-dot Josephson junction** [arXiv:2102.03114 [cond-mat.mes-hall]](https://arxiv.org/abs/2102.03114)

量子ドットを含むJosephson接合のI-V特性がAndreevキュビットのRabi振動を検知するのにどのように使われるのかを示している. 

![2102.03114](/kohgasa/2102.03114.jpg)

- **Microwave response of a chiral Majorana interferometer** [arXiv:2102.03220 [cond-mat.mes-hall]](https://arxiv.org/abs/2102.03220)

トポロジカル超伝導体とカイラル1次元Majorana fermionに基づいた干渉計の考案. 

トポロジカル基盤内で超伝導対相関を誘起する超伝導状態の浮島があることにより, 浮島と地球間のインピーダンスが照射するマイクロ波の周波数に依存し, 浮島の充電と放電が通常チャネルとAndreevチャネルの両方におけるカイラルMajorana励起の干渉によって制御される. 

![2102.03220](/kohgasa/2102.03220.jpg)

- **Evidence of Andreev blockade in a double quantum dot coupled to a superconductor** [arXiv:2102.03283 [cond-mat.mes-hall]](https://arxiv.org/abs/2102.03283)

電流の道に局在したスピン三重項状態がスピン一重項状態の超伝導体に侵入することを禁止され電子輸送のブロッケードが生じるような2つの量子ドットを作成. 

この装置を調査して, 幾つかの観察が示されている. 

![2102.03283](/kohgasa/2102.03283.jpg)

#### 読み物
- [論理的思考の放棄](https://softether.hatenadiary.org/entry/20070324/p1)
- [駅などの人の多いエリアに公共のゴミ箱を設置して欲しい](https://faq.city.ichikawa.lg.jp/faq/p/opinion/faq1117.html)
- [Twitter上のプログラミング入門者観察記](https://keens.github.io/blog/2020/05/20/twitteruenopuroguramingunyuumonshakansatsunikki/)
- [語学と私](https://hsjoihs.hatenablog.com/entry/2020/09/24/152311)

#### 世の中
- [部落差別を目撃したことはありますか？](https://qr.ae/pNrQSX)
- [歴史上、もっとも歪んだ科学の実験は何でしょうか？](https://qr.ae/pNBAka)
- [僕は子供欲しくないです。責任と制限と出費が増えるし、子供が障害を持ってたら面倒くさくて愛せる自信ないし、犯罪でも犯そうもんなら記憶から存在を抹消したくなるでしょう。なぜ子供がほしいのか教えて下さい？](https://qr.ae/pNBAMW)
- [今までの人生の中で、もっとも心の底から満足を味わった経験を教えて下さいますか？あなたの心に深い満足と平穏をもたらしたような経験を聞かせて下さい。](https://qr.ae/pNrHHl)
- [自己評価が高すぎる人は信用できないですか？](https://qr.ae/pNBAuF)
- [私はうつ病です。昔の事も思い出せず、感動せず、感情もわからず、物を覚えられず、体を動かすのもつらく、毎日ただひたすら苦しく、生きているだけでお金がかかるのに生きてる意味ってありますか？ ](https://qr.ae/pNBAuj)
- [仕事でとんでもない失敗をしたけど、誰かに怒られただけで、助かった経験はありますか？](https://qr.ae/pNBA5q)

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">羅生門かよ <a href="https://t.co/uqzVjYRwHP">pic.twitter.com/uqzVjYRwHP</a></p>&mdash; 黒猫ドラネコ (@kurodoraneko15) <a href="https://twitter.com/kurodoraneko15/status/1357354598458028033?ref_src=twsrc%5Etfw">February 4, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">秘密基地をYouTube用の動画撮影で時間貸ししたら、土足で使われて鏡を割られた。白い床なのに。当初は土足利用はしてないと嘘つかれたし。その直後に土足で使ってる様子がインスタのストーリーに上がっててショックである。 <a href="https://t.co/3DO1KfPLLk">pic.twitter.com/3DO1KfPLLk</a></p>&mdash; ぴんぽいんとさん (@pinpoint_m) <a href="https://twitter.com/pinpoint_m/status/1359028908562108420?ref_src=twsrc%5Etfw">February 9, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">これ不謹慎だけどマジで好き <a href="https://t.co/j6fFzdt0wg">pic.twitter.com/j6fFzdt0wg</a></p>&mdash; ばーじぇす (@Bajes_final) <a href="https://twitter.com/Bajes_final/status/1358913466321444865?ref_src=twsrc%5Etfw">February 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">腹が立つので聞いて下さい。数年前の結婚式の二次会で旦那が見当たらないな～と思ったら途中で帰る旦那の女友達のA子と付き添いのB子と駅まで送りに行ったと。え、新婦置き去りでどういうことって不安に思ってたら50分くらい経って旦那とB子がA子を送り終えて帰って来て。何2人で仲良く帰ってきてんの</p>&mdash; ちょこもち (@chocomochi_22) <a href="https://twitter.com/chocomochi_22/status/1359517733570764801?ref_src=twsrc%5Etfw">February 10, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">ぼくは低学歴だけでなく、あらゆる物事に対して無知のまま平気で過ごしてきた。例えば、ヒジキは黒いシラスではないという事実を、普通の人なら遅くても20代のうちには学ぶところを、自分は40過ぎるまで「小さいながらもひとつひとつの命…」と生命に感謝しながらヒジキ食ってた。ひっかけ問題だよな。</p>&mdash; トムトム (@tomzoooo) <a href="https://twitter.com/tomzoooo/status/1359932217334530048?ref_src=twsrc%5Etfw">February 11, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>