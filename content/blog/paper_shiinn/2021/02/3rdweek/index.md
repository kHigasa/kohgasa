---
title: 論文試飲メモ(2021年02月第3週)
date: "2021-02-21T04:00:00.000Z"
description: "2021年2月第3週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **Gapless vortex bound states in superconducting topological semimetals** [arXiv:2102.06494 [cond-mat.supr-con]](https://arxiv.org/abs/2102.06494)

トポロジカル半金属(TSMs)においてギャップレスな渦束縛状態が生じる. これがTSMsの普遍的性質であるという主張. 

この状態の形成には$\pi$位相超伝導体-常伝導体-超伝導体(SNS)接合における鏡面Andreev反射と伝播Andreevモードが深く関連している. 

また, ギャップレス状態がトポロジカルに守られ, トポロジカルポンプ過程に由来するということを示している. 

![2102.06494](/kohgasa/2102.06494.jpg)

- **Proximity-induced spin-orbit coupling and ferromagnetism in magic-angle twisted bilayer graphene** [arXiv:2102.06566 [cond-mat.mes-hall]](https://arxiv.org/abs/2102.06566)

- マジック角だけねじれた2層グラフェン(tBLG)とタングステンジセレニド($\text{WSe}_2$)間の原子界面で, 2次元電子系の電子相関とスピン-軌道結合(SOC)を組み合わせた相互作用の調査
- アイソスピンのオーダーと超伝導体の安定性に対するSOCの影響の調査

前者の調査からはモアレフラットバンド内の強電子相関がquarter-filled状態とhalf-filled状態の相関絶縁状態を安定化させ, 一方SOCがこれらのMott的絶縁体を強磁性体へ変換することが異常ホール効果の履歴スイッチ特性よりわかる. また後者における発見はtBLGとそれに関連する系におけるモアレバンドのトポロジカル特性を作る効率の良い実験的なとっかかりとなる. 

![2102.06566](/kohgasa/2102.06566.jpg)

- **Witworld: A generalised probabilistic theory featuring post-quantum steering** [arXiv:2102.06581 [quant-ph]](https://arxiv.org/abs/2102.06581)

Boxworldを包摂するWitworldの提案. 

後で全文通す. 

![2102.06581](/kohgasa/2102.06581.jpg)

- **Stable Bloch oscillations and Landau-Zener tunneling in a non-Hermitian $\mathcal{PT}$-symmetric flat band lattice** [arXiv:2102.06608 [quant-ph]](https://arxiv.org/abs/2102.06608)

外場の影響の下, 3層構造の非エルミート$\mathcal{PT}$-対称平坦バンド格子におけるLandau-Zenerトンネリングを持つ安定Bloch振動の調査. 

![2102.06608](/kohgasa/2102.06608.jpg)

- **Neutron scattering studies on spin fluctuations in $\text{Sr}_2\text{RuO}_4$** [arXiv:2102.06636 [cond-mat.supr-con]](https://arxiv.org/abs/2102.06636)

磁気的揺らぎを含む超伝導対形成機構や強相関電子系における磁気的励起の理解に関連した幾つかの側面に注目して, $\text{Sr}_2\text{RuO}_4$における磁気的揺らぎに対する中性子散乱実験を行っている. 

また, 準強磁性応答の特性について議論されている. 

![2102.06636](/kohgasa/2102.06636.jpg)

- **Can single photon excitation of two spatially separated modes lead to a violation of Bell inequality via homodyne measurements?** [arXiv:2102.06689 [quant-ph]](https://arxiv.org/abs/2102.06689)

  1光子の非古典性を示すために考えられた典型的なホモダイン測定の2つの実験
  - Tan, Walls and Collett(TWC)による
  - Hardyによる
```
を再考し, TWCのものとは異なり, Hardyのものが完璧な手法であった要因を与えている. 

また, TWCのものがBellの非古典性を示す枠組みとしては失敗したものの, エンタングルメントの指針を与えることを示し, 光学系に対するBell不等式をエンタングルメントの指針とする理論を再考してBell演算子に基づくものより効率的なエンタングルメント測定器を見出している. 

- **Heat current and entropy production rate in local non-Markovian quantum dynamics of global Markovian evolution** [arXiv:2102.06694 [quant-ph]](https://arxiv.org/abs/2102.06694)

量子2準位系(TLSs)の組から成る系(これはMarkov過程によって作られない)における熱流とエントロピー生成率の欠損を調査. これらの欠損は, 全2準位系の(大域的な)それと各2準位系の和の(局所的な)それの差によって定義される. 

総熱流欠損がある状況において, 時間発展した状態における2つの系の間のエンタングルメントの補足的な関係を持ち得ることを発見. 

#### 読み物
- [セミナーの準備のしかたについて](https://www.ms.u-tokyo.ac.jp/~yasuyuki/sem.htm)
- [Book Guide](http://member.ipmu.jp/masahito.yamazaki/books-index.shtml)
- [本の読み方・自主ゼミの仕方・本の紹介](https://a-ki-room.hatenadiary.org/entry/20050601/1229200207)

#### ヴィデオ
- *The Queen's Gambit*
- *M-1グランプリ2002*

#### 世の中
- [「自分の体を売ることはなぜいけないのか」と問われたら、なんと答えますか？](https://qr.ae/pNEego)
- [駅で見知らぬ人から「〇〇まで行くお金がない。恵んで下さい」と言われました。驚いたので無視してその場を去りました。どのような対応をとるべきだったと思いますか？](https://qr.ae/pNEegp)