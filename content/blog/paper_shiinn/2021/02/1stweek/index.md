---
title: 論文試飲メモ(2021年02月第1週)
date: "2021-02-07T09:00:00.000Z"
description: "2021年2月第1週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

9稿分. 基本的には主張と手法をざっくり. 

- **Analytical solutions of the Bogoliubov–de Gennes equations for excitations of a trapped Bose-Einstein-condensed gas** DOI: [10.1103/PhysRevA.69.063608](https://doi.org/10.1103/PhysRevA.69.063608)

球対称, 円柱対称な捕獲ポテンシャルを持つBose-Einstein凝縮体(BECs)における低エネルギーの集団励起を調査し, Thomas-Fermi(TF)極限を超えた励起に対するBogoliubov-de Gennes方程式(BdGEs)を解くための方法が提案されている. 

手法としては, ゼロ点圧力からの寄与を考慮し, Fetterライクな変分基底状態函数を持つ凝縮体の基底状態に変分原理を適用することによって, 凝縮体の境界に現れるBogoliubov振幅の特異性による行列要素の発散を取り除き, 厳密ではないけれどもBdGEsを正確に解いたというものである. 空間の全ての範囲で正しい固有値とそれに対応する固有函数も得られ, BdGEsのゼロエネルギーモードの解も与えられている. 


- **Efficient Numerical Self-Consistent Mean-Field Approach for Fermionic Many-Body Systems by Polynomial Expansion on Spectral Density** DOI: [10.1143/JPSJ.81.024710](https://doi.org/10.1143/JPSJ.81.024710)

様々な非斉次の超伝導系に対するBdG方程式を自己無撞着に計算するための速くて扱いやすい方法を開発することを目的としている. そのためにより包括的な方法で多項式展開の枠組みを再定式化し, 自己無撞着な計算を実行したものである. 
1. 主張1: 多項式展開がGreen函数自体ではなくGreen函数のスペクトル密度に適用される

    スペクトル密度に含まれるDiracの$\delta$函数を展開することによって, 完全な平均場計算の枠組みを得る. *スペクトル密度多項式展開*を用いて, 平均場を持つ実直な数値計算を行うことができる. 

1. 主張2: 平均場自体が局所状態密度よりも速く収束する

    その効率を示すためにs波超伝導体に注目する. 手法は磁場におけるd波超伝導体とスピン-軌道相互作用を持つ多軌道超伝導体を含むより一般の場合に対して効果的であることを述べている. 

![jpsj.81.024710](/kohgasa/jpsj.81.024710.jpg)

- **Fluctuating Nature of Light-Induced d-Wave Superconductivity** [arXiv:2101.03495 [cond-mat.supr-con]](https://arxiv.org/abs/2101.03495)

d波超伝導体対相関がパルスレーザーによって強められることを発見した一方で, 対相関の長さがポンプパルスによって非常に抑制されることも発見. これよりlight-induced超伝導体が揺らぎの性質を持つかもしれないということを示唆している. 

Hubbard-Holsteinモデルにおけるphoto-inducedな動力学と超伝導体を, 発展した非Gaussian厳密対角化を用いて研究している. この方法は平衡状態における電子間と電子-フォノン間相互作用を効率的に補足することが示されている. この方法を非平衡な動力学へ拡張し. 得られた結果がlight-inducedなCooper対が揺らぎの性質を持つかもしれないということを示唆している. 

![2101.03495](/kohgasa/2101.03495.jpg)

- **Cross-dimensional universality classes in static and periodically driven Kitaev models** [arXiv:2102.00009 [cond-mat.str-el]](https://arxiv.org/abs/2102.00009)

Majoranaモード(MMs)を保持している静的, 周期的に駆動された2次元Kitaevモデル両者におけるトポロジカル相転移(TPTs)に注目して研究している. 次の2つの尺度
1. 最近提示されたストロボスコピックWannier状態相関函数のMajorana版(この函数はTPTで発散する相関長を記述している)
1. 運動量空間におけるBloch状態間の距離を測る運動量に依存した情報計量

を使って, universalityの見地からこれらの転移を分類. この分類によって, 次元交差したuniversality classの存在が明らかとなる. 

静的なモデルにおいて, 2次元蜂の巣格子で定義されるにも関わらず, 転移は1次元Diracモデルのuniversality classに属する. 周期的な駆動があるときに, 時間反転対称性(TRS)が操作され, それが不動なMMsとカイラルなMMs間の切り替えをするのに役立つ. 系の対称性を操作できることで, 周期的に駆動される状況は異なる次元を持つ2つのuniversality classに関する一層豊かな臨界現象を示す. 例えば, あるものはプロトタイプな2次元線形Diracモデルを表したり, 時間反転と鏡映対称性が現れることによる1次元の節点のループを持つ半金属のタイプに対応していたりする. 

![2102.00009](/kohgasa/2102.00009.jpg)

- **Dynamic electron correlations with charge order wavelength along all directions in copper oxide plane** [arXiv:2102.00016 [cond-mat.supr-con]](https://arxiv.org/abs/2102.00016)

クプラートにおける空間的, 力学的な電子相関の振る舞いをより深く理解するために, $\text{Bi}_2\text{Sr}_2\text{Ca}\text{Cu}_2\text{O}_{8+\delta}$(Bi2212)において温度とドーピングの値を変えて, 一連の共鳴非弾性X線散乱をの実験を行っている. これまでのクプラートに対するX線実験の多くは, 高い対称性を持つ方向($q_x$, $q_y$そして$q_x=q_y$)のみに注目し, エネルギー損失分解能用いず行われてきたが, それらとは異なり, 非弾性散乱のある$q_x$-$q_y$平面全体をマップしている. 

主要な結果は$q_x$-$q_y$平面における特徴的な動的 "円環like"な散乱パターンの同定である. 特に, この円環は静的な電荷秩序(CO)の周期に一致する波数ベクトルの半径を持っていることが著しい. 動的散乱パターンの形が短距離, 長距離Coulomb相互作用を共に考慮に入れた実効Coulombポテンシャル$V(\bm{q})$の単純な形式を反映しているのではないかということを提起している. 

![2102.00016](/kohgasa/2102.00016.jpg)

- **Multiband charge density wave exposed in a transition metal dichalcogenide** [arXiv:2102.00025 [cond-mat.str-er]](https://arxiv.org/abs/2102.00025)

$\text{2H-NbSe}_2$における多バンド電荷密度波(CDW)に対する証拠の発見. 

このCDWはK点付近の内部バンドでのギャップの開口を結果として伴うのみならず, アウターバンドのそれをも伴って引き起こす. このことがこれら2つのバンドにある電子による空間的に同調的でない電荷変調をもたらす. 手法としては走査型トンネル顕微鏡(STM)の画像におけるCDWコントラストの特徴的なエネルギー依存性を観察してこれを見出している. 

![2102.00025](/kohgasa/2102.00025.jpg)

- **Second quantanization of time and energy in Relativistic Quantum Mechanics** [arXiv:2102.01042 [quant-ph]](https://arxiv.org/abs/2102.01042)

相対論的量子力学(RQM)に自己共軛な時間演算子とそれによって与えられる新たな基底を導入する. これにより, 冷却原子系やBose-Einstein凝縮系の領域におけるFeshbach共鳴と量子重力理論における時間の問題に関係がありそうだということが述べられる. 

ポエムっぽい. 

- **Single-shot discrimination of coherent states beyond the standard quamtum limit** [arXiv:2102.01058 [quant-ph]](https://arxiv.org/abs/2102.01058)

Kennedyレシーバー内で転移端センサー(TESs)という光子数検出器を用いた, バイナリ位相シフトしたコヒーレント状態の区別. 

TESの広範で動的な領域に渡る高効率と1光子の敏感さのために, 平均しておよそ7個の光子までを含む信号に対する通常の量子限界(SQL)を無条件に向上させることができた. レシーバーはSQLに対して最大$7.7\ \mathrm{dB}$の改善を達成した. これはフィードバック測定無しで達成された最高の数値である. さらに, それは光ファイバー通信ネットワークと相性の良い電気通信波長($1550 \mathrm{nm}$)で動作する. 

凄そう. 

![2102.01058](/kohgasa/2102.01058.jpg)

- **Performance and limits of feedback cooling methods for levitated oscillators: a direct comparison** [arXiv:2102.01060 [quant-ph]](https://arxiv.org/abs/2102.01060)

パラメトリックフィードバック結合とPauliトラップに閉じ込められた粒子に対する速度減衰の2つの冷却法を直接比較. 

それぞれの方法で達成できる最も低い温度を考え, 実験的な不正確さの示唆するところについて議論されている. また最終的に冷却された振動子のエネルギー分散が調べられている. 

![2102.01060](/kohgasa/2102.01060.jpg)