---
title: 論文試飲メモ(2021年02月第4週)
date: "2021-02-28T04:00:00.000Z"
description: "2021年2月第4週に目を通した論文をメモっています. その正誤も含め, 玉石混淆です. "
---

7稿分. 基本的には主張と手法をざっくり. 

- **CLUSTERING BY QUANTUM ANNEALING ON THE THREE–LEVEL QUANTUM ELEMENTS QUTRITS** [arXiv:2102.09205 [quant-ph]](https://arxiv.org/abs/2102.09205)

量子ニューラルネットワークを使って, 平面上の点を分類する際にキュビットの代わりにキュートリットを用いることによって効率化できる可能性を指摘している. 

- **Experimental implementation of universal holonomic quantum computation on solid-state spins with optimal control** [arXiv:2102.09227 [quant-ph]](https://arxiv.org/abs/2102.09227)

非断熱的束縛量子計算の拡張版(NHQC+)の枠組みに基づいて, 高い信頼性を持つ単一キュビットと2つのキュビットの幾何学的量子ゲートの実証. 

これらと一緒にダイヤモンドの窒素-空孔(NV)中心におけるスピン状態を非断熱的に操作することによって, ユニバーサルゲートセットが作られている. 

![2102.09227](/kohgasa/2102.09227.jpg)

- **Single photon randomness originating from the symmetry of dipole emission and the unpredictability of spontaneous emission** [arXiv:2102.09357 [quant-ph]](https://arxiv.org/abs/2102.09357)

実験室座標系に対して直交するように並んだ双極子の対称放出性を組み合わせて, 同時放出過程に基づいた量子乱数生成器を実証. 

また, NIST無作為テスト群を利用して相関した光子探知時における無作為性を証明し, 欠損中心に対して無作為性があることを示している. 

![2102.09357](/kohgasa/2102.09357.jpg)

- **From Rényi Entropy Power to Information Scan of Quantum States** [arXiv:2102.09415 [quant-ph]](https://arxiv.org/abs/2102.09415)

Shannonエントロピー(SE)に基づいたエントロピーパワー(EP)の概念をRényiエントロピー(RE)の設定に適切に拡張. 

また, REを用いて, 量子状態に関連した情報確率分散がどのように量子状態トモグラフィに類似する過程において再構築されるのかを示している. 

- **Quantum reading: the experimental set-up** [arXiv:2102.09428 [quant-ph]](https://arxiv.org/abs/2102.09428)

量子読み取りについて光学受容器の特定のクラスの場合における解析. 

2モードスクイーズド真空(TMSV)状態に基づいた送信機が光子カウント受容器と組み合わせて使われるときに量子超越性が達成され得ることを示している. 

- **Ideal pairing of the Stokes and anti-Stokes photons in the Raman process** [arXiv:2102.09450 [quant-ph]](https://arxiv.org/abs/2102.09450)

1つの振動モードと強いコヒーレントポンプビームを持つRaman過程の量子モデルを用いた, Stokesと反Stokesモード間の量子的相関と古典的相関の研究. 

![2102.09450](/kohgasa/2102.09450.jpg)

- **Machine Learning Regression for Operator Dynamics** [arXiv:2102.11868 [quant-ph]](https://arxiv.org/abs/2102.11868)

長い時間で量子多体(QMB)系に作用する演算子に対する期待値を正確に決定する際, 短い時間間隔の枠組み内で計算される行列積状態(MPS)の期待値の回帰に対する道具として多層パーセプトロン(MLP)モデルを用いてより効率的に実行. 

#### 読み物
- [語学学習の主導権を取り戻す](https://phasetr.com/blog/2021/02/24/language-learning-for-science-and-engineering-students/)
- [計算が間違っているのに結果が合っている分数の和 #有害分数](https://tsujimotter.hatenablog.com/entry/freshman-sum)

#### ヴィデオ
- *M-1グランプリ2003*

#### 世の中
- [ゆたぽんというyoutuberがいますが、彼のいうこと (学校いかなくてもいい) も一理あるなと私は考えてます。学校は必要がある人だけいったらいいのではないでしょうか？](https://qr.ae/pNQsu9)
- [人間の愚かな行為でもう二度と見れなくなった風景はどういう写真でしょうか？](https://qr.ae/pNQ5vT)

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">その人にアムウェイやるのは無理あるだろ･･･ <a href="https://t.co/9NT5Xp5ieA">pic.twitter.com/9NT5Xp5ieA</a></p>&mdash; 思考性アライさん(シコいさん) (@HWqYUBfGcTwvGvg) <a href="https://twitter.com/HWqYUBfGcTwvGvg/status/1363667613692268544?ref_src=twsrc%5Etfw">February 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
