---
title: 一人輪講第22回
date: "2021-04-24T02:00:00.000Z"
description: "PDF整理のための一人輪講第22回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 176-200 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 10 他の2状態系
### 10-1 水素分子イオン
> This energy will, however, be split into two energy levels by the possibility of the electron going back and forth—the greater the probability of the transition, the greater the split. [^1]

> We should say that our approximate treatment of the H+2 ion as a two-state system breaks down pretty badly once the protons get as close together as they are at the minimum in the curve of Fig. 10–3, and so, will not give a good value for the actual binding energy. [^1]

> whereas the electron exchange term A gives opposite signs for the two stationary states. [^1]

### 10-2 核力
> More specifically, a “virtual exchange” means that the phenomenon involves a quantum mechanical interference between an exchanged state and a nonexchanged state. [^1]

> We do not “see” the photons inside the electrons before they are emitted or after they are absorbed, and their emission does not change the “nature” of the electron. [^1]

> Since the nuclear forces (disregarding electrical effects) between neutron and proton, between proton and proton, between neutron and neutron are the same, we conclude that the masses of the charged and neutral pions should be the same. Experimentally, the masses are indeed very nearly equal, and the small difference is about what one would expect from electric self-energy corrections [^1]

> From the “particle” point of view the Coulomb interaction between two electrons comes from the exchange of a virtual photon. [^1]

### 10-3 水素分子
### 10-4 ベンゼン分子
### 10-5 染料
> If one shines light on the molecule, there is a very strong absorption at one frequency, and it appears to be brightly colored. That’s why it’s a dye! [^1]

吸収した結果, 余色が見えるということね. 

### 10-6 磁場中のスピン1/2を持つ粒子のHamiltonian
### 10-7 磁場中のスピンしている電子

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)