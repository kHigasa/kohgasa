---
title: 一人輪講第26回
date: "2021-05-22T02:00:00.000Z"
description: "PDF整理のための一人輪講第26回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 276-300 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 14 半導体
### 14-1 半導体中の電子とホール
> At room temperature κT is about 1/40 of an electron volt. At these temperatures there are enough holes and electrons to give a significant conductivity, while at, say, 30K—one-tenth of room temperature—the conductivity is imperceptible. The gap energy of diamond is 6 or 7 eV and diamond is a good insulator at room temperature. [^1]

### 14-2 不純物半導体
> When a donor or an acceptor impurity is added to a semiconductor, we say that the material has been “doped.” [^1]

> In an n-type material the main causes of scattering are the very donor sites that are producing the carriers. Since the conduction electrons have a very slightly different energy at the donor sites, the probability waves are scattered from that point. Even in a perfectly pure crystal, however, there are (at any finite temperature) irregularities in the lattice due to thermal vibrations. From the classical point of view we can say that the atoms aren’t lined up exactly on a regular lattice, but are, at any instant, slightly out of place due to their thermal vibrations. [^1]

> When an electric field is applied to an n-type semiconductor, each negative carrier will be accelerated in this field, picking up velocity until it is scattered from one of the donor sites. This means that the carriers which are ordinarily moving about in a random fashion with their thermal energies will pick up an average drift velocity along the lines of the electric field and give rise to a current through the crystal. The drift velocity is in general rather small compared with the typical thermal velocities so that we can estimate the current by assuming that the average time that the carrier travels between scatterings is a constant. [^1]

> For an n-type material the conductivity is relatively independent of temperature. First, the number of majority carriers Nn is determined primarily by the density of donors in the crystal (so long as the temperature is not so low that too many of the carriers are trapped). Second, the mean time between collisions τn is mainly controlled by the density of impurity atoms, which is, of course, independent of the temperature. [^1]

> For very pure materials, Np and Nn will be nearly equal. They will be smaller than in a doped material, so the conductivity will be less. Also they will vary rapidly with temperature (like e−Egap/2κT, as we have seen), so the conductivity may change extremely fast with temperature. [^1]

### 14-3 ホール効果
> The original discovery of the anomalous sign of the potential difference in the Hall effect was made in a metal rather than a semiconductor. It had been assumed that in metals the conduction was always by electron; however, it was found out that for beryllium the potential difference had the wrong sign. It is now understood that in metals as well as in semiconductors it is possible, in certain circumstances, that the “objects” responsible for the conduction are holes. Although it is ultimately the electrons in the crystal which do the moving, nevertheless, the relationship of the momentum and the energy, and the response to external fields is exactly what one would expect for an electric current carried by positive particles. [^1]

> The Hall coefficient depends just on the density of carriers—provided that carriers of one sign are in a large majority. Measurement of the Hall effect is, therefore, one convenient way of determining experimentally the density of carriers in a semiconductor. [^1]

### 14-4 半導体接合
> Since there is a potential difference from one side of the junction to the other, it looks something like a battery. Perhaps if we connect a wire from the n-type side to the p-type side we will get an electrical current. That would be nice because then the current would flow forever without using up any material and we would have an infinite source of energy in violation of the second law of thermodynamics! There is, however, no current if you connect a wire from the p-side to the n-side. And the reason is easy to see. Suppose we imagine first a wire made out of a piece of undoped material. When we connect this wire to the n-type side, we have a junction. There will be a potential difference across this junction. Let’s say that it is just one-half the potential difference from the p-type material to the n-type material. When we connect our undoped wire to the p-type side of the junction, there is also a potential difference at this junction—again, one-half the potential drop across the p-n junction. At all the junctions the potential differences adjust themselves so that there is no net current flow in the circuit. Whatever kind of wire you use to connect together the two sides of the p-n junction, you are producing two new junctions, and so long as all the junctions are at the same temperature, the potential jumps at the junctions all compensate each other and no current will flow in the circuit. It does turn out, however—if you work out the details—that if some of the junctions are at a different temperature than the other junctions, currents will flow. Some of the junctions will be heated and others will be cooled by this current and thermal energy will be converted into electrical energy. This effect is responsible for the operation of thermocouples which are used for measuring temperatures, and of thermoelectric generators. The same effect is also used to make small refrigerators. [^1]

> If we cannot measure the potential difference between the two sides of an p-n junction, how can we really be sure that the potential gradient shown in Fig. 14–9 really exists? One way is to shine light on the junction. When the light photons are absorbed they can produce an electron-hole pair. In the strong electric field that exists at the junction (equal to the slope of the potential curve of Fig. 14–9) the hole will be driven into the p-type region and the electron will be driven into the n-type region. If the two sides of the junction are now connected to an external circuit, these extra charges will provide a current. The energy of the light will be converted into electrical energy in the junction. The solar cells which generate electrical power for the operation of some of our satellites operate on this principle. [^1]

> In our discussion of the operation of a semiconductor junction we have been assuming that the holes and the electrons act more-or-less independently—except that they somehow get into proper statistical equilibrium. When we were describing the current produced by light shining on the junction, we were assuming that an electron or a hole produced in the junction region would get into the main body of the crystal before being annihilated by a carrier of the opposite polarity. In the immediate vicinity of the junction, where the density of carriers of both signs is approximately equal, the effect of electron-hole annihilation (or as it is often called, “recombination”) is an important effect, and in a detailed analysis of a semiconductor junction must be properly taken into account. We have been assuming that a hole or an electron produced in a junction region has a good chance of getting into the main body of the crystal before recombining. The typical time for an electron or a hole to find an opposite partner and annihilate it is for typical semiconductor materials in the range between 10−3 and 10−7 seconds. This time is, incidentally, much longer than the mean free time τ between collisions with scattering sites in the crystal which we used in the analysis of conductivity. In a typical p-n junction, the time for an electron or hole formed in the junction region to be swept away into the body of the crystal is generally much shorter than the recombination time. Most of the pairs will, therefore, contribute to an external current. [^1]

### 14-5 半導体接合での整流作用
> The current of positive carriers from the n-side, however, remains constant so long as ΔV is not too large. [^1]

これが肝要. 

### 14-6 トランジスター
> The n-type region is made very thin—typically 10−3 cm or less, much narrower than its transverse dimensions. This means that as the holes enter the n-type region they have a very good chance of diffusing across to the other junction before they are annihilated by the electrons in the n-type region. [^1]

> In a typical transistor, all but a fraction of a percent of the hole current which leaves the emitter and enters the base is collected in the collector region, and only the small remainder contributes to the net base current. [^1]

> The transistor is an amplifier; a small current Ib introduced into the base electrode gives a large current—100 or so times higher—at the collector electrode. [^1]

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)