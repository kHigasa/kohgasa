---
title: 一人輪講第25回
date: "2021-05-15T02:00:00.000Z"
description: "PDF整理のための一人輪講第25回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 251-275 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 13 スピン1/2
### 13-1 確率振幅の変換
> If you have a harmonic oscillator which is coupled to another harmonic oscillator, and that one to another, and so on …, and if you start an irregularity in one place, the irregularity will propagate as a wave along the line. The same situation exists if you place an electron at one atom of a long chain of atoms. [^1]

> You must appreciate one thing, however; the amplitude for the electron to be at a place is an amplitude, not a probability. If the electron were simply leaking from one place to another, like water going through a hole, the behavior would be completely different. For example, if we had two tanks of water connected by a tube to permit some leakage from one to the other, then the levels would approach each other exponentially. But for the electron, what happens is amplitude leakage and not just a plain probability leakage. And it’s a characteristic of the imaginary term—the i in the differential equations of quantum mechanics—which changes the exponential solution to an oscillatory solution. What happens then is quite different from the leakage between interconnected tanks. [^1]

うーん. 

### 13-2 確定したエネルギーの状態
> Notice that we have been assuming that the number k that we put in our trial solution, Eq. (13.10), was a real number. We can see now why that must be so if we have an infinite line of atoms. Suppose that k were an imaginary number, say ik′. Then the amplitudes an would go as e−k′xn, which means that the amplitude would get larger and larger as we go toward large negative x’s—or toward large positive x’s if k′ is a negative number. This kind of solution would be O.K. if we were dealing with a line of atoms that ended, but cannot be a physical solution for an infinite chain of atoms. It would give infinite amplitudes—and, therefore, infinite probabilities—which can’t represent a real situation. Later on we will see an example in which an imaginary k does make sense. [^1]

> Let’s look a little more closely at what happens for small k—that is, when the variations of the amplitudes from one xn to the next are quite slow. [^1]

自由電子ライク. 

### 13-3 時間に依存している状態
> It does so by having its amplitudes going pip-pip-pip from one atom to the next, working its way through the crystal. That is how a solid can conduct electricity. [^1]

テクニカルタームは一切使わず全部言ってる. 

### 13-4 3次元格子内の電子
> In a crystal with a lower symmetry than cubic (or even in a cubic crystal in which the state of the electron at each atom is not symmetrical) the three coefficients Ax, Ay, and Az are different. Then the “effective mass” of an electron localized in a small region depends on its direction of motion. It could, for instance, have a different inertia for motion in the x-direction than for motion in the y-direction. (The details of such a situation are sometimes described in terms of an “effective mass tensor.”) [^1]

### 13-5 格子内の他の状態
> As another example, we can think of a line of identical neutral atoms one of which has been put into an excited state—that is, with more than its normal ground state energy. Let Cn be the amplitude that the nth atom has the excitation. It can interact with a neighboring atom by handing over to it the extra energy and returning to the ground state. Call the amplitude for this process iA/ℏ. You can see that it’s the same mathematics all over again. Now the object which moves is called an exciton. It behaves like a neutral “particle” moving through the crystal, carrying the excitation energy. Such motion may be involved in certain biological processes such as vision, or photosynthesis. It has been guessed that the absorption of light in the retina produces an “exciton” which moves through some periodic structure (such as the layers in the rods we described in Chapter 36, Vol. I; see Fig. 36–5) to be accumulated at some special station where the energy is used to induce a chemical reaction. [^1]

### 13-6 格子内の欠陥による散乱
> We want now to consider the case of a single electron in a crystal which is not perfect. Our earlier analysis says that perfect crystals have perfect conductivity—that electrons can go slipping through the crystal, as in a vacuum, without friction. One of the most important things that can stop an electron from going on forever is an imperfection or irregularity in the crystal. [^1]

### 13-7 格子欠陥による捕獲
> There is another interesting situation that can arise if F is a negative number. If the energy of the electron is lower at the impurity atom (at n=0) than it is anywhere else, then the electron can get caught on this atom. That is, if (E0+F) is below the bottom of the band at (E0−2A), then the electron can get “trapped” in a state with E<E0−2A. Such a solution cannot come out of what we have done so far. We can get this solution, however, if we permit the trial solution we took in Eq. (13.10) to have an imaginary number for k. [^1]

### 13-8 散乱振幅と束縛状態

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)