---
title: 一人輪講第21回
date: "2021-04-17T02:00:00.000Z"
description: "PDF整理のための一人輪講第21回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 151-175 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 9 アンモニアメーザー
### 9-1 アンモニア分子の状態
> In order to excite an electron inside an atom, the energies involved are relatively very high—requiring photons in the optical or ultraviolet range. To excite the vibrations of the molecules involves photons in the infrared. If you talk about exciting rotations, the energy differences of the states correspond to photons in the far infrared. [^1]

### 9-2 静電場内のアンモニア分子
### 9-3 時間に依存する場における転移
### 9-4 共鳴での転移
> the molecule enters the cavity, the cavity field—oscillating at exactly the right frequency—induces transitions from the upper to the lower state, and the energy released is fed into the oscillating field. In an operating maser the molecules deliver enough energy to maintain the cavity oscillations—not only providing enough power to make up for the cavity losses but even providing small amounts of excess power that can be drawn from the cavity. Thus, the molecular energy is converted into the energy of an external electromagnetic field. [^1]

### 9-5 非共鳴での転移
### 9-6 光の吸収

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)