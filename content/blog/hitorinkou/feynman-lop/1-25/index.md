---
title: 一人輪講第15回
date: "2021-03-06T02:00:00.000Z"
description: "PDF整理のための一人輪講第15回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 1-25 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 1 量子の振る舞い
### 1-1 原子の力学
実験から入っていく. 

### 1-2 弾丸を用いた実験
### 1-3 波を用いた実験
### 1-4 電子を用いた実験
### 1-5 電子波の干渉
### 1-6 電子の観察
> But wait! What do we have now for the total probability, the probability that an electron will arrive at the detector by any route? We already have that information. We just pretend that we never looked at the light flashes, and we lump together the detector clicks which we have separated into the two columns. We must just add the numbers. For the probability that an electron will arrive at the backstop by passing through either hole, we do find P′12=P′1+P′2. That is, although we succeeded in watching which hole our electrons come through, we no longer get the old interference curve P12, but a new one, P′12, showing no interference! If we turn out the light P12 is restored. [^1]

> We must conclude that when we look at the electrons the distribution of them on the screen is different than when we do not look. Perhaps it is turning on our light source that disturbs things? It must be that the electrons are very delicate, and the light, when it scatters off the electrons, gives them a jolt that changes their motion. We know that the electric field of the light acting on a charge will exert a force on it. So perhaps we should expect the motion to be changed. Anyway, the light exerts a big influence on the electrons. By trying to “watch” the electrons we have changed their motions. That is, the jolt given to the electron when the photon is scattered by it is such as to change the electron’s motion enough so that if it might have gone to where P12 was at a maximum it will instead land where P12 was a minimum; that is why we no longer see the wavy interference effects. [^1]

> You may be thinking: “Don’t use such a bright source! Turn the brightness down! The light waves will then be weaker and will not disturb the electrons so much. Surely, by making the light dimmer and dimmer, eventually the wave will be weak enough that it will have a negligible effect.” O.K. Let’s try it. The first thing we observe is that the flashes of light scattered from the electrons as they pass by does not get weaker. It is always the same-sized flash. The only thing that happens as the light is made dimmer is that sometimes we hear a “click” from the detector but see no flash at all. The electron has gone by without being “seen.” What we are observing is that light also acts like electrons, we knew that it was “wavy,” but now we find that it is also “lumpy.” It always arrives—or is scattered—in lumps that we call “photons.” As we turn down the intensity of the light source we do not change the size of the photons, only the rate at which they are emitted. That explains why, when our source is dim, some electrons get by without being seen. There did not happen to be a photon around at the time the electron went through. [^1]

> This is all a little discouraging. If it is true that whenever we “see” the electron we see the same-sized flash, then those electrons we see are always the disturbed ones. Let us try the experiment with a dim light anyway. Now whenever we hear a click in the detector we will keep a count in three columns: in Column (1) those electrons seen by hole 1, in Column (2) those electrons seen by hole 2, and in Column (3) those electrons not seen at all. When we work up our data (computing the probabilities) we find these results: Those “seen by hole 1” have a distribution like P′1; those “seen by hole 2” have a distribution like P′2 (so that those “seen by either hole 1 or 2” have a distribution like P′12); and those “not seen at all” have a “wavy” distribution just like P12 of Fig. 1–3! If the electrons are not seen, we have interference! [^1]

> That is understandable. When we do not see the electron, no photon disturbs it, and when we do see it, a photon has disturbed it. There is always the same amount of disturbance because the light photons all produce the same-sized effects and the effect of the photons being scattered is enough to smear out any interference effect. [^1]

> Is there not some way we can see the electrons without disturbing them? We learned in an earlier chapter that the momentum carried by a “photon” is inversely proportional to its wavelength (p=h/λ). Certainly the jolt given to the electron when the photon is scattered toward our eye depends on the momentum that photon carries. Aha! If we want to disturb the electrons only slightly we should not have lowered the intensity of the light, we should have lowered its frequency (the same as increasing its wavelength). Let us use light of a redder color. We could even use infrared light, or radiowaves (like radar), and “see” where the electron went with the help of some equipment that can “see” light of these longer wavelengths. If we use “gentler” light perhaps we can avoid disturbing the electrons so much. [^1]

> Let us try the experiment with longer waves. We shall keep repeating our experiment, each time with light of a longer wavelength. At first, nothing seems to change. The results are the same. Then a terrible thing happens. You remember that when we discussed the microscope we pointed out that, due to the wave nature of the light, there is a limitation on how close two spots can be and still be seen as two separate spots. This distance is of the order of the wavelength of light. So now, when we make the wavelength longer than the distance between our holes, we see a big fuzzy flash when the light is scattered by the electrons. We can no longer tell which hole the electron went through! We just know it went somewhere! And it is just with light of this color that we find that the jolts given to the electron are small enough so that P′12 begins to look like P12—that we begin to get some interference effect. And it is only for wavelengths much longer than the separation of the two holes (when we have no chance at all of telling where the electron went) that the disturbance due to the light gets sufficiently small that we again get the curve P12 shown in Fig. 1–3. [^1]

> In our experiment we find that it is impossible to arrange the light in such a way that one can tell which hole the electron went through, and at the same time not disturb the pattern. It was suggested by Heisenberg that the then new laws of nature could only be consistent if there were some basic limitation on our experimental capabilities not previously recognized. He proposed, as a general principle, his uncertainty principle, which we can state in terms of our experiment as follows: “It is impossible to design an apparatus to determine which hole the electron passes through, that will not at the same time disturb the electrons enough to destroy the interference pattern.” If an apparatus is capable of determining which hole the electron goes through, it cannot be so delicate that it does not disturb the pattern in an essential way. No one has ever found (or even thought of) a way around the uncertainty principle. So we must assume that it describes a basic characteristic of nature. [^1]

### 1-7 量子力学の第1原理
> No one has found any machinery behind the law. No one can “explain” any more than we have just “explained.” No one will give you any deeper representation of the situation. We have no ideas about a more basic mechanism from which these results can be deduced. [^1]

> We have implied that in our experimental arrangement (or even in the best possible one) it would be impossible to predict exactly what would happen. We can only predict the odds! This would mean, if it were true, that physics has given up on the problem of trying to predict exactly what will happen in a definite circumstance. Yes! physics has given up. We do not know how to predict what would happen in a given circumstance, and we believe now that it is impossible—that the only thing that can be predicted is the probability of different events. It must be recognized that this is a retrenchment in our earlier ideal of understanding nature. It may be a backward step, but no one has seen a way to avoid it. [^1]

### 1-8 不確定性原理
Heisenbergの不確定性原理が量子力学の要である. 

## 2 波と粒子の観点の関係
### 2-1 確率波の振幅
確率波の振幅を基に位置と運動量の不確定性を述べている. 

### 2-2 位置と運動量の測定
色々な説明の仕方が為されていて良い. 

### 2-3 結晶回折
> Which directions scatter depends on the type of lattice, but how strongly each scatters is determined by what is inside each unit cell, and in that way the structure of crystals is worked out. [^1]

### 2-4 原子の大きさ
### 2-5 エネルギー準位
> Thus an object in which the waves are confined has certain resonance frequencies. It is therefore a property of waves in a confined space—a subject which we will discuss in detail with formulas later on—that they exist only at definite frequencies. And since the general relation exists between frequencies of the amplitude and energy, we are not surprised to find definite energies associated with electrons bound in atoms. [^1]

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)