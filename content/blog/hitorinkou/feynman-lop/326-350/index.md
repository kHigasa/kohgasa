---
title: 一人輪講第28回
date: "2021-06-05T02:00:00.000Z"
description: "PDF整理のための一人輪講第28回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 326-350 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 16 振幅の位置依存性
### 16-1 線上の振幅
### 16-2 波動関数
### 16-3 定まった運動量を持つ状態
>  The Gaussian distribution gives the smallest possible value for the product of the root-mean-square widths. [^1]

### 16-4 $\bm{x}$における状態の規格化
> You see that such a function is not a wave in the sense of an oscillation that moves along in three dimensions. Neither is it generally simply a product of two individual waves, one for each particle. It is, in general, some kind of a wave in the six dimensions defined by x1 and x2. If there are two particles in nature which are interacting, there is no way of describing what happens to one of the particles by trying to write down a wave function for it alone. The famous paradoxes that we considered in earlier chapters—where the measurements made on one particle were claimed to be able to tell what was going to happen to another particle, or were able to destroy an interference—have caused people all sorts of trouble because they have tried to think of the wave function of one particle alone, rather than the correct wave function in the coordinates of both particles. The complete description can be given correctly only in terms of functions of the coordinates of both particles. [^1]

### 16-5 Schr$\text{\"{o}}$dinger方程式
### 16-6 量子化されたエネルギー準位
> You should appreciate that the solution we have drawn in the figure is a very special one. If we were to raise or lower the energy ever so slightly, the function would go over into curves like one or the other of the two broken-line curves shown in Fig. 16–8, and we would not have the proper conditions for a bound particle. We have obtained a result that if a particle is to be bound in a potential well, it can do so only if it has a very definite energy. [^1]

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |


## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)