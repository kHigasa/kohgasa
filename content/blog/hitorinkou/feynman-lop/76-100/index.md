---
title: 一人輪講第18回
date: "2021-03-27T02:00:00.000Z"
description: "PDF整理のための一人輪講第18回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 76-100 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 5 スピン1
### 5-1 Stern-Gerlach装置を用いた原子の分離
### 5-2 濾過された原子を用いた実験
### 5-3 連続して並んだStern-Gerlachフィルター
ここまでStern-Gerlachの実験について長く思索を巡らせているが, 長い割に説明不足なので誤った解釈を生みそう. 

### 5-4 基本状態
ここでやっと感. 

### 5-5 Stern-Gerlach装置を用いた原子の分離
> Humpty Dumpty has been put back together again. [^1]

他の本では語られないような切り口からの説明. 物理的で良い. 

> Redundant truth doesn’t bother us! [^1]

### 5-6 量子力学の機構
### 5-7 異なった基本状態への変換
> Now you see why a spin-one particle is often called a “vector particle.” [^1]

### 5-8 他の状態

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)