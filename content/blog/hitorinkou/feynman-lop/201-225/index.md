---
title: 一人輪講第23回
date: "2021-05-01T02:00:00.000Z"
description: "PDF整理のための一人輪講第23回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 201-225 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 11 さらなる2状態系
### 11-1 Pauliスピン行列
> Since any two-by-two matrix can be represented in terms of the unit matrix and the sigma matrices, we have all that we ever need for any two-state system. [^1]

> Although the sigmas seem to have a geometrical significance in the physical situation of an electron in a magnetic field, they can also be thought of as just useful matrices, which can be used for any two-state problem. [^1]

> It is sometimes said that to each quantity in classical physics there corresponds a matrix in quantum mechanics. It is really more correct to say that the Hamiltonian matrix corresponds to the energy, and any quantity that can be defined via energy has a corresponding matrix. [^1]

> You may try, if you want, to understand how a classical vector is equal to a matrix μσ, and maybe you will discover something—but don’t break your head on it. That’s not the idea—they are not equal. Quantum mechanics is a different kind of a theory to represent the world. It just happens that there are certain correspondences which are hardly more than mnemonic devices—things to remember with. That is, you remember Eq. (11.14) when you learn classical physics; then if you remember the correspondence μ→μσ, you have a handle for remembering Eq. (11.13). Of course, nature knows the quantum mechanics, and the classical mechanics is only an approximation; so there is no mystery in the fact that in classical mechanics there is some shadow of quantum mechanical laws—which are truly the ones underneath. To reconstruct the original object from the shadow is not possible in any direct way, but the shadow does help you to remember what the object looks like. Equation (11.13) is the truth, and Eq. (11.14) is the shadow. Because we learn classical mechanics first, we would like to be able to get the quantum formula from it, but there is no sure-fire scheme for doing that. [^1]

### 11-2 演算子としてのスピン行列
### 11-3 2状態方程式の解
> all systems of two states can be made analogous to a spin one-half object precessing in a magnetic field. [^1]

### 11-4 光子の分極状態
### 11-5 中性K中間子
### 11-6 $N$状態系への一般化
> Then it will not necessarily be so that they are orthogonal—if you are unlucky [^1]

運が良ければ直交している, の方が良さそう. 

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)