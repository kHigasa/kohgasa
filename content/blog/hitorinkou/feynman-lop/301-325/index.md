---
title: 一人輪講第27回
date: "2021-05-29T02:00:00.000Z"
description: "PDF整理のための一人輪講第27回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 301-325 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 15 独立粒子近似
### 15-1 スピン波
### 15-2 2つのスピン波
> (One of the complexities of quantum mechanics is just the bookkeeping. With more and more down spins, the notation becomes more and more elaborate with lots of indices and the equations always look very horrifying; but the ideas are not necessarily more complicated than in the simplest case.) [^1]

> After all, the crystal is supposed to be infinite, and we have an infinite number of terms; neglecting a few might not matter much. So for a first rough approximation let’s forget about the altered equations. [^1]

### 15-3 独立粒子
### 15-4 ベンゼン分子
### 15-5 もっと有機化学
### 15-6 近似を利用した他の例

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |


## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)