---
title: 一人輪講第19回
date: "2021-04-03T02:00:00.000Z"
description: "PDF整理のための一人輪講第19回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 101-125 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 6 スピン1/2
### 6-1 確率振幅の変換
> The excursion is “cultural” in the sense that it is intended to show that the principles of quantum mechanics are not only interesting, but are so deep that by adding only a few extra hypotheses about the structure of space, we can deduce a great many properties of physical systems. [^1]

### 6-2 回転された座標系への変換
### 6-3 $z$軸まわりの回転
### 6-4 $y$軸まわりの$180^{\circ}$及び$90^{\circ}$回転
### 6-5 $x$軸まわりの回転
> You may be thinking: “This is getting ridiculous. What are they going to do next, 47 around y, then 33 about x, and so on, forever?” [^1]

> (These facts of the combinations of rotations, and what they produce, are hard to grasp intuitively. It is rather strange, because we live in three dimensions, but it is hard for us to appreciate what happens if we turn this way and then that way. Perhaps, if we were fish or birds and had a real appreciation of what happens when we turn somersaults in space, we could more easily appreciate such things.) [^1]

### 6-6 任意の回転
Euler角. 

## 7 確率振幅の時間依存性
### 7-1 静止している原子; 定常状態
### 7-2 一様な運動
> Our result, then, is that if we have several amplitudes for pure energy states of nearly the same energy, their interference gives “lumps” in the probability that move through space with a velocity equal to the velocity of a classical particle of that energy. We should remark, however, that when we say we can add two amplitudes of different wave number together to get a beat-note that will correspond to a moving particle, we have introduced something new—something that we cannot deduce from the theory of relativity. We said what the amplitude did for a particle standing still and then deduced what it would do if the particle were moving. But we cannot deduce from these arguments what would happen when there are two waves moving with different speeds. If we stop one, we cannot stop the other. So we have added tacitly the extra hypothesis that not only is (7.9) a possible solution, but that there can also be solutions with all kinds of p’s for the same system, and that the different terms will interfere. [^1]

確かに波の干渉は別物の仮定ですね. 

### 7-3 ポテンシャルエネルギー; エネルギー保存
> In other words, the classical statement of the conservation of energy is equivalent to the quantum mechanical statement that the frequencies for a particle are everywhere the same if the conditions are not changing with time. It all fits with the idea that ℏω=E. [^1]

### 7-4 力; 古典極限
普段だとEhrenfestの定理で主張されることだが, ここではポテンシャルの変化する領域を通過する粒子の運動の幾何的解析から, 古典極限における量子力学とNewton力学の一致が主張されている. 

### 7-5 スピン1/2の粒子の "歳差運動"

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)