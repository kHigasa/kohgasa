---
title: 一人輪講第24回
date: "2021-05-08T02:00:00.000Z"
description: "PDF整理のための一人輪講第24回. Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III, 226-250 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 12 水素原子における超微細分裂
### 12-1 スピン1/2粒子の2粒子系に対する基本状態
> These spins are responsible for the “hyperfine structure” in the energy levels, which splits all the energy levels into several nearly equal levels. [^1]

> For our study of the ground state of hydrogen we don’t need to use the full sets of base states for the various momenta. We are specifying particular momentum states for the proton and electron when we say “the ground state.” The details of the configuration—the amplitudes for all the momentum base states—can be calculated, but that is another problem. Now we are concerned only with the effects of the spin, so we can take only the four base states of (12.1). Our next problem is: What is the Hamiltonian for this set of states? [^1]

### 12-2 水素原子の基底状態に対するHamiltonian
> There is no general rule for writing down the Hamiltonian of an atomic system, and finding the right formula is much more of an art than finding a set of base states. We were able to tell you a general rule for writing a set of base states for any problem of a proton and an electron, but to describe the general Hamiltonian of such a combination is too hard at this level. Instead, we will lead you to a Hamiltonian by some heuristic argument—and you will have to accept it as the correct one because the results will agree with the test of experimental observation. [^1]

### 12-3 エネルギー準位
### 12-4 Zeeman分裂
最高. 

### 12-5 磁場中の状態
### 12-6 スピン1に対する射影行列

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/feynman-lop/feynman-lop.pdf" data-iframely-url="//cdn.iframe.ly/api/iframe?url=https%3A%2F%2Fgithub.com%2FkHigasa%2Fhitorinkou%2Fblob%2Fmain%2Ffeynman-lop%2Ffeynman-lop.pdf&amp;key=cbeba87c9a4e3440d98d4b54d447f439"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[Richard P. Feynman, Robert B. Leighton, Matthew Sands, 1974, The Feynman Lectures on Physics Vol. III](https://www.feynmanlectures.caltech.edu/III_toc.html)