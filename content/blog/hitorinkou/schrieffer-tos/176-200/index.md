---
title: 一人輪講第12回
date: "2021-02-14T02:00:00.000Z"
description: "PDF整理のための一人輪講第12回. J.R.Schrieffer, 1964, Theory of Superconductivity, 176-200 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter7 超伝導体に応用される場の量子論的方法
### 7-2 Nambu-Gor'kov 形式
Green函数に対する級数展開は常伝導状態の不安定性を解決することに対しては適切でなく, Nambuによって修正された枠組みが適切である. 

おそらくNambu形式を理解するための最も単純な方法は, 2体の遅延のないポテンシャル$V$を通して相互作用している電子系を考えることである. このとき系に対するHamiltonianは
$$
\begin{aligned}
    H &= \sum_{k,s}\epsilon_kn_{ks}+\frac{1}{2}\sum_{k,k',q,s,s'}<\bm{k}+\bm{q},\bm{k}'-\bm{q}|V|\bm{k},\bm{k}'>c_{k+q,s}^{\dagger}c_{k'-q,s'}^{\dagger}c_{k's'}c_{ks} \\
    &\equiv H_0+H_{\text{int}}
\end{aligned}
$$
である. Hartree-Fock近似において, 2体演算子$H_{\text{int}}$は1体演算子によって置き換えられるので, 与えられた状態$|0>$に関して, 相互作用項を線形化できる. 線形化の際に, $c_1^{\dagger}c_2^{\dagger}c_3c_4$を
$$
<0|c_1^{\dagger}c_4|0>c_2^{\dagger}c_3-<0|c_1^{\dagger}c_3|0>c_2^{\dagger}c_4+<0|c_2^{\dagger}c_3|0>c_1^{\dagger}c_4-<0|c_2^{\dagger}c_4|0>c_1^{\dagger}c_3
$$
によって置き換える. そうすると状態$|0>$は線形化されたHamiltonianの固有状態という観点から自己無撞着に決定される. しかし我々の目的に対して, この近似を定式化するのに同等のより便利な方法が, 修正された0次のHamiltonian
$$
H_0' = H_0+H_{\chi}-\mu N\text{ , }H_{\chi} = \sum_{k,s}\chi_kn_{ks}
$$
を導入することである. ここで$H_{\chi}$は簡単のために並進不変性とスピンに独立であることを仮定したHartree-Fockポテンシャルであることがあとになってわかる. 因子
$$
\mu N = \mu\sum_{k,s}n_{ks}
$$
はエネルギーの基準をシフトするために導入され, $\mu$は化学ポテンシャルである. そうすると修正された相互作用Hamiltonianは
$$
H_{\text{int}}' = H_{\text{int}}-H_{\chi}
$$
であり, 
$$
H' = H_0'+H_{\text{int}}' = H-\mu N
$$
となる. それ故, $H$と$H'$のエネルギースペクトルは$\mu N$によるシフトを除いて同一である. 

この処方を実行するために, $H_0'$に応じた1粒子Green函数$G_0(\bm{p},p_0)$の極が$H_0'$の素励起エネルギー$\overline{\epsilon}_p$を与えるということに注意する. 議論を絶対零度に制限すると, 
$$
G_{0s}(\bm{p},t) = -\mathrm{i}<0|T\left\lbrace c_{ps}(t)c_{ps}^{\dagger}(0)\right\rbrace |0>
$$
を得る. ここで$|0>$は$H_0'$の$N_0$粒子系の基底状態であり, 
$$
c_{ps}(t) = \mathrm{e}^{\mathrm{i}H_0't}c_{ps}(0)\mathrm{e}^{-\mathrm{i}H_0't} = c_{ps}(0)\mathrm{e}^{-\mathrm{i}(\epsilon_p+\chi_p-\mu)t}
$$
である. これより, 
$$
G(\bm{p},p_0) = \frac{\mathrm{e}^{\mathrm{i}\delta p_0}}{p_0-(\epsilon_p+\chi_p-\mu)+\mathrm{i}\delta p_0}
$$
が見出される. ここで$\delta=0^+$である. 因子$\mathrm{e}^{\mathrm{i}\delta p_0}$は$G_{0s}$の$p_0$-積分が平均占有数に対応することを保証するものである, 
$$
-\mathrm{i}G_{0s}(\bm{p},t=0) = -\frac{\mathrm{i}}{2\pi}\int G_{0s}(\bm{p},p_0)\mathrm{d}p_0 = <0|n_{ps}|0>.
$$
これらより, 上半平面で$p_0$-積分を閉じて, 
$$
<0|n_{ks}|0> = \begin{cases}
    1 &\epsilon_p+\chi_p<\mu \\
    0 &\epsilon_p+\chi_p>\mu
\end{cases}
$$
を得る. また電子の総数は$N_0$でなければならないので
$$
<0|\sum_{p,s}n_{ps}|0> = \frac{2}{(2\pi)^3}\int\mathrm{d}^3p\Bigg|_{\epsilon_p+\chi_p<\mu} = N_0
$$
という制限を得, これは与えられたHartree-Fockポテンシャルに対する化学ポテンシャル$\mu$を決定する. $G_{0s}(\bm{p},p_0)$の極は$p_0=\epsilon_p+\chi_p-\mu\equiv \overline{\epsilon}_p$にある. 

励起スペクトルに対する$H_{\text{int}}'$の効果を観るために, これらの残留相互作用に対する自己エネルギー$\Sigma(\bm{p},p_0)$を必要とする. 標準的なFeynman-Dysonルールを使うことによって, $H_{\text{int}}'$の1次として
$$
\begin{aligned}
    \Sigma(\bm{p},p_0) = &-\frac{2\mathrm{i}}{(2\pi)^4}\int <\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>G_0(\bm{p}',p_0')\mathrm{d}^4p' \\
    &+\frac{\mathrm{i}}{(2\pi)^4}\int <\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>G_0(\bm{p}',p_0')\mathrm{d}^4p'-\chi_p \\
    &= \frac{1}{(2\pi)^3}\int_{\overline{\epsilon}_p<0}\left[\left\lbrace 2<\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>-<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\right\rbrace-\chi_p\right]\mathrm{d}^3p'
\end{aligned}
$$
を得る. それぞれ直接, 交換相互作用, Hartree-Fockポテンシャルに対応している. 最後の変形には2体ポテンシャルの行列要素は$p_0'$に独立であることを用いた. もし$G_0(\bm{p},p_0)$の極が$\Sigma$によって影響されないとすると, 自己無撞着条件
$$
\Sigma(\bm{p},\overline{\epsilon}_p) = 0
$$
を満たすことを必要とするだろう. 我々の問題では特に, $\Sigma$は$p_0$に独立であるので, よく知られた$\chi_p$を決定するHartree-Fockの関係
$$
\chi_p = \frac{1}{(2\pi)^3}\int_{\overline{\epsilon}_p<0}\left\lbrace 2<\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>-<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\right\rbrace\mathrm{d}^3p'
$$
を得る. 

Hamiltonianの基底状態に全粒子数演算子$N_{\text{op}}$の固有函数が最も適した状態である必要はないので, 対相関を含めた枠組みでのHartree的な項は恒等的にゼロにはならないことが注意されている. 

一般化されたHartree-Fock近似を観る方法は, 修正された0次のHamiltonian
$$
H_0' = H_0+(H_{\chi}+H_{\phi})-\mu N
$$
を考えることである. ここで付加された項$H_{\phi}$は
$$
H_{\phi} = \sum_k\left\lbrace\phi_k^*c_{k\uparrow}^{\dagger}c_{-k\downarrow}^{\dagger}+\text{H.c.}\right\rbrace
$$
という形を持ち, $k\uparrow$と$-k\downarrow$の間の対相関を記述することを意図している. $H_{\phi}$があるために, $H_0'$はもはやこれまでの意味における1粒子演算子ではない. このことは修正された相互作用Hamiltonian
$$
H_{\text{int}}' = H_{\text{int}}-(H_{\chi}+H_{\phi})
$$
の摂動の扱いを複雑にする. 

Nambuはこの形式的複雑さを克服する整った方法を発見し, もし2成分(スピノル)場の演算子を
$$
\Psi_k = \begin{pmatrix}
   c_{k\uparrow} \\
   c_{-k\downarrow}^{\dagger}
\end{pmatrix}\ \Psi_{k1}=c_{k\uparrow}\ \Psi_{k2}=c_{-k\downarrow}^{\dagger}
$$
とそのHermite共軛を
$$
\Psi_k^{\dagger} = \left(c_{k\uparrow}^{\dagger}, c_{-k\downarrow}\right)\ \left\lbrace\Psi_k,\Psi_{k'}^{\dagger}\right\rbrace =\delta_{kk'}\bm{1},\ \left\lbrace\Psi_k,\Psi_{k'}\right\rbrace =0
$$
と導入するならば, $H_0'$は形式的に$\Psi_k$ばにおける1体演算子として形式的に記述されると指摘した. これをみるために, 4つのPauli行列
$$
\tau_1 = \begin{pmatrix}
   0 & 1 \\
   1 & 0
\end{pmatrix}\ 
\tau_2 = \begin{pmatrix}
   0 & -\mathrm{i} \\
   \mathrm{i} & 0
\end{pmatrix}\ 
\tau_3 = \begin{pmatrix}
   1 & 0 \\
   0 & -1
\end{pmatrix}\ 
\bm{1} = \begin{pmatrix}
   1 & 0 \\
   0 & 1
\end{pmatrix}
$$
を導入すると, 
$$
\begin{aligned}
    \Psi_k^{\dagger}\tau_1\Psi_k &= c_{k\uparrow}^{\dagger}c_{-k\downarrow}^{\dagger}+c_{-k\downarrow}c_{k\uparrow} \\
    \Psi_k^{\dagger}\tau_2\Psi_k &= -\mathrm{i}\left(c_{k\uparrow}^{\dagger}c_{-k\downarrow}^{\dagger}-c_{-k\downarrow}c_{k\uparrow}\right) \\
    \Psi_k^{\dagger}\tau_3\Psi_k &= n_{k\uparrow}+n_{-k\downarrow}-1 \\
    \Psi_k^{\dagger}\bm{1}\Psi_k &= n_{k\uparrow}-n_{-k\downarrow}+1
\end{aligned}
$$
を得る. これらの関係を用いて, $H_0'$を
$$
H_0' = \sum_k\Psi_k^{\dagger}\left(\overline{\epsilon}_k\tau_3+\phi_{k1}\tau_1+\phi_{k2}\tau_2\right)\Psi_k+\sum_k\overline{\epsilon}_k
$$
と書き改めることができる. ここで$\phi_{k1}$と$\phi_{k2}$はそれぞれ$\phi_k$の実部と虚部である. また, もし$V$の行列要素が対称性の要求
$$
\begin{aligned}
    <\bm{k}_1,\bm{k}_2|V|\bm{k}_3,\bm{k}_4> &= <-\bm{k}_3,-\bm{k}_4|V|-\bm{k}_1,-\bm{k}_2> \\
    &= <-\bm{k}_3,\bm{k}_2|V|-\bm{k}_1,\bm{k}_4> = <\bm{k}_1,-\bm{k}_4|V|\bm{k}_3,-\bm{k}_2> \\
\end{aligned}
$$
を満たすならば, $H_{\text{int}}'$は
$$
\begin{aligned}
    H_{\text{int}}' = &\frac{1}{2}\sum_{k,k',q}<\bm{k}+\bm{q},\bm{k}'-\bm{q}|V|\bm{k},\bm{k}'>\left(\Psi_{k+q}^{\dagger}\tau_3\Psi_k\right)\left(\Psi_{k'-q}^{\dagger}\tau_3\Psi_{k'}\right) \\
    &-\sum_k\Psi_k^{\dagger}\left(\chi_k\tau_3+\phi_{k1}\tau_1+\phi_{k2}\tau_2\right)\Psi_k
\end{aligned}
$$
の形を取る. これらの対称性の条件は電子-フォノンとCoulomb相互作用から生じるポテンシャルに対して満たされる. 化学ポテンシャル$\mu$とHartree場$\chi_k$と$\phi_k$を決定するために, 上述のHartree-Fockの扱いを模倣する. 1粒子Green函数行列は
$$
\bm{G}_{0\alpha\beta}(\bm{p},t) = -\mathrm{i}\left<0\left|T\left\lbrace\Psi_{p\alpha}(t)\Psi_{p\beta}^{\dagger}(0)\right\rbrace\right|0\right>
$$
として定義される. ここで$|0>$は平均電子数$N_0$に対する$H_0'$の基底状態であり, 
$$
\Psi_p(t) = \mathrm{e}^{\mathrm{i}H_0't}\Psi_p(0)\mathrm{e}^{-\mathrm{i}H_0't}
$$
である. 量
$$
\bm{G}_{011}(\bm{p},t) = -\mathrm{i}\left<0\left|T\left\lbrace c_{p\uparrow}(t)c_{p\uparrow}^{\dagger}(0)\right\rbrace\right|0\right>
$$
はアップスピンの電子に対するGreen函数行列で, 一方
$$
\bm{G}_{022}(\bm{p},t) = -\mathrm{i}\left<0\left|T\left\lbrace c_{-p\downarrow}^{\dagger}(t)c_{p\downarrow}(0)\right\rbrace\right|0\right>
$$
はダウンスピンの正孔に対するGreen函数行列である. 非対角要素
$$
\begin{aligned}
    \bm{G}_{012}(\bm{p},t) &= -\mathrm{i}\left<0\left|T\left\lbrace c_{-p\downarrow}(t)c_{p\uparrow}(0)\right\rbrace\right|0\right> \\
    \bm{G}_{021}(\bm{p},t) &= -\mathrm{i}\left<0\left|T\left\lbrace c_{-p\downarrow}^{\dagger}(t)c_{p\uparrow}^{\dagger}(0)\right\rbrace\right|0\right>
\end{aligned}
$$
は励起を作ることのない系に対する粒子の対の除去と付加に対する振幅に関係している. $H_0'$からの無限大のc-数の項を取り除くために, $G_0(p,t=0)$を
$$
\begin{aligned}
    \bm{G}_{011}(\bm{p},t=0) &= \lim_{t\to 0^-}\bm{G}_{011}(\bm{p},t) \\
    \bm{G}_{022}(\bm{p},t=0) &= \lim_{t\to 0^+}\bm{G}_{022}(\bm{p},t)
\end{aligned}
$$
と定義すると便利である. 

これらの条件を用いると, $G_0(\bm{p},t)$の時間Fourier変換が
$$
\bm{G}_0(\bm{p},p_0) = \frac{(p_0\bm{1}+\overline{\epsilon}_p\tau_3+\phi_{p1}\tau_1+\phi_{p2}\tau_2)\mathrm{e}^{\mathrm{i}\delta p_0\tau_3}}{p_0^2-\overline{\epsilon}_p^2-\phi_{p1}^2-\phi_{p2}^2+\mathrm{i}\delta}
$$
となる. $p_0\gt 0$に対する$\bm{G}(\bm{p},p_0)$の極は準粒子エネルギー$E_p$を与えるので, $E_p$が
$$
E_p = \sqrt{\overline{\epsilon}+p^2-\phi_{p1}^2+\phi_{p2}^2}
$$
によって与えられることがわかる. より明らかに, $G_0$の様々な行列成分が
$$
\begin{aligned}
    \bm{G}_{011}(p) &= \frac{(p_0+\overline{\epsilon}_p)\mathrm{e}^{\mathrm{i}\delta p_0}}{p_0^2-E_p^2+\mathrm{i}\delta} \\
    \bm{G}_{022}(p) &= \frac{(p_0-\overline{\epsilon}_p)\mathrm{e}^{-\mathrm{i}\delta p_0}}{p_0^2-E_p^2+\mathrm{i}\delta} \\
    \bm{G}_{012}(p) &= \frac{\phi_p^*}{p_0^2-E_p^2+\mathrm{i}\delta} = \bm{G}_{021}^*(p)
\end{aligned}
$$
によって与えられる. Hartree-Fock近似において, 平均電子総数は$N_0$になることを要求しているので, 極限形より
$$
\begin{aligned}
    \left<0\left|\sum_{p,s}n_{ps}\right|0\right> &= \sum_p(-\mathrm{i})\left(\bm{G}_{011}(p,t=0)-\bm{G}_{022}(p,t=0)\right) \\
    &= \sum_p(-\mathrm{i})\mathrm{Tr}\left[\tau_3\bm{G}_0(\bm{p},t=0)\right] = N_0
\end{aligned}
$$
を得る. $\bm{G}_0$の表式より, 
$$
\begin{aligned}
    -\mathrm{i}\bm{G}_0(\bm{p},t=0) &= -\frac{\mathrm{i}}{2\pi}\int\bm{G}_0(\bm{p},p_0)\mathrm{d}p_0 \\
    &= \frac{(E_p-\overline{\epsilon}_p)`\tau_3-\phi_{p1}\tau_1-\phi_{p2}\tau_2}{2E_p}
\end{aligned}
$$
を見出す. そうすると, 化学ポテンシャル$\mu$を決定する条件式は
$$
\frac{1}{(2\pi)^3}\int\left(1-\frac{\overline{\epsilon}_p}{E_p}\right)\mathrm{d}^3p \equiv \frac{2}{(2\pi)^3}\int v_p^2\mathrm{d}^3p = N_0
$$
となる. ここで$\tau_1^2=\bm{1}$, $\mathrm{Tr}\ \tau_1=0$と$\mathrm{Tr}\ \bm{1}=2$を用いた. 

次に$\chi_k$と$\phi_k$を固定する問題に移る. Feynman-Dysonルールを$H_{\text{int}}'$の効果を計算するために用いることができるので, 最低次の自己エネルギーの寄与が
$$
\begin{aligned}
    \bm{\Sigma}(\bm{p},p_0) = &-\frac{\mathrm{i}\tau_3}{(2\pi)^4}\int <\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>\mathrm{Tr}\left[\tau_3\bm{G}_0(p')\right]\mathrm{d}^4p' \\
    &+\frac{\mathrm{i}}{(2\pi)^4}\int <\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\tau_3\bm{G}_0(p')\mathrm{d}^4p' \\
    &-\left(\chi_p\tau_3+\phi_{p1}\tau_1+\phi_{p2}\tau_2\right) \\
    = &\tau_3\left[\frac{1}{(2\pi)^3}\int\left\lbrace 2<\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>-<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\right\rbrace v_{p'}^2\mathrm{d}^3p'-\chi_p\right] \\
    &+\tau_1\left[\frac{1}{(2\pi)^3}\int<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\frac{\phi_{p'1}}{2E_{p'}}\mathrm{d}^3p'+\phi_{p1}\right] \\
    &+\tau_2\left[\frac{1}{(2\pi)^3}\int<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\frac{\phi_{p'2}}{2E_{p'}}\mathrm{d}^3p'+\phi_{p2}\right]
\end{aligned}
$$
となることがわかる. もし準粒子エネルギー$E_p$が$H_{\text{int}}'$における1次まで影響されないということを要求するならば, 行列等式として, 自己無撞着条件
$$
\bm{\Sigma}(\bm{p},E_p) = 0
$$
を得る. Pauli行列は線型独立であるので, この条件は$\bm{\Sigma}$の$\tau$の係数が全てゼロとなることを要求し, $\chi$, $\phi_1$と$\phi_2$を決定する3つの方程式
$$
\begin{aligned}
    \chi_p &= \frac{1}{(2\pi)^3}\int\left\lbrace 2<\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>-<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\right\rbrace v_{p'}^2\mathrm{d}^3p' \\
    \phi_{p1} &= -\frac{1}{(2\pi)^3}\int V_{pp'}\frac{\phi_{p'1}}{2E_{p'}}\mathrm{d}^3p' \\
    \phi_{p2} &= -\frac{1}{(2\pi)^3}\int V_{pp'}\frac{\phi_{p'2}}{2E_{p'}}\mathrm{d}^3p'
\end{aligned}
$$
を得る. ここで対行列$V_{pp'}$は
$$
V_{pp'} = <\bm{p}',-\bm{p}'|V|\bm{p},-\bm{p}'> = <\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>
$$
によって与えられる. 全Hamiltonianは$\tau$空間における$\tau_3$軸についての回転に対して不変であるので, $\phi_2=0$と$\phi_p$がエネルギーギャップパラメータ$\Delta_p$として同定されるBCSエネルギーギャップ方程式に還元されるように, 自由に位相を選ぶことができる. 

スペクトル重み函数$A(\bm{p},\omega)$と関連付けるために, $G_{011}(p)$が
$$
\bm{G}_{011}(p) = \frac{p_0+\overline{\epsilon}_p}{p_0^2-E_p^2+\mathrm{i}\delta} = \frac{u_p^2}{p_0-E_p+\mathrm{i}\delta}+\frac{v_p^2}{p_0+E_p-\mathrm{i}\delta}
$$
と書かれることに注意する. ここでいつものように
$$
\begin{aligned}
    u_p^2 &= \frac{1}{2}\left(1+\frac{\overline{\epsilon}_p}{E_p}\right) \\ 
    v_p^2 &= \frac{1}{2}\left(1-\frac{\overline{\epsilon}_p}{E_p}\right)
\end{aligned}
$$
である. 全てのエネルギーを$\mu$を基準に測っているので, 
$$
\begin{aligned}
    A(\bm{p},\omega) &= -\frac{\mathrm{sgn}\ \omega}{\pi}\Im{G_{011}(\bm{p},\omega)} \\
    &= u_p^2\delta(\omega-E_p)+v_p^2\delta(\omega+E_p)
\end{aligned}
$$
を得る. 

一般化されたHartree-Fockの枠組みを確認するもう一つの方法は, 自己無撞着摂動理論を使うものである. これは遅延や減衰の効果を含むように簡単に拡張できるという特徴を持つ. この手法において, $\bm{\Sigma}(p)$をそれ自体に計算されるべき自己エネルギーを含む1粒子プロパゲーターを用いる摂動級数として計算する. それ故, $\bm{\Sigma}$を決定する近似的な積分方程式を得る. もし, Nambu記法を用い続けるならば, $\bm{\Sigma}(p)$に対する最も一般的な形は
$$
\bm{\Sigma}(p) = (1-Z(p))p_0\bm{1}+\chi(p)\tau_3+\phi(p)\tau_1+\tilde{\phi}(p)\tau_2
$$
となる. ここで再び, $\tau_2$の係数がゼロとなるように位相を選ぶ. 上述の取り扱いとは違って, $\tau_1$は実数である必要はない. 一般化されたHartree-Fockの枠組みはこの近似の枠組み内で, $\bm{\Sigma}$が
$$
\begin{aligned}
    \bm{\Sigma}(p) = &-\frac{\mathrm{i}\tau_3}{(2\pi)^4}\int <\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>\mathrm{Tr}\left[\tau_3\bm{G}(p')\right]\mathrm{d}^4p' \\
    &+\frac{\mathrm{i}}{(2\pi)^4}\int <\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\tau_3\bm{G}(p')\tau_3\mathrm{d}^4p'
\end{aligned}
$$
によって与えられるので, 1次の自己無撞着な摂動論に対応している. Dyson方程式より, 
$$
\bm{G}^{-1}(p) = \bm{G}_0^{-1}(p)-\bm{\Sigma}(p)
$$
と
$$
\bm{G}(p) = \frac{Z(p)p_0+\overline{\epsilon}(p)\tau_3+\phi(p)\tau_1}{(Z(p)p_0)^2-E(p)^2+\mathrm{i}\delta}
$$
を得る. ここで$\bm{G}_0(p)$によって, Nambu記法における本当の自由電子Green函数は
$$
\bm{G}_0(p) = \frac{p_0\bm{1}+\epsilon_p\tau_3}{p_0^2-\epsilon_p^2+\mathrm{i}\delta} = \frac{1}{p_1\bm{1}-\epsilon_p\tau_3+\mathrm{i}\delta p_0\bm{1}}
$$
となり, $\epsilon_p$は$\mu$を基準に測られている. また以前のように
$$
\begin{aligned}
    \overline{\epsilon}(p) &= \epsilon_p+\chi(p) \\
    E(p) &= \sqrt{\overline{\epsilon}(p)^2+\phi(p)^2}
\end{aligned}
$$
と書いている. そのようにして, $\bm{\Sigma}$は$Z$, $\chi$と$\phi$を決定する積分方程式の集合を表す. $\bm{1}$の係数は右辺で消えるので, この近似の枠組み内で$Z(p)=1$を得, 残りの項は前述の3つの積分方程式に帰着する. 

### 7-3 絶対零度の励起スペクトル
絶対零度における超伝導体の準粒子励起スペクトルを決定する際に, 減衰と遅延の効果を扱うことを考える. NambuとEliashbergは問題を自己無撞着な摂動論によって扱い, $\Sigma$に対する最低次の衣を着たフォノンと衣を着たCoulombの寄与を得た. この近似の枠内で, $\Sigma$を決定する行列等式
$$
\bm{\Sigma}(p) = \frac{\mathrm{i}}{(2\pi)^4}\int\tau_3\bm{G}(p')\tau_3\left[\sum_{\lambda}\left\lbrace g_{pp'\lambda}\right\rbrace^2D_{\lambda}(p-p')+\mathcal{V}_{\text{c}}(p-p')\right]\mathrm{d}^4p'
$$
が見出された. 

これを1次元の形式に簡約するために, 相互作用のフォノン部分から生じる項
$$
\bm{\Sigma}^{\text{ph}}(p) = \frac{\mathrm{i}}{(2\pi)^4}\int\tau_3\bm{G}(p')\tau_3\sum_{\lambda}\left\lbrace\overline{g}_{pp'\lambda}\right\rbrace^2D_{\lambda}(p-p')\mathrm{d}^4p'
$$
から考えることを始める. 右辺は運動量移行$|\bm{p}'-\bm{p}|$を通してのみもたらされる$\bm{p}$の函数である. 運動量移行は$\bm{p}'$-積分の角度部分を実行するときに平均されるので, $\bm{\Sigma}^{\text{ph}}$は$|\bm{p}|$が$\sim (1/2)p_{\text{F}}$程度の変化のとき, $p_{\text{F}}$付近の$|\bm{p}|$に関してゆっくりと変化する. いま興味があるのは, $|p_0|\leq \omega_{\text{D}}\ll E_{\text{F}}$のエネルギー帯の$\bm{\Sigma}^{\text{ph}}(\bm{p},p_0)$である. フォノンのプロパゲーターは$|p_0'-p_0|\gg\omega_{\bm{p}'-\bm{p}}$に対して$1/(p_0'-p_0)^2$の速さで減衰するので, $p_0'$-積分からの主要な寄与が$p_0'\leq\omega_{\text{c}}$のエネルギーから生じることがはっきりする. ここでカットオフエネルギー$\omega_{\text{c}}$はDebyeエネルギーの数倍である. しかしながら, これは$|\bm{p}|$にかけての積分への主要な寄与が$G$の形式のために$\omega_{\text{c}}$以下の運動エネルギーを持つ状態から生じるということを意味していない. それ故, $\bm{\Sigma}^{\text{ph}}$を評価する際, $p_0$のみの函数として$\bm{\Sigma}^{\text{ph}}(\bm{p},p_0)\to\bm{\Sigma}^{\text{ph}}(p_{\text{F}},p_0)$に近似できる. 変数$p'$, $q$と$\phi$を用いることによって, 
$$
\begin{aligned}
    \bm{\Sigma}^{\text{ph}}(p) \simeq &\frac{\mathrm{i}m}{(2\pi)^3|\bm{p}|}\int_{-\infty}^{\infty}\mathrm{d}p_0' \\
    &\int_{-\infty}^{\infty}\left[\frac{Z(p_0')p_0'\bm{1}-\phi(p_0')\tau_1}{\left(Z(p_0')p_0'\right)^2-\overline{\epsilon}_{p'}^2-\phi^2(p_0')+\mathrm{i}\delta}\sum_{\lambda}\int_0^{2k_{\text{F}}}q\left\lbrace\overline{g}_{q\lambda}\right\rbrace^2D_{\lambda}(q,p_0-p_0')\mathrm{d}q\right]\mathrm{d}\overline{\epsilon}_{p'}
\end{aligned}
$$
を得る. 簡単のために, $\tau_3$の項を消すために粒子-正孔対称性がFermi面近くで成り立つと仮定した. Eliashbergの積分手順を用いて, $D(p-p')$を$p_0'$の上半平面で解析的な$D^{\text{u}}$と下半平面で解析的な$D^{\text{l}}$に分ける. $D^{\text{u}}$に対して, 元々正の実軸に沿っていた$p_0'$積分路の部分は, Figure 7-6[^1]に示されるように上半平面を通して負の実軸に沿ったものに折り返される. $D^{\text{u}}$は上半平面で解析的なので, 左の$G$の切断に不連続性はない. 関係
$$
\bm{G}(\bm{p},p_0+\mathrm{i}\delta) = \bm{G}^*(\bm{p},p_0-\mathrm{i}\delta)
$$
を用いて, 変形された積分路はもし$\bm{G}(p)$が$\bm{G}(p)-\bm{G}^*(p)=2\mathrm{i}\Im{\bm{G}(p)}$によって置き換えられるならば, 切断の左側に沿った積分に置き換えられる. それ故, $D^{\text{u}}$の部分の$\bm{\Sigma}^{\text{ph}}$は
$$
\begin{aligned}
    \bm{\Sigma}_{\text{u}}^{\text{ph}}(p) = &\frac{-2m}{(2\pi)^3|\bm{p}|}\int_{-\infty}^0\mathrm{d}p_0' \\
    &\Im{\int_{-\infty}^{\infty}\frac{Z(p_0')p_0'\bm{1}-\phi(p_0')\tau_1}{\left(Z(p_0')p_0'\right)^2-\overline{\epsilon}_{p'}^2-\phi^2(p_0')+\mathrm{i}\delta}} \mathrm{d}\overline{\epsilon}_{p'} \\
    &\sum_{\lambda}\int_0^{2k_{\text{F}}}q\left\lbrace\overline{g}_{q\lambda}\right\rbrace^2D_{\lambda}^{\text{u}}(q,p_0-p_0')\mathrm{d}q \\
    = &\frac{m}{(2\pi)^2|\bm{p}|}\int_{-\infty}^0\Re{\frac{Z(p_0')p_0'\bm{1}-\phi(p_0')\tau_1}{\sqrt{\left(Z(p_0')p_0'\right)^2-\phi^2(p_0')}}}\mathrm{d}p_0' \\
    &\sum_{\lambda}\int_0^{2k_{\text{F}}}q\left\lbrace\overline{g}_{q\lambda}\right\rbrace^2D_{\lambda}^{\text{u}}(q,p_0-p_0')\mathrm{d}q
\end{aligned}
$$
によって与えられる. $D^{\text{l}}$についても同様にして, 
$$
\begin{aligned}
    \bm{\Sigma}_{\text{l}}^{\text{ph}}(p) = &\frac{m}{(2\pi)^2|\bm{p}|}\int_0^{\infty}\Re{\frac{Z(p_0')p_0'\bm{1}-\phi(p_0')\tau_1}{\sqrt{\left(Z(p_0')p_0'\right)^2-\phi^2(p_0')}}}\mathrm{d}p_0' \\
    &\sum_{\lambda}\int_0^{2k_{\text{F}}}q\left\lbrace\overline{g}_{q\lambda}\right\rbrace^2D_{\lambda}^{\text{l}}(q,p_0-p_0')\mathrm{d}q
\end{aligned}
$$
を得る. $D^{\text{u}}$の積分について$p_0'\to -p_0'$として, $Z(p)$と$\phi(p)$が$p_0$の偶函数であることを用いることによって, $|\bm{p}|\sim p_{\text{F}}$に対して, 
$$
\begin{aligned}
    \bm{\Sigma}^{\text{ph}}(p) &= \bm{\Sigma}_{\text{l}}^{\text{ph}}(p)+\bm{\Sigma}_{\text{u}}^{\text{ph}}(p) \\
    &= N(0)\int_0^{\infty}K_{\pm}^{\text{ph}}(p_0,p_0')\Re{\frac{Z(p_0')p_0'\bm{1}-\phi(p_0')\tau_1}{\sqrt{\left(Z(p_0')p_0'\right)^2-\phi^2(p_0')}}}\mathrm{d}p_0'
\end{aligned}
$$
を得る. 相互作用核は
$$
K_{\pm}^{\text{ph}}(p_0,p_0') = \sum_{\lambda}\frac{1}{2k_{\text{F}}^2}\int_0^{2k_{\text{F}}}q\left[\int_0^{\infty}B_{\lambda}(q,\omega)\left\lbrace\overline{g}_{q\lambda}\right\rbrace^2\left(\frac{1}{p_0'+p_0+\omega-\mathrm{i}\delta}\pm\frac{1}{p_0'-p_0+\omega-\mathrm{i}\delta}\right)\mathrm{d}\omega\right]\mathrm{d}q
$$
によって定義される. ここで$B_{\lambda}(q,\omega)$はフォノンのスペクトル重み函数である. 

次にCoulomb項を簡約する問題に移る. 不幸なことに, ポテンシャル$\mathcal{V}_{\text{c}}(p-p')$は$|p_0'-p_0|\lt\omega_{\text{D}}$に対して速く減衰しないのでフォノンと同じようには扱えない. 3次元運動量に関して初めに積分するという技巧も実直なやり方では機能しない. この複雑さを回避するために, 範囲$p_0'\lt |\omega_{\text{c}}|$外のエネルギー領域におけるCoulomb相互作用を考慮する擬ポテンシャルを導入したい. 擬ポテンシャルを決定するために, 電子の自己エネルギーのCoulomb部分
$$
\bm{\Sigma}^{\text{c}}(p) = \frac{\mathrm{i}}{(2\pi)^4}\tau_3\bm{G}(p')\tau_3\mathcal{V}_{\text{c}}(p-p')\mathrm{d}^4p'
$$
を考える. もし$\phi^{\text{c}}$, $\chi^{\text{c}}$と$(1-Z)^{\text{c}}p_0$をそれぞれ$\bm{\Sigma}^{\text{c}}$における$\tau_1$, $\tau_3$と$\bm{1}$の係数に定義するならば, 
$$
\begin{aligned}
    \phi^{\text{c}}(p) &= -\frac{\mathrm{i}}{(2\pi)^4}\int\frac{\phi'}{(Z'p_0')^2-\overline{\epsilon}'^2-\phi'^2}\mathcal{V}_{\text{c}}(p-p')\mathrm{d}^4p' \\
    \chi^{\text{c}}(p) &= \frac{\mathrm{i}}{(2\pi)^4}\int\frac{\overline{\epsilon}'}{(Z'p_0')^2-\overline{\epsilon}'^2-\phi'^2}\mathcal{V}_{\text{c}}(p-p')\mathrm{d}^4p' \\
    (1-Z(p))^{\text{c}}p_0 &= \frac{\mathrm{i}}{(2\pi)^4}\int\frac{Z'p_0'}{(Z'p_0')^2-\overline{\epsilon}'^2-\phi'^2}\mathcal{V}_{\text{c}}(p-p')\mathrm{d}^4p'
\end{aligned}
$$
を得る. 簡単のために, $\mathrm{V}_{\text{c}}(p-p')$は$p_0$と$p_0'$に独立であるので, 統計的に遮蔽されたポテンシャルによってよく表されると仮定する. 第3式の左辺は$p_0$において反対称で, 右辺は$p_0$に独立であるので, $(1-Z(p))^{\text{c}}=0$と結論される. また主要な効果が化学ポテンシャルの重要でないシフトを与えることとBloch状態に関連した有効質量の変化である$\chi^{\text{c}}$の項を無視する. そうすると, 興味があるのは
$$
\phi^{\text{c}}(p) = \frac{2}{(2\pi)^4}\int_{\Delta_0}^{\infty}\left[\int\Im{\frac{\phi'}{(Z'p_0')^2-\overline{\epsilon}'^2-\phi'^2}}\mathcal{V}_{\text{c}}(\bm{p}-\bm{p}')\mathrm{d}^3p'\right]\mathrm{d}p_0'
$$
だけとなる. 積分範囲を$[\Delta_0,\omega_{\text{c}}]$とできるように擬ポテンシャル$U_{\text{c}}$を
$$
U_{\text{c}}(p,p') = \mathcal{V}_{\text{c}}(\bm{p}-\bm{p}')+\frac{2}{(2\pi)^4}\int_{\Delta_0}^{\omega_{\text{c}}}\left[\int\Im{\frac{1}{p_0''^2-E''^2+\mathrm{i}\delta}}\mathcal{V}_{\text{c}}(p-p'')U_c(p'',p')\mathrm{d}^3p''\right]\mathrm{d}p_0''
$$
とし, 
$$
\phi^{\text{c}}(p) = \frac{2}{(2\pi)^4}\int_{\Delta_0}^{\omega_{\text{c}}}\left[\int\Im{\frac{\phi'}{(Z'p_0')^2-\overline{\epsilon}'^2-\phi'^2}}U_{\text{c}}(p,p')\mathrm{d}^3p'\right]\mathrm{d}p_0'
$$
を得る. ここで$p_0\gt\omega_{\text{c}}$に対する
$$
\begin{aligned}
    Z(p) &\to 1 \\
    \phi(p) &\to \phi^{\text{c}}(p)
\end{aligned}
$$
を使って等式を単純化した. 

$U_{\text{c}}$の大きさを推定するために, 超伝導体におけるs波の対を仮定する. この場合には, $\mathcal{V}_{\text{c}}(\bm{p}-\bm{p}')$の球平均のみが影響し, 
$$
\frac{1}{2}\int\mathcal{V}_{\text{c}}(p-p')\mathrm{d}\mu \equiv V_{\text{c}}(p,p')
$$
となる. ここで$\mu$は$\bm{p}$と$\bm{p}'$の間の核の余弦である. すると擬ポテンシャルは, 
$$
U_{\text{c}}(p,p') = V_{\text{c}}(p,p')-\frac{1}{(2\pi)^3}\int\theta(E_{p''}-\omega_{\text{c}})V_{\text{c}}(p,p'')\frac{1}{2E_{p''}}U_c(p'',p')\mathrm{d}^3p''
$$
となる. ここで$\theta$函数は
$$
\theta(x) = \begin{cases}
    1 &x\gt 0 \\
    0 &x\lt 0
\end{cases}
$$
によって定義される. 擬ポテンシャルの物理的解釈は, それがちょうど領域$[-\omega_{\text{c}}, \omega_{\text{c}}]$外で2粒子散乱に対する$t$行列のs波部分によって満足される等式であるので, この等式の形から明らかである. Fermi面付近の粒子間で使うべき有効ポテンシャルはFermi面から離れた領域にFermi面近くの散乱に対するBorn項を足した領域における全ての多重散乱の総和によって与えられるということは理に適っている. 擬ポテンシャルはもし$V_{\text{c}}(p,p')$が
$$
V_{\text{c}}(p,p') = \begin{cases}
    V_{\text{c}} &|\epsilon_p|\lt\omega_{\text{m}}\text{ かつ }|\epsilon_{p'}|\lt\omega_{\text{m}}\text{のとき} \\
    0 &\text{otherwise}
\end{cases}
$$
によって近似されるならば明確に得られる. ここで最大のエネルギー$\omega_{\text{m}}$はFermiエネルギーのオーダーである. これは粗い近似であるが, $U_{\text{c}}$に対する大きさの正確なオーダーを与え, もしBloch状態が$|\epsilon_p|\lt\omega_{\text{m}}$に対する定数に取られるならば, 
$$
U_{\text{c}}(p,p') = \frac{V_{\text{c}}}{1+N(0)V_{\text{c}}\log(\omega_{\text{m}}/\omega_{\text{c}})}
$$
が見出される. 物理的に, この簡約はFermi面から離れた領域の散乱は, 2電子が遮蔽されたCoulombポテンシャルの範囲内にある確率を低くするという効果をもたらす. それ故, 領域$|p_0''|\lt\omega_{\text{c}}$において, これらの相関した状態間の遮蔽されたCoulomb相互作用の行列要素は対応する平面波のそれよりも小さい. 

この積分の主要部分が$U_{\text{c}}(p,p')$が定数$U_{\text{c}}$であるBloch状態に起因するので, 3次元運動量積分を実行する. フォノンのときと同様に簡約して, 最終的に足し合わせた$\bm{\Sigma}$として, 
$$
\bm{\Sigma}(p) = N(0)\int_0^{\omega_{\text{c}}}K_{\pm}(p_0,p_0')\Re{\frac{Z'p_0'\bm{1}+\phi'\tau_1}{\sqrt{(Z'p_0')^2-\phi'^2}}}\mathrm{d}p_0'
$$
を得る. ここで, 
$$
\begin{aligned}
    K_+(p_0,p_0') &= K_+^{\text{ph}}(p_0,p_0')-U_{\text{c}} \\
    K_-(p_0,p_0') &= K_-^{\text{ph}}(p_0,p_0')
\end{aligned}
$$
である. この積分方程式は
$$
\Delta(p_0) \equiv \frac{\phi(p_0)}{Z(p_0)}
$$
によって定義されるエネルギーギャップパラメータ$\Delta$を導入することによって簡単化され得る. BCSパラメータ$\Delta_p$に最も近く対応しているのはこの量である. そうすると, $\Delta$と$Z$を決定する積分方程式
$$
\begin{aligned}
    \Delta(p_0) &= \frac{N(0)}{Z(p_0)}\int_{\Delta_0}^{\omega_{\text{c}}}K_+(p_0,p_0')\Re{\frac{\Delta(p_0')}{\sqrt{p_0'^2-\Delta^2(p_0')}}}\mathrm{d}p_0' \\
    (1-Z(p_0))p_0 &= N(0)\int_{\Delta_0}^{\omega_{\text{c}}}K_-(p_0,p_0')\Re{\frac{p_0'}{\sqrt{p_0'^2-\Delta^2(p_0')}}}\mathrm{d}p_0'
\end{aligned}
$$
を得る. ここで$\Delta_0$はギャップ端でのギャップパラメータの値で
$$
\Delta_0 = \Delta(\Delta_0)
$$
によって定義される. 

数値計算の結果が示される. 

### 7-4 有限温度への拡張
有限温度の問題に対しては, 1粒子Green函数を系の正確な励起状態に対して定義される統計的平均のGreen函数として定義したい. より一般には, 総粒子数の異なった系にかけての集団平均を含め, 適切な重み因子として大正準分布を使うことが便利である. そうして, 
$$
G_s(\bm{r}_1,\bm{r}_2,\tau,\beta,\mu) = -\mathrm{i}\mathrm{Tr}\left[u(\beta,\mu)T\left\lbrace\psi_s(\bm{r}_1,\tau)\psi_s^{\dagger}(\bm{r}_2,0)\right\rbrace\right]
$$
を定義する. ここで大正準分布行列は
$$
u(\beta,\mu) = \frac{\mathrm{e}^{-\beta(H-\mu N)}}{\mathrm{Tr}\ \mathrm{e}^{-\beta(H-\mu N)}}\text{ , }\beta = \frac{1}{k_{\text{B}}T}
$$
によって与えられる. 簡単のために, $H-\mu N=K$とおいて, $H$ではなく$K$による時間発展を考える, 
$$
\psi_s(\bm{r},\tau) = \mathrm{e}^{\mathrm{i}K\tau}\psi_s(\bm{r},0)\mathrm{e}^{-\mathrm{i}K\tau}.
$$
並進不変な系に対しては, Green函数
$$
G_s(p,\tau) = -\mathrm{i}\mathrm{Tr}\left[u(\beta,\mu)T\left\lbrace c_{ps}(\tau)c_{ps}^{\dagger}(0)\right\rbrace\right]
$$
に興味がある. この函数に対する洞察を得るために, $G(\bm{p},\tau)$の時間のFourier変換に対するスペクトル表示を考える, 
$$
G(\bm{p},p_0) = \int_{-\infty}^{\infty}\frac{\rho^{(+)}(\bm{p},\omega)}{p_0-\omega+\mathrm{i}\delta}\mathrm{d}\omega+\int_{-\infty}^{\infty}\frac{\rho^{(-)}(\bm{p},\omega)}{p_0+\omega-\mathrm{i}\delta}\mathrm{d}\omega\ .
$$
ここで, スペクトル函数は
$$
\begin{aligned}
    \rho^{(+)}(\bm{p},\omega) &= \sum_{n,m}u_n\left|<m|c_p^{\dagger}|n>\right|^2\delta(E_m-E_n-\omega) \\
    \rho^{(-)}(\bm{p},\omega) &= \sum_{n,m}u_n\left|<m|c_p|n>\right|^2\delta(E_m-E_n-\omega)
\end{aligned}
$$
によって与えられる. 状態$|n>$は$K$の固有状態で固有値$E_n$を持ち, $u_n$は密度行列の対角要素$\mathrm{e}^{-\beta E_n}/\sum_m\mathrm{e}^{-\beta E_m}$である. またスペクトル函数は
$$
\rho^{(-)}(\bm{p},\omega) = \mathrm{e}^{\beta\omega}\rho^{(+)}(\bm{p},-\omega)
$$
によって関連付けられている. すると, スペクトル表示は
$$
\begin{aligned}
    \Re{G(\bm{p},p_0)} &= P\int_{-\infty}^{\infty}\frac{A(\bm{p},\omega)}{p_0-\omega}\mathrm{d}\omega \\
    \Im{G(\bm{p},p_0)} &= -\pi A(\bm{p},p_0)\tanh\frac{\beta p_0}{2}
\end{aligned}
$$
として書き直され, スペクトル重み函数は
$$
A(\bm{p},\omega) = \rho^{(+)}(\bm{p},\omega)+\rho^{(-)}(\bm{p},-\omega) = \rho^{(+)}(p,\omega)(1+\mathrm{e}^{-\beta\omega})
$$
によって定義される. 絶対零度の場合と違って, 系に粒子を加えたり, 系から粒子を取り除くエネルギーが$\mu$より小さくなっても良いので, スペクトル函数は$\omega$軸全体に渡って有限である. それ故, スペクトル重み函数の正と負の周波数部分が, それぞれ粒子注入と正孔注入過程に対応するという単純な描像は, もはや有限温度では成り立たない. 

絶対零度では, スペクトル重み函数は和の規則
$$
\begin{aligned}
    \int_{-\infty}^{\infty}A(\bm{p},\omega)\mathrm{d}\omega &= \int_{-\infty}^{\infty}\left(\rho^{(+)}(\bm{p},\omega)+\rho^{(-)}(\bm{p},-\omega)\right)\mathrm{d}\omega \\
    &= \sum_{n,m}u_n\left\lbrace <n|c_pc_p^{\dagger}|n>+<n|c_p^{\dagger}c_p|n>\right\rbrace \\
    &= \mathrm{Tr}\ u = 1
\end{aligned}
$$
を満たす. 

$G$に対する絶対零度の摂動級数の高度に自動的な性質は有限温度にも拡張される. その基本的な帰結は, $G(\bm{p},p_0)$が純虚数の周波数$\mathrm{i}\omega_n$の離散的な集合にかけて定義されたGreen函数$\mathcal{G}(\bm{p},\mathrm{i}\omega_n)$の解析接続によって決定されるということである. 

拡張におけるルールの変更と具体例が述べられる. 

この章の一般的な結論として, BCS理論を非即時の相互作用と減衰効果を含めるために洗練するとき, 準粒子スペクトルの詳細しか変化しないということが言える. 

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |
| 176 | 5 | $p$ | $\bm{p}$ |
| 176 | 19 | . | , |
| 183 | 13 | $\phi^{12}$ | $\phi^2$ |
| 184 | 9 | $\phi^{12}$ | $\phi^2$ |
| 193 | 32 | $\bm{r}$ | $\bm{r}_1$ |
| 195 | 7 | $\bm{1}$ | $1$ |


## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)