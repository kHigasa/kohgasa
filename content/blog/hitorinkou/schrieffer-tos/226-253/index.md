---
title: 一人輪講第14回
date: "2021-02-27T02:00:00.000Z"
description: "PDF整理のための一人輪講第14回. J.R.Schrieffer, 1964, Theory of Superconductivity, 226-253 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter8 超伝導体の電磁気的性質
### 8-5 ゲージ不変性
ゲージ不変性が局所的な電荷保存の結果として従うことの説明. 

しかし単純な対近似のゲージ不変性は破れ, それを回復するために背景流動や集団モードを含めることが本質的である. 

ここでNambu場$\Psi$によって
$$
\bm{\Lambda}_{\mu}(x,y,z) = \left<0\left|T\left\lbrace J_{\mu}(z)\Psi(x)\Psi^{\dagger}(y)\right\rbrace\right|0\right>
$$
と定義された時間について順序付けられた量$\bm{\Lambda}_{\mu}$を考える. また積分関係
$$
\bm{\Lambda}(x,y,z) = e\int\bm{G}(x,x')\bm{\Gamma}_{\mu}(x',y',z)\bm{G}(y,y')\mathrm{d}^4x'\mathrm{d}^4y'
$$
によって結節点函数$\bm{\Gamma}_{\mu}$を定義する. ここでNambuの1粒子Green函数は
$$
\bm{G}(x,x') = -\mathrm{i}\left<0\left|T\left\lbrace\Psi(x)\Psi^{\dagger}(x')\right\rbrace\right|0\right>
$$
である. ここで系が並進不変性を持つことを仮定すると, 
$$
\bm{\Gamma}_{\mu}(x',y',z) = \frac{1}{(2\pi)^6}\int\bm{\Gamma}_{\mu}(p+q,p)\mathrm{e}^{\mathrm{i}\lbrace p(x'-y')+q(x'-z)\rbrace}\mathrm{d}^4p\mathrm{d}^4q
$$
とかける. そうすると, 超伝導体に対する一般化されたWard恒等式は
$$
\begin{aligned}
    \sum_{\mu}q_{\mu}\bm{\Gamma}_{\mu}(p+q,p) &= \sum_{i=1}^3q_i\bm{\Gamma}_i(p+q,p)-q_0\bm{\Gamma}_0(p+q,p) \\
    &= \tau_3\bm{G}^{-1}(p)-\bm{G}^{-1}(p+q)\tau_3
\end{aligned}
$$
となる. 

$\bm{\Gamma}_{\mu}$の物理的重要性を理解するために, 4次元電流密度演算子$j_{\mu}^p(\bm{q})$がNambu記法において
$$
j_{\mu}^p(\bm{q}) = \begin{cases}
    \displaystyle\sum_p\Psi_p^{\dagger}\left\lbrace -e\left(\bm{p}+\frac{\bm{q}}{2}\right)_i\bm{1}\right\rbrace\Psi_{p+q} &(\mu=i=1,2,3) \\
    \displaystyle\sum_p\Psi_p^{\dagger}\left(-e\tau_3\right)\Psi_{p+q} &(\mu=0)
\end{cases}
$$
とかけることに注目する. もし "自由な" 結節点函数$\bm{\gamma}_{\mu}$を
$$
\bm{\gamma}_{\mu}(\bm{p}+\bm{q},\bm{p}) = \begin{cases}
    \displaystyle\frac{1}{m}\left(\bm{p}+\frac{\bm{q}}{2}\right)_i\bm{1} &(\mu=i=1,2,3) \\
    \tau_3 &(\mu=0)
\end{cases}
$$
と定義するならば, $j_{\mu}^p(\bm{q})$は
$$
j_{\mu}^p(\bm{q}) = -e\sum_{\bm{p}}\Psi_{\bm{p}}^{\dagger}\bm{\gamma}_{\mu}(\bm{p}+\bm{q},\bm{p})\Psi_{\bm{p}+\bm{q}}
$$
とかける. すると結節点函数$\bm{\Gamma}_{\mu}(p+q,p)$は自由な結節点函数$\bm{\gamma}_{\mu}(p+q,p)$の衣をまとったヴァージョンとなる. これをもっともらしくするために, 一般化されたWard恒等式を相互作用していない電子系に応用する. $G^{-1}(p)$はこの極限で
$$
G_0^{-1}(p) = p_0\bm{1}-\epsilon_p\tau_3\text{ , }\epsilon_p = \frac{p^2}{2m}-\mu
$$
となるので, 一般化されたWard恒等式は
$$
\sum_{\mu}q_{\mu}\bm{\Gamma}_{\mu}(p+q,p) = (\epsilon_{p+q}-\epsilon_p)\bm{1}-q_0\tau_3
$$
となる. しかしこの関係式は, $\bm{\Gamma}_{\mu}$が$\bm{\gamma}_{\mu}$であっても恒等的に満たされる. それ故, 我々は衣をまとった電子系を, 衣を着た結節点$-e\bm{\Gamma}_{\mu}$を通した電磁場との相互作用としてみなすことができる. 

時間に順序付けられた核$P_{\mu\nu}$と結節点函数$\bm{\Gamma}_{\mu}$の定義より, 
$$
P_{\mu\nu}(q) = -\frac{\mathrm{i}e^2}{(2\pi)^4}\int\mathrm{Tr}\left\lbrace\bm{\gamma}_{\mu}(p,p+q)\bm{G}(p+q)\bm{\Gamma}_{\nu}(p+q,p)\bm{G}(p)\right\rbrace\mathrm{d}^4p
$$
を得る. この近似におけるゲージ不変性の破れは, 単に衣をまとった$G$の裸の結節点函数を持つ$P_{\mu\nu}$を計算した結果であり, それ故に一般化されたWard恒等式を侵害している. 

$K_{\mu\nu}$は一般化されたWard恒等式が満たされるならばゲージ不変であることが示されている. 

### 8-6 結節点函数と集団モード
対形成理論の枠組みにおけるゲージ不変性の一般化を考える際, Nambuは一般化されたWard恒等式を満たす近似を構築するための量子場の理論のよく知られた処方を用いた. $\bm{G}$に対する対近似は
$$
\bm{G}^{-1}(p) = p_0\bm{1}-\epsilon_p\tau_3-\bm{\Sigma}(p)
$$
となる. ここで, 
$$
\bm{\Sigma}(p) = \frac{\mathrm{i}}{(2\pi)^4}\int\tau_3\bm{G}(p')\tau_3\mathcal{V}(p-p')\mathrm{d}^4p'
$$
である. これは形式的には図8-4[^1]に示されるように, 相互作用線が交わらないような全てのグラフの和とみなされる. もし$\bm{\gamma}_{\mu}$がこれらのグラフにおける全ての可能な場所に挿入されるとすると, 結果として得られる級数は図8-5[^1]に示されるように, $\bm{\Gamma}_{\mu}$に対する梯子グラフ近似によって和が取られる. それ故, $\bm{\Gamma}_{\mu}$は線形積分方程式
$$
\bm{\Gamma}_{\mu}(p+q,p) = \bm{\gamma}_{\mu}(p+q,p)+\frac{\mathrm{i}}{(2\pi)^4}\int\tau_3\bm{G}(k+q)\bm{\Gamma}_{\mu}(k+q,k)\bm{G}(k)\tau_3\mathcal{V}(p-k)\mathrm{d}^4k
$$
を満たす. この結節点方程式の解が一般化されたWard恒等式にまさに一致するということを確かめるために, 量
$$
\sum_{\mu}q_{\mu}\bm{\Gamma}_{\mu}(p+q,p) = \sum_{\mu}q_{\mu}\bm{\gamma}_{\mu}(p+q,p)+\frac{\mathrm{i}}{(2\pi)^4}\int\tau_3\bm{G}(k+q)\sum_{\mu}q_{\mu}\bm{\Gamma}_{\mu}(k+q,k)\bm{G}(k)\tau_3\mathcal{V}(p-k)\mathrm{d}^4k
$$
を考える. 右辺第2項は一般化されたWard恒等式を仮定すれば, 
$$
\begin{aligned}
    &\left(\frac{\mathrm{i}}{(2\pi)^4}\int\tau_3\bm{G}(k+q)\tau_3\mathcal{V}(p-k)\mathrm{d}^4k\right)\tau_3-\tau_3\left(\frac{\mathrm{i}}{(2\pi)^4}\int\tau_3\bm{G}(k+q)\tau_3\mathcal{V}(p-k)\mathrm{d}^4k\right) \\
    = &\bm{\Sigma}(p+q)\tau_3-\tau_3\bm{\Sigma}(p)
\end{aligned}
$$
となる. 

これらの結果を組み合わせると, 一般化されたWard恒等式は
$$
\begin{aligned}
    &\sum_{\mu}q_{\mu}\bm{\Gamma}_{\mu}(p+q,p) \\
    = &\tau_3\left\lbrace p_0\bm{1}-\epsilon_p\tau_3-\bm{\Sigma}(p)\right\rbrace -\left\lbrace (p_0+q_0)\bm{1}-\epsilon_{p+q}\tau_3-\bm{\Sigma}(p+q)\right\rbrace\tau_3 \\
    = &\tau_3\bm{G}^{-1}(p)-\bm{G}^{-1}(p+q)\tau_3
\end{aligned}
$$
に簡約される. 

### 8-7 磁束の量子化
省略. 

### 8-8 Knightシフト
省略. 

### 8-9 Ginsburg-Landau-Gor'kov理論
これまでは, 弱い電磁場に対する超伝導体の応答に注目してきた. 一方, 強い場への応答を考えなければならない場合に対しては, Ginsburg-Landauの現象論が多くの場合に良い説明を与える. ここでは, Gor'kovによるGinsburg-Landauの現象論の微視的な導出が簡単にまとめられている. 

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |
| 230 | 22 | $\sum_p$ | $\sum_{\bm{p}}$ |


## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)