---
title: 一人輪講第11回
date: "2021-02-07T02:00:00.000Z"
description: "PDF整理のための一人輪講第12回. J.R.Schrieffer, 1964, Theory of Superconductivity, 151-175 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter6 常伝導金属における素励起
### 6-2 電子-フォノン結合系

イオン間やイオンと電子間における長距離Coulomb力は伝導電子によって遮蔽されるので, 遮蔽効果は衣を着たフォノンの振動数や衣を着た電子-フォノン相互作用を決定するのにも重要な役割を果たす. 長波長効果に対して, この極限でRPAが主要な補正を与えるので, 我々は再びRPAを用いることができる. より短い波長に対しては, RPAによって無視されている過程が影響するが, その領域は裸の量に対してそもそも小さな補正しか与えないので, RPAが全ての波長域に渡っておおよそ正しい補正を与える. 

RPAにおける電子-フォノン系全体を扱うことから始め, 簡単のためにCoulomb相互作用と電子-フォノン相互作用の療法においてウムクラップ過程を無視する. 既約な縦波のフォノンの自己エネルギー$\Pi_{l}(q)$はFigure 6-8[^1]に示される一連のダイアグラムによって与えられる. RPAに特徴的である気泡の弦は, 単にイオン間ポテンシャルの遮蔽を表している. $\mathcal{V}_c(q)$に対する級数との比較より, 
$$
\frac{\mathcal{V}_c(q)-V(q)}{V^2(q)} = \frac{1}{V(q)}\left(\frac{1}{\kappa(q)}-1\right) = \frac{1}{|g_{ql}|^2}\Pi_{\text{l}}(q)
$$
という関係を得る. ここで初めの等号において$\mathcal{V}_c^{\text{RPA}}(q)$の表式を用い, 次の等号では$g_{pp'\lambda}$は縦波のフォノンに対する運動量移行にのみ依存する函数であると仮定した. これを整理することにより, 
$$
\Pi_{l}(q) = \frac{|g_{ql}|^2}{V(q)}\left(\frac{1}{\kappa(q)}-1\right)
$$
を得る. Dyson方程式
$$
D_l^{-1}(q) = D_{0l}^{-1}(q)-\Pi_{l}(q)
$$
より, 
$$
D_l(q) = \frac{2\Omega_{ql}}{q_0^2-\displaystyle\frac{2|g_{ql}|^2\Omega_{ql}}{V(q)\kappa(q)}-\left(\Omega_{ql}^2-\frac{2|g_{ql}|^2\Omega_{ql}}{V(q)}\right)+\mathrm{i}\delta}
$$
を見出す. 

第4章で言及したように, ジェリウムモデルに対して得られる関係式
$$
\Omega_{ql}^2 = \frac{2|g_{ql}|^2\Omega_{ql}}{V(q)}
$$
を用いれば, $D_l(q)$は
$$
D_l(q) = \frac{2\Omega_{ql}}{q_0^2-\displaystyle\frac{\Omega_{ql}^2}{\kappa(q)}+\mathrm{i}\delta}
$$
と単純化される. $D$の極は衣を着たフォノンの周波数を与えるので, 
$$
\omega_{ql}^2 = \frac{\Omega_{ql}^2}{\tilde{\kappa}(\bm{q},\omega_{ql})}
$$
を見出す. ここで$\tilde{\kappa}$は$q_0$軸に沿った1粒子的な切断を横切って$\kappa$を解析接続させた函数である. この等式は, 衣を着た振動数がイオン間の実効的な力(もしくは, イオン電荷の2乗)を電気的比誘電率によって弱めることによって与えられるはずであるという直感的な洞察に一致している. $\omega_{ql}$の実部を計算するとき, 典型的なフォノンと電子の周波数は$\sim\sqrt{m/M}\sim 10^{-3}$程しか異ならないので, 静的な比誘電率$\kappa(q,0)$を使えば十分である. 長波長極限において, 上述したThomas-Fermiの静的な比誘電率の結果を用いると, 
$$
\omega_{ql}^2 = \frac{\Omega_{ql}^2}{1+\displaystyle\frac{k_{\text{s}}}{q^2}} = \frac{mZ}{3M}v_{\text{F}}^2q^2
$$
を得る. ここで$v_{\text{F}}$はFermi速度である. これより, 衣を着た縦波フォノンは音速$\sqrt{mZ/3M}v_{\text{F}}$で音波型の分散則を持つことがわかる. 

いま我々は衣を着たフォノン系の電子系への結合に興味がある. イオン間相互作用は遮蔽されてきた一方で, 電子-イオン間相互作用, つまり電子-フォノン相互作用は未だ遮蔽されていない. 直感的には, 遮蔽された行列要素$\overline{g}$は
$$
\overline{g}_{ql} = \frac{g_{ql}}{\kappa(q)}\text{  , }
$$
つまり, 波数と周波数に依存した伝導電子系の誘電函数によって除算された裸の行列要素によって与えられることが期待される. 

計算においてCoulomb相互作用と同様に縦波フォノンも含めた$\Sigma(p)$の遮蔽された交換相互作用の枠内で$G(p)$を決定する. $\Sigma(p)$の遮蔽されたCoulomb相互作用に関する部分は以前与えられているが, Figure 6-10[^1]に示される1フォノン過程は
$$
\Sigma^{\text{ph}}(p) = \frac{\mathrm{i}}{(2\pi)^4}\int G_0(p+q)\lbrace\overline{g}_{ql}\rbrace^2D_l(-q)\mathrm{d}^4q
$$
で与えられる. ここで$\lbrace\overline{g}_{ql}\rbrace^2\equiv\overline{g}_{ql}\overline{g}_{-ql}$である. したがってこの近似の枠内での総自己エネルギーは
$$
\Sigma(p) = \frac{\mathrm{i}}{(2\pi)^4}\int G_0(p+q)\left[\mathcal{V}_c(q)+\lbrace\overline{g}_{ql}\rbrace^2D_l(-q)\right]\mathrm{d}^4q
$$
となる. ここでジェリウムモデルに対する関係式$2|g_{ql}|^2/\Omega_{ql}=V(q)$を用いると, 
$$
\begin{aligned}
    \Sigma(p) &= \frac{\mathrm{i}}{(2\pi)^4}\int G_0(p+q)\mathcal{V}_c(q)\frac{q_0^2}{q_0^2-\Omega_{ql}^2/\kappa(q)+\mathrm{i}\delta}\mathrm{d}^4q \\
    \text{or }\Sigma(p) &= \frac{\mathrm{i}}{(2\pi)^4}\int G_0(p+q)\frac{V(q)}{1+V(q)P(q)-\Omega_{ql}^2/q_0^2+\mathrm{i}\delta}\mathrm{d}^4q
\end{aligned}
$$
という単純な結果を得る. イオン分極は電気分極の形式と同様に$-\Omega_{ql}^2/q_0^2$という高周波数形式によって与えられるので, 第2式の分母はまさに電気的, イオン的分極を両方含めた系の誘電率となっている. そうして伝導電子間の実効的な遮蔽されたポテンシャルは, 裸のCoulombポテンシャル$V(q)$を媒質の総誘電率で割ることによって与えられる. また第1式より, $q_0^2<\omega_q^2$の場合には, 振動しているイオンによって電子間のCoulomb斥力が*過剰遮蔽*されていることに対応して, 実効ポテンシャルは引力的になり, 一方$q_0^2>\omega_q^2$の場合には, 実効的な斥力が裸の相互作用よりも強くなるために電子系とイオン系の振動の位相がずれることによって, 実効的なポテンシャルが*underscreen*される. また$q_0^2\gg\omega_q^2$に対しては, イオン分極は無視できるほどで, 裸のポテンシャルは伝導電子によってのみ遮蔽される. この誘電異常の引力的な領域が主に超伝導性に寄与する. 

遅延や減衰効果が含まれるような超伝導体を扱うには, Hamiltonianの形式ではなくGreen函数の形式を用いるのが良い理由が, Hamiltonian形式を広義的に解釈しなければならなくなるという点から述べられている. 

遮蔽された交換近似の枠内での$\Sigma(p)$に対する明白な表式を得るために, Coulomb相互作用については以前考えたから, ここではフォノンの寄与を考える. 変数を$|\bm{p}'\equiv|\bm{p}+\bm{q}|,q\equiv|\bm{q}|$, そして極軸$\bm{p}$周りの$\bm{p}'$の方角を$\varphi$とおいて, まず3次元運動量積分を実行すると, その寄与は
$$
\Sigma^{\text{ph}}(p) = \frac{\mathrm{i}}{(2\pi)^3|\bm{p}|}\int_{-\infty}^{\infty}\mathrm{d}q_0\int\frac{p'}{(p_0+q_0)(1+\mathrm{i}\delta)-\epsilon_{p'}}\mathrm{d}p'\int q\lbrace\overline{g}_{ql}\rbrace^2D_l(-q)\mathrm{d}q
$$
となる. 

都合が良いので, Fermiエネルギー$E_{\text{F}}$を基準として全てのエネルギーを測る. $D$は大きな$q_0$に対して, $1/q_0^2$の速さで減衰するので, 積分の主要な部分は典型的なフォノンのエネルギー$|q_0|\leq\omega_{\text{av}}\simeq\sqrt{m/M}E_{\text{F}}\simeq 10^{-2}E_{\text{F}}$からである. 最も重要な$|\epsilon_{p'}|$の値も$\omega_{\text{av}}$のオーダーであるので, 電子のエネルギーに関しても$|p_0|\leq\omega_{\text{av}}$に興味がある. この理由のために, $p'$-積分はその範囲を$[-\infty ,\infty]$と拡張した$\epsilon_{p'}$に関する積分
$$
\Sigma^{\text{ph}}(p) \simeq \frac{\mathrm{i}m}{(2\pi)^3\bm{p}}\int_{-\infty}^{\infty}\mathrm{d}q_0\int_{-\infty}^{\infty}\frac{1}{(p_0+q_0)(1+\mathrm{i}\delta)-\epsilon_{p'}}\mathrm{d}\epsilon_{p'}\int_0^{2k_{\text{F}}}q\lbrace\overline{g}_{ql}\rbrace^2D_l(-q)\mathrm{d}q
$$
に置き換えられる. $q$積分の上限は$|p'|\simeq k_{\text{F}}$を持つ状態のみが, 積分に強く寄与するという事実を用いればこのように簡単化された. もしフォノンの最大波数$q_{\text{m}}$が$2k_{\text{F}}$より小さいならば, $q>q_{\text{m}}$を含む遷移はウムクラップ過程と解釈されるべきであり, 適切な還元されたフォノンの波数ベクトルが$D$を計算するときに使用されなければならない. $\epsilon_{p'}$-積分を留数の方法によって実行した後, 
$$
\begin{aligned}
    \Sigma^{\text{ph}}(p) &= \frac{m}{8\pi^2p}\int_{-\infty}^{\infty}\mathrm{sgn}(p_0+q_0)\mathrm{d}q_0\int_0^{2k_{\text{F}}}q\lbrace\overline{g}_{ql}\rbrace^2D_l(-q)\mathrm{d}q \\
    &= \frac{m}{4\pi^2p}\int_0^{p_0}\mathrm{d}q_0\int_0^{2k_{\text{F}}}q\lbrace\overline{g}_{ql}\rbrace^2D_l(-q)\mathrm{d}q
\end{aligned}
$$
を得る. ここで$\lbrace\overline{g}\rbrace^2D$が$q_0$についての偶函数であるという事実を用いた. $\Im{\Sigma^{\text{ph}}(p)}$における最も重要な項は, $\lbrace\overline{g}_{ql}\rbrace^2$を実数に取り, $D_l(q)$に対する極近似をすることから来る. これらの近似の枠内で, 
$$
\Im{\Sigma^{\text{ph}}(p)} = -\frac{m}{4\pi p}\mathrm{sgn}p_0\int_0^{q(p_0)}q\lbrace\overline{g}_{ql}\rbrace^2\frac{\Omega_{ql}}{\omega_{ql}}\mathrm{d}q
$$
を得る. ここで$q(p_0)$は$\omega_{ql}=|p_0|$を満たすような波数である. 静的長波長極限によって与えられた$\kappa(q)$を持つジェリウムモデルに対して, 実フォノン放出のために, 電子の減衰率は
$$
\begin{aligned}
    2\Im{\Sigma^{\text{ph}}(p)} &= -\frac{m}{4\pi k_{\text{F}}n}\mathrm{sgn}p_0\int_0^{|p_0|}\omega^2\mathrm{d}\omega = -\frac{mM}{12\pi k_{\text{F}}n}p_0^3 \\
    \text{or }\left|\Gamma^{\text{ph}}(p)\right| &= \frac{1}{3}\left(\frac{4}{9\pi}\right)^{1/3}r_s\left|\frac{p_0}{\Omega_p}\right|^3\Omega_p
\end{aligned}
$$
によって与えられる. ここで$\Omega_p^2=4\pi NZe^2/M$はイオン的プラズマ振動数であり, $\Sigma$が$p$に関してゆっくり変化するので$p=k_{\text{F}}$と設定している. この結果は$p_0\ll\Omega_p$に対して成り立つが, 一方で最大のフォノンエネルギー$\omega_{\text{max}}\simeq\Omega_p$よりも小さな$|p_0|$に対してはそのままで, また$|p_0|>\omega_{\text{max}}$に対しては$|p_0|=\omega_{\text{max}}$と設定することによって与えられる定数を用いればかなり良い近似となる. 

$\left|2\Gamma^{\text{pair}}(p)\right|$とこれを組み合わせることによって, 電子-正孔対生成と実フォノン放出による総減衰率
$$
\left|2\Gamma(p)\right| \simeq 2\Omega_p\left[0.252\sqrt{r_s}\frac{\Omega_p}{E_{\text{F}}}\left(\frac{p_0}{\Omega_p}\right)^2+\frac{1}{3}\left(\frac{4}{9\pi}\right)^{1/3}r_s\left\lbrace\left|\frac{p_0}{\Omega_p}\right|^3,1\right\rbrace\right]
$$
を得る. ここで記法$\lbrace a,b\rbrace$は$a$と$b$の内小さい方を意味する. $\Omega_p\sim 10^{-3}E_{\text{F}}$なので, フォノン過程は超伝導性に殆ど意味を持たないような$|p_0|\sim 10^{-3}\Omega_p$程度のかなり小さなエネルギーを除いては, $|p_0|\sim 0.1E_{\text{F}}$まで減衰率を支配する. 

また$\Sigma^{\text{ph}}(p)$の実部は
$$
\Re{\Sigma^{\text{ph}}(p)} \simeq -\frac{m}{4\pi^2|p|}\int_0^{2k_{\text{F}}}q\lbrace\overline{g}_{ql}\rbrace^2\frac{\Omega_{ql}}{\omega_{ql}}\log\left|\frac{p_0+\omega_{ql}}{p_0-\omega_{ql}}\right|\mathrm{d}q
$$
によって与えられる. ジェリウムモデルに対して, これはFermi面付近の実効的な質量の補正
$$
\delta m_{\text{ph}} = m\frac{4}{\pi}\left(\frac{\pi^2}{18}\right)^{1/3}r_s\log\left(\frac{(2k_{\text{F}})^2+k_s^2}{k_s^2}\right)
$$
をもたらす. これより電子の周りにあるフォノンの雲がその実効質量を増加させることがわかる. 

ここでMigdalによってなされた重要な洞察に目を向ける. とりあえずのところは, $\Sigma$に対するダイアグラムの組として, Coulomb相互作用が遮蔽された電子-フォノン結節点と衣を着たフォノンの線に取り組むことによって暗黙のうちに含まれるものを除いて現れないようなものを考える. 簡単のために横波フォノンを無視する. このモデルの枠組みにおいて, 
$$
\Sigma^{\text{ph}}(p) = \frac{\mathrm{i}}{(2\pi)^4}\int\overline{g}_qG(p+q)\Gamma(p,q)D(q)\mathrm{d}^4q
$$
という正確な関係を得る. ここで$G$はDysonの方程式によって未知函数$\Sigma$の観点から表現される. 結節点函数$\Gamma(p,q)$は4次元運動量$p+q$を持つ電子が出射して, それぞれの4次元運動量$p$, $q$を持つ電子とフォノンが入射するような1電子線や1フォノン線によって2つの非連結な部分にダイアグラムが切断されないダイアグラムの総和によって与えられる. $\Gamma$の低次の項の幾つかがFigure 6-13a[^1]に示されている. 外線ははっきりとダイアグラムに連結されているが, $\Gamma$の定義には含まれない. この級数の2次以上の項の大きさを見積もることによって, Migdalは主要項$\overline{g}_q$に対する補正が$\sqrt{m/M}\overline{g}_q\sim 10^{-2}\overline{g}_q$のオーダーであると見積もり, それ故それらの寄与が無視できると主張した. 

Migdalの結果は, Figure 6-13a[^1]に示された結節点への最初の補正を考えることによって理解できる. $p_0$と$q_0$の興味ある値はDebyeエネルギー$\omega_{\text{D}}$以下かその程度である. 結節点を横切って交換されるフォノンのプロパゲーターは$(p_0-k_0)^{-2}$の速さで$p_0-k_0\gg\omega_{\text{D}}$に対して変化するので, $k_0$の重要な値もまた$\omega_{\text{D}}$の程度となる. ここで, 電子のプロパゲーターが定数程度の結節点への相対的補正を与えるほどに十分大きいために, 中間状態$\bm{k}$と$\bm{k}+\bm{q}$はどちらもFermi面から$\omega_{\text{D}}$程度のエネルギーの範囲になくてはならない. それ故, 相対的補正$g^2N(0)/\omega_{\text{D}}$の代わりに, $(g^2N(0)/\omega_{\text{D}})(\omega_{\text{D}}/E_{\text{F}})\sim\sqrt{m/M}$という補正を得る. 同様の理屈からより複雑な結節点補正も得られる. もしMigdalの結果を受け容れるならば, われわれは$\Sigma$に対する積分方程式を簡単に解くことが出来て
$$
\Sigma^{\text{ph}}(p) = \frac{\mathrm{i}}{(2\pi)^4}\int G(p+q)\lbrace\overline{g}_q\rbrace^2D(q)\mathrm{d}^4q
$$
を得る. 

Migdal問題における解により, 我々はもし$g_q$と$\omega_q$が与えられるならばこの問題に対するスペクトル重み函数$A(\bm{p},\omega)$を決定することができるのだが, そこでEngelsbergと著者(Schrieffer)は$g_q$と$\omega_q$に対する2つのモデル
1. フォノンのEinsteinスペクトルが定数, つまり$\omega_q=\omega_0$で$g_q$が一般的な函数である
1. Fr$\text{\"{o}}$hlichモデルのように, $g_q$と$\omega_q$が$q$に比例している

を研究した. ここから学ばれるべき主要な教訓は準粒子近似はFermiの極近傍だったり, Fermi面からかなり遠方の状態に対しては良いが, 平均フォノン周波数を$\omega_{\text{av}}$として, $\epsilon_{\bm{p}}\in [\omega_{\text{av}}/2, 2\omega_{\text{av}}]$ではスペクトル重み函数がLorentz函数やその単純な和にはならず近似が悪いということである. この近似の悪い領域で超伝導性は最も発現する. しかし幸いにも, 殆どの超伝導体の性質は$G$に対する準粒子近似を明確にしなくとも導出できる. 

## Chapter7 超伝導体に応用される場の量子論的方法
超伝導状態は常伝導相の減衰のない準粒子間に働く遅延のない相互作用の結果として扱われてきた. しかし実際の金属では, 遅延や減衰の効果が超伝導相の基礎的な理解に重要な役割を果たす. 加えて, 集団モードはゲージ不変な方法で超伝導体の電磁気的性質を理解することにおいて重要である. 

### 7-1 常伝導相の不安定性
常伝導状態は2体の引力ポテンシャルがあるとき束縛対の形成に関して不安定になる. Migdalの議論に立ち返り, ダイアグラムが不安定性をもたらすことを観るのは教育的である. Cooperによって議論された2粒子問題に対応して, 2電子が互いに多重散乱するグラフの集合をわけて考えたい. Figure 7-1[^1]に示される梯子ダイアグラムの級数がそのような効果を表すことは明白である. もし実効的なポテンシャルが引力的であるならば, 2粒子系の束縛状態, もしくは全体として系の不安定性が見つかると期待されるだろう. どのようにこれらのダイアグラムが$\Sigma(p)$に導入されるかを理解するために, Figure 7-2[^1]に示されるような電子の線の内1本がそれ自体に閉じるようなもののみを考える. もし$G$, $D$, $\Gamma$の観点からこの図のダイアグラムを解釈するならば, 不安定性はFigure 7-3[^1]に示されるダイアグラムから生じる$\Gamma$の特異なふるまいのために起こるとわかる. 

より詳細にその不安定性を理解するために, 梯子ダイアグラム級数を考える. 外部の電子線を除いて, 級数の総和は
$$
\begin{aligned}
    &<k'+q,-k'|t|k+q,-k> \\
    = &\mathcal{V}(k'-k)+\frac{\mathrm{i}}{(2\pi)^4}\int\mathcal{V}(k'-k'')G_0(k''+q)G(-k'')<k''+q,-k''|t|k+q,-k>\mathrm{d}^4k''
\end{aligned}
$$
によって定義される$t$-行列によって与えられる. 

この積分方程式は一般に解けないが, もし$\mathcal{V}(q)$をFermi面付近の殻において有限である非遅延の要素分解可能なs波ポテンシャル
$$
\begin{aligned}
    \mathcal{V}(p-k) &= \lambda_0\omega_{\bm{p}}^*\omega_{\bm{k}} \\
    \text{where }\omega_{\bm{k}} &= \begin{cases}
        1 &\text{for }|\epsilon_k|<\omega_c \\
        0 &\text{otherwise}
    \end{cases}
\end{aligned}
$$
によって置き換えれば, 解は
$$
<k'+q,-k'|t|k+q,-k> = \frac{\lambda_0\omega_{\bm{k}'+\bm{q}}^*\omega_{\bm{k}+\bm{q}}}{1-(\mathrm{i}\lambda_0/(2\pi)^4)\int\left|\omega_{\bm{p}+\bm{q}}\right|^2G_0(p+q)G_0(-p)\mathrm{d}^4p}
$$
と見出される. もし$t$が悪く振る舞うことが明らかにされるならば, 我々の近似において$\Gamma$は
$$
\Gamma(p,q) = g_q\left(1-\frac{\mathrm{i}}{(2\pi)^4}\int<p,k+q|t|p+q,k>G_0(k)G_0(k+q)\mathrm{d}^4k\right)
$$
によって与えられるので, $\Gamma$もまた悪く振る舞うだろう. また, $t$は分母が消えるとき, つまり
$$
\frac{1}{\lambda_0} = \frac{\mathrm{i}}{(2\pi)^4}\int|\omega_{\bm{p}+\bm{q}}|^2G_0(k+q)G_0(-k)\mathrm{d}^4k
$$
のとき, 特異であることがわかる. 簡単のために, 我々は電子系の相互作用している対に対する質量中心の運動量がゼロの場合(i.e. $\bm{q}=\bm{0}$)を考える. 次に特異点を取る条件を満たすような$q+0$の値を探す. $G_0$に対する表式を代入し, 留数による$k_0$-積分を実行することによって, 
$$
\frac{1}{\lambda_0} = \sum_{|\bm{k}|>k_{\text{F}}}\frac{|\omega_k|^2}{q_0-2\epsilon_k}-\sum_{|\bm{k}|<k_{\text{F}}}\frac{|\omega_k|^2}{q_0-2\epsilon_k} \equiv \tilde{\Phi}(q_0)
$$
を得る. ここで$\bm{q}=\bm{0}$に対するCooper問題の解の類似を考えることに興味があるので積分を和に置き換えた. 

$\lambda_0>0$(i.e. 斥力ポテンシャル)に対して, 摂動を受けた状態は摂動を受けていない準位間に捕獲され, 束縛状態は現れない. $\lambda_0<0$に対しても束縛状態は現れないが, $\lambda_0\ll 0$と小さくなるにつれて(i.e. 強い引力ポテンシャル), $1/\lambda_0$の線はそれが$q_0=0$付近で$\tilde{\Phi}(q_0)$におけるピークの接線になって臨界値に達するまで, $q_0$-軸に滑らかに近づいていき, 束縛状態は現れない. しかしながら, この点を超えると臨界値よりも大きい$\lambda_0$に対して消失する2つの解に対応する2つの純虚数根が現れる. これらの純虚数根は和を再び積分に直し, Fermi面近くのエネルギーにおけるBloch状態の密度が定数$N(0)$であると仮定することによって得られる. $q_0=\mathrm{i}\alpha$と設定すると, 
$$
\begin{aligned}
    \frac{1}{N(0)\lambda_0} &= \int_0^{\omega_c}\frac{\mathrm{d}\epsilon}{\mathrm{i}\alpha-2\epsilon}-\int_{-\omega_c}^0\frac{\mathrm{d}\epsilon}{\mathrm{i}\alpha-2\epsilon} \\
    &= -\frac{1}{2}\log\left(\frac{(2\omega_c)^2+\alpha^2}{\alpha^2}\right)
\end{aligned}
$$
を得る. また$N(0)\lambda_0<0$に対して, 純虚数根の組
$$
\alpha=\pm\frac{2\omega_c}{\sqrt{\exp\displaystyle\left(\frac{2}{N(0)|\lambda_0|}\right)-1}}\simeq\pm 2\omega_c\exp\left(-\frac{1}{N(0)|\lambda_0|}\right)
$$
を得る. ここで最後の変形は弱い結合$|N(0)\lambda_0|<1/4$に対して正しい. 

Cooperの2粒子問題と$t$行列の問題の結果には幾つかの重要な違いがある. 2粒子問題の解は安定な束縛状態に対応する実のエネルギーを与えた. 一方$t$問題においては, 相互作用がFermi海内に含まれるとき, 常伝導相から形成される安定した束縛状態はないが, 相関した対状態の振幅における急速な不安定な成長がみられる. 

またもう1つの違いは, 1対問題の束縛エネルギーが$\exp({-2/(N(0)|\lambda_0|)})$を含む一方で, $t$問題における増加率$\alpha$がそれより指数的に大きな$\exp({-1/(N(0)|\lambda_0|))}$を含むということである. 第2章でみたように, 超伝導状態におけるエネルギーギャップは後者の量を含み, Fermi面上部と下部両方の相関に一貫した扱いをしていることによりこの結果を得ている. 

区切りが良いのでここで終わる. 

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |
| 153 | 36 |  | $q\equiv$ |
| 156 | 11 | $\Sigma_1$ | $\Sigma$ |


## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)