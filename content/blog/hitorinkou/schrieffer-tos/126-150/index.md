---
title: 一人輪講第10回
date: "2021-01-31T02:00:00.000Z"
description: "PDF整理のための一人輪講第12回. J.R.Schrieffer, 1964, Theory of Superconductivity, 126-150 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter5 多体問題における場の理論的方法
### 5-9 摂動級数

摂動展開は2つの仮定に基づいている. 
1. もし裸の粒子(i.e. Bloch電子や裸のフォノン)を記述するゼロ次のHamiltonianが$H_0$によって表されるならば, $H_0$の基底状態は, 相互作用が時間において断熱的にかかるとき, 断熱的に相互作用している系の基底状態へ移行する. 
1. 相互作用があることによる基底状態の時間依存性と$G$と$D$の$\tau$依存性は相互作用の強さにおいてべき級数に展開される. 

これらの仮定は一見されるよりも強い制限ではない. それというのは, これらの発散する級数部分はしばしば物理的に意味のある結果を与えるために加え合わされるからである. 

簡単のために総結晶運動量(i.e. Bloch函数の運動量$\bm{k}$, フォノンの$\bm{q}$)が相互作用によって保存されると仮定することから始める. つまり, ウムクラップ過程や1体ポテンシャルの非対角部分は無視するということである. 

相互作用している系に対する1電子Green函数$G_s(\bm{p},p_0)$を計算するルールが示されている. [^1]そのルールのもとで, $\mathrm{i}G(p)$に対する摂動級数を表すダイアグラムから, 級数項が
1. 第0近似
    $$
    \mathrm{i}G_0(p) = \frac{\mathrm{i}}{p_0-\epsilon_p+\mathrm{i}\eta_p}
    $$
1. Coulomb相互作用からの最低次の "直接"寄与
    $$
    [\mathrm{i}G_0(p)]\left[(-\mathrm{i})(-1)\sum_s\frac{1}{(2\pi)^4}\int<\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>\mathrm{i}G_0(p')\mathrm{d}^4p'\right][\mathrm{i}G_0(p)]
    $$
1. 1体ポテンシャル$\tilde{U}$からの最低次の寄与
    $$
    [\mathrm{i}G_0(p)][-\mathrm{i}<\bm{p}|\tilde{U}|\bm{p}>][\mathrm{i}G_0(p)]
    $$
1. Coulomb相互作用からの最低次の "交換"寄与
    $$
    [\mathrm{i}G_0(p)]\left[(-\mathrm{i})\frac{1}{(2\pi)^4}\int<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\mathrm{i}G_0(p')\mathrm{d}^4p'\right][\mathrm{i}G_0(p)]
    $$
1. フォノン場からの最低次の寄与
    $$
    [\mathrm{i}G_0(p)]\left[(-\mathrm{i})^2\sum_{\lambda}\frac{1}{(2\pi)^4}\int|g_{pp'\lambda}|^2\mathrm{i}D_{0\lambda}(p-p')\mathrm{i}G_0(p')\mathrm{d}^4p'\right][\mathrm{i}G_0(p)]
    $$

と計算されている. 2と4の寄与は相殺するが, これは1体ポテンシャル$\tilde{U}$の定義を考えれば, 単に電子-イオン系全体の電気的中性を反映しているに過ぎないということは明らかである. 

さてここで$G$に対するこれらの寄与が
$$
G(p) \simeq G_0(p)+G_0(p)\Sigma_R(p)G_0(p)
$$
とかけることに気づくのは重要である. ここで "可約な自己エネルギー"$\Sigma_R(p)$はこの次数までで
$$
\begin{aligned}
    \Sigma_R(p) = &\frac{1}{(2\pi)^4}\int\mathrm{i}G_0(p') \\
    &\left(-\sum_s<\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>+<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>+\sum_{\lambda}|g_{pp'\lambda}|^2D_{0\lambda}(p-p')\right)\mathrm{d}^4p' \\
    &+<\bm{p}|\tilde{U}|\bm{p}>
\end{aligned}
$$
によって与えられる. というのは, $\mathrm{i}G$に対する全てのダイアグラムは裸の電子の線から始まってそれに終わるからである. $G(p)$がまた "既約の自己エネルギー"$\Sigma(p)$を用いて
$$
G(p) = G_0(p)+G_0(p)\Sigma(p)G_0(p)
$$
と表されることは重要な洞察である. ここで$\Sigma(p)$は1本の裸の電子の線を切断することによって2つの非連結部分に分けられるようなダイアグラムを除いて, $\Sigma_R(p)$を与える全てのダイアグラムの総和によって与えられている. この式は($G$に対する)Dyson方程式として知られている. ここで
$$
\frac{1}{G(p)} = \frac{1}{G_0(p)}-\Sigma(p)
$$
であり, これは$G_0(p)$に対する表式を使えば, 
$$
G(p) = \frac{1}{p_0-\epsilon_p-\Sigma(p)+\mathrm{i}\eta_p}
$$
とかける. この関係式を用いて, 
$$
\begin{aligned}
    E_p &= \epsilon_p+\Re\tilde{\Sigma}(\bm{p},E_p+\mathrm{i}\Gamma_p) \\
    \Gamma_p &= \Im\tilde{\Sigma}(\bm{p},E_p+\mathrm{i}\Gamma_p)
\end{aligned}
$$
を得る. ここで$\tilde{\Sigma}$は$\Sigma$を適切に解析接続した函数である. 

$\Sigma$への低次の寄与の議論に立ち返ると, $\Sigma_R(p)$の表式より, 
$$
\begin{aligned}
    \Sigma(p) \simeq &\frac{1}{(2\pi)^3}\int\left(2<\bm{p},\bm{p}'|V|\bm{p},\bm{p}'>-<\bm{p}',\bm{p}|V|\bm{p},\bm{p}'>\right)f_{p'}\mathrm{d}^3p' \\
    &+<\bm{p}|\tilde{U}|\bm{p}> \\
    &+\sum_{\lambda}\mathrm{i}\frac{1}{(2\pi)^4}\int|g_{pp'\lambda}|^2\frac{2\Omega_{q\lambda}}{(p_0-p_0')^2-\Omega_{q\lambda}^2+\mathrm{i}\delta}\frac{1}{p_0'-\epsilon_{p'}+\mathrm{i}\eta_{p'}}\mathrm{d}^4p'
\end{aligned}
$$
を得る. ここで$q\equiv p-p'$であり, 
$$
\begin{aligned}
    \mathrm{i}\frac{1}{2\pi}\int G_0(p')\mathrm{d}p_0' &= \mathrm{i}\frac{1}{2\pi}\int\frac{\mathrm{e}^{\mathrm{i}p_0'\delta}}{p_0'-\epsilon_{p'}+\mathrm{i}\eta_{p'}}\mathrm{d}p_0' \\
    &= -f_{p'} = \begin{cases}
        -1 &\text{if }|\bm{p}'|<p_{\text{F}} \\
        0 &\text{if }|\bm{p}'|>p_{\text{F}}
    \end{cases}
\end{aligned}
$$
という関係を用いた. 右辺の最初の2項は直接と交換のCoulomb相互作用の寄与を与え, その内前者の大部分は第3項$<\bm{p}|\tilde{U}|\bm{p}>$によって相殺される. 最後の項は
$$
\begin{aligned}
    &\mathrm{i}\frac{1}{2\pi}\int D_{0\lambda}(p-p')G_0(p')\mathrm{d}p_0' \\
    = &\mathrm{i}\frac{1}{2\pi}\int\left(\frac{1}{p_0'-p_0-\Omega_{q\lambda}+\mathrm{i}\delta}-\frac{1}{p_0'-p_0+\Omega_{q\lambda}-\mathrm{i}\delta}\right)\frac{1}{p_0'-\epsilon_{p'}+\mathrm{i}\eta_{p'}}\mathrm{d}p_0'
\end{aligned}
$$
と書き, $D_0$における最初の項が$|p'|>p_{\text{F}}$に対する寄与を持たないことに気づくことによってとても簡単に評価される. また, $D_0$の第2項は$|p'|<p_{\text{F}}$に対する寄与を持たないので, 残りの寄与は
$$
\begin{cases}
    \displaystyle\mathrm{i}\frac{1}{2\pi}\int\frac{1}{p_0'-p_0-\Omega_{q\lambda}+\mathrm{i}\delta}\frac{1}{p_0'-\epsilon_{p'}-\mathrm{i}\delta}\mathrm{d}p_0' = \frac{1}{p_0-\epsilon_{p'}+\Omega_{q\lambda}-\mathrm{i}\delta} &\text{if }|\bm{p}'|<p_{\text{F}} \\
    \displaystyle-\mathrm{i}\frac{1}{2\pi}\int\frac{1}{p_0'-p_0+\Omega_{q\lambda}-\mathrm{i}\delta}\frac{1}{p_0'-\epsilon_{p'}+\mathrm{i}\delta}\mathrm{d}p_0' = \frac{1}{p_0-\epsilon_{p'}-\Omega_{q\lambda}+\mathrm{i}\delta} &\text{if }|\bm{p}'|>p_{\text{F}}
\end{cases}
$$
とかける. そうして最低次のフォノン過程から生じる最終項は, 
$$
\Sigma^{\text{ph}}(p) = \sum_{\lambda}\frac{1}{(2\pi)^3}\int|g_{pp'\lambda}|^2\left(\frac{1-f_{p'}}{p_0-\epsilon_{p'}-\Omega_{q\lambda}+\mathrm{i}\delta}+\frac{f_{p'}}{p_0-\epsilon_{p'}+\Omega_{q\lambda}-\mathrm{i}\delta}\right)\mathrm{d}^3p'
$$
となる. 

この展開の2つの項は時間に依存する(Brillouin-Wigner)の摂動論の枠組みにおいて簡単に理解される. 系に対し, Fermi面の上部にある状態$\bm{p}$に電子を付加することを考える. エネルギー$\Omega_{q\lambda}$のフォノンが付加された電子によって仮想的に放出, そして再吸収されるというよく知られた2次の過程(Figure 5-6[^1])が, エネルギーシフト
$$
\Delta E = \sum_{\lambda}P\frac{1}{(2\pi)^3}\int\frac{|g_{pp'\lambda}|^2(1-f_{p'})}{p_0-\epsilon_{p'}-\Omega_{q\lambda}}\mathrm{d}^3p'
$$
をもたらすのである. ここで$P$は積分の主値を取ったことを意味する. 

$\Sigma^{\text{ph}}(p)$の第2項はどういうわけかより微妙であるが, 多体の摂動論の一般的な性質を示す. いまのところは, 電子が付加されることのない系を考える. 電子-フォノン相互作用によって, 系の基底状態に寄与する過程かあるだろう. そこでは, 最初Fermi海内の状態$\bm{p}'$にあった電子が仮想的にエネルギー$\Omega_{q\lambda}$のフォノンを放出し, Fermi面上部の非占有状態$\bm{p}$へ飛び移っている. 励起された電子はフォノンを再吸収することによって最初の状態へ戻ってくる(Figure 5-7[^1]). あるFermi面上部にある中間状態$\bm{p}$に対して, これらの過程は基底状態に対して
$$
\Delta E' = \sum_{\lambda}P\frac{1}{(2\pi)^3}\int\frac{|g_{pp'\lambda}|^2f_{p'}}{\epsilon_{p'}-\epsilon_p+\Omega_{q\lambda}}\mathrm{d}^3p'
$$
だけのエネルギーシフトをもたらす. ここで$P$は積分の主値を取ったことを意味する. もし電子がいま系に対して状態$\bm{p}$のところへ付加されるならば, このシフトによってもたらされるFermi海の仮想的な励起はPauliの原理によって禁止される. それ故, この項は励起エネルギーを計算するときに差し引かれなければならない. そうして, 1つの電子が状態$\bm{p}$に付加されるとき, 系の総励起エネルギーは
$$
\begin{aligned}
    &\epsilon_p+\Delta E-\Delta E' \\
    = &\epsilon_p+\sum_{\lambda}P\frac{1}{(2\pi)^3}\int|g_{pp'\lambda}|^2\left\lbrace\frac{(1-f_{p'})}{p_0-\epsilon_{p'}-\Omega_{q\lambda}}+\frac{f_{p'}}{\epsilon_{p'}-\epsilon_p+\Omega_{q\lambda}}\right\rbrace\mathrm{d}^3p'
\end{aligned}
$$
によって与えられる. これまでの議論より, 系の励起エネルギーは$G(p,p_0)$の極によっても与えられることがわかっている. つまり$g^2$の次数で
$$
E_p = \epsilon_p+\Re\tilde{\Sigma}(\bm{p},E_p) \simeq \epsilon_p+\Re\Sigma(p,\epsilon_p)
$$
を得る. $\Sigma^{\text{ph}}$に対する表式を用いれば, これらの2つの系の励起エネルギーの表式が期待通り一致することがわかる. 

$\Sigma^{\text{ph}}$の表式の解釈を完全なものとするために, 我々はこの表式を用いて, $-2\Im\Sigma(\bm{p},\epsilon_p)=2|\Gamma_p|$が付加された電子の減衰率を与えることを観る. そうすると, 
$$
-2\Im\Sigma(\bm{p},\epsilon_p) = 2\pi\sum_{\lambda}\frac{1}{(2\pi)^3}\int|g_{pp'\lambda}|^2(1-f_{p'})\delta(\epsilon_p-\epsilon_{p'}-\Omega_{q\lambda})\mathrm{d}^3p'
$$
を得, この結果が慣習的な1次の時間に依存する摂動論の結果(i.e. Fermiの黄金律)に一致することがわかる. 同様の議論によって, 付加された正孔の減衰率も与えられる. 

## Chapter6 常伝導金属における素励起

もし摂動級数の方法により常伝導金属の電気的自己エネルギーを計算しようとするならば, 小さな運動量移行$q$に対して評価されるCoulomb行列要素の特異な性質のために, 級数が発散することがすぐにわかる. 

### 6-1 Coulomb相互作用している電子気体

Coulombポテンシャルの長距離性のために生じるこの特異性は, 価電子によってこのポテンシャルを遮蔽することを表すダイアグラムの集合の総和を取ることによって回避することができる. 極限$q\to 0$における最も重要な遮蔽効果は所謂気泡ダイアグラムを足し合わせることによって与えられることが知られている. 実効的には, 元々の裸のCoulomb相互作用$V$をFigure 6-1[^1]に示される級数によって与えられる実効ポテンシャル$\mathcal{V}_c$によって置き換えることができる. この遮蔽効果に対する近似は乱雑位相近似(RPA)として知られている. 代数を簡単にするために, 運動量の保存しない過程を無視し, また, Bloch状態を平面波状態に近似してCoulomb相互作用の行列要素が$V(q)=4\pi e^2/q^2$によって与えられるとする. ルールに則ってFeynmanダイアグラムを評価すると, 
$$
\mathcal{V}_c^{\text{RPA}}(q) = V(q)-V(q)P^{\text{RPA}}(q)\mathcal{V}_c^{\text{RPA}}(q)
$$
を得る. ここでRPAにおいて評価された "既約な分極率"$P(q)$は
$$
P^{\text{RPA}} = 2\mathrm{i}\frac{1}{(2\pi)^4}\int G_0(p+q)G_0(p)\mathrm{d}^4p
$$
によって与えられる. ここに因子$2$はスピンの和から来ている. $\mathcal{V}_c^{\text{RPA}}(q)$について解くと, 
$$
\mathcal{V}_c^{\text{RPA}}(q) = \frac{V(q)}{1+V(q)P^{\text{RPA}}(q)} = \frac{V(q)}{\kappa_0(q)}
$$
を得る. ここで$q=(\bm{q},q_0)$で, $V(q)$は$\bm{q}^2$のみに依存する函数である. この表式より, 分母がRPAにおいて評価される波数ベクトルと周波数に依存する価電子の誘電函数$\kappa_0(\bm{q},q_0)$であることは明らかである. $G_0$の表式を代入することによって, 直ちに
$$
\begin{aligned}
    \Re\kappa_0(\bm{q},q_0) &= 1+V(q)\Re P^{\text{RPA}}(\bm{q},q_0) \\
    &= 1-2V(q)P\frac{1}{(2\pi)^3}\int f_p(1-f_{p+q})\frac{2(\epsilon_{p+q}-\epsilon_p)}{q_0^2-(\epsilon_{p+q}-\epsilon_p)^2}\mathrm{d}^3p
\end{aligned}
$$
がわかる. ここで$P$は積分の主値を取ったことを示す. さらに, 
$$
\Im\kappa_0(\bm{q},q_0) = 2\pi V(q)\frac{1}{(2\pi)^3}\int f_p(1-f_{p+q})\left\lbrace\delta(q_0-\epsilon_{p+q}+\epsilon_p)+\delta(q_0+\epsilon_{p+q}-\epsilon_p)\right\rbrace\mathrm{d}^3p
$$
を得る. これらの積分はLindhardによって
$$
\begin{aligned}
    &\Re\kappa_0(\bm{q},q_0) \\
    = &1+\frac{2e^2mk_{\text{F}}}{\pi q^2}\Bigg[1+\frac{k_{\text{F}}}{2q}\left\lbrace 1-\left(\frac{mq_0}{qk_{\text{F}}}+\frac{q}{2k_{\text{F}}}\right)^2\right\rbrace\log\left|\frac{1+\displaystyle\left(\frac{mq_0}{qk_{\text{F}}}+\frac{q}{2k_{\text{F}}}\right)}{1-\displaystyle\left(\frac{mq_0}{qk_{\text{F}}}+\frac{q}{2k_{\text{F}}}\right)}\right| \\
    &-\frac{k_{\text{F}}}{2q}\left\lbrace 1-\left(\frac{mq_0}{qk_{\text{F}}}-\frac{q}{2k_{\text{F}}}\right)^2\right\rbrace\log\left|\frac{1+\displaystyle\left(\frac{mq_0}{qk_{\text{F}}}-\frac{q}{2k_{\text{F}}}\right)}{1-\displaystyle\left(\frac{mq_0}{qk_{\text{F}}}-\frac{q}{2k_{\text{F}}}\right)}\right|
\end{aligned}
$$
$$
\Im\kappa_0(\bm{q},q_0) = \begin{cases}
    0 &\text{for }2m|q_0|>q^2+2qk_{\text{F}} \\
    0 &\text{for }q>2k_{\text{F}}\text{ and }2m|q_0|<q^2-2qk_{\text{F}} \\
    \displaystyle 2e^2m^2\frac{q_0}{q^3} &\text{for }q<2k_{\text{F}}\text{ and }2m|q_0|<|q^2-2qk_{\text{F}}| \\
    \displaystyle \frac{e^2mk_{\text{F}}^2}{q^3}\left\lbrace 1-\left(\frac{mq_0}{qk_{\text{F}}}-\frac{q}{2k_{\text{F}}}\right)^2\right\rbrace &\text{for }|q^2-2qk_{\text{F}}|<2m|q_0|<|q^2+2qk_{\text{F}}|
\end{cases}
$$
と計算された. $q_0=0$に対して, $\Im\kappa=0$となるので, 静的な比誘電率は
$$
\kappa_0(\bm{q},0) = 1+0.66r_s\left(\frac{k_{\text{F}}}{q}\right)^2u\left(\frac{q}{2k_{\text{F}}}\right)
$$
によって与えられる. ここで, 
$$
u(x) = \frac{1}{2}\left\lbrace 1+\frac{(1-x^2)}{2x}\log\left|\frac{1+x}{1-x}\right|\right\rbrace\text{ , }\frac{4\pi r_{\text{s}}^3a_0^3}{3}=\frac{1}{n}
$$
である. $a_0$はBohr半径を表す. 量$r_{\text{s}}$はCoulomb相互作用の強さを無次元化したものである. Thomas-Fermiの結果と共に$1/\kappa_0(\bm{q},0)$をプロットした図(Figure 6-2[^1])の主な特徴は以下の3点である. 
1. $q\to 0$の極限でThomas-Fermiの結果
    $$
    \kappa_0(\bm{q},0)=1+\frac{k_{\text{s}}^2}{q^2}
    $$
    に近づく. ここで遮蔽波数は
    $$
    k_{\text{s}}^2 = \frac{6\pi ne^2}{E_{\text{F}}}
    $$
    によって与えられる. 
1. $q\to 2k_{\text{F}}$の極限で$\mathrm{d}\kappa_0/\mathrm{d}q\to\infty$となる. これにより, 長距離に対する遮蔽されたCoulombポテンシャルの漸近形がYukawaポテンシャルではなく, 
    $$
    \mathcal{V}(r) \propto \frac{\cos(2k_{\text{F}}r+\phi)}{r^3}
    $$
    という振動する函数となることが導かれる. 
1. $q\to\infty$の極限で$\kappa_0\to 1$となる. そのため, 遮蔽はかなり大きな運動量移行に対して効果がないことが理解る. 


大きな周波数(i.e. $|q_0|\gg q^2+(2qk_{\text{F}}/2m)$)に対して, よく見慣れた極限の形式
$$
\Re\kappa(\bm{q},q_0) = 1-\frac{\omega_{\text{p}}^2}{q_0^2}\text{ , }\omega_{\text{p}}^2=\frac{4\pi ne^2}{m}
$$
を得る. 

RPAが小さな運動量移行に対して正当である一方, 全ての真空の分極過程を含む実効ポテンシャル$\mathcal{V}_c(q)$を導入することが望まれる. もし我々が単一のCoulomb相互作用線がダイアグラムを出入りしており, 単一のCoulomb相互作用線を切断することによって2つの非連結ダイアグラムに分割されないような全てのダイアグラムの総和によって定義される既約な分極率$P(q)$を導入するならば, この級数の部分和は実行可能である. $P(q)$はFigure 6-4[^1]に示されており, それより実効ポテンシャル
$$
\mathcal{V}_c(q) = \frac{V(q)}{1+V(q)P(q)} \equiv \frac{V(q)}{\kappa(q)}
$$
を得る. RPAの結果は$P(q)$に対する級数の初項のみを保持することによって与えられる. 

$\kappa(q)$に対する簡潔な表式を得るために, Heisenberg描像で評価して, 
$$
<0|T\lbrace\rho_{-q}(-\tau)\rho_{q}(0)\rbrace|0>
$$
という量を考える. ここで$\rho_q$は電子密度演算子の第$q$番目のFourier成分
$$
\rho_q(t) = \sum_s\int\mathrm{e}^{-\mathrm{i}\bm{q}\cdot\bm{r}}\psi_s^{\dagger}(\bm{r},t)\psi_s(\bm{r},t)\mathrm{d}^3r = \sum_{p,s}c_{p,s}^{\dagger}(t)c_{p+q,s}(t)
$$
である. 再度, フォノンの過程を無視する. 時間に対する上の量のFourier変換はCoulomb相互作用の強さにおいて摂動級数に展開され, それは$\mathcal{V}_c(q)$に対する級数と
$$
\begin{aligned}
    \frac{\mathcal{V}_c(q)-V(q)}{V(q)} &= \frac{1}{\kappa(q)}-1 \\
    &= -\mathrm{i}V(q)\int_{-\infty}^{\infty}\mathrm{e}^{\mathrm{i}q_0\tau}<0|T\lbrace\rho_{-q}(-\tau)\rho_{q}(0)\rbrace|0>\mathrm{d}\tau
\end{aligned}
$$
のように関連していることが簡単に確かめられる. この表式を用いれば, 我々は固定された$\bm{q}$に対する$q_0$の函数として$\kappa(\bm{q},q_0)$の解析的な構造を調べることができる. $G(\bm{p},p_0)$と同様に, $\rho_q$と$\rho_{-q}$の間に$H$の固有状態の完全系を挿入すると, スペクトル表示
$$
\frac{1}{\kappa(\bm{q},q_0)}-1 = \int_{-\infty}^{\infty}\frac{F(\bm{q},\omega)}{q_0-\omega+\mathrm{i}q_0\delta}\mathrm{d}\omega
$$
を得ることができる. ここで
$$
F(\bm{q},\omega) = \begin{cases}
    V(q)\displaystyle\sum_n|<n|\rho_{-q}|0>|^2\delta(\omega-\omega_{n0}) &\text{for }\omega>0 \\
    F(-\bm{q},|\omega|) &\text{for }\omega<0
\end{cases}
$$
である. 反転対称性を持つ系に対して, $\omega<0$のとき$F(\bm{q},\omega)=-F(\bm{q},-\omega)$となるので, 
$$
\frac{1}{\kappa(\bm{q},q_0)}-1 = \int_0^{\infty}F(\bm{q},\omega)\left(\frac{1}{q_0-\omega+\mathrm{i}q_0\delta}-\frac{1}{q_0+\omega+\mathrm{i}q_0\delta}\right)\mathrm{d}\omega
$$
となる. 

この右辺はフォノン(ボソン)Green函数のスペクトル表示と同じ形式である. これらの本質的な差異は, 重み函数$F(\bm{q},\omega)$が電子密度揺らぎ演算子の行列要素を含む一方, 縦波フォノンの重み函数$\Phi(\bm{q},\omega)$がイオン密度揺らぎ演算子の行列要素を含んでいることである. したがって, フォノンGreen函数との類似からみて, $1/\kappa(\bm{q},q_0)$極は縦波の場によって励起される電子気体のBoson的な励起を与える. 

これらのBoson的な励起の性質はRPAにおいて簡単に理解される. $1/\kappa(\bm{q},q_0)$の虚部を取ると, 
$$
\begin{aligned}
    F(\bm{q},\omega) &= \begin{cases}
        -&\displaystyle\frac{1}{\pi}\Im\frac{1}{\kappa(\bm{q},q_0)} &\text{for }q_0>0 \\
        &\displaystyle\frac{1}{\pi}\Im\frac{1}{\kappa(\bm{q},q_0)} &\text{for }q_0<0
    \end{cases} \\
    &\left(= -\frac{1}{\pi}\frac{\kappa_2(q)\mathrm{sgn}q_0}{\kappa_1^2(q)+\kappa_2^2(q)}\right)
\end{aligned}
$$
を得る. ここで$\kappa_1=\Re\kappa$, $\kappa_2=\Im\kappa$である. 

もしRPAの表式$\Re\kappa_0(\bm{q},q_0)$, $\Im\kappa_0(\bm{q},q_0)$が$\kappa_1$, $\kappa_2$に対して使われれば, ある運動量$\bm{q}$に対して, Boson的な励起は$q^2-2qk_{\text{F}}\leq q_0\leq q^2+2qk_{\text{F}}$の範囲において連続的なスペクトルを形成することがわかる. これらの励起は相互作用している系の励起に1対1対応しており, そこでは電子は最初Fermi海内の状態$\bm{p}$にあり, そこからFermi面の上部にある状態$\bm{p}+\bm{q}$に励起されている. 

背景流(backflow)について触れられているが, ここではあまり拡がりはない. 

$\kappa_1$と$\kappa_2$が共に消えるならば, $1/\kappa$はまだ特異であり得る. 長波長, 高周波数極限において, $\kappa_1$は$q_0^2=\omega_{\text{p}}^2=4\pi ne^2/m$のときに消える. したがって, 励起エネルギーが電気的プラズマ周波数に等しいような系の励起状態が存在する. これらはプラズモンと呼ばれる. 物理的には, それらは単に電子系の密度揺らぎはである. 十分大きな運動量に対して, プラズモンは1粒子的な連続スペクトル帯へ移り, 大変に減衰するのでもはや有効な物理的実体ではなくなる. 小さな$\bm{q}$に対しては, それらは縦波の摂動に対する系の応答を決定するのに重要な役割を果たす. 

遮蔽の問題を議論してきたので, これから相互作用している電子気体に対する1電子Green函数を決定する問題に立ち返る. まず系は電気的に中性であるが, 並進不変であることを仮定すると, 自己エネルギー$\Sigma$におけるHartree項は消える. これまでの遮蔽の議論より, 次に裸のCoulomb行列要素$V(q)$ではなくRPAにおいて評価される遮蔽されたポテンシャル$\mathcal{V}_c(q)$(Figure 6-6[^1])に関する交換相互作用を含めるのが自然に思われる. つまり, 自己エネルギーを
$$
\Sigma(p) = \frac{\mathrm{i}}{(2\pi)^4}\int G_0(p+q)\mathcal{V}_c(-q)\mathrm{d}^4q
$$
とする. するとFermi面近くの準粒子のエネルギーは
$$
E_p = E_{\text{F}}\left[\frac{p^2}{k_{\text{F}}^2}-0.166r_{\text{s}}\left\lbrace\frac{p}{k_{\text{F}}}(\log r_{\text{s}}+0.203)+\log r_{\text{s}}-1.80\right\rbrace\right]
$$
によって与えられ, 電子-正孔対の生成による減衰率が
$$
|2\Gamma^{\text{pair}}(p)| = 2E_{\text{F}}(0.252\sqrt{r_{\text{s}}})\left|\left(\frac{p}{k_{\text{F}}}\right)^2-1\right|
$$
となることが見出されている. 

Fermi面での準粒子の実効質量は$E_p$を微分して, 
$$
\frac{1}{m^*} = \frac{1}{k_{\text{F}}}\frac{\partial E_p}{\partial p}\Bigg|_{p=k_{\text{F}}} = \frac{1}{m}\lbrace 1-0.083r_{\text{s}}(\log r_{\text{s}}+0.203)\rbrace
$$
によって与えられる. 電気的比熱$C$は実効質量$m^*$に比例するので, 
$$
\frac{C}{C_{\text{free}}} = 1+0.083r_{\text{s}}(\log r_{\text{s}}+0.203)
$$
という関係式が見出されている. 

Coulomb相互作用が強いほど準粒子の励起エネルギーと有効質量は大きい. 

減衰率の表式より, $|\epsilon_p-E_{\text{F}}|\leq (1/5)E_{\text{F}}$の限りでは$|E(p)-E_{\text{F}}|\gg|\Gamma(p)|$において, 準粒子はうまく定義されているということがわかる. 後で見るように, この準粒子の描像は一旦フォノンが参画すれば成り立たなくなる. 

RPAによるこれらの結果は高密度極限($r_{\text{s}}<1$)でのみ厳密に正しい. しかし$r_{\text{s}}$は実際の金属に対しては通常$2<r_{\text{s}}<5$の値を持つので, これらの結果を使う際は注意しなければならない. 

区切りが良いのでここで終わる. 

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |
| 131 | 15 | $\Sigma$ | $\tilde{\Sigma}$ |
| 139 | 16 | $g$ | $q$ |

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)