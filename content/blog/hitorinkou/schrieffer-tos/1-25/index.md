---
title: 一人輪講第1回
date: "2020-11-29T02:00:00.000Z"
description: "PDF整理のための一人輪講第1回. J.R.Schrieffer, 1964, Theory of Superconductivity, 1-25 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter1 導入

超伝導とはどういった物理的現象なのか, どのように理論/実験がそれに説明を与えてきたのか, 本書で語るようなところを示した導入部である. 冒頭部にも幾つか面白い文章はあるが, この部分は後で説明されるところであるので, ここではキーワードを1単語だけ心に留めておきたい, "rigidity"である. 

### 1-1 実験事実
完全反磁性, 比熱の温度依存性, 相転移温度の同位体質量への依存性, エネルギーギャップの測定手法, 加えて電磁波と音波の吸収率と核スピン緩和率に対する2流体モデルとの矛盾まで, 実験的事実が述べられている. これらを踏まえて次の現象論的な理論のお話を歴史的な時間発展のもとで見ていくことになる. 

### 1-2 現象論的な理論
#### Gorter-Casimirモデル(1934)

2流体モデルを発展させたモデルである. 端的に言ってしまえば超流動体/通常流体の自由エネルギーそれぞれが全自由エネルギーに対して幅を利かす割合が変化するのだが, その形式はやや非直感的である, 
$$
F(x,T) = \sqrt{x}f_n(T)+(1-x)f_s(t).
$$
ここに超流動体/通常流体の自由エネルギーは
$$
f_n(T) = -\frac{1}{2}\gamma T^2\ ,\ f_s(T) = -\beta=\text{const.}
$$
で与えられている. このモデルによると比熱が相転移温度で通常状態のときと比べて3倍の飛びをみせるという帰結を得る. しかし結局のところBCSの微視的な理論との関係性は殆ど無い. 

#### Londonの理論

こちらの現象論的理論も2流体の概念に基づいた枠組みにある. 超流動体/通常流体の電流密度が, 
$$
\begin{aligned}
    \frac{\mathrm{d}\bm{J}_s}{\mathrm{d}t} &= \frac{n_se^2}{m}\bm{E}\ ,\ \bm{J}_s=-en_sv_s \\
    \bm{J}_n &= \sigma_n\bm{E}\ ,\ \bm{J}_n=-en_nv_n
\end{aligned}
$$
を満たすことを仮定する. これより超流動流が通常の伝導の機構とは別の機構を持っているように思える. さらに, 
$$
\nabla\times\bm{J}_s = -\frac{n_se^2}{mc}\bm{B}
$$
の式が仮定され, これよりMeissner-Oschenfeldの効果が導かれる. これを導くにはMaxwellの方程式より微分方程式を適当な境界条件の下で解いて磁場の減衰を認めれば良いので簡単であるが, 完全反磁性を導けたことはやはり劇的に思われる. しかしこれは最初の式の抵抗なく伝導する性質から従うので, 最後の式よりLondonが超伝導体のヒステリシスに無関係に完全反磁性となる制約を課したのが肝だった. 

Gorter-Casimirモデルと合わせてみれば, Londonの侵入長の温度依存性$\lambda(T)$が密度を介すことにより求まり(1-17)[^1], 超伝導状態で完全反磁性が現れることがさらに明らかになる. 

p12の第3段落の一文
> The fact that the supercurrents are uniquely determined by the magnetic-field configuration (according to the Meissner effect) guarantees that one can apply reversible thermodynamics to quasi-static processes in superconductors, an important fact.

はまだ理解っていない. 

次にはLondonのゲージ条件$\nabla\cdot\bm{A}=0$を満たすときに理論がゲージ不変であることが述べられているが, 結局物理的予測はゲージの選択如何に依らないと結論されている. 

#### F.LondonによるLondon理論の説明

F.LondonはLondon理論の仮定の式
$$
\bm{J}_s = -\frac{n_se^2}{mc}\bm{A}
$$
が, Londonのゲージ条件を満たすベクトルポテンシャルによる摂動に対して超流動体が堅いことを表す多体の波動関数$\Psi_s$を仮定すれば第一原理的に演繹されることを指摘した. これは通常流体では常磁性的/反磁性的電流密度が相殺するのに対し, 超流動体では摂動部の寄与が反磁性的電流密度に現れることより導かれる. 

さらに後の議論への布石としてこのLondonの"堅さ"の起源が系の励起スペクトルに現れるエネルギーギャップであるのだ, と言及されている. 

最後にLondonはLondon方程式を量子的に解釈して超伝導体に空いた穴に磁束$\Phi=hc/e$がトラップされる, さらに言えばその磁束が量子化されていると結論したことが述べられており, これを2つの中空円筒の系について考察して理解に努めている. これは波動関数$\Psi$の一価性を仮定することにより得られる. ただこれは1953のOnsagerの示唆の通り誤りで, 超流動体を形成する電子の2倍の有効電荷を持つ物質の存在から予想されるように磁束量子は半分の値$\Phi/2$を持つことが, 次のLondon理論の困難と伴に言及されている. 
> In essence the difficulty in London's argument is that there is another series of low-lying states which are distinct from London's state $\Psi_n$ and *cannot* be generated from the ground state $\Psi_0$ by a gauge transformation.

#### PippardによるLondon理論の非局所的な一般化

Londonの基礎方程式は空間の同じ位置での電流密度とベクトルポテンシャルの関係を与えるという意味で"局所的"であった. Pippardはこれらの局所的な関係を空間における任意の位置での電流を議論している位置の周りに$\xi_0$程度で空間的に平均化された場の強さとして与える非局所的な関係によって置き換えられねばならないと結論した. この一般化による最も強烈な結論は侵入長$\lambda$はバルクに不純物が十分な量印加されているとかなり長くなるというものであり, これは通常状態における電子の平均自由行程$l$がPippardのコヒーレンス長$\xi_0$程度より短くなると現れる. この長さは微視的理論におけるエネルギーギャップに関連している. PippardはBoltzmannの輸送方程式よりChamberが導いた式を念頭に置いて, 
$$
\bm{J}_s(\bm{r}) = -\frac{3}{4\pi\xi_0c\Lambda(T)}\int\frac{\bm{R}[\bm{R}\cdot\bm{A}(\bm{r}')]}{R^4}\mathrm{e}^{-\frac{R}{\xi}}\mathrm{d}\bm{r}'\ ,\ \frac{1}{\Lambda(T)}\equiv\frac{n_s(T)e^2}{m}
$$
とLondon方程式を置き換えた. 純粋な物質であればこれは元々のLondon方程式に還元されるが, 不純物が十分多いと平均自由行程が短くなることが反映されて実効的なコヒーレンス長の式(1-37)[^1]より上述の結論を得る. 

最後に$\lambda\ll\xi$という条件の重要性と$\xi$が長さ$l$によって境界を定められていることよりPippardの洞察の正当性が言及されている. 

#### Ginsburg-Landauの理論(1950)

これはLondonの理論を拡張したもので, 超流動体の密度$n_s$が空間で変化する可能性を考慮している. 新たに導入された実効的な波動関数は的な規格化条件を満たすとした, 
$$
|\Psi(\bm{r})|^2 = \frac{n_s(\bm{r})}{n}.
$$

これは結局, 実効的な波動関数は超流動部(内の確率密度を)考えているのだと言及している式だろう. 

実効的波動関数は荒っぽく言うとBCS対の質量中心の波動関数を意味している. GinsburgとLandauはこれを系の自由エネルギー関数$F(\Psi,T)$を最小化することによって, 空間内の各位置において決定される秩序変数であるとして扱った. そうすると秩序変数を求めるには系の自由エネルギーを求めることが必要で, この適切な形を推測することが目下直面する問題となる. 彼らはこれをまず秩序変数が空間的に一様/非一様の場合に分けて考察した. 

- $\Psi(\bm{r})$が空間的に一様

このとき超流動相/通常流相間での単位体積あたりの自由エネルギー差が$f(\Psi,T)$とかけるとし(但しこの形は決してアプリオリにはわかっていない), 当然この空間積分を系の自由エネルギーは含むので, $f(\Psi,T)$を求める問題に直した. 彼らはこれが$T$が$T_c$に近いときに$|\Psi|^2$冪級数に展開できるとして, 自由エネルギーを最小にするという秩序変数の条件よりその係数を求め, $f(\Psi,T)$の関数形を決定した(1-40)-(1-43)[^1]. さらにLondon理論の帰結を用いて, これを実験的に測定可能な量のみで表せるように定式化している(1-44)-(1-45)[^1]. 

- $\Psi(\bm{r})$が空間的に非一様

このとき彼らは前者の自由エネルギーに対して, 秩序変数の変化率を受け持ってくれる項を余分に加えねばならないだろうと主張した. それというのは恐らく
1. $n_s\ \text{and/or}\ v_s$を記述する多体波動関数における余分なブレに関係する運動エネルギー
1. 問題にしている位置周りの領域における超流動密度の変化に影響される相互作用エネルギー密度
から生じるだろうと考えられた. $|\Psi|^2$の空間変化が緩やかであれば$|\nabla\Psi|^2$の(いま秩序変数の変化を考えているのでこれが出る)主要項を残せば十分で, そこにベクトルポテンシャルの効果を合わせれば, 新たな自由エネルギーへの寄与は(1-46)[^1]で与えられると期待される. この式においては超流動体を形成している物質の実効電荷が$2e$に取られている. 

以上の議論より結局系の自由エネルギーは, 
$$
\begin{aligned}
    F(\Psi,T) = &\int\left(a(T)|\Psi(\bm{r})|^2+\frac{1}{2}b(T)|\Psi(\bm{r})|^4\right)\mathrm{d}\bm{r} + \int\frac{H^2(\bm{r})}{8\pi}\mathrm{d}\bm{r} \\
    &+ \int\frac{n^*}{2m^*}\left|\frac{\hbar}{\mathrm{i}}\nabla\Psi(\bm{r})+\frac{e^*}{c}\bm{A}(\bm{r})\Psi(\bm{r})\right|^2\mathrm{d}\bm{r}
\end{aligned}
$$
で与えられ, 秩序変数はこれを最小化するように選ばれることから, 次のGinsburg-Landau理論の構成方程式を得る, 
$$
\frac{\hbar^2}{2m^*}\left(\nabla+\frac{\mathrm{i}e^*}{\hbar c}\bm{A}(\bm{r})\right)^2\Psi(\bm{r})+\frac{H_c^2(T)}{4\pi n^*}\frac{\lambda^2(T)}{\lambda^2(0)}\left(1-\frac{\lambda^2(T)}{\lambda^2(0)}|\Psi(\bm{r})|^2\right)\Psi(\bm{r})=0.
$$
さらに電流密度はLondonゲージ$\nabla\cdot\bm{A}=0$にとって, 
$$
\begin{aligned}
\bm{J}_s(\bm{r}) = &-\frac{n^*|\Psi(\bm{r})|^2}{m^*c}e^{*2}\bm{A}(\bm{r}) \\
&-\frac{n^*e^*\hbar}{2m^*\mathrm{i}}\Psi^*(\bm{r})(\nabla\Psi(\bm{r})-\Psi(\bm{r})\nabla\Psi^*(\bm{r}))
\end{aligned}
$$
で与えられる, ここに波動関数の規格化条件を用いた. 

Londonの電流密度に波動関数の流れの密度が加わっている. 

構成方程式と電流密度の式, Maxwell方程式を合わせれば秩序変数に関する2本の非線形偏微分方程式が得られるが, それらは秩序変数が熱力学的平衡時の値に等しいとき($\bm{A}=\bm{0}$かつ$\Psi$が空間的に一様), その値からの1次摂動を考えて次の線形化されたGinsburg-Landauの方程式に帰着する, 
$$
\frac{\hbar^2\nabla^2}{2m^*}\tilde{\Psi}(\bm{r})-\frac{H_c^2(T)}{2\pi n^*}\frac{\lambda^2(T)}{\lambda^2(0)}\tilde{\Psi}(\bm{r})=0. 
$$
これを解いて指数の肩の分母に現れる特徴的な長さから, 我々はGinsburg-Landauの理論が電流密度とベクトルポテンシャルについて局所的な定式化がなされているにも関わらず, 非局所的効果とコヒーレンス長を自然に現出させることがわかる. 

対(また磁束量子)を正確に組み込んでおり最も完成された現象論である. 一方このままの理論では相転移温度$T_c$近傍の$T(<T_c)$にしか適用できない(上述のように摂動で扱った). 

区切りがいいのでここで終わる. 

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)
