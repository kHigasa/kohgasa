---
title: 一人輪講第4回
date: "2020-12-20T02:00:00.000Z"
description: "PDF整理のための一人輪講第4回. J.R.Schrieffer, 1964, Theory of Superconductivity, 76-100 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter3 対形成理論の応用
### 3-5 コヒーレンス因子の物理的起源
コヒーレンス因子を除いて, この章の結果は超伝導体に対する単純な1粒子エネルギーギャップモデルに基づいて推測される. しかしコヒーレンス因子の物理的起源はかなり単純である. 

Fig. 3-6[^1]で始状態から終状態への散乱の様子4つを把握できる. その散乱振幅がコヒーレンス因子の起源である. 

1体演算子である相互作用Hamiltonianはせいぜい1電子の状態を変えることのできる演算子である. したがって, 音波減衰や電磁気的吸収, 核スピン緩和, 超微細相互作用等の各相互作用において, $m,l,p$等のコヒーレンス因子が現れる. 

注意として, 粒子数は散乱過程で明らかに保存している. したがって図示されている電子が2個少ないならば, 図示されていないところにある電子が2個余分にあると解釈されるということである. 

### 3-6 電子トンネリング
これまで見てきた過程と違い, 絶縁層に隔てられた2つの金属間の電子トンネリングは$N_0\to N_0\pm n$粒子状態への遷移で各金属の粒子数が保存しない過程である. とりあえず1粒子トンネル過程に集中し, 2粒子過程は後で議論する. 

実効的Hamiltonianは, 
$$
H = H_l+H_r+H_T
$$
で表される. ここで$H_l$と$H_r$はトンネリングのないときの左右の金属に対する多体Hamiltonianで, $H_T$は2金属間で電子を遷移させる1体演算子である, 
$$
H_T = \sum_{kk's}(T_{kk'}c_{k's}^{r\dagger}c_{ks}^l+\text{H.c.})\ .
$$
Bardeenは$T{kk'}$が酸化膜の中心での電流密度行列要素によって与えられ, 酸化膜層へ侵入するにつれて指数関数的に減衰することを示した. Harrisonはこの行列要素をWKB近似によって評価し, 
$$
|T_{kk'}|^2 = \frac{1}{4\pi^2}\frac{\delta_{k\parallel,k'\parallel}}{\rho_{\perp}^r\rho_{\perp}^l}\mathrm{exp}\left(-2\int_{x_l}^{x_r}k_{\perp}(x)\mathrm{d}x\right)
$$
となることを見出した. ここで$\rho_{\perp}$は区間$[x_l,x_r]$にある皮膜障壁に対して垂直な運動に対する1次元状態密度である. 

トンネル電流を計算するために, $T=0$から考え始め, $H_T$を1次の時間に依存する摂動論によって扱う. $l\to r$の電子の遷移率は, 電子が印加電圧のために遷移過程でポテンシャルエネルギーを$V$だけ減衰させるならば, 
$$
w_{l\to r} = 2\pi\sum_{\alpha ,\beta}|<\alpha_l|<\beta_r|\sum_{kk's}T_{kk'}c_{k's}^{r\dagger}c_{ks}^l|0_l>|0_r>|^2\delta(\epsilon_{\alpha}+\epsilon_{\beta}-V)
$$
となる. ここで$|\alpha_l>,|\beta_r>$は$H_l,H_r$の左右の金属の基底エネルギーから測った固有エネルギー$\epsilon_{\alpha},\epsilon_{\beta}$に属する正確な多体の固有ベクトルである. $T=0$ではエネルギー保存則により, 電子は逆方向にトンネルすることはできない. この遷移率より, 電流密度には次のような比例関係が成り立つことがわかる, 
$$
\begin{aligned}
    I(V) &\propto \int_0^VN_{T+}^r(E)N_{T-}^l(V-E)\mathrm{d}E \\
    N_{T+}^r(E) &= \sum_{k,\beta}|<\beta_r|c_{k'}^{\dagger}|0_r>|^2\delta(\epsilon_{\beta}-E) \\
    &\simeq N_r(0)\int_{-\infty}^{\infty}\rho_r^{(+)}(\bm{k},E)\mathrm{d}\epsilon_k \\
    N_{T-}^l(E) &= \sum_{k,\alpha}|<\alpha_l|c_k|0_l>|^2\delta(\epsilon_{\alpha}-E) \\
    &\simeq N_l(0)\int_{-\infty}^{\infty}\rho_l^{(-)}(\bm{k},E)\mathrm{d}\epsilon_k \\
    \rho^{(+)}(\bm{k},\omega) &= -\frac{1}{\pi}\mathrm{Im}G(\bm{k},\omega)\ \omega\geq 0 \\
    \rho^{(-)}(\bm{k},\omega) &= \frac{1}{\pi}\mathrm{Im}G(\bm{k},-\omega)\ \omega\geq 0\ .
\end{aligned}
$$
$\rho$はスペクトル函数であり, Green函数$G$を用いて表されている. ここでFermi面についてのエネルギー領域$V$で$T_{kk'}=\mathrm{const.}$としており, トンネリングの性質を持つ超伝導体を考える際に興味のある電圧に対して良い近似である. 

超伝導体に対する実効的なトンネリング状態密度は対近似において非遅延2体ポテンシャルを用いて計算される. 粒子数を保存するB-V変換を用いて, 
$$
\begin{aligned}
    N_{T+}(E) &= \sum_{k,p}|<p|c_{k\uparrow}^{\dagger}|0>|^2\delta(E_p-E) \\
    &\simeq N(0)\int_{-\infty}^{\infty}u_k^2\delta(E_k-E)\mathrm{d}\epsilon_k \\
    &= N(0)\left|\frac{\mathrm{d}\epsilon_k}{\mathrm{d}E_k}\right|_{E_k=E} \\
    N_{T-}(E) &\simeq N(0)\int_{-\infty}^{\infty}v_k^2\delta(E_k-E)\mathrm{d}\epsilon_k \\
    &= N(0)\left|\frac{\mathrm{d}\epsilon_k}{\mathrm{d}E_k}\right|_{E_k=E}
\end{aligned}
$$
を得る. コヒーレンス因子は消える. これは丁度コヒーレンス効果なしで単純なエネルギーギャップモデルに基づいて推測された, 準粒子状態密度に等しい. 

しかしこの扱いは実際の金属においては, フォノンと電子間の相互作用に関連した強い遅延効果によって正しくない. 正しい結果は, 
$$
N_{T\pm}(E) = N(0)\mathrm{Re}\left(\frac{E}{\sqrt{E^2-\Delta^2(E)}}\right)
$$
と表される. この表式を用いれば, 常伝導金属と超伝導金属間のトンネル電流に対して, 
$$
\frac{\mathrm{d}I_S/\mathrm{d}V}{\mathrm{d}I_N/\mathrm{d}V} = \mathrm{Re}\left(\frac{V}{\sqrt{V^2-\Delta^2(V)}}\right)
$$
という関係を得る. 有限温度のトンネル電流は, $|0_l>,|0_r>$の代わりに始状態にかけての熱平均を取り, さらに左右双方向の電流を考慮すれば同様に扱われる. 

常伝導金属と超伝導金属間の1粒子トンネル過程のスケッチFig.3-9[^1]はわかりやすい. 2超伝導体間の1粒子トンネル過程のスケッチはFig.3-10[^1]に示されている. 

次にJosephsonによって示唆された超流動体の対トンネル過程に移る. 障壁への印加ポテンシャルとトンネルHamiltonianがないとき, $\nu$個の超流動対を障壁を介して移動させるのにエネルギーは必要ない. もし左から右へ$\nu$個の対が遷移する状態が$\Phi_{\nu}$によって表すと, トンネル演算子のあるときの正確な固有状態に対する強束縛近似は, 
$$
\Psi_{\alpha} = \sum_{\nu}\mathrm{e}^{\mathrm{i}\alpha\nu}\Phi_{\nu}
$$
となる. 正準運動量$\alpha$はバンド理論における波数$k$の役目を果たす. $H_T$は金属間で1電子を遷移させるだけなので, $\Phi_{\nu}$間の結合は$H_T$において2次であり, $H_T$による固有状態のエネルギーシフトは, 
$$
E_{\alpha} = \frac{<\Psi_{\alpha}|H_T^{(2)}|\Psi_{\alpha}>}{<\Psi_{\alpha}|\Psi_{\alpha}>} = -\frac{\hbar J_1}{2}\mathrm{cos}\ \alpha
$$
と求まる. ここで$H_T^{(2)}$は2次のトンネルHamiltonianで
$$
H_T^{(2)} = H_T\frac{1}{E-H_0}H_T
$$
と表され, また
$$
\hbar J_1 = 4|<\Phi_{\nu +1}|H_T^{(2)}|\Phi_{\nu}>|
$$
である. 対の遷移率は, 
$$
\frac{\mathrm{d}<\nu>}{\mathrm{d}t} = \left<\frac{\mathrm{d}E_{\alpha}}{\mathrm{d}\hbar_{\alpha}}\right> = \frac{J_1}{2}<\mathrm{sin}\ \alpha>
$$
となる. 平均は様々な$\Psi_{\alpha}$で形成される波束状態において取られている. 印加電圧がなければ, 運動量$\hbar\alpha$は運動の定数であるが, 印加電圧があれば, 
$$
\frac{\mathrm{d}<\hbar\alpha>}{\mathrm{d}t} = 2V
$$
となる. Josephson効果のこれらの式から, 障壁を介する電子の流れ率は
$$
J(t) = \frac{2\mathrm{d}<\nu>}{\mathrm{d}t} = J_1\mathrm{sin}\frac{2Vt}{\hbar}+\alpha_0
$$
とかける. 

Fig.3-11[^1]の2粒子トンネル過程のスケッチとその説明も楽しい. 

### 3-7 対形成理論の他の応用
Meissner効果やKnightシフト等への応用は第8章を待たれよ. 

## Chapter4 電子-イオン系
これまでに見たように超伝導体の性質は常伝導金属の準粒子対間に速度依存の引力ポテンシャルが働くモデルに基づいて説明される. しかし, 異方性効果や電子トンネル電流の異常性から電子-フォノン相互作用が多くの超伝導体に超伝導性をもたらすのに必要不可欠な役割を果たすことがわかる. 

### 4-1 電子-イオンHamiltonian
超伝導をより完璧に理解するために, 電子-イオン系を徹底して考えるべきであり, どのようにして第2,3章において議論された単純化されたモデルの結果が第一原理的に現れるのかを示すべきである. 

我々の議論において, イオンが伝導電子の海と相互作用し, その内殻電子は固定されている単純化された金属のモデルを考える. もちろんイオン同士, 伝導電子同士も相互作用している. 内殻電子は核の振動に断熱的に従うがそれ以外では励起されないと仮定する. またイオンの分極は無視する. これはイオン間相互作用を決めるのに重要な役割を果たすが, この効果は近似的にイオン間ポテンシャルに含まれる. さらに簡単のためにスピン-軌道相互作用(c.f. Knightシフト)も無視して良く, 微細構造と電子スピンの相互作用もとりあえず無視する. また伝導電子の軌道運動によるそれらの間の磁気相互作用(c.f. Meissner効果)もとりあえず省くと都合が良い. この相互作用は外場がなければ伝導電子間の磁気力がお互いに打ち消し合い, あまり影響をもたらさないので最初は無視して良い. 

伝導電子-イオン系のHamiltonianは, 
$$
H = \sum_i\frac{p_i^2}{2m}+\frac{1}{2}\sum_{i\neq j}\frac{e^2}{|\bm{r}_i-\bm{r}_j|}+\sum_{\nu}\frac{P_{\nu}^2}{2M_{\nu}}+\frac{1}{2}\sum_{\nu\neq\nu'}W(\bm{R}_{\nu},\bm{R}_{\nu'})+\sum_{i,\nu}U(\bm{r}_i,\bm{R}_{\nu})
$$
で与えられる. ここで$\bm{r}_i$は$i$番目の伝導電子の位置, $\bm{R}_{\nu}$は$\nu$番目のイオンの位置, $\bm{R}_{\nu}^0$はその平行位置を表している. $\nu$はセル$n$とサイト$\alpha$療法をラベル付したものである. 最初の2項は伝導電子の運動エネルギーとそれらの間の相互作用を, 第3,4項はイオンの運動エネルギーとそれらの間の相互作用を, 最後の項は伝導電子-イオン相互作用を表している. 

単位体積で周期的境界条件を持つ系に取り組む. まず本章では裸の粒子たちから議論を始める. ここで言う裸の粒子は, Bloch状態を占めている単一電子と量子化されたイオン格子振動の2種である. 

### 4-2 裸のフォノン
波数$\bm{q}$, 分極$\lambda$を持つ裸のフォノンの座標$Q_{q,\lambda}$を導入する. それらはイオンの平衡位置からの偏差$\delta\bm{R}_{\nu}$を記述する. これは次の正準変換によって導入される, 
$$
\bm{R}_{n\alpha} = \bm{R}_{n\alpha}^0+\frac{1}{\sqrt{N_cM_c}}\sum_{q,\lambda}Q_{q\lambda}\bm{\epsilon_{q\lambda}}(\alpha)\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{R}_{n\alpha}^0}\ .
$$
$Q$はイオン間相互作用が調和近似によって扱われるとき振動しているイオン系の基準座標である. $N_c$は単位体積あたりの単位セル数であり, $M_c$は単位セル内のイオンイオン質量を表している. 分極ベクトル$\bm{\epsilon_{q\lambda}}(\alpha)$は基準モードの問題を解くことによって決定される. 次の規格化条件を用いると便利である, 
$$
\sum_{\alpha}M_{\alpha}|\bm{\epsilon_{q\lambda}}(\alpha)|^2 = M_c\ .
$$
この規格化条件と各分極ベクトルの直交化条件は, あわせて
$$
\sum_{\alpha}M_{\alpha}\bm{\epsilon_{q\lambda}}(\alpha)\cdot\bm{\epsilon_{-q\lambda}}(\alpha) = M_c\delta_{\lambda\lambda'}\ .
$$
と書かれる. 波数$\bm{q}$は第一Brillouinゾーンに制限されており, ゾーンは$N_c$個のセルを含む. 独立な分極の数$\alpha_0$は単位セルにおけるイオン数の3倍であるので, フォノンのモードの総数はイオン格子の自由度に等しい. また完全性の条件も備わっており, 
$$
\sum_{q,\lambda}\bm{\epsilon_{q\lambda}}(\alpha)\cdot\bm{\epsilon_{-q\lambda}}(\alpha)\mathrm{e}^{\mathrm{i}\bm{q}\cdot(\bm{R}_{n\alpha}^0-\bm{R}_{n'\alpha'}^0)} = \delta_{nn'}\delta_{\alpha\alpha'}
$$
とかける. 

裸のフォノンの力学を完成させるために, 正準変換の運動量部分は, 
$$
\bm{P}_{n\alpha} = \sqrt{\frac{M_c}{N_c}}\sum_{q,\lambda}\Pi_{q\lambda}\bm{\epsilon_{-q\lambda}}(\alpha)\mathrm{e}^{-\mathrm{i}\bm{q}\cdot\bm{R}_{n\alpha}^0}
$$
で与えられる. ここで$\Pi_{q\lambda}$はフォノンの運動量である. イオン変数に対する正準交換関係は, 
$$
\begin{aligned}
    [\bm{P}_{n\alpha},\bm{R}_{n'\alpha'}] &= \frac{\hbar}{\mathrm{i}}\delta_{nn'}\delta_{\alpha\alpha'}\bm{1} \\
    [\bm{P}_{n\alpha},\bm{P}_{n'\alpha'}] &= [\bm{R}_{n\alpha},\bm{R}_{n'\alpha'}]=0
\end{aligned}
$$
であり, フォノン変数に対しては, 
$$
\begin{aligned}
    [\Pi_{q\lambda},Q_{q'\lambda'}] &= \frac{\hbar}{\mathrm{i}}\delta_{qq'}\delta_{\lambda\lambda'} \\
    [\Pi_{q\lambda},\Pi_{q'\lambda'}] &= [Q_{q\lambda},Q_{q'\lambda'}]=0
\end{aligned}
$$
である. ここに$\bm{1}$は単位テンソルである. この正準交換関係を用いてイオンに関する運動エネルギーと相互作用をフォノンの座標へ変換すると, 
$$
\begin{aligned}
    \sum_{\nu}\frac{\bm{P}_{\nu}^2}{2M_{\nu}}+\frac{1}{2} &\simeq \frac{1}{2}\sum_{q,\lambda}(\Pi_{q\lambda}^{\dagger}\Pi_{q\lambda}+\Omega_{q\lambda}^2Q_{q\lambda}^{\dagger}Q_{q\lambda})+\text{const.} \\
    Q_{q\lambda} &= \sqrt{\frac{\hbar}{2\Omega_{q\lambda}}}(a_{q\lambda}+a_{-q\lambda}^{\dagger}) \\
    \Pi_{q\lambda} &= \mathrm{i}\sqrt{\frac{\hbar\Omega_{q\lambda}}{2}}(a_{q\lambda}^{\dagger}+a_{-q\lambda})
\end{aligned}
$$
となる(調和振動近似して非調和項を無視した). ここで$a_{q\lambda}^{\dagger}/a_{q\lambda}$は裸のフォノンの生成/消滅演算子であり, 次のBose交換関係に従う, 
$$
\begin{aligned}
    [a_{q\lambda},a_{q'\lambda'}^{\dagger}] &= \delta_{qq'}\delta_{\lambda\lambda'} \\
    [a_{q\lambda},a_{q'\lambda'}] &= [a_{q\lambda}^{\dagger},a_{q'\lambda'}^{\dagger}]=0\ .
\end{aligned}
$$
したがって, 裸のフォノンのHamiltonianは, 
$$
H_{ph} = \sum_{q\lambda}\hbar\Omega_{q\lambda}\left(N_{q\lambda}+\frac{1}{2}\right)
$$
とかける. 

### 4-3 裸の電子
裸の伝導電子を記述するために1電子固有状態のセットを導入したいのだが, Schr$\text{\"{o}}$dinger方程式
$$
\left(\frac{p^2}{2m}+U_0\right)\chi_k = \epsilon_k\chi_k
$$
を満たす$\chi_k$は一般に内殻状態に直交していない. たとえこの方程式の伝導バンドの解がイオンが平衡位置にあるとき内殻状態に直交するとしても, その直交性はイオンが振動すると維持されない. これを解決するためにここでは擬ポテンシャルの方法ではなく, いつでも直交性を満たすように補助的な波動場を導入する. この補助波動場の運動方程式は, ポテンシャルの再定義が必要であることを除いて, 元々の波動場と同じである. このポテンシャルを第一原理から推定するのは今の所難しいので, 上述の複雑さを無視して, 1電子状態$\chi_k$を裸の伝導電子状態として使うようにする. 

これを正確に決めるためには, ポテンシャル$U_0$を定義しなければならない. $U_0$は格子と同じ周期性を持つように選ばれるはずなので, Blochの定理より
$$
\chi_k(\bm{r}+\bm{a}) = \mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{a}}\chi_k(\bm{r})
$$
を得る. ここで$\bm{a}$は結晶格子が不変であるような並進操作である. この関係が状態の結晶運動量$\hbar\bm{k}$を定義する. $\chi_k$は結晶運動量の固有函数であるが, それが波数ベクトル$\bm{k}+\bm{K}_n$を持つ平面波の線型結合として表されるので, 一般に物理的運動量の固有函数ではない. ここで$\bm{K}_n$は逆格子ベクトルで, $\bm{K}_n\cdot\bm{a}=2\pi i\ ,\ i\in\mathbb{Z}$によって定義される. 

波数ベクトル$\bm{k}$と$z$方向のスピン$s$を持つ電子に対する生成消滅演算子を$c_{k,s}^{\dagger},c_{k,s}$で定義する. これらの演算子はFermi反交換関係
$$
\begin{aligned}
    \lbrace c_{ks},c_{k's'}^{\dagger}\rbrace &= \delta_{kk'}\delta_{ss'} \\
    \lbrace c_{ks},c_{k's'}\rbrace &= \lbrace c_{ks}^{\dagger},c_{k's'}^{\dagger}\rbrace = 0\ .
\end{aligned}
$$
を満たす. したがって裸の電子のHamiltonianは
$$
H_{el} = \sum_{k,s}\epsilon_kn_{ks}
$$
とかける. 拡張ゾーン形式で記述しているので, $\bm{k}$についての和は内殻状態を除いた全ての状態について行っている. 

[4-2節](#4-2-裸のフォノン)の結果と合わせて, 0次のHamiltonianは裸の粒子のエネルギーとして, 
$$
H_0 = H_{el}+H_{ph} = \sum_{k,s}\epsilon_kn_{ks}+\sum_{q,\lambda}\hbar\Omega_{q\lambda}\left(N_{q\lambda}+\frac{1}{2}\right)
$$
とかける. 

### 4-4 裸の電子-フォノン相互作用
次に平衡位置にあるイオンと電子の相互作用ポテンシャルと全電子-イオンポテンシャルの差をイオンの変位$\delta\bm{R}_{\nu}$の摂動として扱い, 1次まで考える. したがって電子-フォノン相互作用はBoson-Fermion結合の慣習的な形式となる, つまり, Boson場, Fermion場各々について線形ということである. 

結合の計算を第一原理に立脚して行うのは困難であるが, 結合に関する幾つかの一般的な主張を作ることはできる. $i$番目の電子に作用する摂動ポテンシャルは, 
$$
\begin{aligned}
    \sum_{\nu}(U_{i,\nu}-U_{i,\nu}^0) &= -\sum_{\nu}\delta\bm{R}_{\nu}\cdot\Delta_iU_{i,\nu} \\
    &= -\frac{1}{\sqrt{N_cM_c}}\sum_{q,\lambda}Q_{q\lambda}\sum_{\nu}\bm{\epsilon}_{q\lambda}(\nu)\cdot\Delta_iU_{i,\nu}\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{R}_{\nu}^0}
\end{aligned}
$$
とかける. $U(\bm{r}_i,\bm{R}_{\nu})$を$U_{i,\nu}$と表した. $\bm{q},\lambda$の値が与えられれば, 状態$\bm{k},\bm{k}'$間の裸の電子間のポテンシャルの行列要素は, 
$$
-\frac{Q_{q\lambda}}{\sqrt{N_cM_c}}\sum_{\nu}<k'|\Delta_iU_{i,\nu}|k>\cdot\bm{\epsilon}_{q\lambda}(\nu)\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{R}_{\nu}^0}
$$
で与えられる. セルの位置$\bm{R}_n^0$と相対位置$\bm{\rho}_{\alpha}^0$より, 座標$\bm{R}_{n\alpha}^0=\bm{R}_n^0+\bm{\rho}_{\alpha}^0$を導入すれば, 行列要素はBlochの定理を用いて, 
$$
-Q_{q\lambda}\sqrt{\frac{N_c}{M_c}}\sum_{\alpha}<k'|\Delta_iU_{i,\alpha}|k>\cdot\bm{\epsilon}_{q\lambda}(\alpha)\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{\rho}_{\alpha}^0}\sum_{\bm{K}_n}\delta_{\bm{k}'-\bm{k},\bm{q}+\bm{K}_n}
$$
に還元される. これより法$\hbar\bm{K}$の下で電子-フォノン相互作用は結晶運動量を保存することが理解る. ここで行列要素は添字が紛らわしいので, 省略形として
$$
g_{kk'\lambda} \equiv -\sqrt{\frac{\hbar N_c}{2\Omega_{q\lambda}M_c}}\sum_{\alpha}<k'|\Delta_iU_{i,\alpha}|k>\cdot\bm{\epsilon}_{q\lambda}(\alpha)\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{\rho}_{\alpha}^0} = g_{k'k\lambda}^*
$$
を導入する. したがって裸の電子-フォノン相互作用は, 
$$
H_{el-ph} = \sum_{k,k',s,\lambda}g_{kk'\lambda}\varphi_{\bm{k}'-\bm{k},\lambda}c_{k's}^{\dagger}c_{ks}
$$
とかける. ここでフォノン場の振幅は
$$
\varphi_{q\lambda} = a_{q\lambda}+a_{-q\lambda}^{\dagger}
$$
で定義される. 第一原理的に行列要素$g_{k,k',\lambda}$を理解することは今の所粗い状態にあるが, ジェリウムモデルでは簡単に計算される. 

ジェリウムモデルについて縦波フォノン, 横波フォノンに対する行列要素と, Coulomb力による特異点, その力を遮蔽する効果について言及されている. 

### 4-5 電子-フォノン Hamiltonian
電子-イオンHamiltonianを完成させるために, 伝導電子間のCoulomb相互作用を含めなければならず, それは, 
$$
H_{el-el} = \frac{1}{2}\sum_{k_1,\cdots ,k_4,s,s'}<\bm{k}_3,\bm{k}_4|V|\bm{k}_1,\bm{k}_2>c_{k_3s}^{\dagger}c_{k_4s'}^{\dagger}c_{k_2s'}c_{k_1s}
$$
とかける. ここでCoulomb行列要素は, 
$$
<\bm{k}_3,\bm{k}_4|V|\bm{k}_1,\bm{k}_2> = \int\chi_{k_3}^*(\bm{r})\chi_{k_4}^*(\bm{r}')\frac{e^2}{|\bm{r}-\bm{r}'|}\chi_{k_1}(\bm{r})\chi_{k_2}(\bm{r'})\mathrm{d}\bm{r}\mathrm{d}\bm{r}'
$$
によって与えられる. Blochの定理よりCoulomb相互作用も逆格子ベクトル$\bm{K}$を法として結晶運動量を保存する, つまり$\bm{k}_1+\bm{k}_2=\bm{k}_3+\bm{k}_4=\bm{K}$を満たさなければ行列要素は消える. 

最後に1体ポテンシャルと平衡位置に固定されたイオンと電子の相互作用$U_{i,\nu}^0$の差からの寄与は, 
$$
\begin{aligned}
    H_{\tilde{U}} &= \sum_{k,K,s}<\bm{k}+\bm{K}|\tilde{U}|\bm{k}>c_{\bm{k}+\bm{K},s}^{\dagger}c_{\bm{k}s} \\
    \tilde{U} &= \sum_{\nu}U_{i,\nu}^0-U_0
\end{aligned}
$$
とかける. したがってHamiltonianの全体が
$$
H = H_{el}+H_{ph}+H_{el-ph}+H_{el-el}+H_{\tilde{U}}
$$
と得られた. 

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |
| 85 | 12 | $T$ | $H_T$ |

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)