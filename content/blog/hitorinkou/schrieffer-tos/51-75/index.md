---
title: 一人輪講第3回
date: "2020-12-13T02:00:00.000Z"
description: "PDF整理のための一人輪講第3回. J.R.Schrieffer, 1964, Theory of Superconductivity, 51-75 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter2 超伝導体の対形成理論
### 2-6 線形化された運動方程式(Valatin)
ここではBCSの有限温度の取り扱いを繰り返すのではなく, Valatinによる1粒子演算子$c_{p\uparrow}^{\dagger}$と$c_{-p\downarrow}$に対する運動方程式の線形化に基づいた方法により, 熱的性質の議論をする. 

簡約化されたHamiltonian
$$
H_{red} = \sum_{k,s'}\epsilon_kn_{ks}+\sum_{k,k'}b_{k'}^{\dagger}b_k
$$
から議論を始める. 基本的な考えは, 次の交換関係を満たすような固有演算子$\mu_{\alpha}^{\dagger}$や$\mu_{\beta}$を見つけることであり, 
$$
\begin{aligned}
    [H_{red},\mu_{\alpha}^{\dagger}] &= \Omega_{\alpha}\mu_{\alpha}^{\dagger} \\
    [H_{red},\mu_{\beta}] &= -\Omega_{\beta}\mu_{\beta}
\end{aligned}
$$
それそれ系の励起を生成と消滅させる固有演算子である. 

極端に単純な系を除いて, 正確に固有演算子を見出すことはできないが, これを近似的に満たすような演算子であれば近似的に系の励起を説明していると考えられる. 

$H_{red}$の基底状態に運動量$\bm{p}$, スピン$\uparrow$を持つ準粒子を付け加える演算子を見つけてみる. このような最も単純なFermion演算子は$c_{p\uparrow}^{\dagger}$なので, 
$$
[H_{red},c_{p\uparrow}^{\dagger}] = \epsilon_pc_{p\uparrow}^{\dagger}+\sum_{k'}V_{k'p}b_{k'}^{\dagger}c_{-p\downarrow}
$$
を考えてみる. これをみればわかるように相互作用がないとき, $c_{p\uparrow}^{\dagger}$は上の交換関係を満たし, 励起エネルギー$\Omega_{\alpha}=\epsilon_p$はちょうど系の状態$\bm{p}$に電子を加えるのに必要なエネルギーである. しかし相互作用があると固有演算子にはならない. 実際$\mu_{\alpha}^{\dagger}$を構築するのに, $c_{k\uparrow}^{\dagger}$の部分空間から外れて, $c^{\dagger}cc$のような形の積を含めなければならず, 交換関係を満たすようにするには更に高次の項を含めなければならない. 幸運なことに$H_{red}$における相互作用は$c$と$c^{\dagger}$の線型結合までだけを含めて級数をカットオフすることができるほど十分に単純である. 

$N$粒子の基底状態$|0,N>$と状態$p\uparrow$に1準粒子が存在する$N+1$粒子状態$|p\uparrow ,N+1>$の間の行列要素を取ることによって, 
$$
\begin{aligned}
    &(\Omega_{p\uparrow}-\epsilon_p)<p\uparrow ,N+1|c_{p\uparrow}^{\dagger}|0,N> \\
    = &\sum_{\alpha ,k'}V_{k'p}<p\uparrow ,N+1|c_{-p\downarrow}|\alpha ,N+2><\alpha ,N+2|b_{k'}^{\dagger}|0,N>
\end{aligned}
$$
を得る. ここで和は$N+2$の固有状態にかけて取っている. 巨大な系に対して, 中間状態の和は$N+2$粒子の基底状態のみを含めることによって得られる. これは$\alpha =0$に対する$b_{k'}^{\dagger}$の行列要素に比べて他の全ての$\alpha$に対する行列要素が小さいということではなく, $\alpha\neq 0$に対する$b_{k'}^{\dagger}$の行列要素が大きいときには$c_{-p\downarrow}$の行列要素が小さくなるので, 結果として積が小さくなり無視できるということである. したがって, この行列要素は, 
$$
\begin{aligned}
    (\Omega_p-\epsilon_p)F_p &= \sum_kV_{kp}B_kG_p \\
    F_p &= <p\uparrow ,N+1|c_{p\uparrow}^{\dagger}|0,N> \\
    G_p &= <p\uparrow ,N+1|c_{-p\downarrow}|0,N+2> \\
    B_k &= <0,N+2|b_k^{\dagger}|0,N>
\end{aligned}
$$
となる. 状態$|0,N+2>$と$|p\uparrow ,N+1>$で$[H_{red},c_{-p\downarrow}]$の行列要素を取ることによって, 中間状態の和を$\alpha =0$に対してのみ取って, 
$$
(\Omega_p+\epsilon_p)G_p = \sum_kV_{kp}B_kF_p
$$
を得る. ここで全ての量が実数となるように位相を選んでいる. したがって永年方程式は
$$
\left|
    \begin{array}{cc}
        \Omega_p-\epsilon_p & \Delta_p \cr
        \Delta_p & \Omega_p+\epsilon_p
    \end{array}
\right| = \Omega_p^2-\epsilon_p^2-\Delta_p^2 = 0
$$
となる. ギャップパラメータは, 
$$
\Delta_p = -\sum_kV_{kp}B_k
$$
によって決定される. 状態$|p\uparrow ,N+1>$の(正の)励起エネルギーは, 
$$
\Omega_p = \sqrt{\epsilon_p^2+\Delta_p^2} \equiv E_p
$$
と求まる. 負のエネルギー$-E_p$は時間反転状態$-\bm{p}\downarrow$にある準粒子が破壊される過程に対応している. 正/負の各エネルギー解に対応する固有状態$\mu_{\alpha}\equiv\gamma_{p\uparrow}^{\dagger}/\mu_{\beta}\equiv\gamma_{-p\downarrow}$は次の形である, 
$$
\begin{aligned}
    \gamma_{p\uparrow}^{\dagger} &= u_pc_{p\uparrow}^{\dagger}-v_pc_{-p\downarrow}R^{\dagger} \\
    \gamma_{-p\downarrow} &= u_pc_{-p\downarrow}+v_pRc_{p\uparrow}^{\dagger} .
\end{aligned}
$$
演算子$R^{\dagger}$は$N$粒子系におけるある状態を$N+3$粒子系の対応する状態に変換している. これら固有演算子を行列要素の式達に代入して, 反交換関係を要請すると係数について, 
$$
\begin{aligned}
    u_p^2 &= \frac{1}{2}\left(1+\frac{\epsilon_k}{E_p}\right) \\
    v_p^2 &= \frac{1}{2}\left(1-\frac{\epsilon_k}{E_p}\right) \\
    u_pv_p &= \frac{\Delta_p}{2E_p}
\end{aligned}
$$
の関係を得る. これらの結果と前節のそれらの間の形式的な類似は$|0,N>$が系の基底状態になること, つまり
$$
\gamma_{p\uparrow}|0,N> = 0\ ,\ \gamma_{-p,\downarrow}|0,N> = 0
$$
を要請すれば完全に一致する. つまり, $\gamma^{\dagger}$が真空状態$|0,N>$から相互作用のないFermionの励起を生成するのである. 

また関係を演算子$c/c^{\dagger}$について解いて, $B_k = u_kv_k$を得るので, 結局ギャップ方程式は, 
$$
\Delta_p = -\sum_kV_{kp}\frac{\Delta_k}{2E_k}
$$
となる. BCSの手法と同じ励起エネルギーが得られ, 準粒子の演算子が$R$の存在分異なるだけである. 

これらの結果を有限温度へ一般化するには, 基底状態$|0,N>$の代わりに温度$T$で励起された$|T,N>$の状態を用いるだけで良い. したがって$T\neq 0$に対して, 
$$
B_k = <T,N+2|b_k^{\dagger}|T,N> = \frac{\Delta_k}{2E_k}(1-f_{k\uparrow}-f_{-k\downarrow})
$$
であり, $f_{ks}$は状態$|T,N>$における準粒子占有数の期待値$<\gamma_{ks}^{\dagger}\gamma_{ks}>$である. 準粒子は本質的に独立なFermionなので, $f_{ks}$はFermi分布函数で与えられ, 
$$
\begin{aligned}
    f_{ks} &= \frac{1}{\mathrm{e}^{\beta E_k}+1}\ (E_k>0) \\
    B_k &= \frac{\Delta_k}{2E_k}\mathrm{tanh}\frac{\beta E_k}{2}
\end{aligned}
$$
となる. したがって有限温度でのBCSギャップ方程式
$$
\Delta_p = -\sum_kV_{kp}\frac{\Delta_k}{2E_k}\mathrm{tanh}\frac{\beta E_k}{2}
$$
を得る. ポテンシャルが
$$
V_{kp} = \begin{cases}
   -V<0 &\text{for } |\epsilon_k|\text{ and }|\epsilon_{p}|<\omega_c \\
   0 &\text{otherwise}
\end{cases}
$$
のような形であれば, ギャップ$\Delta_0(\beta)$は
$$
\frac{1}{N(0)V} = \int_0^{\omega_c}\frac{1}{\sqrt{\epsilon^2+\Delta_0^2}}\mathrm{tanh}\left(\frac{\beta\sqrt{\epsilon^2+\Delta_0^2}}{2}\right)
$$
を満たす. $T$が$0$から増加するにつれて, $\Delta_0$は減少し, $T_c$で消える. $T_c$は
$$
\frac{1}{N(0)V} = \int_0^{\omega_c}\frac{1}{\epsilon}\mathrm{tanh}\frac{\epsilon}{2kT_c}\mathrm{d}\epsilon
$$
によって与えられる. 

超伝導状態の自由エネルギー$F_s=W_s-TS$は典型的な状態$|T,N>$に関する$H_{red}$の期待値をとり, 準粒子通常流体のエントロピーに対する式$S=-2k\sum_k\lbrace f_k\mathrm{log}\ f_k+(1-f_k)\mathrm{log}(1-f_k)\rbrace$を用いることによって得られる. したがってエネルギー$W_s$は
$$
\begin{aligned}
    W_s = &2\sum_k|\epsilon_k|\left\lbrace f_k+\frac{1}{2}\left(1-\frac{|\epsilon_k|}{E_k}\right)\mathrm{tanh}\frac{\beta E_k}{2}\right\rbrace \\
    &+ \sum_k\frac{\Delta_k^2}{2E_k}\mathrm{tanh}\frac{\beta E_k}{2} +2\sum_{|k|<k_F}\epsilon_k
\end{aligned}
$$
とかける. また比熱は
$$
c_{es} = 2k\beta^2\sum_kf_k(1-f_k)\left(e_k^2+\frac{\beta}{2}\frac{\mathrm{d}\Delta_k^2}{\mathrm{d}\beta}\right)
$$
と計算され, $\Delta^2$が$T_c$で不連続であるので, 比熱も不連続になる. 

### 2-7 結びにあたっての注記
1. 凝縮しているBose-Einstein気体と$H_{red}$の基底状態は形式的に類似しているが, 実際の金属では対函数が強く重なり合っているので, Bose気体の連続的な励起スペクトルではなく, エネルギーギャップを示す離散的な励起スペクトルとなる. 
1. これまで超流動の運動量密度がゼロの状態($\text{i.e.}\ (k\uparrow ,-k\downarrow$))が扱われてきたが, 系のHamiltonianがGalilei不変性を持つならば, 有限の超流動運動量を持つ状態は, ゼロ運動量の固有函数を運動量空間において$\bm{q}/2$だけ並進させることによって得られる. 
1. これまでは結晶異方性のないs波状態の$\varphi$を持つスピンシングレットの対を集中して考えた. 
1. 基底状態$|\psi_0>$に不安定な対揺らぎは生じない. これは残留相互作用によって結合しようとしている準粒子を超流動体から生成するために有限のエネルギーが必要とされることによる. 
1. 行列要素$<\alpha ,N+2|b_k^{\dagger}|0,N>$は$N+2$粒子系の基底状態$|0,N+2>$だけでなく, 他の$\alpha$の値でも大きくなる. 特に2準粒子状態$|k\uparrow ,-k\downarrow ,N+2>$は(2-86)[^1]となり, Fermi面では$1/\sqrt{2}$となり基底状態のときと大きさが等しくなる. しかし行列要素$<p\uparrow ,N+1|c_{-p\downarrow}|\alpha ,N+2>$は2準粒子状態ではゼロとなり, 基底状態では$1/\sqrt{2}$となるから, 前述した中間状態の和を$\alpha =0$のみで取ることは正当化される. 
1. 基底状態の波動関数$|\psi_0>$は偶数個の電子を持つ系に対する基底状態の波動関数$|\psi_{0N}>$の集団平均を表している. 

## Chapter3 対形成理論の応用
BCS理論が提唱されて以来, 理論の基礎となる追走感を正当化する試みは2つのアプローチで進められた. 各アプローチは, 
1. BCS理論を超伝導体における広い範囲の現象に応用して, 対近似の理論的予測を実験に照らし合わせてチェックする
1. 様々な近似法によって, 対形成の枠組みの中で無視されている残留相互作用を扱い, それらの影響が対近似の下で予測された系の性質に大きな変更をもたらさないことを示す

というものである. 

### 3-1 対形成の仮定の正当化
ここでは1つ目のアプローチに従い対近似におけるたくさんの系の性質の計算を振り返る. 

### 3-2 音波の減衰率
電子-フォノン相互作用は常伝導状態と超伝導状態に対する波動関数を形成するときに考慮されるが, 共鳴フォノン吸収と放出の過程に対応した部分も存在する. これらの共鳴過程は音波(衣をきたフォノン)の減衰を生じさせる. 波数$\bm{q}_0$と分極$\lambda_0$を持つフォノン数$<N_{q_0\lambda_0}>$の時間変化率を計算するために, 温度$T$で励起された典型的な状態$|I>$を考える. 対近似においてそれは
$$
|I> = \prod_{k,s}\gamma_{ks}^{\dagger}\prod_{q,\lambda}a_{q\lambda}^{\dagger}\overline{N}_{q\lambda}|0,T>
$$
のような形にかける. ここで衣をきた電子-フォノン相互作用が
$$
H_{el-ph} = \sum_{pp's\lambda}\overline{g}_{pp'\lambda}(a_{q\lambda}+a_{-q\lambda}^{\dagger})c_{p's}^{\dagger}c_{ps}\ ,\ \bm{q} = \bm{p}'-\bm{p}+\bm{K}
$$
の形にかけると仮定する. $<N_{q_0\lambda_0}>$の遷移率を計算するためにFermiの黄金律を用いるが, $H_{el-ph}$の$|I>$とそれと縮退しており, 1つの粒子の状態を変更し, フォノン${\bm{q}_0\lambda_0}$を吸収または放出するような全ての状態$|F>$との間の行列要素が必要となる. 終状態は
$$
|F> = \begin{cases}
   \gamma_{p_2s}^{\dagger}\gamma_{p_1s}a_{q_0\lambda_0}|I> &\text{absorption} \\
   \gamma_{p_1s}^{\dagger}\gamma_{p_2s}a_{q_0\lambda_0}^{\dagger}|I> &\text{emission}
\end{cases}
$$
の形である. その行列要素は
$$
\begin{aligned}
    c_{p\uparrow}^{\dagger} &= u_p\gamma_{p\uparrow}^{\dagger}+v_p\gamma_{-p\downarrow}R^{\dagger} \\
    c_{p\uparrow} &= u_p\gamma_{p\uparrow}+v_pR\gamma_{-p\downarrow}^{\dagger} \\
    c_{-p\downarrow}^{\dagger} &= u_p\gamma_{-p\downarrow}^{\dagger}+v_p\gamma_{p\uparrow}R^{\dagger} \\
    c_{-p\downarrow} &= u_p\gamma_{-p\downarrow}-v_pR\gamma_{p\uparrow}^{\dagger}
\end{aligned}
$$
を用いて, $\gamma$表示に$H_{el-ph}$を書き直せばすぐに評価され, 所謂コヒーレンス因子を
$$
\begin{aligned}
    m(\bm{p},\bm{p}') &= u_pv_{p'}+v_pu_{p'} \\
    n(\bm{p},\bm{p}') &= u_pu_{p'}-v_pv_{p'}
\end{aligned}
$$
とおくと, フォノンの吸収過程の行列要素はアップスピン($s=\uparrow$)に対して, 
$$
<F|H_{el-ph}|I> = n(\bm{p},\bm{p}')\overline{\nu}_{p_1\uparrow}(1-\overline{\nu}_{p_2\uparrow})\sqrt{\overline{N}_{q_0\lambda_0}}\ \overline{g}_{p_1p_2\lambda_0}
$$
で与えられる. ここで$\overline{\nu}_{p_1\uparrow}$と$\overline{\nu}_{p_2\uparrow}$は始状態に対する準粒子の占有数であり, $0$か$1$の値を取っている. また$\overline{N}_{q_0\lambda_0}$はこの状態に対するフォノンの占有数である. そうすると$\bm{q}_0\lambda_0$のフォノンの吸収率は, 
$$
\omega_{abs} = 2\pi\times 2\sum_{\bm{p}\bm{p}'}|\overline{g}_{pp'\lambda_0}|^2n^2(\bm{p},\bm{p}')\overline{\nu}_{p\uparrow}(1-\overline{\nu}_{p'\uparrow})\overline{N}_{q_0\lambda_0}\delta(E_{p'}-E_p-\omega_{q_0\lambda_0})
$$
とかける. 同様にして, 放出率は, 
$$
\omega_{emiss} = 2\pi\times 2\sum_{\bm{p}\bm{p}'}|\overline{g}_{pp'\lambda_0}|^2n^2(\bm{p},\bm{p}')\overline{\nu}_{p'\uparrow}(1-\overline{\nu}_{p\uparrow})(\overline{N}_{q_0\lambda_0}+1)\delta(E_{p'}-E_p-\omega_{q_0\lambda_0})
$$
とかけ, 正味の吸収率は
$$
-\frac{\mathrm{d}\overline{N}_{q_0\lambda_0}}{\mathrm{d}t} = \alpha_{q_0\lambda_0}\overline{N}_{q_0\lambda_0}-S_{q_0\lambda_0}
$$
となる. ここで音波の減衰率
$$
\begin{aligned}
    \alpha_{q_0\lambda_0} &= 4\pi\sum_{\bm{p}\bm{p}'}|\overline{g}_{pp'\lambda_0}|^2n^2(\bm{p},\bm{p}')(\overline{\nu}_{p\uparrow}-\overline{\nu}_{p'\uparrow})\overline{N}_{q_0\lambda_0}\delta(E_{p'}-E_p-\omega_{q_0\lambda_0}) \\
    &= 4\pi\sum_{\bm{p}\bm{p}'}|\overline{g}_{pp'\lambda_0}|^2n^2(\bm{p},\bm{p}')(f_p-f_p')\overline{N}_{q_0\lambda_0}\delta(E_{p'}-E_p-\omega_{q_0\lambda_0})
\end{aligned}
$$
であり, $S_{q_0\lambda_0}$は自発放出率である. 和が取られたとき占有数は局所平均値に置き換えられるので, 2つ目の等号ではFermi分布函数を用いた. もし$g$が移行運動量($\text{i.e.}$通常過程に対する$\bm{q}_0$)にのみ依存するとすると, この和は
$$
\alpha_{q_0\lambda_0} = 2\pi|\overline{g}_{q_0\lambda_0}|^2\sum_{\bm{p}}\left(1+\frac{\epsilon_p\epsilon_{p'}-\Delta_p\Delta_{p'}}{E_pE_{p'}}\right)(f_p-f_p)\delta(E_{p'}-E_p-\omega_{q_0\lambda_0})
$$
と簡単になる. 

エネルギーと運動量保存により$\bm{q}_0$はFermi面に対して接線方向であることが要求される, とあるがこれは$\bm{p}\sim\bm{p}'$だからか?

Bloch状態の等エネルギー面を有効質量$m^*$を持つ球によって近似するならば, 和を積分に変えて, 
$$
\begin{aligned}
    &\alpha_{q_0\lambda_0} \\
    = &\frac{1}{2\pi}|\overline{g}_{q_0\lambda_0}|^2\frac{m^{*2}}{|\bm{q}_0|}\int\left(1-\frac{\Delta_p\Delta_{p'}}{E_pE_{p'}}\right)(f_p-f_{p'})\delta(E_{p'}-E_p-\omega_{q_0\lambda_0})\mathrm{d}\epsilon_p\mathrm{d}\epsilon_{p'} \\
    \simeq &\frac{2}{\pi}|\overline{g}_{q_0\lambda_0}|^2\frac{m^{*2}}{|\bm{q}_0|}\int_{\Delta}^{\infty}\frac{E}{\sqrt{E^2-\Delta^2}}\frac{E'}{\sqrt{E'^2-\Delta^2}}\left(1-\frac{\Delta^2}{EE'}\right)(f(E)-f(E'))\mathrm{d}E
\end{aligned}
$$
となる. ここで$E'=E+\omega_{q_0\lambda_0}$であり, $\Delta$は$\bm{p}$に独立に取っている. 多くの実験では, $\omega_{q_0\lambda_0}\ll\Delta$であり, $E=E'$とおけ, Fermi因子を除いて, 
$$
\begin{aligned}
    \alpha_{q_0\lambda_0} &= \frac{2}{\pi}|\overline{g}_{q_0\lambda_0}|^2\frac{m^{*2}}{|\bm{q}_0|}\int_{\Delta}^{\infty}\left(-\frac{\partial f}{\partial E}\right)\omega_{q_0\lambda_0}\mathrm{d}E \\
    &= \frac{2}{\pi}|\overline{g}_{q_0\lambda_0}|^2\frac{m^{*2}\omega_{q_0\lambda_0}}{|\bm{q}_0|}f(\Delta)
\end{aligned}
$$
とさらに簡単になる. この表式を常伝導状態に応用して, $\Delta =0$とおくならば, 温度$T$での超伝導相と常伝導相における音波の減衰率の比は
$$
\frac{\alpha_S(T)}{\alpha_N(T)} = \frac{f(\Delta)}{f(0)} = \frac{2}{\mathrm{exp}(\Delta(T)/kT)+1}
$$
となる. $T$が$0$から増加し$T_c$で$1$になる. 

### 3-3 核スピン緩和率
音波減衰率の計算を価電子に結合している超微細構造による核スピンの緩和率を扱うために修正するのは簡単である. 原子核スピン$\bm{I}$に対する相互作用は
$$
H_{\bm{I}\cdot\bm{S}} = A\sum_{k,k'}a_{k'}^*a_k\lbrace I_z(c_{k'\uparrow}^{\dagger}c_{k\uparrow}-c_{k'\downarrow}^{\dagger}c_{k\downarrow})+I_+c_{k'\downarrow}^{\dagger}c_{k\uparrow}+I_-c_{k'\uparrow}^{\dagger}c_{k\downarrow}\rbrace
$$
の形を取る. ここで$a_k$は問題にしている原子核のサイトでのBloch函数$\chi_k(r)$の振幅に比例しており, $a_{-k}=a_k^*$, $I_{\pm}\equiv I_x\pm\mathrm{i}I_y$である. Zeemanエネルギーと超微細エネルギーは一般にギャップエネルギーに比べて小さいので, 準粒子のスピン反転過程だけを考慮して, 終状態は
$$
|F> = \begin{cases}
   \gamma_{p_2\uparrow}^{\dagger}\gamma_{p_1\downarrow}|I> &\text{nuclear spin flips down} \\
   \gamma_{p_1\downarrow}^{\dagger}\gamma_{p_2\uparrow}|I> &\text{nuclear spin flips up}
\end{cases}
$$
の形となる. したがってコヒーレンス因子
$$
\begin{aligned}
    l(\bm{p}_1,\bm{p}_2) &= u_{p_1}u_{p_2}+v_{p_1}v_{p_2} \\
    p(\bm{p}_1,\bm{p}_2) &= u_{p_1}v_{p_2}-v_{p_1}u_{p_2}
\end{aligned}
$$
を用いて, 核スピンの下向き反転過程の行列要素は
$$
<F|H_{\bm{I}\cdot\bm{S}}|I> = Aa_{p_2}^*a_{p_1}l(\bm{p}_1,\bm{p}_2)\overline{\nu}_{p_1\downarrow}(1-\overline{\nu}_{p_2\uparrow})(I_-)_{fi}
$$
とかける. この過程の遷移率は
$$
\omega_{down} = 2\pi|A|^2\sum_{\bm{p}_1,\bm{p}_2}|a_{p_1}|^2|a_{p_2}|^2l^2(\bm{p}_1,\bm{p}_2)f_{p_1}(1-f_{p_2})\delta(E_{p_2}-E_{p_1}-\omega)N_{\uparrow}
$$
に比例する. もし結晶の異方性を無視するならば, 核スピンの$z$成分の緩和率は, 
$$
\alpha_S = 2\pi|A|^2\sum_{\bm{p}_1,\bm{p}_2}|a_{p_1}|^4\frac{1}{2}\left(1+\frac{\Delta_{p_1}\Delta_{p_2}}{E_{p_1}E_{p_2}}\right)f_{p_1}(1-f_{p_2})\delta(E_{p_2}-E_{p_1}-\omega)
$$
に比例する. またもし$\Delta$がFermi面近くで$\bm{p}$に独立であれば, $\omega\ll\Delta$の極限で, この式は
$$
\alpha_S = 2\pi|A|^2|a|^4N^2(0)\int_{\Delta}^{\infty}\left\lbrace 1+\frac{\Delta^2}{E(E+\omega)}\right\rbrace\frac{E(E+\omega)kT(-\partial f/\partial E)}{\sqrt{E^2-\Delta^2}\sqrt{(E+\omega)^2-\Delta^2}}\mathrm{d}E
$$
と書き直される. したがって, 超伝導相と常伝導相における緩和率の比は
$$
\frac{\alpha_S}{\alpha_N} = 2\int_{\Delta}^{\infty}\frac{\lbrace E(E+\omega)+\Delta^2\rbrace(-\partial f/\partial E)}{\sqrt{E^2-\Delta^2}\sqrt{(E+\omega)^2-\Delta^2}}\mathrm{d}E
$$
となる. 

### 3-4 電磁気的吸収
共鳴エネルギー吸収のもう1つの例は, 薄膜における電気伝導度$\sigma_1$の実部である. もしベクトルポテンシャル
$$
\bm{A}(\bm{r},t) = \bm{A}_0\mathrm{e}^{\mathrm{i}(\bm{q}\cdot\bm{r}-\omega t)}+\text{c.c.}
$$
によって電場を記述するならば, 1次の結合は
$$
\begin{aligned}
    H_A(t) &= -\frac{1}{c}\int\bm{j}(\bm{r})\cdot\bm{A}(\bm{r},t)\mathrm{d}^3r \\
    &= \frac{e}{2mc}\sum_{p,s}\bm{A}_0\cdot(2\bm{p}+\bm{q})c_{p+q,s}^{\dagger}c_{ps}\mathrm{e}^{-\mathrm{i}\omega t}+\text{H.c.}
\end{aligned}
$$
の形を取る. 簡単のために$T=0$で$\omega\geq 2\Delta$に対してのみ吸収が起こるとする. 前節と同様にして, 超伝導相と常伝導相における電気伝導度の比は, 
$$
\frac{\sigma_{1S}}{\sigma_{1N}} = \frac{1}{\omega}\int_{\Delta}^{\omega -\Delta}\frac{\lbrace E(\omega -E)-\Delta^2\rbrace}{\sqrt{E^2-\Delta^2}\sqrt{(\omega -E)^2-\Delta^2}}\mathrm{d}E
$$
とかける. これも実験とかなりよく一致している. 

区切りが良いのでここで終わる. 

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |
| 49 | 28 | (2-24) | (2-17) |
| 50 | 5 | (2-56b) | (2-56a) |
| 51 | 29 | $(H_{red},c_{-p\downarrow})$ | $[H_{red},c_{-p\downarrow}]$ |
| 59 | 5 | (2-32) | (2-30) |
| 64 | 27 | $\bm{p}_1$ | $\bm{p}$ |
| 70 | 1 | $\mid T>$ | $\mid I>$ |

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)