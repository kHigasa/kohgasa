---
title: 一人輪講第13回
date: "2021-02-20T02:00:00.000Z"
description: "PDF整理のための一人輪講第13回. J.R.Schrieffer, 1964, Theory of Superconductivity, 201-225 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter8 超伝導体の電磁気的性質


### 8-1 Londonの堅さ
Meissner効果のLondonによる解釈を微視的に理解する. Maxwell方程式は磁場が横波の場であることを保証するので, 時期的な摂動$H'$は系の横波励起にしか影響しない. もし超流動電子系の波動関数$\Psi_{\text{s}}$がこの弱い摂動によって本質的な影響を受けないのであれば, 1次の摂動級数振幅の2乗の和
$$
\sum_{\alpha}|a_{\alpha}|^2 = \sum_{\alpha}\left|\frac{<\Psi_{\alpha}|H'|\Psi_{\text{s}}>}{E_{\alpha}-E_{\text{s}}}\right|^2
$$
は殆どゼロになるくらい小さくなければならない. ここで$\Psi_{\alpha}$は横波励起$\alpha$の存在する状態である. おそらく, この和の以上に小さな値は, 分母の励起エネルギーが有限のままで, 分子の行列要素がゼロに近づくときに生じるだろう. ここで要求されるのは, 磁場への系の応答が空間的に非常にゆっくり変化することのみである. この極限では, Fermi面近傍の電気的状態のみが励起状態に侵入する. それ故, Meissner効果のLondonによる解釈により, 我々は次の2点を疑わしく思う, 
1. 磁場によって超流動状態から生成される横波励起に対する行列要素は空間的に非常にゆっくり変化する地場に対してゼロになるのか
1. 超流動状態の横波励起スペクトルにおけるエネルギーギャップがあるのか. 

以下でみるようにこれらの条件はBCS理論によって満足される. 

### 8-2 弱い場に対する応答
ベクトルポテンシャル$\bm{A}(\bm{r},t)$とスカラーポテンシャル$\varphi(\bm{r},t)$によって表される弱い外部印加電磁場があるときに, 単位体積の単純に接続されたバルクの超伝導体を考えることから始める. いつものように周期的境界条件を課す. 簡単のために, 
$$
A_{\mu}(x) = \begin{cases}
    A_i(x) &(\mu=i=1,2,3) \\
    c\varphi(x) &(\mu=0)
\end{cases}
$$
とかく. ここで$x\equiv (\bm{r},t)$である. $A_{\mu}$の1次までで, 電磁場と電子系の結合は
$$
\begin{aligned}
    H^{\text{p}} &= -\frac{1}{c}\int\sum_{\mu}j_{\mu}^{\text{p}}(x)A_{\mu}(x)\mathrm{d}^3r \\
    &= -\frac{1}{c}\int\left\lbrace\bm{j}^{\text{p}}(x)\cdot\bm{A}(x)-\rho_e(x)c\varphi(x)\right\rbrace\mathrm{d}^3r
\end{aligned}
$$
となる. ここで$\mu$の和において, 計量$(1,1,1,-1)$を用いている. $H^{\text{p}}$を常磁性結合と呼ぶ. 常磁性4次元電流は
$$
j_{\mu}^{\text{p}}(x) = \begin{cases}\displaystyle
    j_i^{\text{p}}(x) \equiv -\frac{e}{2m\mathrm{i}}\sum_s\left\lbrace\psi_s^{\dagger}(x)\nabla_i\psi_s(x)-[\nabla_i\psi_s^{\dagger}(x)]\psi_s(x)\right\rbrace &(\mu=i=1,2,3) \\
    \rho_e(x) = -e\displaystyle\sum_s\psi_s^{\dagger}(x)\psi_s(x) = -e\rho(x) &(\mu=0)
\end{cases}
$$
によって定義される. $(1,2,3)$成分は$A$のないときの電流密度演算子を, 最終成分は電荷密度演算子を与える. $A$があるときの物理的な電流密度は
$$
j_{\mu}(x) = j_{\mu}^{\text{p}}(x)+j_{\mu}^{\text{d}}(x)
$$
であり, 反磁性電流$j^{\text{d}}$は
$$
j_{\mu}^{\text{d}}(x) = \begin{cases}\displaystyle
    \frac{e}{mc}\rho_e(x)A_i(x) &(\mu=i=1,2,3) \\
    0 &(\mu=0)
\end{cases}
$$
によって与えられる. したがって, 摂動電磁場と電子系の全結合は
$$
H' = H^{\text{p}}+H^{\text{d}}
$$
であり, 反磁性結合は
$$
H^{\text{d}} = -\frac{e}{2mc^2}\int\rho_e(x)\sum_{i=1}^3A_i^2(x)\mathrm{d}^3r
$$
によって定義される. それ故, 系の全Hamiltonianは
$$
\mathcal{H} = H+H'
$$
となる. 

$H'$を摂動とみなし, 相互作用表示で考える, また$t\to -\infty$で$A_{\mu}\to 0$と仮定すれば, $A$があるとき系の基底状態は
$$
|\Phi(t)> = T\exp\left(-\mathrm{i}\int_{-\infty}^tH'(t')\mathrm{d}t'\right)|0> \equiv U(t,-\infty)|0>
$$
に従って時間発展する. ここで$|0>$は$H$の基底状態である. それ故, 状態$|\Phi(t)>$における電流密度の期待値は$A_{\mu}$における1次で
$$
\begin{aligned}
    J_{\mu}(x) &= <\Phi(t)|j_{\mu}(x)|\Phi(t)> \\
    &= \frac{e}{mc}<0|\rho_e(x)|0>A_{\mu}(x)(1-\delta_{\mu,0})-\mathrm{i}<0|\left[j_{\mu}^{\text{p}}(x),\int_{-\infty}^tH'(t')\mathrm{d}t'\right]|0>
\end{aligned}
$$
となる. また線形応答$J_{\mu}$と外部印加ベクトルポテンシャル$A_{\mu}$が非局所核$K_{\mu\nu}$と
$$
J_{\mu}(x) = -\frac{c}{4\pi}\sum_{\nu}\int K_{\mu\nu}(\bm{r},t;\bm{r}',t')A_{\nu}(\bm{r}',t')\mathrm{d}^3r'\mathrm{d}t'
$$
によって関連付いていることが見出される. ここで空間積分は単位体積にかけて行われ, 時間積分は$[-\infty,\infty]$で行われる. ここで電磁気応答核は
$$
\begin{aligned}
    K_{\mu\nu}(x;x') = &-\frac{4\pi\mathrm{i}}{c^2}<0|\left[j_{\mu}^{\text{p}}(x),j_{\nu}^{\text{p}}(x')\right]|0>\theta(t-t') \\
    &-\frac{4\pi c}{mc^2}<0|\rho_e(x)|0>\delta^4(x-x')\delta_{\mu\nu}(1-\delta_{\nu,0})
\end{aligned}
$$
によって与えられる. ここで$\theta$函数は
$$
\theta(t-t') = \begin{cases}
    1 &(t>t') \\
    0 &(t<t')
\end{cases}
$$
によって定義される. 

もし系が並進不変であるならば, $K_{\mu\nu}$は差$x-x'$にのみ依存する. この場合, $K_{\mu\nu}$の空間に関するFourier変換
$$
\begin{aligned}
    K_{\mu\nu}(\bm{q},t-t') &= \int K_{\mu\nu}(x;x')\mathrm{e}^{-\mathrm{i}\bm{q}\cdot (\bm{r}-\bm{r}')}\mathrm{d}^3r\mathrm{d}^3r' \\
    &= -\frac{4\pi\mathrm{i}}{c^2}<0|\left[j_{\mu}^{\text{p}}(\bm{q},t),j_{\nu}^{\text{p}}(-\bm{q},t')\right]|0>\theta(t-t')+\frac{4\pi ne^2}{mc^2}\delta(t-t')\delta_{\mu\nu}(1-\delta_{\nu,0})
\end{aligned}
$$
を行うと都合が良い. ここで$n$は単位体積あたりの電子数である. 最終辺の第2項の反磁性項は明確にわかっているので, 第1項の常磁性項に注目し, 
$$
R_{\mu\nu}(\bm{q},\tau) = -\mathrm{i}<0|\left[j_{\mu}^{\text{p}}(\bm{q},t),j_{\nu}^{\text{p}}(-\bm{q},t')\right]|0>\theta(\tau)
$$
を定義する. もし基底状態の波動関数が全ての摂動に関して堅いならば, $R_{\mu\nu}$は恒等的にゼロとなり, $J_{\mu}$はLondon方程式
$$
J_i(x) = -\frac{ne^2}{mc}A_i(x)\ (\mu=i=1,2,3)
$$
に還元される. 

### 8-3 Meissner-Ochsenfeld効果
Meissner効果が要求するのは, 核$K_{\mu\nu}$の横波部分がゼロ周波数($q_0=0$)に対する長波長極限($\bm{q}\to\bm{0}$)において有限のままであるということである. また, ゲージ不変性と電荷保存により$q_0=0$に対して, 
$$
\begin{aligned}
    \sum_{j=1}^3K_{ij}q_j = 0\ &(\text{ゲージ不変性}) \\
    \sum_iq_iK_{ij} = 0\ &(\text{電荷保存})
\end{aligned}
$$
が要求される. これらの関係が基底状態$|0>$の回転不変性に結び付けられるとき, $K_{ij}$は
$$
K_{ij}(\bm{q},0) = \left(\delta_{ij}-\frac{q_iq_j}{\bm{q}^2}\right)K(\bm{q}^2)
$$
という形を持つ. そうするとMeissner効果は$\bm{q}^2\to 0$として, 
$$
K(\bm{q}^2)>0 
$$
ということを要求する. それというのは因子$(\delta_{ij}-q_iq_j/\bm{q}^2)$がこの場合において$K_{ij}$が純粋に横波部分であることを保証するからである. 

$K$の常磁性部分を計算するために, 
$$
P_{ij}(\bm{q},\tau) = -\mathrm{i}<0|T\left\lbrace j_i(\bm{q},\tau)j_j(-\bm{q},0)\right\rbrace |0>
$$
を要求する. Nambu記法を用いると, $\bm{j}(\bm{q})$は
$$
\bm{j}(\bm{q}) = -\frac{e}{m}\sum_{\bm{k}}\left(\bm{k}+\frac{\bm{q}}{2}\right)\left(\Psi_{\bm{k}}^{\dagger}\bm{1}\Psi_{\bm{k}+\bm{q}}\right)
$$
の形となり, $P_{ij}$は
$$
P_{ij}(\bm{q},\tau) = -\frac{\mathrm{i}e^2}{m^2}\sum_{\bm{k},\bm{k}'}\left(\bm{k}+\frac{\bm{q}}{2}\right)_i\left(\bm{k}'+\frac{\bm{q}}{2}\right)_j<0|T\left\lbrace\Psi_{\bm{k}}^{\dagger}(\tau)\bm{1}\Psi_{\bm{k}+\bm{q}}(\tau)\Psi_{\bm{k}'+\bm{q}}^{\dagger}(0)\bm{1}\Psi_{\bm{k}'}(0)\right\rbrace |0>
$$
となる. Hartree分解の枠組み内で, 期待値は
$$
\begin{aligned}
&-\mathrm{Tr}\left[<0|T\left\lbrace\Psi_{\bm{k}+\bm{q}}(\tau)\Psi_{\bm{k}+\bm{q}}^{\dagger}(0)\right\rbrace |0><0|T\left\lbrace\Psi_{\bm{k}}(-\tau)\Psi_{\bm{k}}^{\dagger}(0)\right\rbrace |0>\right]\delta_{\bm{k},\bm{k}'} \\
= &\mathrm{Tr}\left[\bm{G}(\bm{k}+\bm{q},\tau)\bm{G}(\bm{k},-\tau)\right]\delta_{\bm{k},\bm{k}'}
\end{aligned}
$$
となる. 

Hartree近似の枠組み内で, $P_{ij}$の時間に関するFourier変換は
$$
P_{ij}(q) = -\frac{\mathrm{i}e^2}{(2\pi)^4m^2}\int\left(\bm{k}+\frac{\bm{q}}{2}\right)_i\left(\bm{k}+\frac{\bm{q}}{2}\right)_j\mathrm{Tr}\left[\bm{G}(\bm{k}+\bm{q},\tau)\bm{G}(\bm{k},-\tau)\right]\mathrm{d}^4k
$$
によって与えられる. ここで$q\equiv (\bm{q},q_0)$である. もし対ポテンシャルが遅延していないならば, 対近似における$\bm{G}(\bm{k})$を用いて, いま静的Meissner効果に興味があるので, $q_0=0$として, $P_{ij}$は
$$
P_{ij}(\bm{q},0) = -2\frac{e}{(2\pi)^3m^2}\int\left(\bm{k}+\frac{\bm{q}}{2}\right)_i\left(\bm{k}+\frac{\bm{q}}{2}\right)_jL(\bm{k},\bm{q})\mathrm{d}^3k
$$
に簡約される. ここで函数$L(\bm{k},\bm{q})$は
$$
\begin{aligned}
    L(\bm{k},\bm{q}) &= \frac{\mathrm{i}}{2\cdot 2\pi}\int_{-\infty}^{\infty}\mathrm{Tr}\left[\bm{G}(k+q)\bm{G}(k)\right]\mathrm{d}k_0 \\
    &= \frac{\mathrm{i}}{2\pi}\int_{-\infty}^{\infty}\frac{k_0^2+\epsilon_k\epsilon_{k+q}+\Delta_k\Delta_{k+q}}{(k_0^2-E_k^2+\mathrm{i}\delta)(k_0^2-E_{k+q}^2+\mathrm{i}\delta)}\mathrm{d}k_0 \\
    &= \frac{1}{2}\left(1-\frac{\epsilon_k\epsilon_{k+q}+\Delta_k\Delta_{k+q}}{E_k+E_{k+q}}\right)\frac{1}{E_k+E_{k+q}} \\
    &= \frac{p^2(\bm{k},\bm{k}+\bm{q})}{E_k+E_{k+q}}
\end{aligned}
$$
によって定義される. 積分は上または下半平面の閉じた経路で実行され, ここで$p(\bm{k},\bm{k}+\bm{q})$はコヒーレンス因子である. これを評価して, 絶対零度と有限温度で$K$が消えず, Meissner効果が示される. 

### 8-4 有限の$\bm{q}$と$\omega$に対する電磁気的性質
略. 

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)