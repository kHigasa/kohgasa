---
title: 一人輪講第2回
date: "2020-12-06T02:00:00.000Z"
description: "PDF整理のための一人輪講第2回. J.R.Schrieffer, 1964, Theory of Superconductivity, 26-50 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter2 超伝導体の対形成理論
常伝導金属であれば巨大系で, 1粒子励起スペクトルはゼロから始まって連続的である. 一方超伝導体では$2\Delta$から始まり, このエネルギーギャップと呼ばれるものが, 基底状態から1粒子励起状態を作るのに必要なエネルギーなのである. 

### 2-1 超伝導状態の物理的性質
ここでは超伝導体(相)の定性的な物理的性質を常伝導金属(相)と比較しながら(つまり対が形成されることによる違いを中心に)述べられている. 

また超伝導状態の結合の強さによってのBCS近似適用可否や2流体モデルの観点から微視的理論を解釈できることについて言及されている. 

### 2-2 1対問題
対相関の起源と結果を理解するために, Cooperが最初に考えた問題を考える. それは, Fermi海上の2つの相互作用している電子と相互作用のないFermi海を形成している他の電子群のモデルである. またFermi海上に他の電子も存在するがそれらはPauliの原理によりFermi海中の電子が, Fermi海上に励起していまの2体問題に参画することを妨げる役割としてのみの存在として考えている. 

並進不変な系であることとスピン依存の力を無視できることを仮定して, 対の質量中心の運動量$\hbar\bm{q}$と総スピン$S$を定数とする. このとき対の軌道波動関数は相対座標と重心座標を各々$\bm{r}=\bm{r}_1-\bm{r}_2$,$\bm{R}=(\bm{r}_1+\bm{r}_2)/2$と定義して, 
$$
\psi(\bm{r}_1,\bm{r}_2) = \varphi_q(\bm{r})\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{R}}
$$
とかける. 

相対座標の波動関数と重心の平面波に分離できるとしている. 

まず$\bm{q}=0$の場合, つまり相対座標の問題を考える. このとき対の軌道波動関数はFourier展開できて, 
$$
\psi(\bm{r}_1,\bm{r}_2) = \phi_q(\bm{r}) = \sum_{\bm{k}}a_k\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}} = \sum_{\bm{k}}a_k\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}_1}\mathrm{e}^{-\mathrm{i}\bm{k}\cdot\bm{r}_2}
$$
を得る. $\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}_1}$と$\mathrm{e}^{-\mathrm{i}\bm{k}\cdot\bm{r}_2}$という因子が運動量$\bm{k}$と$-\bm{k}$を持つ1粒子の状態を表しているとみなせるので, 対の波動関数は運動量の組$(\bm{k},-\bm{k})$が占有されている状態の重ね合わせで表されているとわかる. この波動関数を用いて, スピン$0$のときの対の固有状態を求める. 固有値$W$が求まったとして, Schr$\text{\"{o}}$dinger方程式は
$$
(\hat{H}_0+\hat{V})\psi = W\psi
$$
とかける. 波動関数を代入すれば, これは
$$
(W-2\epsilon_k)a_k = \sum_{\bm{k}'}V_{\bm{k}\bm{k}'}a_{\bm{k}'}\ ,\ V_{\bm{k}\bm{k}'} = <\bm{k},-\bm{k}|\hat{V}|\bm{k}',-\bm{k}'>
$$
に還元される. ここで系が等方的であると仮定すれば, 行列要素は
$$
V_{\bm{k}\bm{k}'} = \sum_{l=0}^{\infty}\sum_{m=-l}^lV_l(|\bm{k}|,|\bm{k}'|)Y_l^m(\Omega_\bm{k})Y_l^{-m}(\Omega_{\bm{k}'})
$$
と部分波展開できる. さらに, 
$$
V_l(|\bm{k}|,|\bm{k}'|) = \lambda_lw_k^lw_{k'}^{l*}
$$
と因数分解できるなら, 固有値$W_{lm}$が単純な計算より次の式で$\Phi(W)$と定数函数$1/\lambda_l$の交点により決定できることがわかる, 
$$
1 = \lambda_l\sum_k|w_k^l|^2\frac{1}{W_{lm}-2\epsilon_k}\equiv\lambda_l\Phi(W_{lm}).
$$
この式を見るとその交点は$W>0$では$2\epsilon_k$だけ離れた離散的な値の集合となっており, 系の箱が無限に大きくなれば連続的になっていくことがわかる. これは斥力(引力)ポテンシャル, $\lambda_l>0$($\lambda_l<0$), 双方において同様である. 一方$W<0$では引力的な$l$波ポテンシャルの下で1個だけ束縛状態が存在する(ref.図2-2[^1])

ここでポテンシャルが単純な引力ポテンシャル
$$
w_k^l =\begin{cases}
   1 &\text{if } 0<\epsilon_k<\omega_c \\
   0 &\text{otherwise}
\end{cases}
$$
の場合に, 唯1つの束縛エネルギーが1電子状態密度$N(\epsilon_k)$が$0<\epsilon_k<\omega_c$でゆっくり変化するとしてFermi面上の$N(0)$に近似して, 
$$
|W_{lm}| = \frac{2\omega_c}{\mathrm{exp}\left(\displaystyle\frac{2}{N(0)|\lambda_l|}\right)-1}
$$
と求まる. これより弱結合極限($N(0)|\lambda_l|\ll 1$)で
$$
|W_{lm}| \simeq 2\omega_c\mathrm{exp}\left(-\frac{2}{N(0)|\lambda_l|}\right)\text{,}
$$
強結合極限($N(0)|\lambda_l|\gg 1$)で
$$
|W_{lm}| \simeq N(0)|\lambda_l|\omega_c
$$
という表式を得る. 

強結合極限では引力ポテンシャルの下でのかなり(絶対値の)大きい束縛エネルギー(定数)が再現されていることがわかる. 図2-2[^1]の第3象限のことである. 

弱結合極限で束縛エネルギーは結合強度に特に敏感であり, Fermi面近くでポテンシャルが引力的である限りどんな結合強度に対しても束縛状態が存在することがわかる. これはCooperによると, 電子対がこの種の束縛状態にあるために常伝導相では不安定であることが超伝導相の発現に関係しているとのことである. 

これまでのCooperの描像は対が十分に離れていれば適切な取り扱いであるが, 実際にはかなり対の密度は高く, Cooperの描像とは逆に対が強く重なり合っている極限を考えるほうが現実に即している. ただこれまでのゼロ次近似としてのCooperの問題の考察が現実の超伝導系の性質を正しく表せていることは驚くべきことである. 

Cooperの1対問題に戻って, 束縛状態のエネルギーが質量中心の運動量$\hbar\bm{q}$によってどのように変化するかを考えることは面白い. s波部分のみのポテンシャル$V$を仮定すると, 束縛エネルギーは
$$
1=|\lambda_0|\sum_{\bm{k}}\frac{1}{|W_q|-\epsilon_{\bm{k}+\bm{q}/2}-\epsilon_{\bm{k}-\bm{q}/2}}\ \text{,}\ |\bm{k}+\bm{q}/2|\ \text{and}\ |\bm{k}-\bm{q}/2|>k_F
$$
を満たす. 和は$\epsilon_{k}=\omega_c$でカットオフされている. 小さな$q$に対しては, $|W_0|=|W_{lm}|$とおいて, 
$$
|W_q| = |W_0|-\frac{v_F\hbar q}{2}
$$
とかける. つまり$q\to 0$で最大値をとる. Cooperが上で指摘したように(それを逆に読んで)相互作用していないFermi海に対する対のドリフトが束縛エネルギーを減衰させることがわかる. 

Cooperの問題の議論に関して, 対函数$\varphi(\bm{r})$を計算すると, 束縛状態の大きさが$\sim\xi_0$のオーダーとなることがわかる. したがって束縛された対の密度はこのモデルだとかなり小さい必要があるが, 実際その密度より得られる多くの物理量の大きさが実験と照らして小さすぎるのである. また, このモデルではエネルギーギャップが再現されず, 基底状態上で連続スペクトルを得ていたことも最後に強調しておく. 

### 2-3 LandauのFermi液体の理論
Cooperの議論を振り返ってみて, その結論に対してその困難を克服するために例えば次の2つの疑問があろう. 
1. 常伝導状態における全電子間の強い相関が電子対の弱い束縛状態を壊すような揺らぎを必ずしももたらさないようにすることは可能であろうか. 
1. そのような束縛状態が金属中にあったとして, 観測された凝縮エネルギーに合うために必要とされる対の強い重なりは, 束縛された対の概念を壊してしまうような相互作用を本当にもたらすのだろうか. 

1つ目の問に答えるために, LandauのFermi液体の理論が常伝導状態の低いレベルの1粒子励起への説明を与えることを認識しておくことは重要である. Landauの理論の中で, 常伝導金属の励起状態は自由電子気体の励起状態と1対1対応しており, 常伝導状態の電子間の相互作用のもたらす本質的な効果は, 今では準粒子と呼ばれる, 電子の有効質量のシフトであると主張されている. また理論の重要な特徴は裸の電子に対して, 準粒子は(極低温で)Fermi面付近の安定した励起を表しているというものである. 実は理論中では無視されている相互作用から準粒子結合が生じるのであるが, この結合こそが超伝導体をもたらすのである. 

超伝導状態への理論的アプローチとしてこのLandauの理論から出発するようなものを考えられる. 

Landauの理論は常伝導状態をよく説明するので, その波動関数の完全系を超伝導状態の波動関数を構成するための基底として利用することは理に適っている. この手続きは超伝導状態の波動関数がもとより準粒子励起がFermi面付近でのみ存在するという常伝導状態の形態を含む点が特に魅力的である. これはCooperの結果に焼き直せば, 2電子問題が実際は2準粒子問題という意味で理解されるべきものであるということである. このアプローチには常伝導相における準粒子間の相互作用について殆どわからないという困難がある. 

2つ目の問に関して, 確かに超伝導体の基底状態を形成している束縛された電子対の単純な描像より, 対がかなり重なることでたしかに壊れることがわかる. それにも関わらずCooperの問題におけるある準粒子状態とそのペアの占有は強く連関したままである. それというのは, この対を壊してしまうような準粒子間の相互作用は, すでにLandauの常伝導状態の描像に含まれているからであり, 対相関と相互作用のない準粒子海を含む上述の単純なモデルは超伝導体の説明の出発点として不合理なものではないのである. 実際Bardeen,Cooper,そして著者(Schrieffer)はこの観点から微視的理論の構築に取り掛かったのであった. 

### 2-4 対(BCS)近似
[2-3](#2-3-landauのfermi液体の理論)節でみたように$\bm{q}=\bm{0}$の対状態が, 最大の束縛エネルギーを持つ対であるという意味で最も不安定であった. この最大の不安定性を解決することが, 同時に$\bm{q}\neq\bm{0}$における不安定性をも取り除くように系を修正することが期待される. 天下り的ではあるが, 符号が逆の運動量, 反対向きのスピンを持つ電子の対に注目することとする. 

相互作用している電子系を記述するのに第二量子化の形式を用いる. 波数$\bm{k}$とスピンの$z$成分$s=\uparrow\ \text{or}\ \downarrow$を持つ電子を生成/消滅させる演算子を$c_{ks}^{\dagger}$/$c_{ks}$, 1粒子状態$k\uparrow$と$-k\downarrow$を持つ対を生成/消滅させる演算子を$b_k^{\dagger}=c_{k\uparrow}^{\dagger}c_{-k\downarrow}^{\dagger}$/$b_k=c_{-k\downarrow}c_{k\uparrow}$とかいて, $\bm{q}=\bm{0}$の簡約化された問題(対間の相互作用のみを考慮する)に対するHamiltonianは, 
$$
\hat{H}_{red} = \sum_{ks}\epsilon_k\hat{n}_{ks}+\sum_{kk'}V_{k'k}b_{k'}^{\dagger}b_k\ ,\ V_{k'k}=<k',-k'|\hat{V}|k,-k>
$$
で与えられる. このHamiltonianがBCS理論の基礎となる. またこれは裸の電子の観点で書かれてはいるものの, 常伝導相における準粒子間の残留相互作用を記述しているモデルとしてみなされる. ポテンシャル$V$が引力的であるならば$\hat{H}_{red}$の基底状態において組の状態$(\bm{k}\uparrow,-\bm{k}\downarrow)$が1電子によって占められるという状況はありえないから, 電子数演算子$n_{k\uparrow}+n_{-k\downarrow}$を対(数)演算子を用いて$2b_k^{\dagger}b_k$と置き換えられる. したがって簡約化されたHamiltonianは
$$
\hat{H}_{red}^0 = \sum_k2\epsilon_kb_k^{\dagger}b_k+\sum_{k,k'}V_{k'k}b_{k'}^{\dagger}b_k
$$
と書き改められる. この対(ペアロン)生成/消滅演算子はBosonicだがPauliの排他原理が内部で(対を形成する電子に)効いてくる影響で, 次の独特な交換関係が導かれる, 
$$
\begin{aligned}
   [b_k,b_{k'}^{\dagger}] &= 0\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \text{for}\ \bm{k}\neq\bm{k}' \\
   [b_k,b_{k'}^{\dagger}] &= 1-(n_{k\uparrow}+n_{-k\downarrow})\ \text{for}\ \bm{k}=\bm{k}' \\
   [b_k,b_{k'}] &= [b_k^{\dagger},b_{k'}^{\dagger}] = 0.
\end{aligned}
$$

変分法を用いて$\hat{H}_{red}$の基底状態/エネルギーを見積もるために, SchriefferはTomonagaの中間結合近似を応用した. つまり素論の場合はBosonが連続して同じ軌道の状態に排出され, $N/2$ボソンの状態の軌道状態$\varphi$と$N/2$ボソンの状態の重み$A_N$が系のエネルギーを最小化することによって決定される. これを超伝導体に応用するのは幾つかの点において複雑であり, まずペアロンは正確にはBose-Einstein統計に従わない, さらに電子の総数が系において$N_0$に固定されている(素論では数に関する有限の幅を重みが持っていた)という点をうまく取り扱わないとならない. Schriefferは試行的に基底の波動関数は
$$
|\psi_0>\propto\prod_k\mathrm{e}^{g_kb_k^{\dagger}}|0> = \prod_k(1+g_kb_k^{\dagger})|0>\ \therefore\ b_k^{\dagger 2}=0
$$
とかけるとした. $g_k$は本質的に軌道波動関数$\varphi$のFourier変換である. 規格化は簡単で, 規格化された波動関数は
$$
|\psi_0> = \prod_k\frac{1+g_kb_k^{\dagger}}{(1+|g_k|^2)^{1/2}}|0>
$$
とかける. この無限乗積を展開すれば, 基底状態$|\psi_0>$は電子数が偶数のときに有限の振幅を持つことがわかる. 

前述した問題点に立ち返ると, この基底状態ならペアロン独自の統計性を考慮できているとわかる. また電子の総数が固定化された下$<\psi_0|\hat{N}|\psi_0>=<\psi_0|\sum_{ks}n_{ks}|\psi_0>=N_0$で基底状態のエネルギーを最小にするためにはLagrangeの未定乗数法を使えば良い, 
$$
\delta W = \delta<\psi_0|\hat{H}_{red}-\mu\hat{N}|\psi_0> = 0.
$$
したがって最小化されるべき量は
$$
\begin{aligned}
   W &= \sum_k2(\epsilon_k-\mu)v_k^2+\sum_{k,k'}V_{k'k}u_kv_ku_{k'}v_{k'} \\
   u_k &= \frac{1}{(1+g_k^2)^{1/2}}\ ,\ v_k=\frac{g_k}{(1+g_k^2)^{1/2}} \\
   (\therefore\ &= u_k^2+v_k^2=1)
\end{aligned}
$$
となり, 導かれた()最小化するような条件は
$$
\begin{aligned}
    u_k^2 &= \frac{1}{2}\left(1+\frac{\epsilon_k-\mu}{E_k}\right)\ ,\ v_k^2 = \frac{1}{2}\left(1-\frac{\epsilon_k-\mu}{E_k}\right) \\
    u_kv_k &= \frac{\Delta_k}{2E_k} \\
    E_k &\equiv \lbrace(\epsilon_k-\mu)^2+\Delta_k^2\rbrace^{1/2}
\end{aligned}
$$
である. エネルギーギャップパラメータはギャップ方程式
$$
\Delta_k = -\sum_{k'}V_{kk'}\frac{\Delta_{k'}}{2E_{k'}}
$$
によって決定される. ここで係数$u_k,v_k$,エネルギーギャップ$\Delta_k$は実数体上に取られている. 1粒子エネルギー$\epsilon_k$は常伝導状態におけるFermiエネルギーを基準に測られ, $\mu$は単に常伝導状態と超伝導状態間の化学ポテンシャルのシフトを表している. $\mu =0$としてギャップ方程式を解くのは良い近似となる. ギャップ方程式を解けば常伝導状態と超伝導状態のエネルギー差が得られるが, ポテンシャルが次のような形(s波)のときは明確に解ける, 
$$
V_{kk'} = \begin{cases}
   -V<0 &\text{for } |\epsilon_k|\text{ and }|\epsilon_{k'}|<\omega_c \\
   0 &\text{otherwise .}
\end{cases}
$$
ギャップ方程式を解くのは簡単(ref.[PDF](#pdf))で
$$
\Delta_k = \begin{cases}
   \displaystyle\Delta_0\equiv\frac{\omega_c}{\mathrm{sinh}\left(\frac{1}{N(0)V}\right)} &\text{for } |\epsilon_k|\leq\omega_c \\
   0 &\text{otherwise .}
\end{cases}
$$
と求まり, 弱結合極限$N(0)V\leq 1/4$で$\simeq 2\omega_c\mathrm{exp}(-1/N(0)V)$と近似され, 最小化されるべき基底状態のエネルギーに戻るとそこから常伝導相の基底状態のエネルギーを引いて, 凝縮エネルギーが
$$
W_N-W_S = \frac{1}{2}N(0)\Delta_0^2
$$
と求まる. 基底状態の波動関数に話を戻すと, Tomonagaの枠組みにおける基底を予期して, $|\psi_0>$をN粒子空間へ射影すると座標表示で
$$
\begin{aligned}
    \psi_{0N} &\equiv <r_1,s_1;\cdots r_N,s_N|\psi_0> \\
    &= \mathcal{A}\varphi(\bm{r}_1-\bm{r}_2)\chi_{12}\cdots\varphi(\bm{r}_{N-1}-\bm{r}_N)\chi_{N-1,N}
\end{aligned}
$$
という函数を得る. $\varphi$は対の相対座標波動関数(全対に対して同じマッピング)であり, 
$$
\varphi(\bm{r})=\sum_kg_k\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{r}}
$$
とかけ, $\chi_{ij}$は対応するスピン函数$\uparrow (i)\downarrow (j)$である. つまり対近似においては基底状態の波動関数においては同じ状態にあることがわかる. また$\mathcal{A}$は全体の函数を反対称化する演算子である. 

### 2-5 準粒子励起
BCSの簡約化されたHamiltonianの励起状態を見るために, 系の状態$\bm{p}\uparrow$に1個の電子を付け加えることを考える(状態$-\bm{p}\downarrow$は対形成のための相互作用をしないように空にしておく). 

基底状態と比べたときの励起状態のエネルギーの増分$W_{diff}$は, 添加した電子の1粒子エネルギーを$\epsilon_p$として, 上述の変換を用いれば, 
$$
\begin{aligned}
   W_{diff} &\equiv W_{p\uparrow} - W_0 = \epsilon_p-2\epsilon_pv_p^2-2\sum_{k'}V_{pk'}u_{k'}v_{k'}u_pv_p \\
   &= \epsilon_p(1-2v_p^2)+2\Delta_pu_pv_p \\
   &= \frac{\epsilon_p^2}{E_p}+\frac{\Delta_p^2}{E_p} = E_p
\end{aligned}
$$
と求まる. ここになって上で定義した$E_p$の物理的意味が状態$\bm{p}\uparrow$に1つの準粒子を生成するのに必要とされるエネルギーであるとわかった. 電子を添加したことにより化学ポテンシャルがシフトするがその$E_p$への影響は巨大系で小さいので無視している. また, 1粒子的な励起を超伝導の基底状態から作るのに必要な最小のエネルギーは$2\Delta_0$で, それぞれ一方の状態から電子を取り除く, 他方の状態に電子を付け加えるエネルギー$\Delta_0$に使われている. 

$E_p$は$|\bm{p}|>p_f$で得られるが, これは図2-4[^1]の$k_F$で滑らかな函数をみれば理解る. 

$|\psi_{0N}>$の状態$\bm{p}\uparrow$に電子を付け加える/$|\psi_{0N}>$の状態$-\bm{p}\downarrow$から電子を取り除く, この励起状態を生成する2つの手順は, 超流動体の対の数が1つ違うだけで対近似において同等であることを認識されたい. このことは状態$|\psi_{0N}>$の代わりに状態$|\psi_0>$を用いて簡単に確認される, 
$$
\begin{aligned}
   c_{p\uparrow}^{\dagger}|\psi_0> &= c_{p\uparrow}^{\dagger}\prod_k(u_k+v_kb_k^{\dagger})|0> \\
   &= u_pc_{p\uparrow}^{\dagger}\prod_{k\neq p}(u_k+v_kb_k^{\dagger})|0> \\
   &\equiv u_p|\psi_{p\uparrow}> \\
   c_{-p\downarrow}|\psi_0> &= c_{-p\downarrow}\prod_k(u_k+v_kb_k^{\dagger})|0> \\
   &= -v_pc_{p\uparrow}^{\dagger}\prod_{k\neq p}(u_k+v_kb_k^{\dagger})|0> \\
   &= -v_p|\psi_{p\uparrow}>. \\
\end{aligned}
$$

次の線型結合を考えることにより数学的取り扱いは簡単になる, 
$$
\begin{aligned}
   \gamma_{p\uparrow}^{\dagger} &= u_pc_{p\uparrow}^{\dagger}-v_pc_{-p\downarrow} \\
   \gamma_{-p\downarrow}^{\dagger} &= u_pc_{-p\downarrow}^{\dagger}+v_pc_{p\uparrow} \\
   \gamma_{p\uparrow} &= u_pc_{p\uparrow}-v_pc_{-p\downarrow}^{\dagger} \\
   \gamma_{-p\downarrow} &= u_pc_{-p\downarrow}+v_pc_{p\uparrow}^{\dagger}.
\end{aligned}
$$
所謂Bogoliubov-Valatin変換である. ここで$\gamma_{p\uparrow}^{\dagger}$と$\gamma_{-p\downarrow}$, $\gamma_{-p\downarrow}^{\dagger}$と$\gamma_{p\uparrow}$は直交している. 新たな演算子$\gamma_{p\uparrow}^{\dagger},\gamma_{-p\downarrow}^{\dagger}$/$\gamma_{p\uparrow},\gamma_{-p\downarrow}$はそれぞれ状態$,$に準粒子を生成/消滅させるものであると示唆される, 
$$
\begin{aligned}
   \gamma_{p\uparrow}^{\dagger}|\psi_0> &= |\psi_{p\uparrow}> \\
   \gamma_{-p\downarrow}^{\dagger}|\psi_0> &= |\psi_{-p\downarrow}> \\
   \gamma_{p\uparrow}|\psi_0> &= 0 \\
   \gamma_{-p\downarrow}|\psi_0> &= 0.
\end{aligned}
$$
この最後の2つの表式は$|\psi_0>$が準粒子に対する真空状態を表していることを主張している. これらの準粒子演算子はFermi-Dirac統計に従う, 
$$
\begin{aligned}
   \lbrace\gamma_{ps},\gamma_{p's'}^{\dagger}\rbrace &= \delta_{pp'}\delta_{ss'} \\
   \lbrace\gamma_{ps},\gamma_{p's'}\rbrace &= \lbrace\gamma_{ps}^{\dagger},\gamma_{p's'}^{\dagger}\rbrace = 0.
\end{aligned}
$$
また励起状態
$$
|\psi_{p\uparrow,-p\downarrow}> = \gamma_{p\uparrow}^{\dagger}\gamma_{-p\downarrow}^{\dagger}|\psi_0>
$$
は基底状態$|\psi_0>$に直交している. 

これらの演算子がN粒子空間へ射影された状態$|\psi_{0N}>$ではなく, $|\psi_0>$のような集団平均の状態に作用させなければならないことを覚えておくことは重要である. 仮に$\gamma_{p\uparrow}^{\dagger}$を$|\psi_{0N}>$に作用させたとすると, 準粒子のある状態とない状態の重ね合わせを得るが, 実際は重ね合わせではなく状態$p\uparrow$を準粒子が占め, 状態$-p,\downarrow$が空であるというように状態は決定されている. 

$\hat{H}_{red}$に対する変分法による解$|\psi_0>$と準粒子スペクトルは, 
1. 巨視的な系
1. 励起状態にある粒子数が対相互作用に参画している電子数に比べて少ない

という条件を満たすときに正確である. 条件2を満たさないときの拡張を次節でみる. 

区切りが良いのでここで終わる. 

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)