---
title: 一人輪講第9回
date: "2021-01-24T02:00:00.000Z"
description: "PDF整理のための一人輪講第11回. J.R.Schrieffer, 1964, Theory of Superconductivity, 101-125 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter5 多体問題における場の理論的方法
### 5-1 Schr$\text{\"{o}}$dinger, Heisenberg, そして相互作用描像
見慣れた話なので省略. 

### 5-2 Green函数の手法
多体問題における系の巨視的物理量は, 極単純な系を除いて正確な多体固有状態を求めることによっては決定できないことは明らかである. そのため実験により強く関連した力学量を求めようとするほうが良いだろう. このような力学量は量子場の理論におけるGreen函数に当たる. 1電子Green函数は系の励起スペクトルの情報と同様に, 電子系のスピンと電荷の密度と運動量分散についての情報を与える. 系が瞬間2体ポテンシャルを介して相互作用しているフェルミオンのみから成る場合に, 1粒子Green函数は系の基底状態を決定するのに十分な情報をも持つ. また2粒子Green函数からは, 電気伝導率, 磁化率, その他多くの非平衡的な性質が得られる. 

今のところはとりあえず, 2体のスピン依存のポテンシャルを介して相互作用しているスピン$1/2$を持つフェルミオンの系を考える. 1粒子Green函数$G$は
$$
G_s(\bm{r}_1,t_1;\bm{r}_2,t_2) = -\mathrm{i}<0|T\left\lbrace\psi_s(\bm{r}_1,t_1)\psi_s^{\dagger}(\bm{r}_2,t_2)\right\rbrace|0>
$$
によって定義される. ここで$|0>$はHeisenberg描像における相互作用している系の正確な基底状態を表し, $\psi_s/\psi_s^{\dagger}$はHeisenberg描像における場の演算子を表している. 時間を順序付ける演算子$T$は$\lbrace\rbrace$の中身の演算子を時間の早い順に右から並べ, その順序の入れ替えは演算子の反交換関係を守るように行われる. そうすれば, 
$$
G_s(\bm{r}_1,t_1;\bm{r}_2,t_2) = \begin{cases}
    -&\mathrm{i}<0|\psi_s(\bm{r}_1,t_1)\psi_s^{\dagger}(\bm{r}_2,t_2)|0> &\text{ if }t_1>t_2 \\
    &\mathrm{i}<0|\psi_s^{\dagger}(\bm{r}_2,t_2)\psi_s(\bm{r}_1,t_1)|0> &\text{ if }t_1\leq t_2 \\
\end{cases}
$$
を得る. $G$は$\tau\equiv t_1-t_2$を通してのみ時間に依存することに注意されたい. もし系が並進不変性を持つならば, $G$は相対座標$\bm{r}\equiv\bm{r}_1-\bm{r}_2$と$\tau$にのみ依存し, 
$$
G_s(\bm{r}_1,t_1;\bm{r}_2,t_2) = G_s(\bm{r},\tau)
$$
となる. $G_s(\bm{r},\tau)$のFourier変換は, 
$$
G_s(\bm{p},p_0) = \int\mathrm{e}^{-\mathrm{i}(\bm{p}\cdot\bm{r}-p_0\tau)}G_s(\bm{r},\tau)\mathrm{d}\bm{r}\mathrm{d}\tau
$$
によって定義され, これは後で見るように, $p_0$の函数としてこれの極が系の素励起スペクトルに関連しているので有用である. また$G_s(\bm{p},p_0)$のFourier逆変換は, 
$$
G_s(\bm{r},\tau) = \frac{1}{(2\pi)^4}\int\mathrm{e}^{\mathrm{i}(\bm{p}\cdot\bm{r}-p_0\tau)}G_s(\bm{p},p_0)\mathrm{d}\bm{p}\mathrm{d}p_0
$$
によって定義される. 周期的境界条件と単位体積の箱を用いて, 
$$
\sum_{\bm{p}}\leftrightarrow\frac{1}{(2\pi)^3}\int\mathrm{d}\bm{p}
$$
の関係を使う. また以下の記法を用いる, 
$$
\begin{aligned}
    x &\equiv (\bm{r},\tau) \\
    p &\equiv (\bm{p},p_0) \\
    px &\equiv \bm{p}\cdot\bm{r}-p_0\tau \\
    \mathrm{d}^4x &\equiv \mathrm{d}\bm{r}\mathrm{d}\tau \\
    \mathrm{d}^4p &\equiv \mathrm{d}\bm{p}\mathrm{d}p_0\text{ .}
\end{aligned}
$$
並進不変系に対して, $G_s(\bm{r},\tau)$の空間的なFourier変換は
$$
$$
となる. $\psi_s(\bm{r},t)=\sum_pc_{ps}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{r}}$($c_{ps}$は平面波状態$\bm{p}$のスピン$s$の電子を消滅させる演算子)という関係を用いることによって, これは
$$
G_s(\bm{p},\tau) -\mathrm{i}<0|T\left\lbrace c_{ps}(\tau)c_{ps}^{\dagger}(0)\right\rbrace|0>
$$
となる. 

### 5-3 自由Fermi気体
$G$の構造を明らかにするために, 単位体積の箱に閉じ込められたスピン$1/2$を持つ相互作用のないフェルミオン系を調べる. 基底状態$|0>$において, Fermi運動量$p_{\text{F}}$より小さな運動量を持つ全平面波状態が上向きと下向きのスピンを持つ電子で占められている. 一方, 全ての他の状態は空である. 系のHamiltonianは
$$
H = \sum_{p,s}\epsilon_pc_{ps}^{\dagger}c_{ps}\text{ where }\epsilon_p=p^2/2m
$$
である. 系はスピン変数において対称的であるので, 上向きスピンの電子に注目し, 添え字$s$を省略する. 反交換関係
$$
\begin{aligned}
    \left\lbrace c_{ps},c_{p's'}^{\dagger}\right\rbrace &= \delta_{pp'}\delta_{ss'} \\
    \left\lbrace c_{ps},c_{p's'}\right\rbrace &= \left\lbrace c_{ps}^{\dagger},c_{p's'}^{\dagger}\right\rbrace = 0
\end{aligned}
$$
から, Heisenberg描像において
$$
c_p(\tau) = c_p(0)\mathrm{e}^{-\mathrm{i}\epsilon_p\tau}
$$
を得る. これを用いて, 並進不変な系に対する$G$の定義より, 
$$
G(\bm{p},\tau) = \begin{cases}
    -&\mathrm{i}<0|c_pc_p^{\dagger}|0>\mathrm{e}^{-\mathrm{i}\epsilon_p\tau} &\text{ if }\tau>0 \\
    &\mathrm{i}<0|c_p^{\dagger}c_p|0>\mathrm{e}^{-\mathrm{i}\epsilon_p\tau} &\text{ if }\tau\leq 0 \\
\end{cases}
$$
を得る. さらに反交換関係を用いれば, 
$$
G(\bm{p},\tau) = \begin{cases}
    -&\mathrm{i}(1-f_p)\mathrm{e}^{-\mathrm{i}\epsilon_p\tau} &\text{ if }\tau>0 \\
    &\mathrm{i}f_p\mathrm{e}^{-\mathrm{i}\epsilon_p\tau} &\text{ if }\tau\leq 0 \\
\end{cases}
$$
とかける. ここでFermi函数$f_p$は
$$
f_p = \begin{cases}
    1 &\text{ if }p<p_{\text{F}} \\
    0 &\text{ if }p>p_{\text{F}}
\end{cases}
$$
を満たす. 

多体問題において, $G$の時間に関するFourier変換はその特異点が系の素励起スペクトルを決定するので, 大変に興味を持たれるものとなる. 自由Fermi気体に対しては, 
$$
G(\bm{p},p_0) \equiv G(p) = \frac{1}{p_0-\epsilon_p+\mathrm{i}\eta_p}\mathrm{e}^{\mathrm{i}p_0\delta}
$$
となる. ここで$\eta_p$は, 
$$
\eta_p \begin{cases}
    >0 &\text{ if }p\geq p_{\text{F}} \\
    <0 &\text{ if }p\leq p_{\text{F}}
\end{cases}
$$
を満たす. また$\delta$は正の無限小に取られている. $G(\bm{p})$は系に運動量$\bm{p}$を持つ電子を付け加えるのに必要とされるエネルギー$p_0=\epsilon_p$で極を持つ. 上述の自由Fermi気体に対する$G$を得るために, 逆Fourier変換
$$
G(\bm{p},\tau) = \frac{1}{2\pi}\int_{-\infty}^{\infty}\mathrm{e}^{-\mathrm{i}p_0\tau}\frac{\mathrm{e}^{\mathrm{i}p_0\delta}}{p_0-\epsilon_p+\mathrm{i}\eta_p}\mathrm{d}p_0
$$
を考える. Cauchyの積分定理を用いて, $\tau>0$に対して, 
$$
G(\bm{p},\tau) = \begin{cases}
    0 &\text{ if }p<p_{\text{F}} \\
    -\mathrm{i}\mathrm{e}^{-\mathrm{i}\epsilon_p\tau} &\text{ if }p>p_{\text{F}}
\end{cases}
$$
を, $\tau\leq 0$に対して, 
$$
G(\bm{p},\tau) = \begin{cases}
    \mathrm{i}\mathrm{e}^{-\mathrm{i}\epsilon_p\tau} &\text{ if }p<p_{\text{F}} \\
    0 &\text{ if }p>p_{\text{F}}
\end{cases}
$$
を得る. これは因子$\mathrm{e}^{\mathrm{i}p_0\delta}$を除いて上述の$G$によって得られるものと等しいが, この因子は殆どの目的に対して$1$として良い. 

自由Fermi気体に対する$G(\bm{p},p_0)$を得るためのもう一つの方法は, $G(\bm{p},\tau)$に対する運動の方程式を構築することである. これは, 並進不変系に対する$G$を
$$
G(\bm{p},\tau) = -\mathrm{i}<0|c_p(\tau)c_p^{\dagger}(0)|0>\theta(\tau)+\mathrm{i}<0|c_p^{\dagger}(0)c_p(\tau)|0>\theta(-\tau)
$$
$$
\theta(\tau) = \begin{cases}
    1 &\text{ if }\tau>0 \\
    0 &\text{ if }\tau<0
\end{cases}
$$
と書き直すことによって, それを$\tau$に関して微分することによって
$$
\mathrm{i}\frac{\partial G(\bm{p},\tau)}{\partial\tau} = <0|T\left\lbrace\frac{\partial c_p(\tau)}{\partial\tau}c_p^{\dagger}(0)\right\rbrace|0>+<0|\left\lbrace c_p(0),c_p^{\dagger}(0)\right\rbrace|0>\delta(\tau)
$$
と得られる. したがって
$$
\left(\mathrm{i}\frac{\partial}{\partial\tau}-\epsilon_p\right)G(\bm{p},\tau) = \delta(\tau)
$$
を得る. この微分方程式の最も一般的な解は, 特解
$$
P(\bm{p},\tau) = \begin{cases}
    -\mathrm{i}\mathrm{e}^{-\mathrm{i}\epsilon_p\tau} &\text{ if }\tau>0 \\
    0 &\text{ if }\tau<0
\end{cases}
$$
と, 同次方程式の一般解
$$
h(\bm{p},\tau) = \mathrm{i}\tilde{f}_p\mathrm{e}^{-\mathrm{i}\epsilon_p\tau}\text{ for all }\tau
$$
の和として得られる. 任意定数の$\tilde{f}_p$は例えば境界条件$G(\bm{p},0^-)=\mathrm{i}f_p$から決定され, このように選んだ$\tilde{f}_p$を使えば, $P+h$は上述の$G$に一致する. 

この時点でどのように相互作用している系に対する$G$を決定するのかわからないかもしれない. それには主要な2つの方法がある. 1つ目は$G$に対する運動の方程式を構築することである. このやり方は実直であるが, たとえ2体の相関以外を無視したとしても数学的にかなり複雑である. 

2つ目の方法は, Feynman, Tomonaga, そしてDysonの摂動展開の方法である. このやり方は展開した級数が収束する問題に制限されるが, 物理的に意味のある結果をもたらす. 以下の議論はこのやり方に従う. 

### 5-4 $G(\bm{p},\tau)$のスペクトル表示
相互作用しているFermi気体に対する1粒子Green函数$G$は相互作用していない系に対する$G$に深く関係している. これを見るために, 上向きスピンを持つ粒子に対するGreen函数
$$
\begin{aligned}
    G(\bm{p},\tau) &= -\mathrm{i}<0|T\left\lbrace c_p(\tau)c_p^{\dagger}(0)\right\rbrace|0> \\
    &= \begin{cases}
        -&\mathrm{i}<0|c_p(0)\mathrm{e}^{-\mathrm{i}H\tau}c_p^{\dagger}(0)|0>\mathrm{e}^{\mathrm{i}E_0^n\tau} &\text{ if }\tau>0 \\
        &\mathrm{i}<0|c_p^{\dagger}(0)\mathrm{e}^{\mathrm{i}H\tau}c_p(0)|0>\mathrm{e}^{-\mathrm{i}E_0^n\tau} &\text{ if }\tau\leq 0
    \end{cases}
\end{aligned}
$$
を考える. ここで$E_0^n$は相互作用している$n$粒子系の基底状態のエネルギーである. $c$と$c^{\dagger}$の間に$n\pm 1$粒子系に対する$H$の固有状態$|\Psi_m^{n+1}>$の完全系を挿入することによって, 
$$
G(\bm{p},\tau) = \begin{cases}
    -&\mathrm{i}\displaystyle\sum_m|(c_p^{\dagger})_{m,0}|^2\mathrm{e}^{-\mathrm{i}(E_m^{n+1}-E_0^n)\tau} &\text{ if }\tau>0 \\
    &\mathrm{i}\displaystyle\sum_m|(c_p)_{m,0}|^2\mathrm{e}^{\mathrm{i}(E_m^{n-1}-E_0^n)\tau} &\text{ if }\tau\leq 0
\end{cases}
$$
を得る. 行列要素は
$$
\begin{aligned}
    (c_p^{\dagger})_{m,0} &= <\Psi_m^{n+1}|c_p^{\dagger}|\Psi_0^n> \\
    (c_p)_{m,0} &= <\Psi_m^{n-1}|c_p^{\dagger}|\Psi_0^n>
\end{aligned}
$$
によって定義される. また簡単のために, エネルギー$\omega_m^{n\pm 1}$を
$$
\begin{aligned}
    E_m^{n+1}-E_0^n &= \omega_m^{n+1}+\mu_n \\
    E_m^{n-1}-E_0^n &= -\omega_m^{n-1}-\mu_{n-1}
\end{aligned}
$$
によって定義する. ここで$\mu_n=E_0^{n+1}-E_0^n$は$n$粒子系の化学ポテンシャルである. いま巨視的な系に関心があるので, $\mu_n\simeq\mu_{n-1}\equiv\mu$とできる. これらのエネルギーに関する式を用いて, 
$$
G(\bm{p},\tau) = \begin{cases}
    -&\mathrm{i}\displaystyle\sum_m|(c_p^{\dagger})_{m,0}|^2\mathrm{e}^{-\mathrm{i}(\omega_m^{n+1}+\mu)\tau} &\text{ if }\tau>0 \\
    &\mathrm{i}\displaystyle\sum_m|(c_p)_{m,0}|^2\mathrm{e}^{-\mathrm{i}(\omega_m^{n-1}+\mu)\tau} &\text{ if }\tau\leq 0
\end{cases}
$$
を得る. また重要な量はスペクトル函数$A(\bm{p},\omega)$であり, これは
$$
A(\bm{p},\omega) = \sum_m|(c_p^{\dagger})_{m,0}|^2\delta(\omega-\omega_m^{n-1})+\sum_m|(c_p)_{m,0}|^2\delta(\omega-\omega_m^{n-1})
$$
によって定義される. これを用いれば, 相互作用している系に対する1粒子Green函数は
$$
G(\bm{p},p_0) = \int_{-\infty}^{\infty}\frac{A(\bm{p},\omega)}{p_0-\omega-\mu+\mathrm{i}\omega\delta}\mathrm{d}\omega
$$
と表される. ここで$\delta$は正の無限小に取っている. 

自由Fermi気体に対する$G$は$A$を$\delta$函数を用いて書くことによって, 
$$
\begin{aligned}
    A(\bm{p},\omega) &= (1-f_p)\delta(\omega-[\epsilon_p-\mu])+f_p\delta(\omega-[\epsilon_p-\mu]) \\
    &= \delta(\omega-[\epsilon_p-\mu])
\end{aligned}
$$
で与えられる. 

スペクトル重み函数は正の実数$A(\bm{p},\omega)=A^*(\bm{p},\omega)$であり, 和のルール
$$
\int_{-\infty}^{\infty}A(\bm{p},\omega)\mathrm{d}\omega = 1
$$
を満たす. これより, 
$$
\int_{-\infty}^{\infty}A(\bm{p},\omega)\mathrm{d}\omega = \sum_m|(c_p^{\dagger})_{m,0}|^2+\sum_m|(c_p)_{m,0}|^2
$$
を得, 状態$|\Psi_m^{n+1}>$の完全系と反交換関係を用いて, 
$$
\int_{-\infty}^{\infty}A(\bm{p},\omega)\mathrm{d}\omega = <0|c_pc_p^{\dagger}+c_p^{\dagger}c_p|0> = 1
$$
とかける. 

$A$の定義より, 正の周波数($\omega>0$)部分が系に電子を付け加えることを含む過程についての情報を含み, 一方負の周波数($\omega<0$)部分が系に正孔を付け加える(電子を取り除く)過程についての情報を含むことは明らかである. そうすれば上述の和のルールはこれら2つのタイプの過程を結びつけるものと解釈される. 

たいていは直接$A$を求めるよりも, $G$を決定するための方程式を立式する. この2つの函数の間の簡単な関係は$G$のスペクトル表示の虚部を取ることによって与えられる, 
$$
\begin{aligned}
    \Im G(\bm{p},\omega+\mu) = \begin{cases}
        -&\pi A(\bm{p},\omega) &\text{if }\omega>0 \\
        &\pi A(\bm{p},\omega) &\text{if }\omega<0\text{ .}
    \end{cases}
\end{aligned}
$$
ここで積分中の因子に対するよく知られた関係
$$
\frac{1}{x\pm\mathrm{i}\eta} = \frac{P}{x}\mp\mathrm{i}\delta(x)
$$
を用いた. $P$は特異点の主値を示す. $G$の実部と虚部を関係付ける分散関係は
$$
\Re G(\bm{p},p_0) = -\frac{1}{\pi}P\int_{\mu}^{\infty}\frac{\Im G(\bm{p},p_0')}{p_0-p_0'}\mathrm{d}p_0'+\frac{1}{\pi}P\int_{-\infty}^{\mu}\frac{\Im G(\bm{p},p_0')}{p_0-p_0'}\mathrm{d}p_0'
$$
に従う. 

### 5-5 $G$の解析的性質
もしFigure 5-2[^1]に示されているように積分路$C$を取り, それに沿って$\omega$に関する積分を実行するならば, $G$のスペクトル表示において無限小因子$\mathrm{i}\omega\delta$を無視することができる. それ故, 
$$
G(\bm{p},p_0) = \int_C\frac{A(\bm{p},\omega)}{(p_0-\mu)-\omega}\mathrm{d}\omega
$$
を得る. 今までのところ$G(\bm{p},p_0)$は$p_0-\mu$の実数値に対してのみ定義されてきた. これを
$$
\hat{G}(\bm{p},p_0) = \int_C\frac{A(\bm{p},\omega)}{(p_0-\mu)-\omega}\mathrm{d}\omega
$$
と定義することによって, $p_0$が複素数値を取れるように拡張する. この種の積分は積分路$C$上の$(p_0-\mu)$の値を除いて, 平面全体に渡って解析的な$p_0$の函数を定義する. 我々の場合において, Figure 5-2[^1]に示すように積分路$C$は平面を領域IとIIに分割され, それぞれの領域で異なった函数$f_{\text{I}}(p_0-\mu)$と$f_{\text{II}}(p_0-\mu)$が定義され, これらは各領域で解析的である. 実数$p_0$に対して, 
$$
G(\bm{p},p_0) = \begin{cases}
    f_{\text{I}}(p_0-\mu) &\text{if }p_0-\mu>0 \\
    f_{\text{II}}(p_0-\mu) &\text{if }p_0-\mu<0
\end{cases}
$$
となる. 函数$f_{\text{I}}$は積分路を跨いで解析接続することによって領域IIでも定義され, 同様に領域Iで函数$f_{\text{II}}$も定義される. 

$A(\bm{p},\omega)$は実数であるので, 積分路の限りなく近くの点で, 函数$f_{\text{I}}$と$f_{\text{II}}$は複素共役である. 

### 5-6 $G(\bm{p},p_0)$の物理的解釈
$G$を解釈する方法は沢山ある. もし$x$表示において記述し, $x_1=x_2$とするならば, 
$$
G_s(\bm{r},t;\bm{r},t) = \mathrm{i}<0|\psi_s^{\dagger}(\bm{r},t)\psi_s(\bm{r},t)|0> = \mathrm{i}<0|\rho_s(\bm{r},t)|0>
$$
を得る. この極限において, $-\mathrm{i}G$はスピン$s$を持つ粒子の密度の期待値を与える. 並進不変な系に対して, $-\mathrm{i}G(p,\tau=0^-)$が
$$
G_s(p,0^-) = \mathrm{i}<0|c_{ps}^{\dagger}c_{ps}|0> = \mathrm{i}<0|n_{ps}|0>
$$
より, 裸の電子の運動量分散を与える. 

$G$はまた$\tau>0$で$\tau=0^+$における状態と同じ状態に系が見出される確率振幅を与える. Heisenberg描像とSchr$\text{\"{o}}$dinger描像が$t=0$で同一になるように位相を選ぶ. とりあえずSchr$\text{\"{o}}$dinger描像で記述する. いま$t=0$で裸の状態$\bm{p}$における粒子を作る. そうすると系の波動関数は, $\Psi_{s,0}$が基底状態を表すならば, 
$$
c_p^{\dagger}|\Psi_{s,0}(0)>
$$
とかける. $\tau$だけ時間が進むと, この状態は
$$
\mathrm{e}^{-\mathrm{i}H\tau}c_p^{\dagger}|\Psi_{s,0}(0)>
$$
と時間発展する. また, $t=\tau$で状態$\bm{p}$において粒子が生成される場合, 波動関数は
$$
c_p^{\dagger}\mathrm{e}^{-\mathrm{i}H\tau}|\Psi_{s,0}(0)>
$$
とかける. そうして, $t=\tau>0$での系が$t=0$と同じ状態を見出す確率振幅は, 
$$
<\Psi_{s,0}(0)|\mathrm{e}^{\mathrm{i}H\tau}c_p\mathrm{e}^{-\mathrm{i}H\tau}c_p^{\dagger}|\Psi_{s,0}(0)>
$$
となる. そしてこれはHeisenberg描像において, 
$$
<0|c_p(\tau)c_p^{\dagger}(0)|0> = -\mathrm{i}G(\bm{p},\tau)\ \tau>0
$$
となる. 物理的見地では, この確率振幅がある周波数$(E_p+\mu)$で振動し, 大きさ$|\Gamma_p|$で減衰すると予想される. つまり, 
$$
G(\bm{p},\tau) \propto \mathrm{e}^{-\mathrm{i}(E_p+\mu)\tau}\mathrm{e}^{-|\Gamma_p|\tau}
$$
ということである. 

いま正の大きな時間に対して$G(\bm{p},\tau)$の振る舞いを調べる. $G$のスペクトル表示の逆Fourier変換を取ることによって, 
$$
G(\bm{p},\tau) = -\mathrm{i}\int_0^{\infty}A(\bm{p},\omega)\mathrm{e}^{-\mathrm{i}(\omega+\mu)\tau}\mathrm{d}\omega\ \tau>0
$$
を得る. $A(\bm{p},\omega)$は有限系に対する$\delta$函数の和であるが, 巨視的な系では連続函数である. Figure 5-3[^1]に示されるように下半平面に積分路を押し込めることによって積分を実行すると, $\omega=E_p-\mathrm{i}|\Gamma_p|$で1つの極を持つので, その留数を$r_p$として, 極からの寄与
$$
-2\pi r_p\mathrm{e}^{-\mathrm{i}(E_p+\mu)\tau}\mathrm{e}^{-|\Gamma_p|\tau}
$$
と線積分
$$
\int_0^{-\mathrm{i}\infty}A(\bm{p},\omega)\mathrm{e}^{-\mathrm{i}\omega\tau}\mathrm{d}\omega
$$
の和となる. $E_p\gg 1$であり, かつ$\mathrm{e}^{-|\Gamma_p|\tau}\geq 1/E_p\tau$でないことを満たすような時間$\tau$に対して, 極の寄与は支配的である. 

極の解釈が書かれているがここが面白い. 

もし極の寄与を準粒子と呼ばれる準定常状態の寄与と解釈するならば, 下半平面への$A(\bm{p},\omega)$の極がこれらの準粒子のエネルギーと減衰率を与えるとわかる. 

スペクトル重み函数ではなくGreen函数自体の観点から, この結果を解釈するのはしばしば便利である. $\mu$よりも大きな実数$p_0$に対する$G(\bm{p},p_0)$を考える. この函数を複素$p_0$平面の下半平面へ解析接続した函数$\tilde{G}(\bm{p},p_0)$は
$$
\tilde{G}(\bm{p},p_0) = \begin{cases}
    \tilde{G}(\bm{p},p_0) &\text{if }\Im p_0>0 \\
    \tilde{G}(\bm{p},p_0)-2\pi\mathrm{i}A(\bm{p},p_0-\mu) &\text{if }\Im p_0<0
\end{cases}
$$
によって与えられる. $\tilde{G}(\bm{p},p_0)$は実軸に沿ってのみ特異点を持つので, 下半平面における$A(\bm{p},p_0-\mu)$の極は, $p_0>\mu$に対する$\tilde{G}(\bm{p},p_0)$の特異点と一致している. その1つの極は準粒子のエネルギーと減衰率を与えると解釈される. もし複数の極があれば, 軸に最も近い極が支配的な寄与をもたらすだろう. 極の寄与はもし裸の粒子が時間$t=0$で生成されるならば, それは運動量$\bm{p}$の準粒子状態になる確率振幅$2\pi\mathrm{i}r_p$を持つ, とも解釈される. 

全く同様にして, $\mu$よりも小さな実数$p_0$に対する$G(\bm{p},p_0)$を考える. この函数を複素$p_0$平面の上半平面へ解析接続した函数$\tilde{\tilde{G}}(\bm{p},p_0)$は
$$
\tilde{\tilde{G}}(\bm{p},p_0) = \begin{cases}
    \tilde{G}(\bm{p},p_0)+2\pi\mathrm{i}A(\bm{p},p_0-\mu) &\text{if }\Im p_0>0 \\
    \tilde{G}(\bm{p},p_0) &\text{if }\Im p_0<0
\end{cases}
$$
によって与えられる. $\tilde{G}(\bm{p},p_0)$は実軸に沿ってのみ特異点を持つので, 上半平面における$A(\bm{p},p_0-\mu)$の極は, $p_0<\mu$に対する$\tilde{\tilde{G}}(\bm{p},p_0)$の特異点と一致している. したがって, $A(\bm{p},\omega)$を$\omega<0$に対する上半平面へ解析接続した函数の極は, 準正孔のエネルギーと減衰率を与えると解釈される. 

### 5-7 $A(\bm{p},\omega)$の解釈
これまでの議論は, もしスペクトル重み函数
$$
A(\bm{p},\omega) = \sum_m|(c_p^{\dagger})_{m,0}|^2\delta(\omega-\omega_m^{n+1})+\sum_m|(c_p)_{m,0}|^2\delta(\omega-\omega_m^{n-1})
$$
をより詳しく理解しようとすれば, 更にはっきりとする. まず, $A$の正の周波数($\omega>0$)部分を考える. もし系が初め基底状態$|\psi_0^n>$にあって, 裸の電子が時刻$t=0$で状態$\bm{p}$にあるならば, 1個の電子が付け加えられた直後の状態ベクトルは
$$
|\Phi_p> = c_p^{\dagger}|\Psi_0^n>
$$
によって与えられる. ここで状態$|\Psi_m^{\nu}>$と違って, 状態$|\Phi_p>$は一般的に$1$に規格化されていないことに留意されたい. 

$|\Phi_p>$によって記述される系が固有状態$|\Psi_m^{n+1}>$にある相対的確率が知りたいとする. 規格化因子を除いて, その確率は, 
$$
P_m(\bm{p}) = |<\Psi_m^{n+1}|\Phi_p>|^2 = |(c_p^{\dagger})_{m,0}|^2
$$
で与えられる. そうしてこれが以前見たように, Fermi自由気体に対する$A(\bm{p},\omega)$の第1項の$\delta$函数の強度となっている. また電子が1個加えられた直後, 系がエネルギー$[\omega,\omega+\delta\omega]$を持つ固有状態の集合に属する相対的確率を求めたいとする. 規格化因子を除いて, その確率は, 
$$
\begin{aligned}
    P_{\omega}(\bm{p})\delta\omega &= \int_{\omega}^{\omega+\delta\omega}\sum_m P_m(\bm{p})\delta(\omega'-\omega_m^{n+1})\mathrm{d}\omega' \\
    &= \int_{\omega}^{\omega+\delta\omega}A(\bm{p},\omega')\mathrm{d}\omega'
\end{aligned}
$$
で与えられる. それ故, $A(\bm{p},\omega)$は$|\Phi_p>$によって記述される系が, 基底状態$|\Psi_0^n>$よりも$\omega+\mu$だけ大きいエネルギーを持つ単位体積あたりの相対確率を与える. 

もし$|\Phi_p>$が規格化されているならば, $n+1$粒子系に対する状態$|\Psi_m^{n+1}>$の完全性のために, 全確率の条件
$$
\sum_mP_m(\bm{p}) = \int_0^{\infty}A(\bm{p},\omega)\mathrm{d}\omega = 1
$$
が成り立つ必要があるだろう. ここでは代わりに
$$
\int_0^{\infty}A(\bm{p},\omega)\mathrm{d}\omega = <\Phi_p|\Phi_p> = <\Psi_0^n|(1-n_p)|\Psi_0^n> = 1-<n_p>
$$
を要求する. 

いま$A(\bm{p},\omega)$の負の周波数($\omega<0$)部分に話を転じて, 状態
$$
|\tilde{\Phi}_p> = c_p|\Psi_0^n>
$$
を考える. この状態は運動量$\bm{p}$を持つ裸の電子が除去された直後の系を記述している. ここでも一般に$|\tilde{\Phi}_p>$は$1$に規格化されていないことに注意されたい. $|\tilde{\Phi}_p>$によって記述される系が, $n-1$粒子系の固有状態$|\Psi_m^{n-1}$にある相対的確率を求めたいとする. 規格化因子を除いて, その確率は, 
$$
\tilde{P}_m(\bm{p}) = |<\Psi_m^{n-1}|\Phi_p>|^2 = |(c_p)_{m,0}|^2
$$
となる. これはFermi自由気体に対する$A(\bm{p},\omega)$の第2項の$\delta$函数の強度となっている. $\omega_m^{n-1}$の定義において符号をあのように選択したために, 励起エネルギー$E_m^{n-1}-E_0^{n-1}$は負$\omega_m^{n-1}<0$になる. もし$A(\bm{p},\omega)$が$\omega$の函数として描かれるならば, 電子の除去を含む状態の励起エネルギーを$\omega$軸の負側に沿って測られるものだとみなす必要がある. 我々のこの符号の選択は, $A$の電子と正孔の部分を重なりなく扱っていることになる. 

準粒子近似は以下のようにして簡単に理解される. 系のエネルギーに対する確率分布は, 裸の電子が基底状態に(から)加えられた(除去された)直後に, 1つもしくはそれ以上のLorentz函数によって近似される, 
$$
A(\bm{p},\omega) = \frac{u_p^2|\Gamma_p^{(+)}|/\pi}{(\omega-E_p^{(+)})^2+\Gamma_p^{(+)2}}+\frac{v_p^2|\Gamma_p^{(-)}|/\pi}{(\omega+E_p^{(-)})^2+\Gamma_p^{(-)2}}\text{ .}
$$
この表式は$u_p^2+v_p^2=1$ならば, $A(\bm{p},\omega)$の和のルールを満たす. この重み函数はFermi自由気体に対するものよりも次の3点の理由から複雑である. 
1. 準粒子エネルギー$E_p^{(\pm)}$は一般に$\pm|\epsilon_p-\mu|$に一致しておらず, 電子または正孔を加える際の媒質との相互作用から生じる "自己エネルギー"を含んでいる. 
1. $|\Phi_p>$は一般に相互作用している系の固有状態ではないので, $\delta$函数は有限の幅を持つLorentz函数に変化している. 
1. 正と負の各々の周波数$\omega$に対応する2つのピークが生じるようになっている. それぞれ有限の幅$u_p^2$と$v_p^2$を持つ. 上述の議論に一致して, この2つのピークは, 運動量状態$\bm{p}$に電子を加える確率(正のエネルギーピークをもたらす)と運動量状態$\bm{p}$から電子を除去する確率(負のエネルギーピークをもたらす)を反映している. 状態$\bm{p}$が占有されている確率が$1$でも$0$でもないので, この両過程は相互作用している系において可能となる. 

以下, 物理的考察が言及されていてありがたい. 

大きな自己エネルギーがあるということが, 必ずしも大きな準位幅$\Gamma$をもたらすということを意味するわけではない. 実際, BCS簡約Hamiltonianによって記述される系に対して, スペクトル重み函数はまさに$\Gamma_p^{(+)}=\Gamma_p^{(-)}\to 0$にした形
$$
A_{\text{BCS}}(\bm{p},\omega) = u_p^2\delta(\omega-E_p)+v_p^2\delta(\omega+E_p)
$$
となる. したがってBCS簡約Hamiltonianは, 自由電子気体のように, $c_p|0>$と$c_p^{\dagger}|0>$が系の固有状態であることに特徴がある. 準粒子エネルギー$E_p$は
$$
E_p = \sqrt{(\epsilon_p-\mu)^2+\Delta_p^2}
$$
によって与えられる. ここで$\Delta_p$はエネルギーギャップパラメータである. $\epsilon_p\simeq\mu$に対して, $E_p$は$\epsilon_p-\mu$とはかなり違うことがわかる. 函数$u_p^2$と$v_p^2$は
$$
\begin{aligned}
    u_p^2 &= \frac{1}{2}\left(1+\frac{\epsilon_p-\mu}{E_p}\right) \\
    v_p^2 &= \frac{1}{2}\left(1-\frac{\epsilon_p-\mu}{E_p}\right)
\end{aligned}
$$
であり, これらはそれぞれ裸の粒子の状態$\bm{p}$が占有されていない/占有されている確率を与える. $\Delta_p\to 0$とすると, BCSスペクトル重み函数は自由Fermi気体のものに移行する. $A(\bm{p},\omega)$は一般にかなり複雑な函数である一方で, 小さな$\epsilon_p-\mu$と小さな$\omega$に対しては, それは単に上述の2つのLorentz函数の和となる. この単純な描像は超伝導状態にも成立するけれども, Fermi液体のLaudau理論において見出される. 殆どの輸送の性質はスペクトル重み函数のこの低エネルギー部分を強調するので, 準粒子近似はしばしば電気的・熱的伝導率を計算するのに十分である. 

### 5-8 1フォノンGreen函数
完全に同様の方法で, 1フォノンGreen函数$D$は
$$
D_{\lambda}(\bm{r}_1,t_1;\bm{r}_2,t_2) = -\mathrm{i}\left<0\left|T\left\lbrace\varphi_{\lambda}(\bm{r}_1,t_1)\varphi_{\lambda}^{\dagger}(\bm{r}_2,t_2)\right\rbrace\right|0\right>
$$
によって定義される. $D$は時間差$\tau=t_1-t_2$を通してのみ時間に依存し, 並進不変な系に対しては, 相対座標$\bm{r}=\bm{r}_1-\bm{r}_2$を通してのみ空間変数に依存する. 場の演算子が$\varphi_{\lambda}(\bm{r},t) = \sum_q\varphi_{q\lambda}(t)\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{r}}$と展開できることを用いれば, 波数ベクトル$\bm{q}$と分極$\lambda$を持つフォノンのプロパゲーターは, 並進不変な系に対して, 
$$
D_{\lambda}(\bm{q},\tau) = -\mathrm{i}\left<0\left|T\left\lbrace\varphi_{q\lambda}(\tau)\varphi_{q\lambda}^{\dagger}(0)\right\rbrace\right|0\right>
$$
で与えられる. フォノン場の振幅が$\varphi_{q\lambda}=a_{q\lambda}+a_{-q\lambda}^{\dagger}$によって, 生成消滅演算子と関係していることを思い出せば, $G$と同様にして, $D$のスペクトル表示を, スペクトル重み函数
$$
B_{\lambda}(\bm{q},\omega) = \sum_m|<m|\varphi_{q\lambda}^{\dagger}|0>|^2\delta(\omega-\omega_m)-\sum_m|<m|\varphi_{q\lambda}|0>|^2\delta(\omega+\omega_m)
$$
を導入することによって記述することができる. ここで$\omega_m$は$n$粒子系の励起エネルギー$E_m^n-E_0^n$である. そうすると, $D_{\lambda}(\bm{q},\tau)$の時間に関するFourier変換は, 
$$
D_{\lambda}(\bm{q},q_0) = \int_{-\infty}^{\infty}\frac{B_{\lambda}(\bm{q},\omega)}{q_0-\omega+\mathrm{i}\omega\delta}\mathrm{d}\omega
$$
で与えられる. ここで$\delta=0^+$である. $B$は実数の値を持つ量であるので, その虚部は関係$\Im D_{\lambda}(\bm{q},q_0)=-\pi B_{\lambda}(\bm{q},q_0)\ \mathrm{sgn}q_0$を与えるから, $D$に対して分散関係
$$
D_{\lambda}(\bm{q},q_0) = -\frac{1}{\pi}\int_{-\infty}^{\infty}\frac{\Im D_{\lambda}(\bm{q},\omega)\ \mathrm{sgn}\ \omega}{q_0-\omega+\mathrm{i}\omega\delta}\mathrm{d}\omega
$$
が成り立つ. 

$G(\bm{p},p_0)$と全く同じように, $D_{\lambda}(\bm{q},q_0)$の定義を複素$q_0$平面に拡張することができる. そして, $D$を$q_0>0$に対する下半平面へ解析接続した函数の極が波数ベクトル$\bm{p}$($-\bm{p}$)と分極$\lambda$を持つフォノン(フォノンホール)のエネルギーと減衰率を与えることが示される. $G$とは違って$D$を定義するときに使われたのは消滅演算子ではなくフォノン場の振幅であったので, フォノンの状況は電子よりも複雑である. 反転対称性を持つ系では, $B$は$\omega$に反対称函数となるので, $\varphi_{q\lambda}^{\dagger}=\varphi_{-q\lambda}$という関係を持つ. この場合は, 
$$
D_{\lambda}(\bm{q},q_0) = \int_0^{\infty}B_{\lambda}(\bm{q},\omega)\frac{2\omega}{q_0^2-\omega^2+\mathrm{i}\delta}\mathrm{d}\omega
$$
とかける. 

裸のフォノンのみからなる系(i.e. 電子とカップルしていない)に対して, スペクトル函数は
$$
B_{\lambda}(\bm{q},\omega) = \delta(|\omega|-\Omega_{q\lambda})\ \mathrm{sgn}\ \omega
$$
となり, 裸のフォノンのプロパゲーターは
$$
D_{0\lambda}(\bm{q},q_0) = \frac{2\Omega_{q\lambda}}{q_0^2-\Omega_{q\lambda}^2+\mathrm{i}\delta}
$$
となる. 

## errata

| Page. | Line. | Error. | Correction. |
| ---- | ---- | ---- | ---- |
| 108 | 19 | 3-17 | 5-17 |
| 109 | 6 | $\mathrm{e}^{-\mathrm{i}p_0\delta}$ | $\mathrm{e}^{-\mathrm{i}p_0\tau}$ |
| 120 | 15 | 5-37 | 5-36 |
| 122 | 12 | that | ~~that~~ |

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/schrieffer-tos/schrieffer-tos.pdf" data-iframely-url="//cdn.iframe.ly/r4URh9D"></a></div></div>

[^1]:[(PDF)J.R.Schrieffer, 1964, Theory of Superconductivity](http://zimp.zju.edu.cn/~qchen/Teaching/AdvStat/Schrieffer.pdf)