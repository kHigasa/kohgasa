---
title: 一人輪講第6回
date: "2021-01-03T02:00:00.000Z"
description: "PDF整理のための一人輪講第6回. David Tong, 2006, Lectures on Quantum Field Theory, 26-50 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 2. 自由場
### 2.3 真空(続き)
基底状態のエネルギー$E_0$の無限大をより実践的に扱うことができる. 物理において我々が興味のあることはエネルギー差である. $E_0$を直接測定する方法がないならば, 単純にこの無限大を差し引くことによって, Hamiltonianを
$$
H = \frac{1}{(2\pi)^3}\int\omega_{\bm{p}}a_{\bm{p}}^{\dagger}a_{\bm{p}}\mathrm{d}^3p
$$
再定義すれば良い. この新しい定義のもとで, $H|0>=0$である. 実際, 新旧Hamiltonianの間の差異は単に古典論から量子論へ移行する際の曖昧さの程度である. 

##### 定義
正規順序化された(消滅演算子を含む積の形で, 消滅演算子が右側に配置されている)演算子$\phi_1(\bm{x}_1)\ldots\phi_n(\bm{x}_n)$のひもを$:\phi_1(\bm{x}_1)\ldots\phi_n(\bm{x}_n):$とかく. 

この定義によれば, 正規順序化されたHamiltonianのひもは, 
$$
:H: = \frac{1}{(2\pi)^3}\int\omega_{\bm{p}}a_{\bm{p}}^{\dagger}a_{\bm{p}}\mathrm{d}^3p
$$
とかける. 

#### 2.3.1 宇宙定数
上で$E_0$が直接測れないと言ったが, 重力に関しては完全にわかるようである. 全てのゼロ点エネルギーの合計は, Einsteinの方程式の右辺に現れるエネルギー-運動量テンソルに寄与するはずである. 我々はそれらが宇宙定数$\Lambda=E_0/V$として現れることを期待する, 
$$
R_{\mu\nu}-\frac{1}{2}Rg_{\mu\nu} = -8\pi GT_{\mu\nu}+\Lambda g_{\mu\nu}\ .
$$

#### 2.3.2 Casimir効果
我々が注意すべきことは, 真空のエネルギーの揺らぎの差が測定される状況があり得るということである. 赤外発散を抑制するために, $x^1$方向のサイズ$L$の周期性を用意し(Figure 3[^1]), $\phi(\bm{x})=\phi(\bm{x}+L\bm{n})\ ,\ \bm{n}=(1,0,0)$という周期的境界条件を課す. $x^2,x^3$方向はそのままにしておくが, 単位体積$A$あたりの物理量を計算することを忘れてはいけない. $x^1$方向に距離$d\ll L$だけセパレートするための2枚の反射板を入れる. これらの板は場のFourier展開に影響を与える, つまりそれは板に挟まれた場の運動量が
$$
\bm{p}=\left(\frac{n\pi}{d},p_y,p_z\right)\ ,\ n\in\mathbb{Z^+}
$$
と量子化されるということを意味する. 質量のないスカラー場に対して, 平板内の基底状態のエネルギーが
$$
\frac{E(d)}{A} = \sum_{n=1}^{\infty}\frac{1}{(2\pi)^2}\int\frac{1}{2}\sqrt{\left(\frac{n\pi}{d}\right)^2+p_y^2+p_z^2}\ \mathrm{d}p_y\mathrm{d}p_z
$$
で与えられる. 一方で, 平板の外のエネルギーは, $E(L-d)$で与えられる. したがって総エネルギーは, $E=E(d)+E(L-d)$と平板間の厚さ$d$に依存する量になる. 

> If this naive guess is true, it would mean that there is a force on the plates due to the fluctuations of the vacuum. 

こういう演繹になるのか. 

しかし$E$が無限大であるという問題がある. これは高い運動量のモードから生じている. 物理的には, いかなる実際の板も高周波の波を反射することはできないと主張できるだろう. つまりある周波数で, 波は漏れ出す. 数学的には, ある距離尺度$a\ll d$に対して, $p\gg a^{-1}$を満たす運動量のモードを無視する方法を見つけたい. これは紫外(UV)カットオフと呼ばれる. これを実現するために, エネルギーを求める積分を
$$
\frac{E(d)}{A} = \sum_{n=1}^{\infty}\frac{1}{(2\pi)^2}\int\frac{1}{2}\sqrt{\left(\frac{n\pi}{d}\right)^2+p_y^2+p_z^2}\exp\left(-a\sqrt{\left(\frac{n\pi}{d}\right)^2+p_y^2+p_z^2}\right)\mathrm{d}p_y\mathrm{d}p_z
$$
と変更する. この積分において$a\to 0$とすると, 前述の表式を得る. ところが変更後の積分はちゃんと有限になっている. 勿論, アドホックなやり方によらずに積分を有限にして, UVカットオフによらない計算により物理量を得るほうが良い. 

変更後の積分は少し複雑なので, 問題を$d=3+1$次元ではなく, $d=1+1$次元におけるものと考えれば, ずいぶん簡単になる. そうすればエネルギーは, 
$$
E_{1+1}(d) = \frac{\pi}{2d}\sum_{n=1}^{\infty}n
$$
によって与えられる. いまこの和をUVカットオフを導入することによって制限する. これにより有限の表式は, 
$$
\begin{aligned}
    E_{1+1}(d) &\to \frac{\pi}{2d}\sum_{n=1}^{\infty}n\exp\left(-a\frac{n\pi}{d}\right) \\
    &= -\frac{1}{2}\frac{\partial}{\partial a}\sum_{n=1}^{\infty}\exp\left(-a\frac{n\pi}{d}\right) \\
    &= -\frac{1}{2}\frac{\partial}{\partial a}\frac{1}{1-\exp(-a\pi/d)} \\
    &= \frac{\pi}{2d}\frac{\partial}{\partial a}\frac{\exp(a\pi/d)}{(\exp(a\pi/d)-1)^2} \\
    &= \frac{d}{2\pi a^2}-\frac{\pi}{24d}+\mathcal{O}(a^2)\ \because\ a\ll d
\end{aligned}
$$
となる. これより総エネルギーは, 
$$
E_{1+1}=E_{1+1}(d)+E_{1+1}(L-d)=\frac{L}{2\pi a^2}-\frac{\pi}{24}\left(\frac{1}{d}+\frac{1}{L-d}\right)+\mathcal{O}(a^2)
$$
とかける. これは$a\to 0$の極限ではまだ無限である. しかし, Casimir力と呼ばれる平板間の力は, 
$$
\frac{\partial E_{1+1}}{\partial d} = \frac{\pi}{24d^2}+\ldots
$$
となり, 我々が$a\to 0,L\to\infty$として制限を取り除いても有限なままだということが肝要である. 

### 2.4 粒子
これまで真空を扱ってきたので, 場の励起に移ることができる. いまちょうど調和振動子のように, 
$$
[H,a_{\bm{p}}^{\dagger}] = \omega_{\bm{p}}a_{\bm{p}}^{\dagger}\ \ \text{and}\ \ [H,a_{\bm{p}}] = -\omega_{\bm{p}}a_{\bm{p}}
$$
という交換関係が成り立っており, これは演算子$a_{\bm{p}}^{\dagger}$を真空状態$|0>$に
$$
|\bm{p}> = a_{\bm{p}}^{\dagger}|0>
$$
と作用させることによって, 励起状態を構築することができることを意味している. この状態は
$$
H|\bm{p}>=\omega_{\bm{p}}|\bm{p}>\ ,\ \omega_{\bm{p}}^2=\bm{p}^2+m^2
$$
と, エネルギー固有値$\omega_{\bm{p}}$を持つ. 我々はこれを質量$m$,3次元運動量$\bm{p}$を持つ粒子に対する相対論的エネルギー散逸$E_{\bm{p}}^2=\bm{p}^2+m^2$とみなす. そして状態$|\bm{p}>$を質量$m$を持つ1粒子の運動量固有状態を解釈する. 次に古典的な総運動量$\bm{P}$を演算子にすると, 正規順序化の後に, 
$$
\bm{P} = -\int\pi\nabla\phi\mathrm{d}^3x = \frac{1}{(2\pi)^3}\int\bm{p}a_{\bm{p}}^{\dagger}a_{\bm{p}}\mathrm{d}^3p
$$
を得る. これを状態$|\bm{p}>$に作用させると, 
$$
\bm{P}|\bm{p}> = \bm{p}|\bm{p}>
$$
となり, それは正確に固有状態となっており, 運動量$\bm{p}$を固有値として持つことがわかる. 我々の研究できる状態$|\bm{p}>$の別の性質は角運動量である. 再び, 場の総角運動量に対する古典的表式を演算子に変えると, 
$$
J^i = \epsilon^{ijk}\int(J^0)^jk\mathrm{d}^3x
$$
となる. また, 
$$
J^i|\bm{p}=\bm{0}> = 0
$$
であり, 運動量0の1粒子は内的な角運動量を運ばないと解釈される. 

##### 多粒子状態, Boson統計, そしてFock空間
演算子$a^{\dagger}$を複数回作用させることによって, 多粒子状態を作ることができる. したがって, 
$$
|\bm{p}_1,\ldots,\bm{p}_n> = a_{\bm{p}_1}^{\dagger},\ldots,a_{\bm{p}_n}^{\dagger}|0>
$$
を$n$粒子状態と解釈する. 任意の$a^{\dagger}$は交換するので, 例えば$|\bm{p},\bm{q}>=|\bm{q},\bm{p}>$が成り立ち, これは粒子がBosonであることを意味する. 

我々の理論の完全なHilbert空間は, Fock空間と呼ばれ, 全ての可能な$a^{\dagger}$の組み合わせを真空状態に作用させ, 
$$
|0>\ ,\ a_{\bm{p}}^{\dagger}|0>\ ,\ a_{\bm{p}}^{\dagger}a_{\bm{q}}^{\dagger}|0>\ ,\ a_{\bm{p}}^{\dagger}a_{\bm{q}}^{\dagger}a_{\bm{r}}^{\dagger}|0>\ ,\ldots
$$
これらによって展開される. Fock空間は単に$n$粒子のHilbert空間の総和である. Fock空間におけるある状態の粒子数を数える数演算子は, 
$$
N = \frac{1}{(2\pi)^3}\int a_{\bm{p}}^{\dagger}a_{\bm{p}}\mathrm{d}^3p
$$
によって与えられ, 
$$
N|\bm{p}_1,\ldots,\bm{p}_n> = n|\bm{p}_1,\ldots,\bm{p}_n>
$$
を満たす. また数演算子はHamiltonianと交換し($[N,H]=0$), 粒子数が保存されることを保証する. これは我々を$n$粒子の区画に押し留め置けることを意味し, 自由場の理論に特徴的な性質である. しかし自由場でも相互作用を考えると, それらによって粒子が生成消滅されるので, もはや粒子数は保存せず, Fock空間における異なった区画を取ることになる. 

##### 演算子値分散
我々は状態$|\bm{p}>$を粒子とみなしているが, それらは空間で局在化してはいない. それというのはそれらは運動量の固有状態であるからである. 量子力学で位置固有状態と運動量固有状態が規格化できないためにHilbert空間の良い要素でなかったように, 場の量子論でも演算子$\phi(\bm{x})$と$a_{\bm{p}}$はFock空間に作用させても, 規格化可能な状態が得られないので良い演算子ではない. 

規格化不能な原因は$\delta$函数の存在による. 

例えば, 
$$
<0|a_{\bm{p}}a_{\bm{p}}^{\dagger}|0>=<\bm{p}|\bm{p}>=(2\pi)^3\delta(0)\ \ \text{and}\ \ <0|\phi(\bm{x})|\phi(\bm{x})0>=<\bm{x}|\bm{x}>=\delta(0)
$$
となるが, これらは演算子値分散を意味する. どういうことかというと, $\phi(\bm{x})$は真空でwell definedな期待値$<0|\phi(\bm{x})|0>=0$を持つけれども, 固定点での演算子の揺らぎは無限大となるということである. 

#### 2.4.1 相対論的規格化
真空状態を規格化して定義した. それを使うと1粒子状態$|\bm{p}> = a_{\bm{p}}|0>$は, 
$$
<\bm{p}|\bm{q}> = (2\pi)^3\delta^{(3)}(\bm{p}-\bm{q})
$$
を満たす. これがLorentz不変かどうかは, 3次元ベクトルしかないのではっきりしない. 3次元ベクトルのLorentz変換
$$
p^{\mu} \to (p')^{\mu}=\Lambda^{\mu}_{\ \nu}p^{\nu}
$$
を考える. 規格化条件に注意を払っていないので, 一般に未知函数$\lambda(\bm{p},\bm{p}')$に対して, 
$$
|\bm{p}> \to \lambda(\bm{p},\bm{p}')|\bm{p}'>
$$
を得る. Lorentz不変なものに注目することによって, この未知函数を見つけたい. そのようなもののうち1つは, 1粒子状態に作用する恒等演算子である. 1粒子状態の規格化条件より, 
$$
1 = \frac{1}{(2\pi)^3}\int |\bm{p}><\bm{p}|\mathrm{d}^3p
$$
を得る. この演算子はLorentz不変であるが, 測度$\int\mathrm{d}^3p$と射影子$|\bm{p}><\bm{p}|$の2つの項から成る. これらは個々にはLorentz不変ではない. 

##### 主張
Lorentz不変な測度は
$$
\int\frac{\mathrm{d}^3p}{2E_{\bm{p}}}
$$
である. 

##### 証明
測度$\int\mathrm{d}^4p$はLorentz不変である. また質量のある粒子に対する相対論的な散逸は
$$
p_{\mu}p^{\mu}=m^2 \Rightarrow p_0^2=E_{\bm{p}}^2=\bm{p}^2+m^2
$$
であり, これもLorentz不変である. そうすれば, 組み合わせ
$$
\int\delta(p_0^2-\bm{p}^2-m^2)\mathrm{d}^4p\Big|_{p_0>0} = \int\frac{\mathrm{d}^3p}{2p_0}\Bigg|_{p_0=E_{\bm{p}}}
$$
もLorentz不変に違いないので主張が示された. <div class="QED" style="text-align: right">$\Box$</div>

この結果より, 他のあらゆることが理解できるようになった. 例えば, 3次元ベクトルに対するLorentz不変な$\delta$函数は
$$
2E_{\bm{p}}\delta^{(3)}(\bm{p}-\bm{q})\ \because\ \int\frac{1}{2E_{\bm{p}}}2E_{\bm{p}}\delta^{(3)}(\bm{p}-\bm{q})\mathrm{d}^3p = 1
$$
となる. そして最後に, 相対論的に規格化された運動量状態は
$$
|p> = \sqrt{2E_{\bm{p}}}|\bm{p}> = \sqrt{2E_{\bm{p}}}|a_{\bm{p}}^{\dagger}|0>
$$
で与えられることがわかる. 

表記が$|p>$と$|\bm{p}>$等とかなり捉え難いことに注意されたい. 

### 2.5 複素スカラー場
Lagrangian
$$
\mathcal{L} = \partial_{\mu}\psi^*\partial^{\mu}\psi-M^2\psi^*\psi
$$
によって記述される複素スカラー場を考えよう. 運動の方程式は, 
$$
\partial_{\mu}\partial^{\mu}\psi+M^2\psi=0\ \ \text{and}\ \ \text{c.c.}
$$
で与えられる. ここで複素場の演算子を平面波で展開して, 
$$
\psi = \frac{1}{(2\pi)^3}\int\frac{1}{\sqrt{2E_{\bm{p}}}}\left(b_{\bm{p}}^{\dagger}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}+c_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}}\right)\mathrm{d}^3p\ \ \text{and}\ \ \text{c.c.}
$$
を得る. 古典場$\psi$は実数ではないので, 対応する量子場$\psi$はエルミートではない. こういうわけで, 異なる演算子$b,c^{\dagger}$が正と負の周波数領域に現れるのである. 古典場の運動量は$\pi=\partial\mathcal{L}/\partial\dot{\psi}=\dot{\psi}^*$であり, これを量子場の演算子にすると, 
$$
\pi = \frac{1}{(2\pi)^3}\int\mathrm{i}\sqrt{\frac{E_{\bm{p}}}{2}}\left(b_{\bm{p}}^{\dagger}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}-c_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}}\right)\mathrm{d}^3p\ \ \text{and}\ \ \text{c.c.}
$$
とかける. 場と運動量の交換関係は
$$
\begin{aligned}
    [\psi(\bm{x}),\pi{\bm{y}}] &= \mathrm{i}\delta^{(3)}(\bm{x}-\bm{y})\ \ \text{and}\ \ [\psi(\bm{x}),\pi^{\dagger}{\bm{y}}]=0 \\
    [\psi(\bm{x}),\psi(\bm{y})] &= [\psi(\bm{x}),\pi^{\dagger}(\bm{y})]=0
\end{aligned}
$$
等で与えられる(複素共軛も同様である). 実はこれらの場の交換関係は演算子$b_{\bm{p}}$と$c_{\bm{p}}$に対する交換関係と同等で, 
$$
\begin{aligned}
    [b_{\bm{p}},b_{\bm{q}}^{\dagger}] &= (2\pi)^3\delta^{(3)}(\bm{p}-\bm{q}) \\
    [c_{\bm{p}},c_{\bm{q}}^{\dagger}] &= (2\pi)^3\delta^{(3)}(\bm{p}-\bm{q}) \\
    [b_{\bm{p}},b_{\bm{q}}] &= [c_{\bm{p}},c_{\bm{q}}] = [b_{\bm{p}},c_{\bm{q}}] = [b_{\bm{p}},c_{\bm{q}}^{\dagger}] = 0
\end{aligned}
$$
である. 要するに, 複素スカラー場の量子化は2つの生成演算子$b_{\bm{p}}^{\dagger}$と$c_{\bm{p}}^{\dagger}$を生んだ. つまり, 質量Mでスピンゼロの2種の粒子, 粒子と反粒子を生成していると解釈される. 対象的に実スカラー場に対しては, 生成される粒子は1種類のみである. それはつまり粒子自身がその反粒子にほかならないことを意味している. 

このとき古典的な保存電荷$Q=\mathrm{i}\int(\dot{\psi}^*\psi-\psi^*\dot{\psi})\mathrm{d}^3x=\mathrm{i}\int(\pi\psi-\psi^*\pi^*)\mathrm{d}^3x$は, 正規順序化の後, 量子演算子
$$
Q = \frac{1}{(2\pi)^3}\int(c_{\bm{p}}^{\dagger}c_{\bm{p}}-b_{\bm{p}}^{\dagger}b_{\bm{p}})\mathrm{d}^3p = N_c-N_b
$$
となり, $Q$は$c^{\dagger}$によって生成される反粒子の数から$b^{\dagger}$によって生成される粒子の数を引いたものを数えている. また, $[H,Q]=0$より, $Q$は量子場の理論においても保存される量であることが保証される. 勿論, 我々の自由場の理論において, これはそんなに大したことではない. それというのは, 上述したように$N_c$と$N_b$が別々に保存するからである. しかしながらまた上述したように個別に数が保存しない相互作用している理論においても, $Q$は保存するのだ. 

### 2.6 Heisenberg描像
Lorentz不変なLagrangianから始めたけれども, 望ましい時間座標$t$を導入すると量子化の際にその不変性は段々だめになった. 理論が量子化されてもなおLorentz不変かどうかは全くはっきりしないのだ. 例えば, 1粒子状態はSchr$\text{\"{o}}$dinger方程式に従って, 
$$
\mathrm{i}\frac{\mathrm{d}|\bm{p}(t)>}{\mathrm{d}t} = H|\bm{p}(t)>\ \ \Rightarrow\ \ |\bm{p}(t)> = \mathrm{e}^{-\mathrm{i}E_{\bm{p}}t}|\bm{p}>
$$
と時間発展する. 物事はHeisenberg描像においてより良く見え始める. ただSchr$\text{\"{o}}$dinger描像でも時間が固定されていれば, 演算子は一致する. したがって交換関係は, 
$$
\begin{aligned}
    [\phi(\bm{x},t),\phi(\bm{y},t)] &= [\pi(\bm{x},t),\pi(\bm{y},t)] = 0 \\
    [\phi(\bm{x},t),\pi(\bm{y},t)] &= \mathrm{i}\delta^{(3)}(\bm{x}-\bm{y})
\end{aligned}
$$
である. いまや演算子$\phi(x)=\phi(\bm{x},t)$は時間に依存しているので, それがどう時間発展するのか探求することができる. 例えば, 
$$
\begin{aligned}
    \dot{\phi} = \mathrm{i}[H,\phi] &= \frac{\mathrm{i}}{2}\left[\int(\pi^2(y)+\nabla\phi^2(y)+m^2\phi^2(y))\mathrm{d}^3y, \phi(x)\right] \\
    &= \mathrm{i}\int\pi(y)(-\mathrm{i})\delta^{(3)}(\bm{y}-\bm{x})\mathrm{d}^3y = \pi(x)
\end{aligned}
$$
である. 一方, $\pi$に対する運動の方程式は, 
$$
\begin{aligned}
    \dot{\pi} = \mathrm{i}[H,\pi] &= \frac{\mathrm{i}}{2}\left[\int(\pi^2(y)+\nabla\phi^2(y)+m^2\phi^2(y))\mathrm{d}^3y, \pi(x)\right] \\
    &= \frac{\mathrm{i}}{2}\int\lbrace(\nabla_y[\phi(y),\pi(x)])\nabla\phi(y)+\nabla\phi(y)\nabla_y[\phi(y),\pi(x)]+2\mathrm{i}m^2\phi(y)\delta^{(3)}(\bm{x}-\bm{y})\rbrace\mathrm{d}^3y \\
    &= -\left(\int\left(\nabla_y\delta^{(3)}(\bm{x}-\bm{y})\right)\nabla_y\phi(y)\right)-m^2\phi(x) \\
    &= \nabla^2\phi-m^2\phi
\end{aligned}
$$
となる. これら2式を合わせれば, 場の演算子$\phi$はKlein-Gordon方程式
$$
\partial_{\mu}\partial^{\mu}\phi+m^2\phi = 0
$$
を満たすとわかる. 物事はより相対論的に見えてくる. $\phi(x)$のFourier展開を書けば, 
$$
\mathrm{e}^{\mathrm{i}Ht}a_{\bm{p}}\mathrm{e}^{-\mathrm{i}Ht} = \mathrm{e}^{-\mathrm{i}E_{\bm{p}}t}a_{\bm{p}}\ \ \text{and}\ \ \mathrm{e}^{\mathrm{i}Ht}a_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}Ht} = \mathrm{e}^{\mathrm{i}E_{\bm{p}}t}a_{\bm{p}}^{\dagger}
$$
となり, これらは交換関係
$$
[H,a_{\bm{p}}] = -E_{\bm{p}}a_{\bm{p}}\ \ \text{and}\ \ [H,a_{\bm{p}}^{\dagger}] = E_{\bm{p}}a_{\bm{p}}^{\dagger}
$$
を満たし, 
$$
\phi(\bm{x},t) = \frac{1}{(2\pi)^3}\int\frac{1}{\sqrt{2E_{\bm{p}}}}\left(a_{\bm{p}}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}}+a_{\bm{p}}^{\dagger}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}\right)\mathrm{d}^3p
$$
を得る. 

#### 因果律
$\phi(x)$がKlein-Gordon方程式を満たすとき, Heisenberg描像においてLorentz不変な何かに近づいている. $\phi$と$\pi$が同じ時間で交換関係
$$
[\phi(\bm{x},t),\pi(\bm{y},t)] = \mathrm{i}\delta^{(3)}(\bm{x}-\bm{y})
$$
を満足するので, 非Lorentz不変性の要因を示すようなヒントをこれより感じ得る. 任意の時空の分離についてはどうだろうか. 特に我々の理論が因果関係を示すために, 全ての空間的に分離された演算子は
$$
[\mathcal{O}_1(x),\mathcal{O}_2(y)] = 0\ \ \forall(x-y)^2=-(\bm{x}-\bm{y})^2<0
$$
のように交換することを要請しなければならない. これは$x$と$y$が因果関係で結ばれていないとき, $x$での測定が$y$での測定になんの影響も与えないことを保証する. 我々の理論はこの重大な性質を満たすだろうか. 
$$
\Delta(x-y) = [\phi(x),\phi(y)]
$$
を定義する. この表式の右辺は演算子(q-数)である. しかし, 左辺は次の積分により決定されるc-数である, 
$$
\Delta(x-y) = \frac{1}{(2\pi)^3}\int\frac{1}{\sqrt{2E_{\bm{p}}}}\left(\mathrm{e}^{-\mathrm{i}\bm{p}\cdot(\bm{x}-\bm{y})}-\mathrm{e}^{\mathrm{i}\bm{p}\cdot(\bm{x}-\bm{y})}\right)\mathrm{d}^3p\ .
$$

??

この函数について我々は何がわかるか. 
- Lorentz不変:測量$\int\mathrm{d}^3p/2E_{\bm{p}}$のおかげ
- 時間的な分離に対して残る:例えば$x-y=(t,0,0,0)$であれば, $[\phi(\bm{x},0),\phi(\bm{x},t)]\sim\mathrm{e}^{-\mathrm{i}mt}-\mathrm{e}^{\mathrm{i}mt}$である. 
- 空間的な分離に対して消える

### 2.7 プロパゲーター(伝播函数)
時空間の点$y$にある粒子を準備する. それを$x$に見つける振幅はどんなものだろうか. 計算すれば, 
$$
\begin{aligned}
    <0|\phi(x)\phi(y)|0> &= \frac{1}{(2\pi)^6}\int\frac{1}{\sqrt{4E_{\bm{p}}E_{\bm{p}'}}}<0|a_{\bm{p}}a_{\bm{p}'}^{\dagger}|0>\mathrm{e}^{-\mathrm{i}(\bm{p}\cdot\bm{x}-\bm{p}'\cdot\bm{y})}\mathrm{d}^3p\mathrm{d}^3p' \\
    &= \frac{1}{(2\pi)^3}\int\frac{1}{2E_{\bm{p}}}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot(\bm{x}-\bm{y})}\mathrm{d}^3p \equiv D(x-y)
\end{aligned}
$$
となる. 函数$D(x-y)$はプロパゲーターと言われる. 空間的な分離に対して, $D(x-y)$は
$$
D(x-y) \sim \mathrm{e}^{-m|\bm{x}-\bm{y}|}
$$
のように減衰すると示される. つまりそれは光円錐の外で指数関数的に減衰するが, それにも関わらず消えるわけではないことが驚きである. 量子場は光円錐から漏れ出すように思われる. しかし我々はちょうど空間的な測定は交換し, 理論は因果律を満たすとわかったところである. どのようにこれらの2つの事実を調和させればよいか. 

滲み出しがあるが測定は確定しているという. 

$$
[\phi(x),\phi(y)] = D(x-y)-D(y-x)=0\ \ \text{if}\ \ (x-y)^2<0
$$
を考える. $(x-y)^2<0$のとき, 出来事を順序付けるためのLorentz不変な方法はない. もし粒子が空間的な方向において$x\to y$へ移れるならば, 容易くちょうど逆へも移れる. いかなる測定においても, これら2つの振幅は相殺する. 

複素スカラー場であれば, 話はより面白くなる. 光円錐の外で$[\psi(x),\psi^{\dagger}(y)]=0$に注目できる. $x\to y$へ伝播する粒子に対する振幅が$y\to x$へ移動する反粒子に対する振幅をキャンセルすると解釈できる. 実際この解釈は実スカラー場に対してもそのまま当てはまる. 

#### 2.7.1 Feynmanプロパゲーター
相互作用している場の理論において最も重要な量の1つは
$$
\Delta_F(x-y) = <0|T\phi(x)\phi(y)|0> = \begin{cases}
    D(x-y) &\text{if }x^0>y^0 \\
    D(y-x) &\text{if }y^0>x^0
\end{cases}
$$
で与えられるFeynmanプロパゲーターである. ここで$T$は時間を順序付ける役割で, 全ての演算子を評価しより遅い時間のものを左に置き換える, 
$$
T\phi(x)\phi(y) = \begin{cases}
    \phi(x)\phi(y) &\text{if }x^0>y^0 \\
    \phi(y)\phi(x) &\text{if }y^0>x^0\ .
\end{cases}
$$

##### 主張
Feynmanプロパゲーターを4次元運動量の積分の観点から書く便利な方法がある, 
$$
\Delta_F(x-y) = \frac{1}{(2\pi)^4}\int\frac{\mathrm{i}}{p^2-m^2}\mathrm{e}^{-\mathrm{i}p\cdot(x-y)}\mathrm{d}^4p\ .
$$
しかしながら, 主張されているようにこの積分はill definedである. それというのは, $\bm{p}$のそれぞれの値に対して, 分母$p^2-m^2=(p^0)^2-\bm{p}^2-m^2$は$p^0=\pm E_{\bm{p}}=\pm\sqrt{\bm{p}^2+m^2}$のとき極を持つからである. $p_0$の積分におけるこれらの特異点を避けるための処方箋が必要である. Feynmanプロパゲーターを得るために, 特異点を避けるような積分路を選ばねばならない(Figure 5[^1]). 

##### 証明
$$
\frac{1}{p^2-m^2} = \frac{1}{(p^0)^2-E_{\bm{p}}^2} = \frac{1}{(p^0-E_{\bm{p}})(p^0+E_{\bm{p}})}
$$
であるから, 極$p^0=\pm E_{\bm{p}}$での留数は$\pm(1/2)E_{\bm{p}}$となる. $x^0>y^0$のとき, 積分路を複素平面の下半分に限ることができる. $p^0\to -\mathrm{i}\infty$とすれば, 被積分函数は$\mathrm{e}^{-\mathrm{i}p^0(x^0-y^0)}\to 0$となるために消えることが保証される. そうすれば$p^0$に渡る積分は, $p^0=E_{\bm{p}}$での留数$-2\pi\mathrm{i}/2E_{\bm{p}}$を取る. この負符号は, 時計回りの積分路を取ったために現れた. それ故, $x^0>y^0$のとき, 
$$
\begin{aligned}
    \Delta_F(x-y) &= \frac{1}{(2\pi)^4}\int\frac{-2\pi\mathrm{i}}{2E_{\bm{p}}}\mathrm{i}\mathrm{e}^{-\mathrm{i}E_{\bm{p}}(x^0-y^0)+\mathrm{i}\bm{p}\cdot(\bm{x}-\bm{y})}\mathrm{d}^4p \\
    &= \frac{1}{(2\pi)^3}\int\frac{1}{2E_{\bm{p}}}\mathrm{e}^{-\mathrm{i}p\cdot(x-y)}\mathrm{d}^3p = D(x-y)
\end{aligned}
$$
となり, まさに$x^0>y^0$に対するFeynmanプロパゲーターを得る. 対照的に, $y^0>x^0$のとき, 積分路を反時計回りに複素平面の上半分にとって, 
$$
\begin{aligned}
    \Delta_F(x-y) &= \frac{1}{(2\pi)^4}\int\frac{2\pi\mathrm{i}}{-2E_{\bm{p}}}\mathrm{i}\mathrm{e}^{\mathrm{i}E_{\bm{p}}(x^0-y^0)+\mathrm{i}\bm{p}\cdot(\bm{x}-\bm{y})}\mathrm{d}^4p \\
    &= \frac{1}{(2\pi)^3}\int\frac{1}{2E_{\bm{p}}}\mathrm{e}^{-\mathrm{i}E_{\bm{p}}(y^0-x^0)}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot(\bm{y}-\bm{x})}\mathrm{d}^3p \\
    &= \frac{1}{(2\pi)^3}\int\frac{1}{2E_{\bm{p}}}\mathrm{e}^{-\mathrm{i}p\cdot(y-x)}\mathrm{d}^3p = D(y-x)
\end{aligned}
$$
と再びFeynmanプロパゲーターを得た. <div class="QED" style="text-align: right">$\Box$</div>

積分路を特定する代わりに, Feynmanプロパゲーターを無限小の$\epsilon>0$に対して, 
$$
\Delta_F(x-y) = \frac{1}{(2\pi)^4}\int\frac{\mathrm{i}\mathrm{e}^{-\mathrm{i}p\cdot(x-y)}}{p^2-m^2+\mathrm{i}\epsilon}\mathrm{d}^4p
$$
と書くことも普通である($\mathrm{i}\epsilon$処方箋). 

#### 2.7.2 Green函数
プロパゲーターのもう一つの具現化がある. それはKlein-Gordon演算子に対するGreen函数である. もし特異点を取り去るならば, 
$$
\begin{aligned}
    (\partial_t^2-\nabla^2+m^2)\Delta_F(x-y) &= \frac{1}{(2\pi)^4}\int\frac{\mathrm{i}}{p^2-m^2}(-p^2+m^2)\mathrm{e}^{-\mathrm{i}p\cdot(x-y)}\mathrm{d}^4p \\
    &= -\mathrm{i}\frac{1}{(2\pi)^4}\int\mathrm{e}^{-\mathrm{i}p\cdot(x-y)}\mathrm{d}^4p \\
    &= -\mathrm{i}\delta^{(4)}(x-y)
\end{aligned}
$$
となる. 幾つかの目的のためにGreen函数を生む他の積分路を選ぶことも便利である. 例えば遅延Green函数$\Delta_R(x-y)$は図7[^1]の積分路によって定義され, 
$$
\Delta_R(x-y) = \begin{cases}
D(x-y)-D(y-x) &\text{if }x^0>y^0 \\
0 &\text{if }y^0>x^0
\end{cases}
$$
という性質を持つ. 遅延Green函数は, もしある場の配置の初期値がわかっており, 源が存在するときにそれがどのように時間発展するかを理解したければ, つまりある固定された背景の函数$J(x)$に対する非同次のKlein-Gordon方程式$\partial_{\mu}\partial^{\mu}\phi+m^2\phi=J(x)$の解を知りたいならば, 古典場の理論において便利である. 

また同様にして$y^0<x^0$で消える先進Green函数を定義することができ, もし場の配置の終点がわかっていれば, それがどこから来たのかを理解したいときに便利である. 

### 2.8 非相対論的場
Klein-Gordon方程式に従う古典的な複素スカラー場に戻ろう. 我々は場を
$$
\psi(\bm{x},t) = \mathrm{e}^{-\mathrm{i}mt}\tilde{\psi}(\bm{x},t)
$$
と分解する. そうするとKlein-Gordon方程式は, 
$$
\partial_t^2\psi-\nabla^2\psi+m^2\psi = \mathrm{e}^{-\mathrm{i}mt}\left(\ddot{\tilde{\psi}}-2\mathrm{i}m\dot{\tilde{\psi}}-\nabla^2\tilde{\psi}\right)=0
$$
となる. 粒子の非相対論的極限$|\bm{p}|\ll m$をとることは, 場の表現ではFourier変換の後に, $|\ddot{\tilde{\psi}}|\ll m|\dot{\tilde{\psi}}|$ととることに同じである. この極限で, 2階の微分の項は無視され, Klein-Gordon方程式は
$$
\mathrm{i}\frac{\partial\tilde{\psi}}{\partial t} = -\frac{1}{2m}\nabla^2\tilde{\psi}
$$
となる. これは確率解釈されないことを除けば, 質量$m$を持つ非相対論的な自由粒子に対するSchr$\text{\"{o}}$dinger方程式によく似ている. 

$\partial_t\psi\ll m\psi$の極限を取ることによって, スカラー場に対する相対論的Lagrangianから, $\tilde{\psi}\to\psi$と書き換えて, 非相対論的Lagrangianは
$$
\mathcal{L} = \mathrm{i}\psi^*\dot{\psi}-\frac{1}{2m}\nabla\psi^*\nabla\psi
$$
となる. ここで$1/2m$で割っている. このLagrangianは内部対称性$\psi\to\mathrm{e}^{\mathrm{i}\alpha}\psi$から生じる保存電流を持ち, それは時間と空間の要素から成り, 
$$
j^{\mu} = \left(-\psi^*\psi\ ,\ \frac{\mathrm{i}}{2m}(\psi^*\nabla\psi-\psi\nabla\psi^*)\right)
$$
とかける. Hamiltonian形式に移行するために, 正準運動量を計算すると, 
$$
\pi = \frac{\partial\mathcal{L}}{\partial\dot{\psi}} = \mathrm{i}\psi^*
$$
となる. 運動量が時間微分に全く依存していないことは少し不安に思われるが, それは時間微分の1次まで残す理論に完全に一致している. 場の完全な軌跡を決定するためには, $t=0$での$\psi$と$\psi^*$を特定することのみが必要である. つまり始めの状態で時間微分の項は必要とされないということである. 

Hamiltonianを計算すると, 
$$
\mathcal{H} = \frac{1}{2m}\nabla\psi^*\nabla\psi
$$
となる. 量子化するために, Schr$\text{\"{o}}$dinger描像における正準交換関係
$$
\begin{aligned}
    [\psi(\bm{x}),\psi^{\dagger}(\bm{y})] &= \delta^{(3)}(\bm{x}-\bm{y}) \\
    [\psi(\bm{x}),\psi(\bm{y})] &= [\psi^{\dagger}(\bm{x}),\psi^{\dagger}(\bm{y})] = 0 \\
\end{aligned}
$$
を課す. $\psi(\bm{x})$をFourier展開
$$
\psi(\bm{x}) = \frac{1}{(2\pi)^3}\int a_{\bm{p}}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}\mathrm{d}^3p
$$
して, 演算子$a,a^{\dagger}$に関して上の正準交換関係の要請より
$$
[a_{\bm{p}},a_{\bm{q}}^{\dagger}] = (2\pi)^3\delta^{(3)}(\bm{p}-\bm{q})
$$
が必要である. 真空状態は$a_{\bm{p}}|0>=0$であり, 励起状態は$a_{\bm{p}_1}\ldots a_{\bm{p}_1}|0>$であるから, 1粒子状態は
$$
H|\bm{p}> = \frac{\bm{p}^2}{2m}|\bm{p}>
$$
という, 非相対論的散逸関係を持つ. したがって, 時間微分の1次まで残したLagrangianは質量$m$を持つ非相対論的粒子を生むと結論できる. 

#### 2.8.1 量子力学の回復
量子力学に戻るためにはどうすればよいだろうか. 場の総運動量に対する演算子は
$$
\bm{P} = \frac{1}{(2\pi)^3}\int\bm{p}a_{\bm{p}}^{\dagger}a_{\bm{p}}\mathrm{d}^3p
$$
であり, 1粒子状態は$\bm{P}|\bm{p}>=\bm{p}|\bm{p}>$と正確に固有状態となっている. また, 場の位置演算子も簡単に構築できて, それは非相対論的極限で, $\bm{x}$で局在した粒子を生成する演算子$\psi^{\dagger}(\bm{x})\ ,\ |\bm{x}>=\psi^{\dagger}(\bm{x})|0>$を用いて, 
$$
\bm{X} = \int\bm{x}\psi^{\dagger}(\bm{x})\psi(\bm{x})\mathrm{d}^3x
$$
となる. したがって1粒子状態は$\bm{X}|\bm{x}>=\bm{x}|\bm{x}>$と正確に固有状態となっている. 

1粒子状態の重ね合わせを取ることによって, 状態$|\varphi>$を
$$
|\varphi> = \int\varphi(\bm{x})|\bm{x}>\mathrm{d}^3x
$$
と構成する. ここで函数$\varphi(\bm{x})$は普段の位置表示におけるSchr$\text{\"{o}}$dinger方程式と呼ぶものである. この状態に$\bm{X}$を作用させると, 
$$
X^i|\varphi> = \int x^i\varphi(\bm{x})|\bm{x}>\mathrm{d}^3x
$$
となるのは明らかである. 一方$\bm{P}$に対しては, 
$$
\begin{aligned}
    P^i|\varphi> &= \frac{1}{(2\pi)^3}\int p^ia_{\bm{p}}^{\dagger}a_{\bm{p}}\varphi(\bm{x})\psi^{\dagger}(\bm{x})|0>\mathrm{d}^3x\mathrm{d}^3p \\
    &= \frac{1}{(2\pi)^3}\int p^ia_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}}\varphi(\bm{x})|0>\mathrm{d}^3x\mathrm{d}^3p\ \because\ [a_{\bm{p}},\psi^{\dagger}(\bm{x})]=\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}} \\
    &= \frac{1}{(2\pi)^3}\int a_{\bm{p}}^{\dagger}\left(\mathrm{i}\frac{\partial}{\partial x^i}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}}\right)\varphi(\bm{x})|0>\mathrm{d}^3x\mathrm{d}^3p \\
    &= \frac{1}{(2\pi)^3}\int\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}}\left(-\mathrm{i}\frac{\partial\varphi}{\partial x^i}\right)a_{\bm{p}}^{\dagger}|0>\mathrm{d}^3x\mathrm{d}^3p \\
    &= \int\left(-\mathrm{i}\frac{\partial\varphi}{\partial x^i}\right)|\bm{x}>\mathrm{d}^3x \\
\end{aligned}
$$
を得る. したがって, 1粒子状態に作用させるとき, 演算子$\bm{X},\bm{P}$は量子力学における位置,運動量演算子として振る舞う. 正順交換関係$[X^i,P^i]|\varphi>=\mathrm{i}\delta^{ij}|\varphi>$を満たす. 一方波動関数$\varphi(\bm{x},t)$の時間発展は, Hamiltonianが
$$
H = \int\frac{1}{2m}\nabla\psi^*\nabla\psi\mathrm{d}^3x = \frac{1}{(2\pi)^3}\int\frac{\bm{p}^2}{2m}a_{\bm{p}}^{\dagger}a_{\bm{p}}\mathrm{d}^3p
$$
と書き直されるので, $\bm{p} = -\mathrm{i}(\partial\varphi/\partial\bm{x})$より, 
$$
\mathrm{i}\frac{\partial\varphi}{\partial t} = -\frac{1}{2m}\nabla^2\varphi
$$
という方程式に従うとわかる. これは元々の場の時間発展の方程式と同じであり, 波動関数$\varphi$に対する普段の確率解釈を満足するSchr$\text{\"{o}}$dinger方程式である. 

第二量子化という言葉への混乱について言及している. 

##### 相互作用
ある固定された背景ポテンシャル$V(\bm{x})$に対して, Lagrangian
$$
\mathcal{L} = \mathrm{i}\psi^*\psi-\frac{1}{2m}\nabla\psi^*\nabla\psi-V(\bm{x})\psi^*\psi
$$
を考える. 

さらに粒子間相互作用を考慮でき, それはLagrangianに
$$
\Delta\mathcal{L} = \psi^*(\bm{x})\psi^*(\bm{x})\psi(\bm{x})\psi(\bm{x})
$$
の形を加えることで達成されると期待される. 

## 3. 相互作用している場
これより相互作用項を含むより複雑な理論を調査する. これらはLagrangianのより高次の項を含むであろう. 我々は理論にどういった小さな摂動を加えることができるかを議論することから始める. 例えば実スカラー場に対するLagrangian
$$
\mathcal{L} = \frac{1}{2}\partial_{\mu}\phi\partial^{\mu}\phi-\frac{1}{2}m^2\phi^2-\sum_{n\geq 3}\frac{\lambda_n}{n!}\phi^n
$$
を考えよう. 係数$\lambda_n$は結合定数と呼ばれる. 付加された項が小さな摂動であることを保証するために結合定数にはどのような制限が必要であろうか. 単に$\lambda_n\ll 1$とするのではいけない. それは次元解析をすれば直ぐにわかることである. 作用は角運動量, それと同等にDirac定数$\hbar$の次元を持ち, いま自然単位系を用いているので, $[S]=1$とすると, $S=\int\mathcal{L}\mathrm{d}^4x$, $[\mathrm{d}^4x]=[M^{-4}]=-4$であるから, Lagrangian密度は$[\mathcal{L}]=4$という次元を持つ. つまり, Lagrangianに戻ると, $[\partial_{\mu}]=1$であるので, 全ての因子が質量の次元となり$[\phi]=[m]=1$, さらに
$$
[\lambda_n] = 4-n
$$
とわかる. 結局結合定数によってパラメータ化される様々な項は以下の3種類に類別される. 
- $[\lambda_3]=1$:このとき無次元のパラメータは$\lambda_3/E$であり, $\lambda_3\phi^3/3!$が小さな摂動となるためには, $E\gg\lambda_3$を満たす高エネルギー極限を取らなければならない. 低エネルギーで大きく高エネルギーで小さいパラメータである. Lagrangianに付加されたこのような振る舞いを持つ摂動項は低エネルギーで殆ど相対化されることより, 相対的(relevant)と呼ばれる. 
- $[\lambda_4]=0$:このとき$\lambda_4\phi^4/4!$が小さな摂動と成るためには$\lambda_4\ll 1$とならねばならず, このような摂動は境界的(marginal)と呼ばれる. 
- $[\lambda_n]<0\ \text{for}\ n\geq 5$:このとき無次元のパラメータは$\lambda_nE^{n-4}$であり, 摂動項が小さくなるには$E^{4-n}\gg\lambda_n$が必要である. 低エネルギーで小さく高エネルギーで大きいパラメータである. このような摂動は非相対的(irrelevant)と呼ばれる. 

後で見るように普通, 量子場の理論において高エネルギー過程を避けることはできない. これが意味するのは, irelevantな問題が期待されるということである. 実際これらによって, 繰り込み不能な場の理論が生じ, これは任意の高エネルギーでの無限大を理解できない. これは必ずしも理論が無駄であることを意味しないが, あるエネルギースケールでは不完全であると言える. 

##### 重要な余談: なぜQFTは単純なのか
典型的に量子場の理論では, relevantとmarginalな結合だけが重要である. 低エネルギーではirelevantな結合が弱いからである. これは書き下せる無限個の相互作用項の内, 厄介なものだけが必要とされるということで大変助かるものである. 

その厄介なものはこれまでに実スカラー場で扱った2つの場合である. 

##### 弱結合場の理論の例
この講義では, 弱結合場の理論だけを扱う. すなわち全てのエネルギーで自由場の理論の小さな摂動とみなされるものということである. 

##### 1)$\phi^4$理論
Lagrangianは
$$
\mathcal{L} = \frac{1}{2}\partial_{\mu}\phi\partial^{\mu}\phi-\frac{1}{2}m^2\phi^2-\frac{\lambda}{4!}\phi^4\ \ \text{with}\ \ \lambda\ll 1
$$
である. $\phi^4$の項を$a_{\bm{p}}$と$a_{\bm{p}}^{\dagger}$の項で展開すると, $a_{\bm{p}}^{\dagger}a_{\bm{p}}^{\dagger}a_{\bm{p}}^{\dagger}a_{\bm{p}}^{\dagger}$や$a_{\bm{p}}^{\dagger}a_{\bm{p}}^{\dagger}a_{\bm{p}}^{\dagger}a_{\bm{p}}$のような相互作用の和が現れる. これらが粒子を生成消滅させるので, $\phi^4$のLagrangianは粒子数の保存されない理論を記述すると示唆される. 実際$[H,N]\neq 0$である. 

##### 2)スカラーのYukawa理論
Lagrangianは
$$
\mathcal{L} =  \partial_{\mu}\psi^*\partial^{\mu}\psi+\frac{1}{2}\partial_{\mu}\phi\partial^{\mu}\phi-M^2\psi^*\psi-\frac{1}{2}m^2\phi^2-g\psi^*\psi\phi\ \ \text{with}\ \ g\ll M,m
$$
である. この理論は複素スカラー場$\psi$を実スカラー場$\phi$へ結合したものである. $\psi$と$\phi$の個々の粒子数はもはや保存されないが, $\psi$の位相の回転対称性は持ち, これは$[Q,H]=0$を満たす保存電荷$Q$の存在を保証しているので, $\psi$の粒子数から$\psi$の反粒子数を引いたものが保存していることがわかる. 

スカラーのYukawa理論はちょっと困った側面を持つ. ポテンシャルは$\phi=\psi=0$で安定な極小値を取るが, 十分大きな$-g\phi$に対して下に有界でない. これはこの理論をあんまり遠くに広げられないことを意味する. 

##### 強結合場の理論へのコメント
強結合場の理論の研究はもっとずいぶん難しい. 例えば, 
- 電荷分割
- 監禁
- 新興の空間
等を含む幾つかの驚くべきことが起こる(各詳細は[^1]). 

区切りが良いのでここで終わる. 


## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/tong-qft/tong-qft.pdf" data-iframely-url="//cdn.iframe.ly/ZUFTTag"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[David Tong: Lectures on Quantum Field Theory](http://www.damtp.cam.ac.uk/user/dt281/qft.html)