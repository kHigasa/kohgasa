---
title: 一人輪講第7回
date: "2021-01-10T02:00:00.000Z"
description: "PDF整理のための一人輪講第7回. David Tong, 2006, Lectures on Quantum Field Theory, 51-75 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 3. 相互作用している場
### 3.1 相互作用描像
量子力学の復習故, 省略. 

#### 3.1.1 Dysonの公式
相互作用描像の状態に対するSchr$\text{\"{o}}$dinger方程式
$$
\mathrm{i}\frac{\mathrm{i}\mathrm{d}|\psi>_I}{\mathrm{d}t} = H_I(t)|\psi>_I
$$
を解くために, Unitary時間発展演算子$U(t,t_0)$(性質:$U(t_1,t_2)U(t_2,t_3)=U(t_1,t_3)\ ,\ U(t,t)=1$等)を用いて, 解を
$$
|\psi(t)>_I = U(t,t_0)|\psi(t_0)>_I
$$
とおくと, 
$$
\mathrm{i}\frac{\mathrm{d}U}{\mathrm{d}t} = H_I(t)U
$$
を得る. 

$H_I(t)$は演算子故, この微分方程式は単純には解けない. 

##### 主張
この微分方程式の解はDysonの公式
$$
U(t,t_0) = T\exp\left(-\mathrm{i}\int_{t_0}^tH_I(t')\mathrm{d}t'\right)
$$
で与えられる. ここで$T$はより遅い時間で評価される演算子を左に配置する時間順序を表す, 
$$
T(\mathcal{O}_1(t_1)\mathcal{O}_2(t_2)) = \begin{cases}
    \mathcal{O}_1(t_1)\mathcal{O}_2(t_2) &\text{if }t_1>t_2 \\
    \mathcal{O}_2(t_2)\mathcal{O}_1(t_1) &\text{if }t_2>t_1\ .
\end{cases}
$$
$U(t,t_0)$を展開すると, 
$$
\begin{aligned}
    U(t,t_0) = &1-\mathrm{i}\int_{t_0}^tH_I(t')\mathrm{d}t' \\
    &+\frac{(-\mathrm{i}^2)}{2}\left(\int_{t_0}^t\int_{t'}^tH_I(t'')H_I(t')\mathrm{d}t''\mathrm{d}t'+\int_{t_0}^t\int_{t_0}^{t'}H_I(t')H_I(t'')\mathrm{d}t''\mathrm{d}t'\right) \\
    &+\cdots \\
    = &1-\mathrm{i}\int_{t_0}^tH_I(t')\mathrm{d}t'+(-\mathrm{i})^2\int_{t_0}^t\int_{t_0}^{t'}H_I(t')H_I(t'')\mathrm{d}t''\mathrm{d}t'
\end{aligned}
$$
を得る. 

##### 証明
$$
\begin{aligned}
    \mathrm{i}\frac{\partial}{\partial t}T\exp\left(-\mathrm{i}\int_{t_0}^tH_I(t')\mathrm{d}t'\right) &= T\left\lbrace H_I(t)\exp\left(-\mathrm{i}\int_{t_0}^tH_I(t')\mathrm{d}t'\right)\right\rbrace \\
    &= H_I(t)T\exp\left(-\mathrm{i}\int_{t_0}^tH_I(t')\mathrm{d}t'\right)
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### 3.2 散乱に対する最初の考え
ここで相互作用描像を場の理論に応用してみる. スカラーのYukawa理論に対する相互作用Hamiltonian
$$
H_{\text{int}} = g\int\psi^{\dagger}\psi\phi\ \mathrm{d}^3x
$$
から始める. この相互作用はある種の粒子が他の種の粒子へ変態することを許すので, 粒子数を保存しない. 具体的に理解るために, 相互作用描像を用いて状態の時間発展($|\psi(t)>=U(t,t_0)|\psi(t_0)>$)を追う. $H_{\text{int}}$は各種粒子毎に生成消滅演算子を持つ. 特に, 
- $\phi\sim a+a^{\dagger}$: $\phi$粒子(中間子)を生成または消滅させることのできる演算子
- $\psi\sim b+c^{\dagger}$: $\psi$粒子(核子)を$b$を通して消滅させ, その反粒子を$c^{\dagger}$を通して生成することのできる演算子
- $\psi^{\dagger}\sim b^{\dagger}+c$: $\psi$粒子(核子)を$b^{\dagger}$を通して生成し, その反粒子を$c$を通して消滅させることのできる演算子

が挙げられる. 重要なことは, $Q=N_c-N_b$は$H_{\text{int}}$の作用があっても保存することである. 1次の摂動$H_{\text{int}}$で, $c^{\dagger}b^{\dagger}a$のような項を持つ. この項は中間子を消滅させ, 核子-反核子の対を生成し, 中間子の崩壊$\phi\to\psi\tilde{\psi}$に寄与する. 

摂動の2次$(H_{\text{int}})^2$では, $(c^{\dagger}b^{\dagger}a)(cba^{\dagger})$のような項を持つ. この項は散乱過程$\psi\tilde{\psi}\to\phi\to\psi\tilde{\psi}$に寄与する. この散乱過程の振幅を計算するために, 始状態$|i>\text{ at }t\to -\infty$と終状態$|f>\text{ at }t\to +\infty$は自由場の理論のHamiltonian $H_0$の固有状態となっている, という重要だが少しずるい仮定をおく. 

互いの粒子がとても離れていれば相互作用を感じないのに等しいということである. 

$|i>$から$|f>$への散乱振幅は, 
$$
\lim_{t_{\pm}\to\pm\infty}<f|U(t_+,t_-)|i> \equiv <f|S|i>
$$
であり, このUnitary演算子SはS-行列と呼ばれる. 

#### 3.2.1 例: 中間子の崩壊
相対論的に規格化された始状態と終状態
$$
\begin{aligned}
    |i> &= \sqrt{2E_{\bm{p}}}a_{\bm{p}}^{\dagger}|0> \\
    |f> &= \sqrt{4E_{\bm{q}_1}E_{\bm{q}_2}}b_{\bm{q}_1}^{\dagger}c_{\bm{q}_2}^{\dagger}|0> \\
\end{aligned}
$$
を考える. 始状態は運動量$p$を持つ1個の中間子を持ち, 終状態はそれぞれ運動量$q_1$と$q_2$を持つ核子と反核子の対を持つことを表している. この状態を用いて, 核子-反核子対への中間子の崩壊を計算でき, $g$についての主要項
$$
<f|S|i> = -\mathrm{i}g<f|\int\psi^{\dagger}(x)\psi(x)\phi(x)\mathrm{d}^4x|i>
$$
を得る. まず$\phi\sim a+a^{\dagger}$を展開する. 中間子が2つになるような状態は考えていないので, 
$$
\begin{aligned}
    <f|S|i> &= -\mathrm{i}g<f|\int\psi^{\dagger}(x)\psi(x)\frac{1}{(2\pi)^3}\int\frac{\sqrt{2E_{\bm{p}}}}{\sqrt{2E_{\bm{k}}}}a_{\bm{k}}a_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}k\cdot x}\mathrm{d}^4x|0> \\
    &= -\mathrm{i}g<f|\int\psi^{\dagger}(x)\psi(x)\mathrm{e}^{-\mathrm{i}p\cdot x}\mathrm{d}^3k\mathrm{d}^4x|0>
\end{aligned}
$$
となる. 同様にして$\psi\sim b+c^{\dagger}$と$\psi^{\dagger}\sim b^{\dagger}+c$を展開すると, 今は$b^{\dagger}$と$c^{\dagger}$の寄与のみ考えているので, 
$$
\begin{aligned}
    <f|S|i> &= -\mathrm{i}g<0|\frac{1}{(2\pi)^6}\iint\frac{\sqrt{E_{\bm{q}_1}E_{\bm{q}_2}}}{\sqrt{E_{\bm{k}_1}E_{\bm{k}_2}}}c_{\bm{q}_2}b_{\bm{q}_1}c_{\bm{k}_1}^{\dagger}b_{\bm{k}_2}^{\dagger}\mathrm{e}^{\mathrm{i}(k_1+k_2-p)\cdot x}\mathrm{d}^3k_1\mathrm{d}^3k_2\mathrm{d}^4x|0> \\
    &= -\mathrm{i}g(2\pi)^4\delta^{(4)}(q_1+q_2-p)
\end{aligned}
$$
となり, 最初の量子場の理論の振幅を得る. 

### 3.3 Wickの定理
Dysonの公式を用いて, $|i>,|f>$が自由場の理論の固有状態であるときに, $<f|T\lbrace H_I(x_1)\ldots H_I(x_n)\rbrace|i>$のような量を計算したい. これは時間順序によって固定されているが正規順序にして作用させたほうがより単純である. Wickの定理はまさに時間順序積を正規順序積に変換する方法を教えてくれるものである. 

#### 3.3.1 例: プロパゲーターの回復
実スカラー場を考え, それはHeisenberg描像で
$$
\begin{aligned}
    \phi(x) &= \phi^+(x)+\phi^-(x) \\
    \phi^+(x) &= \frac{1}{(2\pi)^3}\int\frac{1}{\sqrt{2E_{\bm{p}}}}a_{\bm{p}}\mathrm{e}^{-\mathrm{i}p\cdot x}\mathrm{d}^3p \\
    \phi^-(x) &= \frac{1}{(2\pi)^3}\int\frac{1}{\sqrt{2E_{\bm{p}}}}a_{\bm{p}}^{\dagger}\mathrm{e}^{\mathrm{i}p\cdot x}\mathrm{d}^3p \\
\end{aligned}
$$
と分解される. $x^0>y^0$のとき, 
$$
\begin{aligned}
    T\phi(x)\phi(y) &= \phi(x)\phi(y) \\
    &= (\phi^+(x)+\phi^-(x))(\phi^+(y)+\phi^-(y)) \\
    &= \phi^+(x)\phi^+(y)+\phi^-(x)\phi^+(y)+\phi^-(y)\phi^+(x)+\phi^-(x)\phi^-(y)+[\phi^+(x), \phi^-(y)] \\
    &= :\phi(x)\phi(y):+\Delta(x-y)
\end{aligned}
$$
となり, 第3行で正規秩序化した. 一方, $y^0>x^0$に対して, 
$$
T\phi(x)\phi(y) = :\phi(x)\phi(y):+\Delta(y-x)
$$
となる. これらを合わせると, 最終的に, 
$$
T\phi(x)\phi(y) = :\phi(x)\phi(y):+\Delta_F(x-y)
$$
を得る. 

##### 定義
演算子のひも$\ldots\phi(x_1)\ldots\phi(x_2)\ldots$における場の対の縮約は, Feynmanプロパゲーターを含む演算子を他の演算子はそのままにして置き換えることを意味し, それを
$$
\ldots\overbrace{\phi(x_1)\ldots\phi(x_2)}\ldots
$$
と表す. 縮約は
$$
\begin{aligned}
    T\psi(x)\psi^{\dagger}(y) &= :\psi(x)\psi^{\dagger}(y):+\Delta_F(x-y) \\
    \overbrace{\psi(x)\psi^{\dagger}(y)} &= \Delta_F(x-y) \\
    \overbrace{\psi(x)\psi(y)} &= \overbrace{\psi^{\dagger}(x)\psi^{\dagger}(y)} = 0
\end{aligned}
$$
のように定義される. 

#### 3.3.2 Wickの定理
$n$個の場の集合に対して, 
$$
T(\phi_1\ldots\phi_n) = :\phi_1\ldots\phi_n:+:\text{all possible contractions}:
$$
である. 

##### 証明
数学的帰納法によって証明される. 

#### 3.3.3 例: 核子散乱
$\psi\psi\to\psi\psi$散乱を見よう. 始状態と終状態は
$$
\begin{aligned}
    |i> &= \sqrt{2E_{\bm{p}_1}}\sqrt{2E_{\bm{p}_2}}b_{\bm{p}_1}^{\dagger}b_{\bm{p}_2}^{\dagger}|0> \equiv |p_1,p_2> \\
    |f> &= \sqrt{2E_{\bm{p}'_1}}\sqrt{2E_{\bm{p}'_2}}b_{\bm{p}'_1}^{\dagger}b_{\bm{p}'_2}^{\dagger}|0> \equiv |p'_1,p'_2>
\end{aligned}
$$
である. $<f|S|i>$を展開するが, 散乱の起こらない状況に興味はないので, $<f|S-1|i>$を計算することが本質的に意味を持つ. $g^2$のオーダーで, 
$$
\frac{(-\mathrm{i}g)^2}{2}\int T\left(\psi^{\dagger}(x_1)\psi(x_1)\phi(x_1)\psi^{\dagger}(x_2)\psi(x_2)\phi(x_2)\right)\mathrm{d}^4x_1\mathrm{d}^4x_2
$$
という項を持つ. この項は, Wickの定理を用いれば, 
$$
:\psi^{\dagger}(x_1)\psi(x_1)\psi^{\dagger}(x_2)\psi(x_2):\overbrace{\phi(x_1)\phi(x_2)}
$$
のような演算子のひもを持つことがわかる. これは2つの$\psi$場が$\psi$粒子を消滅させ, 2つの$\psi^{\dagger}$場が$\psi$粒子を生成するので散乱に寄与していることがわかる. したがって散乱行列要素$<f|S|i>$は, 
$$
\begin{aligned}
    &<p'_1,p'_2|:\psi^{\dagger}(x_1)\psi(x_1)\psi^{\dagger}(x_2)\psi(x_2):|p_1,p_2> \\
    = &<p'_1,p'_2|:\psi^{\dagger}(x_1)\psi^{\dagger}(x_2)|0><0|\psi(x_1)\psi(x_2):|p_1,p_2> \\
    = &\left(\mathrm{e}^{\mathrm{i}p'_1\cdot x_1+\mathrm{i}p'_2\cdot x_2}+\mathrm{e}^{\mathrm{i}p'_1\cdot x_2+\mathrm{i}p'_2\cdot x_1}\right)\left(\mathrm{e}^{-\mathrm{i}p'_1\cdot x_1-\mathrm{i}p'_2\cdot x_2}+\mathrm{e}^{-\mathrm{i}p'_1\cdot x_2-\mathrm{i}p'_2\cdot x_1}\right)\ \because\ <0|\psi(x)|p>=\mathrm{e}^{-\mathrm{i}p\cdot x} \\
    = &\mathrm{e}^{\mathrm{i}x_1\cdot(p'_1-p_1)+\mathrm{i}x_2\cdot(p'_2-p_2)}+\mathrm{e}^{\mathrm{i}x_1\cdot(p'_2-p_1)+\mathrm{i}x_2\cdot(p'_1-p_2)}+(x_1\leftrightarrow x_2)
\end{aligned}
$$
を積分すればよく, $g^2$のオーダーで
$$
\begin{aligned}
    &\frac{(-\mathrm{i}g)^2}{2}\int\left(\mathrm{e}^{\mathrm{i}x_1\cdot(p'_1-p_1)+\mathrm{i}x_2\cdot(p'_2-p_2)}+\mathrm{e}^{\mathrm{i}x_1\cdot(p'_2-p_1)+\mathrm{i}x_2\cdot(p'_1-p_2)}+(x_1\leftrightarrow x_2)\right)\mathrm{d}^4x_1\mathrm{d}^4x_2 \\
    &\cdot\frac{1}{(2\pi)^4}\int\frac{\mathrm{i}\mathrm{e}^{\mathrm{i}k\cdot(x_1-x_2)}}{k^2-m^2+\mathrm{i}\epsilon}\mathrm{d}^4k \\
    = &(-\mathrm{i}g)^2\frac{1}{(2\pi)^4}\int\frac{\mathrm{i}(2\pi)^8}{k^2-m^2+\mathrm{i}\epsilon}\left\lbrace\delta^{(4)}(p'_1-p_1+k)\delta^{(4)}(p'_2-p_2-k)\right. \\
    &\left.+\delta^{(4)}(p'_2-p_1+k)\delta^{(4)}(p'_1-p_2-k)\right\rbrace\mathrm{d}^4k \\
    = &\mathrm{i}(-\mathrm{i}g)^2\left\lbrace\frac{1}{(p_1-p'_1)^2-m^2+\mathrm{i}\epsilon}+\frac{1}{(p_1-p'_2)^2-m^2+\mathrm{i}\epsilon}\right\rbrace(2\pi)^4\delta^{(4)}(p_1+p_2-p'_1-p'_2) \\
    = &\mathrm{i}(-\mathrm{i}g)^2\left\lbrace\frac{1}{(p_1-p'_1)^2-m^2}+\frac{1}{(p_1-p'_2)^2-m^2}\right\rbrace(2\pi)^4\delta^{(4)}(p_1+p_2-p'_1-p'_2) \\
    = &\mathrm{i}\mathcal{A}_{\text{fi}}(2\pi)^4\delta^{(4)}(p_1+p_2-p'_1-p'_2)
\end{aligned}
$$
を得る. 分母は決してゼロにはならないので, $+\mathrm{i}\epsilon$の項を落とし 振幅を$\mathcal{A}_{\text{fi}}$とおいた. 

##### もう1つの例: 中間子-核子散乱
$\psi\phi\to\psi\phi$の散乱行列要素は$g^2$のオーダーで
$$
:\psi^{\dagger}(x_1)\phi(x_1)\psi^{\dagger}(x_2)\phi(x_2):\overbrace{\psi(x_1)\psi^{\dagger}(x_2)}
$$
の項を持ち, また$\psi$と$\psi^{\dagger}$を入れ換えた項も持つ. 

これを上の例と同じく変換していけばよい. 

### 3.4 Feynmanダイアグラム
今計算したい$<f|S-1|i>$のそれぞれの展開に1対1対応したダイヤグラムを描く. 

#### 3.4.1 Feynmanルール
ダイヤグラムの描き方とルールの説明は省略する. 

### 3.5 散乱振幅の例
Feynmanルールを適用して, Feynmanダイアグラムから種々の振幅を計算する. 

Feynmanダイアグラムを描くことが全てである. 

##### 改めて, 核子散乱
Figure 9[^1]. 

##### 振幅


##### 中間子-核子散乱
Figure 12[^1]. 

##### 核子-反核子散乱
Figure 13[^1]. 

##### 中間子散乱
Figure 15[^1]. 

#### 3.5.1 Mandelstam変数
上述の振幅には同じような運動量の組み合わせの項が分母に現れている. これらはMandelstam変数と呼ばれ, 
$$
\begin{aligned}
    s &= (p_1+p_2)^2=(p'_1+p'_2)^2 \\
    t &= (p_1-p'_1)^2=(p_2-p'_2)^2 \\
    u &= (p_1-p'_2)^2=(p_2-p'_1)^2
\end{aligned}
$$
で与えられる. ここで, 2つの始状態にある粒子の運動量を$p_1$と$p_2$, 2つの終状態にある粒子の運動量を$p'_1$と$p'_2$としている. 

これらの変数が意味することを掴むために, 4つの粒子全てが同じ状況を仮定する. 重心系で考えると, 始めの2粒子は4次元運動量
$$
p_1=(E,0,0,p)\text{ and }p_2=(E,0,0,-p)
$$
を持つ. その粒子は角度$\theta$で散乱し, 最終的に運動量
$$
p'_1=(E,0,p\sin\theta,p\cos\theta)\text{ and }p'_2=(E,0,-p\sin\theta,-p\cos\theta)
$$
を持つ. これらよりMandelstam変数を計算すると, 
$$
s = 4E^2\text{ and }t=-2p^2(1-\cos\theta)\text{ and }u=-2p^2(1+\cos\theta)
$$
となる. 変数$s$は衝突の質量中心の総エネルギーを測っており, 変数$t$と$u$は粒子間で交換された運動量を測っている. 改めて振幅を思い出すと, 例えば核子散乱での$\mathcal{A}\sim(t-m^2)^{-1}+(s-m^2)^{-1}$は「$t$チャネル」と「$u$チャネル」のダイヤグラムを含むという言い方をすることができる. 

さらに, この例でも確かめられることだが, Mandelstam変数の間に
$$
s+t+u=\sum_iM_i^2
$$
という関係があることに注意されたい. 

#### 3.5.2 Yukawaポテンシャル
核子散乱の振幅をどのようにして粒子間ポテンシャルのような力学量に還元するのかを示す. 

まず古典場の理論における単純な問題より始める. 実スカラー場$\phi$に対して, 全ての時間に対して持続する固定された$\delta$函数の源を持つとすれば, その実スカラー場を明らかにするには, 静的なKlein-Gordon方程式
$$
-\nabla^2\phi+m^2\phi=\delta^{(3)}(\bm{x})
$$
を解かねばならない. これにFourier変換した場を代入すれば, 
$$
(\bm{k}^2+m^2)\tilde{\phi}(\bm{x})=1
$$
を得るので, 結局解は, 
$$
\begin{aligned}
    \phi(\bm{x}) &= \frac{1}{(2\pi)^3}\int\frac{\mathrm{e}^{\mathrm{i}\bm{k}\cdot\bm{x}}}{\bm{k}^2+m^2}\mathrm{d}^3k \\
    &= \frac{1}{(2\pi)^2}\int_0^{\infty}\frac{k^2}{k^2+m^2}\frac{2\sin kr}{kr}\mathrm{d}k \\
    &= \frac{1}{(2\pi)^2r}\int_{-\infty}^{\infty}\frac{k\sin kr}{k^2+m^2}\mathrm{d}k \\
    &= \frac{1}{2\pi r}\Re\int_{-\infty}^{\infty}\frac{k\mathrm{e}^{\mathrm{i}kr}}{2\pi\mathrm{i}(k^2+m^2)}\mathrm{d}k \\
    &= \frac{1}{4\pi r}\mathrm{e}^{-mr}
\end{aligned}
$$
となる. つまり場は距離$1/m$で指数関数的に減衰することであり, これが中間子のCompton波長である. 

古典的な類推より, この場が静的なポテンシャルとして感じられていると考えられるが, 少なくとも$M\gg m$の極限では実際そうである. このような類推も可能だが, やはり正確にするには量子振幅を直接考えねばならない. 

核子の散乱振幅をポテンシャルを通して相互作用している2つの粒子に対する非相対論量子力学における確率振幅と紐付けたい. 質量中心系で, $\bm{p}\equiv\bm{p}_1=-\bm{p}_2$, $\bm{p}'\equiv\bm{p}'_1=-\bm{p}'_2$ であり, 非相対論的極限というのは$|\bm{p}|\ll M$, 同時に運動量保存より$|\bm{p}'|\ll M$を意味する. まずこの極限では散乱振幅は変わらず, 
$$
\mathrm{i}\mathcal{A} = \mathrm{i}g^2\left\lbrace\frac{1}{(\bm{p}-\bm{p}')^2+m^2}+\frac{1}{(\bm{p}+\bm{p}')^2+m^2}\right\rbrace
$$
である. 距離$\bm{r}$だけ離れてポテンシャル$U(\bm{r})$を通して相互作用している2つの粒子を考える. Born近似してその振幅は, 
$$
<\bm{p}'|U(\bm{r})|\bm{p}> = -\mathrm{i}\int U(\bm{r})\mathrm{e}^{-\mathrm{i}(\bm{p}-\bm{p}')\cdot\bm{r}}\mathrm{d}^3r
$$
で与えられる. これらを比べて相対論的因子$(2M)^2$を得る. この因子を含めて, 2つの振幅に対する表式を等号で結ぶと, 
$$
\int U(\bm{r})\mathrm{e}^{-\mathrm{i}(\bm{p}-\bm{p}')\cdot\bm{r}}\mathrm{d}^3r = \frac{-\lambda^2}{(\bm{p}-\bm{p}')^2+m^2}
$$
を得る. ここで無次元のパラメータ$\lambda=g/2M$を導入した. これを単純に
$$
U(\bm{r}) = -\lambda^2\frac{1}{(2\pi)^2}\int\frac{\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{r}}}{\bm{p}^2+m^2}\mathrm{d}^3p
$$
と変形する. 

Oh...

古典場の理論のときにやったように解けば, 
$$
U(\bm{r}) = \frac{-\lambda^2}{4\pi r}\mathrm{e}^{-mr}
$$
とYukawaポテンシャルを得る. 

#### 3.5.3 $\phi^4$理論
Figure 16[^1]から得た散乱振幅を用いて, 小節[3.5.2](#352-yukawaポテンシャル)と同様に比較して, $\phi^4$理論に対するポテンシャルを得る. 

#### 3.5.4 連結ダイアグラムと切断ダイアグラム
Feynmanダイヤグラムを書き下すときに2つ警告すべきことがある. これらは「始状態と終状態が自由場の理論の固有状態となっている」という仮定をおいたことに関係している. 
- 我々は連結ダイアグラムだけ考えている. このことは, 自由場の理論の真空$|0>$は相互作用している理論におけるそれ$|\Omega>$と一致していないという事実に関係している. 
- 我々は外部の線にループを持つダイヤグラムを考えていない. このことは, 自由場の理論の1粒子状態が相互作用している理論のそれと一致していないという事実に関係している. 

### 3.6 我々の測定するもの: 断面積と崩壊率
本節では断面積と崩壊率を計算するが, 散乱行列要素$<f|S-1|i>$が, 無限の空間で取り組んでいることに起因する因子$(2\pi)^4\delta^{(4)}(p_F-p_I)$から生じている(つまり$\delta$函数の2乗を結局扱う)ということに小さな巧妙さがある. 

#### 3.6.1 Fermiの黄金律
Dysonの公式からFermiの黄金率を導くことより始める. 異なる固有値$E_m\neq E_n$に属する固有状態$|m>$と$|n>$に対して, 相互作用における主要項を計算すると, 
$$
\begin{aligned}
    <m|U(t)|n> &= -\mathrm{i}<m|\int_0^tH_I(t')\mathrm{d}t'|n> \\
    &= -\mathrm{i}<m|H_{\text{int}}|n>\int_0^t\mathrm{e}^{\mathrm{i}\omega t'}\mathrm{d}t'\text{  where  }\omega\equiv E_m-E_n \\
    &= -<m|H_{\text{int}}|n>\frac{\mathrm{e}^{\mathrm{i}\omega t}-1}{\omega}
\end{aligned}
$$
となり, 時刻$t$における状態間の遷移確率
$$
\begin{aligned}
    P_{n\to m}(t) &= |<m|U(t)|n>|^2=2|<m|H_{\text{int}}|n>|^2\left(\frac{1-\cos\omega t}{\omega^2}\right) \\
    &\to 2\pi|<m|H_{\text{int}}|n>|^2\delta(\omega)t\text{  as  }t\to\infty
\end{aligned}
$$
を得る. したがって単位時間における転移確率として, Fermiの黄金率
$$
\dot{P}_{n\to m}(t) =  2\pi|<m|H_{\text{int}}|n>|^2\delta(\omega)
$$
を得る. 

#### 3.6.2 崩壊率
運動量$p_I$を持つ1粒子$|i>$が運動量$p_j$を持ち, 総運動量は$p_F=\sum_jp_j$である幾つかの粒子$|f>$に崩壊する確率を計算する. この確率は, 
$$
\begin{aligned}
    P &= \frac{|<f|S|i>|^2}{<f|f><i|i>} \\
    <i|i> &= (2\pi)^32E_{\bm{p}_I}\delta^{(3)}(0) \equiv 2E_{\bm{p}_I}V \\
    <f|f> &= \prod_{\text{final states}}2E_{\bm{p}_j}V
\end{aligned}
$$
によって与えられる. いま始状態を$\bm{p}_I=\bm{0}$と$2E_{\bm{p}_I}=m$の状態とすれば, 崩壊確率
$$
P = \frac{|\mathcal{A}_{\text{fi}}|^2}{2mV}(2\pi)^4\delta^{(4)}(p_I-p_F)VT\prod_{\text{final states}}\frac{1}{2E_{\bm{p}_j}V}
$$
を得る. $\mathcal{A}_{\text{fi}}$はこれまで計算してきた振幅を表す. Lorentz不変な測度を用いれば, 崩壊率は
$$
\begin{aligned}
    \dot{P} &= \frac{1}{2m}\sum_{\text{final states}}\int |\mathcal{A}_{\text{fi}}|^2\mathrm{d}\Pi \\
    \text{where }\mathrm{d}\Pi &= (2\pi)^4\delta^{(4)}(p_I-p_F)\frac{1}{(2\pi)^3}\prod_{\text{final states}}\frac{1}{2E_{\bm{p}_j}}\mathrm{d}^3p_j
\end{aligned}
$$
となる. 

#### 3.6.3 断面積
2つの粒子のビームを衝突させる. 散乱過程が固定された角$(\theta,\phi)$で起こる確率を表す微分断面積$\mathrm{d}\sigma$を量子場の理論から計算する. それは
$$
\mathrm{d}\sigma = \frac{1}{4E_1E_2V}\frac{1}{F}|\mathcal{A}_{\text{fi}}|^2\mathrm{d}\Pi
$$
で与えられ, ここで$E_1$と$E_2$を入射粒子のエネルギー, $F$を入射フラックスとした. これまで考えてきたのは単位体積中の1粒子であるので, フラックスは3次元速度を用いて, $F=|\bm{v}_1-\bm{v}_2|/V$で与えられるので, このとき微分断面積は
$$
\mathrm{d}\sigma = \frac{1}{4E_1E_2}\frac{1}{|\bm{v}_1-\bm{v}_2|}|\mathcal{A}_{\text{fi}}|^2\mathrm{d}\Pi
$$
となる. 

区切りが良いのでここで終わる. 

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/tong-qft/tong-qft.pdf" data-iframely-url="//cdn.iframe.ly/ZUFTTag"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[David Tong: Lectures on Quantum Field Theory](http://www.damtp.cam.ac.uk/user/dt281/qft.html)