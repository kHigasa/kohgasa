---
title: 一人輪講第5回
date: "2020-12-27T02:00:00.000Z"
description: "PDF整理のための一人輪講第5回. David Tong, 2006, Lectures on Quantum Field Theory, 1-25 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## Chapter0 導入
### 0.1 単位と目盛
興味を引く導入部だが, 省く. 

自然単位系$c=\hbar =1$を用いる. 

## Chapter1 古典場の理論
### 1.1 場の動力学
場とは空間上の全ての位置と時間の組$(\bm{x},t)$で定義される量である. 場の理論では, $a$と$\bm{x}$でラベル付けされた場$\phi_a(\bm{x},t)$のダイナミクスに興味がある. そういうわけで無限の自由度を持つ系を扱うことになる. 位置という概念は場の理論では古典力学における力学変数から単なるラベルに追いやられていることに注意されたい. 

##### 例:電磁場
最も見慣れた場の例は電磁場$\bm{E}(\bm{x},t),\bm{B}(\bm{x},t)$である. これらの場は空間的に3次元のベクトルである. より高級な電磁気の扱いにおいて, 1つの4次元場$A^{\mu}(\bm{x},t)=(\phi,\bm{A})\ ,\ \mu=0,1,2,3$から, これらの2つの3次元ベクトルを導くことができる. ここで$\mu$という添字によってこの場が時空間におけるベクトルであることがわかる. 

高等学校で言われるような電磁場の連関がここで明示される. 

##### Lagrangian
場のダイナミクスは$\phi(\bm{x},t),\dot{\phi}(\bm{x},t),\nabla\phi(\bm{x},t)$の函数であるLagrangianによって支配される. このコースで学ぶ全ての系において, Lagrangianは
$$
L(t) = \int\mathcal{L}(\phi_a,\partial_{\mu}\phi_a)\mathrm{d}\bm{x}
$$
の形式で与えられる. ここで$\mathcal{L}$はLagrangian密度と呼ばれる. また作用は, 
$$
S = \int_{t_1}^{t_2}\int\mathcal{L}\mathrm{d}\bm{x}\ \mathrm{d}t = \int\mathcal{L}\ \mathrm{d}^4x
$$
である. 

最小作用の原理によって運動の方程式を決定できる. つまり終わりの点を固定し, 経路を変化させて$\delta S=0$を要求するということである. 作用の変分は, 
$$
\begin{aligned}
    \delta S &= \int\left\lbrace\frac{\partial\mathcal{L}}{\partial\phi_a}\delta\phi_a+\frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi_a)}\delta(\partial_{\mu}\phi_a)\right\rbrace\mathrm{d}^4x \\
    &= \int\left[\left\lbrace\frac{\partial\mathcal{L}}{\partial\phi_a}-\partial_{\mu}\left(\frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi_a)}\right)\right\rbrace\delta\phi_a+\partial_{\mu}\left(\frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi_a)}\delta\phi_a\right)\right]\mathrm{d}^4x
\end{aligned}
$$
となる. ここで無限に広い空間で$\delta\phi_a(\bm{x},t)$が消えるとすれば, 上述の要求により, 場$\phi_a$に対するEuler-Lagrangeの方程式(運動の方程式)
$$
\partial_{\mu}\left(\frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi_a)}\right)-\frac{\partial\mathcal{L}}{\partial\phi_a}=0
$$
を得る. 

#### 1.1.1 例:Klein-Gordon方程式
#### 1.1.2 次の例:1次のLagrangian
#### 1.1.3 最後の例:Maxwell方程式
LagrangianからEuler-Lagrangeの方程式を用いて, 目的の方程式を得る例が示されている. Maxwell方程式については, う〜ん()となってしまったが, まぁこれらもイントロと思って先に進む. 

#### 1.1.4 局所性, 局所性, とにかく局所性
上述の各例において, Lagrangianは局所的である. これの意味するところは, Lagrangianにおいて, $\bm{x}\neq\bm{y}$における$\phi(\bm{x},t)$の$\phi(\bm{y},t)$への直接的な結合(これらの単純積のような)を表す項はないということである. アプリオリにはこれに対する理由はない. しかしラベル$\bm{x}$に対してその最も近傍では, gradientの項$(\nabla\phi)^2$を通して$\phi(\bm{x})$と$\phi(\bm{x}+\delta\bm{x})$の結合を得る. この局所性は管見の限り全ての自然を説明する理論の鍵となる性質である. 実際場の理論を古典物理に持ち込む主要な理由の一つはこの局所性を実装することであり, これからも局所的なLagrangianのみを考える. 

### 1.2 Lorentz不変性
空間と時間が対等な立場に位置づけられる場の理論を構築したい. 理論はLorentz変換
$$
x^{\mu} \to (x')^{\mu}=A^{\mu}_{\ \ \nu}x^{\nu}
$$
の下で不変である. $A^{\mu}_{\ \ \nu}$は
$$
A^{\mu}_{\ \ \sigma}\eta^{\sigma\tau}A^{\nu}_{\ \ \tau} = \eta^{\mu\nu}
$$
の関係を満たす. 

Lorentz変換は場の表示を持つ. 単純な例はスカラー場で, Lorentz変換$x\to\Lambda x$の下で$\phi(x)\to\phi'(x)=\phi(\Lambda^{-1}x)$である. 引数に逆変換が現れるのは, 場が本当にシフトされる能動的な変換を行っているからである. 

Lorentz不変な理論の定義は, $\phi(x)$が運動方程式の解となるならば, $\phi(\Lambda^{-1}x)$もまた解になるということである. 我々は作用がLorentz不変であるということを要求することによってこの性質を保つことを保証することができる. 

##### 例1:Klein-Gordon方程式
##### 例2:1次のLagrangian
##### 例3:Maxwell方程式
それぞれLorentz不変・可変・不変であることが説明されている. 

### 1.3 対称性
Noetherの定理を場の理論的枠組みにおいて再構成することによって, 場の理論における対称性の役割を論じることを始める. 

#### 1.3.1 Noetherの定理
Lagrangianのあらゆる連続的対称性は保存電流$j^{\mu}(x)$を生じさせる. それは運動の方程式によって
$$
\begin{aligned}
    \partial_{\mu}j^{\mu} &= 0 \\
    (\frac{\partial j^0}{\partial t}+\nabla\cdot\bm{j} &= 0)
\end{aligned}
$$
と示唆される. 

##### コメント
保存電流の存在は保存電荷の存在よりも強い主張である. それというのはそれが電荷が局所的にも保存されるということを示唆するからである. 

##### Noetherの定理の証明
無限小の変化を考えることによって定理を証明する. 連続的対称性があればいつもこの方法でよいだろう. 我々はある函数の集合$F^{\mu}(\phi)$に対して, Lagrangianが全微分
$$
\delta\mathcal{L} = \partial_{\mu}F^{\mu}
$$
だけ変化するならば, 変換
$$
\delta\phi_a(x) = X_a(\phi)
$$
が対称的であるという. Noetherの定理を導くために, 初めに場$\delta\phi_a$の任意の変換をすることを考える. Lagrangianの変分は, 
$$
\delta\mathcal{L} = \partial_{\mu}\left(\frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi_a)}\delta\phi_a\right)
$$
である. 対称的な変換に対しては, 
$$
j^{\mu} = \frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi_a)}X_a(\phi)-F^{\mu}(\phi)
$$
と選べば, Noetherの定理$\partial_{\mu}j^{\mu}=0$が示された. 

#### 1.3.2 例:並進とエネルギー-運動量テンソル
無限小変換
$$
x^{\nu} \to x^{\nu}-\epsilon^{\nu} \Rightarrow \phi_a(x)\to\phi_a(x)+\epsilon^{\nu}\partial_{\nu}\phi_a(x)
$$
を考える. ここに場の変換の正の符号は能動的な変換を行っているために現れたものである. 同様に一旦Lagrangianへ特定の場の形$\phi(x)$を代入すれば, Lagrangian自体もまた
$$
\mathcal{L}(x) \to \mathcal{L}(x)+\epsilon^{\nu}\partial_{\nu}\mathcal{L}(x)
$$
と変換を受ける. Lagrangianにおける変化は全微分であるので, Noetherの定理を想い起こさせ, 4つの保存電流$(j^{\mu})_{\nu}$の変換$\epsilon^{\nu}\ ,\ \nu=0,1,2,3$の内1つは, 
$$
(j^{\mu})_{\nu} = \frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi_a)}\partial_{\mu}\phi_a-\delta^{\mu}_{\ \ \nu}\mathcal{L} \equiv T^{\mu}_{\ \ \nu}
$$
とかける. $T^{\mu}_{\ \ \nu}$はエネルギー-運動量テンソルと呼ばれるものである. それは$\partial_{\mu}T^{\mu}_{\ \ \nu}=0$を満たす. 4つの保存量は, 
$$
E = \int T^{00}\mathrm{d}^3x\ \ \ \text{and}\ \ \ P^i = \int T^{0i}\mathrm{d}^3x
$$
によって与えられ, ここで$E$は場の全エネルギー, $P^i$は場の全運動量を示す. 

##### エネルギー-運動量テンソルの例
最も単純なスカラー場の場合が述べられている. 

##### キュートトリック
Hmmm. よくわかりません. 

#### 1.3.3 もう一つの例:Lorentz変換と角運動量
まずLorentz変換の無限小の形式
$$
\Lambda^{\mu}_{\ \ \nu} = \delta^{\mu}_{\ \ \nu}+\omega^{\mu}_{\ \ \nu}
$$
を考える. ここで$\omega^{\mu}_{\ \ \nu}$が無限小である. (少し表記が判り難いな, まぁ回転を意図していると汲み取ればそのこころは理解るが).$\Lambda^{\mu}_{\ \ \nu}$の満たす式より, 
$$
\omega^{\mu\nu}+\omega^{\nu\mu} = 0
$$
を得る. したがってLorentz変換の無限小部の形$\omega^{\mu\nu}$は反対称行列でなければならないということである. いまスカラー場の変換は, 
$$
\begin{aligned}
    \phi(x) \to \phi'(x) &= \phi(\Lambda^{-1}x) \\
    &= \phi(x^{\mu}-\omega^{\mu}_{\ \ \nu}x^{\nu}) \\
    &= \phi(x^{\mu})-\omega^{\mu}_{\ \ \nu}x^{\nu}\partial_{\mu}\phi(x)
\end{aligned}
$$
によって与えられる. これより, 
$$
\delta\phi = -\omega^{\mu}_{\ \ \nu}x^{\nu}\partial_{\mu}\phi
$$
となることがわかる. 同様にして, Lagrangian密度は
$$
\begin{aligned}
    \delta\mathcal{L} &= -\omega^{\mu}_{\ \ \nu}x^{\nu}\partial_{\mu}\mathcal{L} \\
    &= -\partial_{\mu}(\omega^{\mu}_{\ \ \nu}x^{\nu}\mathcal{L})\ \because\ \omega^{\mu}_{\mu}=0
\end{aligned}
$$
のように変換される. やはりLagrangianは全微分だけ変化するので, $F^{\mu}=-\omega^{\mu}_{\ \ \nu}x^{\nu}\mathcal{L}$としてNoetherの定理を適用すれば, 保存電流
$$
\begin{aligned}
    j^{\mu} &= -\frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi)}\omega^{\rho}_{\ \ \nu}x^{\nu}\partial_{\rho}\phi+\omega^{\mu}_{\ \ \nu}x^{\nu}\mathcal{L} \\
    &= -\omega^{\rho}_{\ \ \nu}\left\lbrace\frac{\partial\mathcal{L}}{\partial(\partial_{\mu}\phi)}x^{\nu}\partial_{\rho}\phi -\delta^{\mu}_{\ \ \rho}x^{\nu}\mathcal{L}\right\rbrace = -\omega^{\rho}_{\ \ \nu}T^{\mu}_{\ \ \rho}x^{\nu}
\end{aligned}
$$
を得る. 前の例と違って, この電流の表式は無限小の部分$\omega^{\mu}_{\ \ \nu}$の選択余地を残している. しかし実際には, 我々は$\omega^{\mu}_{\ \ \nu}$をそれぞれ選んで, それらに対応する6つの異なった保存電流に還元するべきであり, それらは
$$
(\mathcal{J}^{\mu})^{\rho\sigma} = x^{\rho}T^{\mu\sigma}-x^{\sigma}T^{\mu\rho}
$$
と書き下せる. $\rho ,\sigma =1,2,3$に対して, Lorentz変換は回転となり, 3つの保存電荷は場の全角運動量を与える, 
$$
Q^{ij} = \int(x^iT^{0j}-x^jT^{0i})\mathrm{d}^3x\ .
$$
一方ブーストについては, 保存電荷は
$$
Q^{0i} = \int(x^0T^{0i}-x^iT^{00})\mathrm{d}^3x
$$
である. これらが保存されることから, 
$$
\begin{aligned}
    0= \frac{\mathrm{d}Q^{0i}}{\mathrm{d}t} &= \int T^{0i}\mathrm{d}^3x+t\int\frac{\partial T^{0i}}{\partial t}-\frac{\mathrm{d}}{\mathrm{d}t}\int x^iT^{00}\mathrm{d}^3x \\
    &= P^i+t\frac{\mathrm{d}P^i}{\mathrm{d}t}-\frac{\mathrm{d}}{\mathrm{d}t}\int x^iT^{00}\mathrm{d}^3x
\end{aligned}
$$
となることがわかる. しかし我々はすでに$P^i$が保存されることはわかっているので, ブーストの下での不変性の結果は結局
$$
\frac{\mathrm{d}}{\mathrm{d}t}\int x^iT^{00}\mathrm{d}^3x = \text{const.}
$$
となる. これの主張するところは, 場のエネルギー中心が一定の速度で移動しているということである. 

#### 1.3.4 内部対称性
内部対称性は場の変換を含むだけで, 時空間における全ての点で同じように振る舞うものである. 最も単純な例は複素スカラー場$\psi(x)=(\phi_1(x)+\mathrm{i}\phi_2(x))/\sqrt{2}$に対して生じる. 我々は実Lagrangianを
$$
\mathcal{L} = \partial_{\mu}\psi^*\partial^{\mu}\psi-V(|\psi|^2)
$$
によって作ることができる. ここでポテンシャル$V$は$|\psi|^2=\psi^*\psi$における一般の多項式である. 運動の方程式を見つけるために, $\phi_1$と$\phi_2$に関して$\psi$を展開して以前と同様にする. $\psi$と$\psi^*$を独立な変数として扱い, それら両方に関して作用を変化させることは簡単である. 例えば$\psi^*$に関して変化させることにより, 運動の方程式
$$
\partial_{\mu}\partial^{\mu}\psi+\frac{\partial V(|\psi|^2)}{\partial\psi^*} = 0
$$
を得る. ここでLagrangianは$\phi_1$と$\phi_2$, つまり$\psi$の位相を回転する連続的対称性
$$
\psi\to\mathrm{e}^{\mathrm{i}\alpha}\psi\ \ \ \text{or}\ \ \ \delta\psi=\mathrm{i}\alpha\psi
$$
を持つ. ここで後者の方程式は無限小の$\alpha$より得る. 加えてLagrangianはこの変化の下で不変であるので, $\delta\mathcal{L}=0$であり, 関連する保存電流は
$$
j^{\mu} = \mathrm{i}(\partial^{\mu}\psi^*)`\psi-\mathrm{i}\psi^*(\partial^{\mu}\psi)
$$
とかける. 

##### 非Abel内部対称性
全て同じ質量を持つ$N$個のスカラー場$\psi_a$とLagrangian
$$
\mathcal{L} = \frac{1}{2}\sum_{a=1}^N\partial_{\mu}\phi_a\partial^{\mu}\phi_a-\frac{1}{2}\sum_{a=1}^Nm^2\phi_a^2-g\left(\sum_{a=1}^N\phi_a^2\right)^2
$$
を考える. この場合において, Lagrangianは非Abel対称群$G=SO(N)$の下で不変である(この場合において実際は$O(N))$. 同様にして, 複素場から$SU(N)$対称群の下で不変である理論を構築することができる. この種の非Abel対称性は局所的ゲージ対称性と区別して, 大域的対称性と呼ばれる. 

##### もう一つのキュートトリック
内部対称性$\delta\phi=\alpha\phi$(これに対してLagrangianは不変である)に関連した保存電流を決定する素早い方法がある. ここに$\alpha$は定数実数である. いまこの変換で$\alpha$が時空間に依存するもの$\alpha(x)$を実行することを考えると, 作用はもはや不変でない. しかしながらその変化は
$$
\delta\mathcal{L} = (\partial_{\mu}\alpha)h^{\mu}(\phi)
$$
の形でなければならない. なぜなら, $\alpha$が定数のとき$\delta\mathcal{L}=0$であるからである. それゆえ作用における変化は, 
$$
\delta S = \int\delta\mathcal{L}\mathrm{d}^4x = -\int\alpha(x)\partial_{\mu}h^{\mu}\mathrm{d}^4x
$$
であり, これは運動の方程式が満たされる$\delta S=0$とき, 
$$
\partial_{\mu}h^{\mu} = 0
$$
であるということを意味する. これより函数$h^{\mu}$を保存電流$j^{\mu}$と同定することができるとわかる. この見方だと, 電流に寄与するのは作用におけるポテンシャル項ではなく微分項であることが強調されている. 

### 1.4 Hamiltonian形式
本講の場の理論ではLagrangian形式と経路積分の方法を取らず, Hamiltonian形式と正準量子化の方法を取る. $\phi_a(x)$に共軛な運動量$\pi^a(x)$を
$$
\pi^a(x) = \frac{\partial\mathcal{L}}{\partial\dot{\phi}_a}
$$
と定義することから始める. 共軛運動量$\pi^a(x)$は場$\phi_a(x)$自体と同じで$x$の函数である. Hamiltonian密度は古典力学と同様に
$$
\mathcal{H} = \pi^a(x)\dot{\phi}_a(x)-\mathcal{L}(x)
$$
によって与えられ, ここで共軛運動量により場の微分を除去している. そうするとHamiltonianはこれを積分して, 
$$
H = \int\mathcal{H}\mathrm{d}^3x
$$
となる. 

##### 例:実スカラー場
Lagrangian
$$
\mathcal{L} = \frac{1}{2}\dot{\phi}^2-\frac{1}{2}(\nabla\phi)^2+V(\phi)
$$
に対して, 運動量は$\pi=\dot{\phi}$で与えられ, それよりHamiltonian
$$
H = \int\left(\frac{1}{2}+\frac{1}{2}(\nabla\phi)^2+V(\phi)\right)\mathrm{d}^3x
$$
を得る. HamiltonianはこのLagrangianの例で前述した総エネルギーの定義に一致していることに注意されたい. 

## 2. 自由場 
### 2.1 正準量子化
場$\phi_a(\bm{x})$と共軛運動量$\pi^b(\bm{x})$に対して, 量子力学と同様にする. そうすれば量子場は交換関係
$$
\begin{aligned}
    [\phi_a(\bm{x}), \phi_b(\bm{y})] &= [\pi^a(\bm{x}), \pi^b(\bm{y})] = 0 \\
    [\phi_a(\bm{x}), \pi^b(\bm{y})] &= \mathrm{i}\delta^{(3)}(\bm{x}-\bm{y})\delta_a^b
\end{aligned}
$$
に従う空間の演算子函数となる. ここで空間と時間を分けたので, Lorentz不変かどうか全くわからなくなっていることに注意されたい. いまSchr$\text{\"{o}}$dinger描像で記述している. 状態$|\psi>$における全ての時間依存性は普通のSchr$\text{\"{o}}$dinger方程式
$$
\mathrm{i}\frac{\mathrm{d}|\psi>}{\mathrm{d}t} = H|\psi>
$$
に従う. 

典型的には, 量子論に関して我々の知りたい情報はHamiltonian$H$のスペクトルである. 量子場の理論では, 普通これを知るのはとても難しい. その1つの理由は, 場が無限の自由度を持っているということである. しかしながら, 自由場の理論として知られる理論に対しては, 各自由度が独立に発展するので, ダイナミクスを記述する方法を見つけることができる. 例えば, 最も単純な相対論的自由場は実スカラー場$\phi(\bm{x},t)$に対する古典的なKlein-Gordon方程式$\partial_{\mu}\partial^{\mu}\phi+m^2\phi=0$である. 自由度が互いに分離する座標系を明示するために, Fourier変換
$$
\phi(\bm{x},t) = \frac{1}{(2\pi)^3}\int\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}\phi(\bm{p},t)\mathrm{d}\bm{p}
$$
を行う. そうすると$\phi(\bm{p},t)$は, 
$$
\left(\frac{\partial^2}{\partial t^2}+(\bm{p}^2+m^2)\right)\phi(\bm{p},t) = 0
$$
を満たす. したがって, $\bm{p}$の各値に対して, $\phi(\bm{p},t)$は振動数$\omega_{\bm{p}}=\sqrt{\bm{p}^2+m^2}$で振動する調和振動子の方程式の解となることがわかる. Klein-Gordon方程式に対する最も一般的な解は単純な調和振動子の線型結合であり, 各振動子は異なる振幅を持つ異なった振動数で振動している. $\phi(\bm{x},t)$を量子化するために, 我々はその無限個の調和振動子を量子化しなければならない. 

#### 2.1.1 単純な調和振動子
初等的な補節故省略. 

#### 2.1.2 自由スカラー場
いま自由スカラー場に対する調和振動子の量子化に取り組む. $\phi$と$\pi$を生成消滅演算子$a_{\bm{p}}^{\dagger},a_{\bm{p}}$の線形の無限和
$$
\begin{aligned}
    \phi(\bm{x}) &= \frac{1}{(2\pi)^3}\int\frac{1}{\sqrt{2\omega_{\bm{p}}}}(a_{\bm{p}}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}+a_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}})\mathrm{d}\bm{p} \\
    \pi(\bm{x}) &= \frac{1}{(2\pi)^3}\int(-\mathrm{i})\sqrt{\frac{\omega_{\bm{p}}}{2}}(a_{\bm{p}}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}-a_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}})\mathrm{d}\bm{p}
\end{aligned}
$$
で書くとする. 

##### 主張
$\phi$と$\pi$に対する交換関係は次に示す$a_{\bm{p}}$と$a_{\bm{p}}^{\dagger}$に対する交換関係
$$
\begin{aligned}
    [a_{\bm{p}}, a_{\bm{q}}] &= [a_{\bm{p}}^{\dagger}, a_{\bm{q}}^{\dagger}] = 0 \\
    [a_{\bm{p}}, a_{\bm{q}}^{\dagger}] &= (2\pi)^3\delta^{(3)}(\bm{p}-\bm{q})
\end{aligned}
$$
と同値であり, 
$$
\begin{aligned}
    [\phi(\bm{x}), \phi(\bm{y})] &= [\pi(\bm{x}), \pi(\bm{y})] = 0 \\
    [\phi(\bm{x}), \pi(\bm{y})] &= \mathrm{i}\delta^{(3)}(\bm{x}-\bm{y})
\end{aligned}
$$
となる. 

##### 証明
実直に示す. $[a_{\bm{p}}, a_{\bm{q}}^{\dagger}] = (2\pi)^3\delta^{(3)}(\bm{p}-\bm{q})$を仮定すれば, 
$$
\begin{aligned}
    [\phi(\bm{x}), \pi(\bm{y})] &= \frac{1}{(2\pi)^6}\iint\left(-\frac{\mathrm{i}}{2}\right)\left(-[a_{\bm{p}}, a_{\bm{q}}^{\dagger}]\mathrm{e}^{\mathrm{i}(\bm{p}\cdot\bm{x}-\bm{q}\cdot\bm{y})}+[a_{\bm{p}}, a_{\bm{q}}^{\dagger}]\mathrm{e}^{-\mathrm{i}(\bm{p}\cdot\bm{x}-\bm{q}\cdot\bm{y})}\right)\mathrm{d}\bm{p}\mathrm{d}\bm{q} \\
    &= \frac{1}{(2\pi)^3}\int\left(-\frac{\mathrm{i}}{2}\right)\left(-\mathrm{e}^{\mathrm{i}\bm{p}\cdot(\bm{x}-\bm{y})}-\mathrm{e}^{\mathrm{i}\bm{p}\cdot(\bm{y}-\bm{x})}\right)\mathrm{d}\bm{p} \\
    &= \mathrm{i}\delta^{(3)}(\bm{x}-\bm{y})
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

場の量子論によく現れる$\delta$函数の積分形である. 

##### Hamiltonian
生成消滅演算子を用いてHamiltonianを計算すれば, 
$$
\begin{aligned}
    H = &\frac{1}{2}\int\left(\pi^2+(\nabla\phi)^2+m^2\phi^2\right)\mathrm{d}\bm{x} \\
    = &\frac{1}{2}\frac{1}{(2\pi)^6}\int\left\lbrace -\frac{\sqrt{\omega_{\bm{p}}\omega_{\bm{q}}}}{2}(a_{\bm{p}}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}-a_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}})(a_{\bm{q}}\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{x}}-a_{\bm{q}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{q}\cdot\bm{x}})\right. \\
    &+\frac{1}{2\sqrt{\omega_{\bm{p}}\omega_{\bm{q}}}}\mathrm{i}\bm{p}(a_{\bm{p}}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}-a_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}})\mathrm{i}\bm{q}(a_{\bm{q}}\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{x}}-a_{\bm{q}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{q}\cdot\bm{x}}) \\
    &\left.+\frac{m^2}{2\sqrt{\omega_{\bm{p}}\omega_{\bm{q}}}}(a_{\bm{p}}\mathrm{e}^{\mathrm{i}\bm{p}\cdot\bm{x}}+a_{\bm{p}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{p}\cdot\bm{x}})(a_{\bm{q}}\mathrm{e}^{\mathrm{i}\bm{q}\cdot\bm{x}}+a_{\bm{q}}^{\dagger}\mathrm{e}^{-\mathrm{i}\bm{q}\cdot\bm{x}})\right\rbrace\mathrm{d}\bm{p}\mathrm{d}\bm{q} \\
    = &\frac{1}{4}\frac{1}{(2\pi)^3}\int\frac{1}{\omega_{\bm{p}}}\left\lbrace (-\omega_{\bm{p}}^2+\bm{p}^2+m^2)\left(a_{\bm{p}}a_{-\bm{p}}+a_{\bm{p}}^{\dagger}a_{-\bm{p}}^{\dagger}\right)\right. \\
    &\left.+(\omega_{\bm{p}}^2+\bm{p}^2+m^2)\left(a_{\bm{p}}a_{\bm{p}}^{\dagger}+a_{\bm{p}}^{\dagger}a_{\bm{p}}\right)\right\rbrace\mathrm{d}\bm{p} \\
    = &\frac{1}{2}\frac{1}{(2\pi)^3}\int\omega_{\bm{p}}\left(a_{\bm{p}}a_{\bm{p}}^{\dagger}+a_{\bm{p}}^{\dagger}a_{\bm{p}}\right)\mathrm{d}\bm{p} \\
    = &\frac{1}{(2\pi)^3}\int\omega_{\bm{p}}\left\lbrace a_{\bm{p}}^{\dagger}a_{\bm{p}}+\frac{1}{2}(2\pi)^3\delta^{(3)}(0)\right\rbrace\mathrm{d}\bm{p}
\end{aligned}
$$
を得る. ここで$\delta$函数はゼロで評価されているので, 無限大のスパイクを持つ. その上, $\omega_{\bm{p}}$にかけての積分は大きな$\bm{p}$で発散する. これらをどう取り扱うべきか. この無限大が初めて現れる基底状態を考えることから始める. 

### 2.3 真空
調和振動子に対する手続きに従って, 真空状態$|0>$を
$$
a_{\bm{p}}|0> = 0\ \forall\bm{p}
$$
によって定義する. この定義より, 基底状態のエネルギー$E_0$は
$$
H|0> \equiv E_0|0> = \left(\int\frac{1}{2}\omega_{\bm{p}}\delta^{(3)}(0)\mathrm{d}\bm{p}\right)|0> = \infty |0>
$$
のように無限大となる. 量子場の理論の話題は無限大で蔓延している. それぞれは普通, 我々の扱いがなにか間違っているということや, 誤った問を立てていることなどを示している. 今回の無限大がどこから来たのか, またそれをどのように扱うべきなのかを調査する. 

2つの無限大の内, 最初の無限大(赤外発散)は空間が無限に大きいことより生ずる. この種の無限大を抽出するために, 理論を幅$L$の体積$V$の箱に適用してみる. これにより場に周期的境界条件が課される. そして$L\to\infty$の極限を取れば, 
$$
(2\pi)^3\delta^{(3)}(0) = \lim_{L\to\infty}\int_{-\frac{L}{2}}^{\frac{L}{2}}\left.\mathrm{e}^{\mathrm{i}\bm{x}\cdot\bm{p}}\right|_{\bm{p}=\bm{0}}\mathrm{d}\bm{x} = \lim_{L\to\infty}\int_{-\frac{L}{2}}^{\frac{L}{2}}\mathrm{d}\bm{x} = V
$$
を得る. それ故$\delta(0)$の発散はエネルギー密度$\mathcal{E}_0$でなく全エネルギーを計算したことから生じたのである. エネルギー密度$\mathcal{E}_0$を求めるには, 単に全エネルギーを体積で割ればよく, 
$$
\mathcal{E}_0 = \frac{E}{V} = \frac{1}{(2\pi)^3}\int\frac{1}{2}\omega_{\bm{p}}\mathrm{d}\bm{p}
$$
であり, 各調和振動子に対する基底状態のエネルギーの和とみなされる. これは未だ高周波(短距離)で無限大で$|\bm{p}|\to\infty$で$\mathcal{E}_0\to\infty$である(紫外発散). この種の無限大は我々の驕りから生じている. 我々の理論が高周波に対応する任意の短距離に対して正しいと仮定する. これは明らかに不条理であるので, 我々の理論が高周波では破綻をきたすようなことを反映するために, 積分をある周波数でカットオフすべきである. 

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/tong-qft/tong-qft.pdf" data-iframely-url="//cdn.iframe.ly/ZUFTTag"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[David Tong: Lectures on Quantum Field Theory](http://www.damtp.cam.ac.uk/user/dt281/qft.html)