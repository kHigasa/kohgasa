---
title: 一人輪講第8回
date: "2021-01-17T02:00:00.000Z"
description: "PDF整理のための一人輪講第8回. David Tong, 2006, Lectures on Quantum Field Theory, 76-80 を読んでいきます. 議論を追いつつ考察と感想,疑問を書き残しています. "
---

```toc
# This code block gets replaced with the TOC
```

## 3 相互作用している場
### 3.7 Green函数
散乱実験には直接関係していないが, 量子場の理論においてどのようにして説明されているのか気になる疑問は多い. これらの疑問は, 相関函数として知られる基本的なものを計算することによって場の量子論の枠組みで計算される. 本節では, 相関函数を定義し, どのようにFeynmanダイアグラムを使ってそれらを計算するのか説明する. そしてそれらを散乱振幅に関連付ける. 

相互作用している理論の本当の真空状態を$|\Omega>$と表記する, 
$$
H|\Omega>=0\ \text{ , }\ <\Omega|\Omega>=1\text{ .}
$$
相関函数を
$$
G^{(n)}(x_1,\ldots,x_n) = <\Omega|T\phi_{\text{H}}(x_1)\ldots\phi_{\text{H}}(x_n)|\Omega>
$$
と定義する. ここで$\phi_{\text{H}}$はHeisenberg描像における$\phi$である. 

この$G^{(n)}$をFeynmanダイアグラムを用いてどのように計算するかを見る. 

##### 主張
$\phi_1=\phi(x_1)$, Heisenberg描像における場を$\phi_{\text{1H}}$, 相互作用描像における場を$\phi_{\text{1I}}$と表記する. すると, 相関函数は, 
$$
G^{(n)}(x_1,\ldots,x_n) = <\Omega|T\phi_{\text{1H}}\ldots\phi_{\text{nH}}|\Omega> = \frac{<0|T\phi_{\text{1I}}\ldots\phi_{\text{nI}}S|0>}{<0|S|0>}
$$
とかける. ここで$|0>$は自由場の理論の真空状態である. 

##### 証明
$t_1>t_2>\ldots>t_n$とし, Tを除去して, 右辺の分子を書き下だせば, 
$$
<0|U_{\text{I}}(+\infty,t_1)\phi_{\text{1I}}U_{\text{I}}(t_1,t_2)\phi_{\text{2I}}\ldots\phi_{\text{nI}}U_{\text{I}}(t_n,-\infty)|0>
$$
となる. $U_{\text{I}}(t_k,t_{k+1})=T\exp(-\mathrm{i}\int_{t_k}^{t_{k+1}}H_{\text{I}}\mathrm{d}t)$を用いて, $\phi_{\text{I}}$を$\phi_{\text{H}}$へ変換し, ある時間$t_0$でどちらの描像でも等しい演算子を選ぶ. 任意の状態$|\Psi>$を考え, 
$$
<\Psi|U_{\text{I}}(t,-\infty)|0>=<\Psi|U(t,-\infty)|0>
$$
に注目する. ここで$U(t,-\infty)$はSchr$\text{\"{o}}$dinger描像における時間発展演算子である. Hamiltonian $H_0+H_{\text{int}}$に対する状態の完全系を挿入して, 
$$
\begin{aligned}
    <\Psi|U(t,-\infty)|0> &= <\Psi|U(t,-\infty)\left(|\Omega><\Omega|+\sum_{n\neq 0}|n><n|\right)|0> \\
    &= <\Psi|\Omega><\Omega|0>+\lim_{t'\to-\infty}\sum_{n\neq 0}\mathrm{e}^{\mathrm{i}E_n(t'-t)}<\Psi|n><n|0> \\
    &= <\Psi|\Omega><\Omega|0>\ \because\ \text{Riemann-Lebesegueの補題}
\end{aligned}
$$
を得る. これを用いれば, 主張は示された. <div class="QED" style="text-align: right">$\Box$</div>

#### 3.7.1 連結ダイアグラムと真空泡
$\phi^4$理論で考える. このときのFeynmanルールとダイアグラム的展開が示されている. 

##### 例: 4点相関函数$<\Omega|T\phi_H(x_1)\ldots\phi_H(x_4)|\Omega>$


##### Feynmanルール


#### 3.7.2 Green函数からS-行列へ
Feynmanダイアグラムを用いて相関函数を計算する方法が理解ったので, 次はすでに計算したS-行列要素と相関函数を関連付ける. まずFourier変換して, 
$$
\hat{G}^{(n)}(p_1,\ldots,p_n) = \int\prod_{i=1}^n\mathrm{d}^4x_i\mathrm{e}^{-\mathrm{i}p_i\cdot x_i}G^{(n)}(x_1,\ldots,x_n)
$$
を得る. S-行列要素との違いは, 相関函数に対するFeynmanルールは内線と同様に外線に対するFeynmanプロパゲーター$\Delta_{\text{F}}$を含むということである. また外線に割り当てられる4次元運動量が任意である. つまりそれらはオンシェルでないということを意味する. これらの問題の療法はS-行列要素を考え直し, 外線のプロパゲーターを相殺し, それらの運動量をオンシェルに戻せば良い. そうすれば, 
$$
\begin{aligned}
    &<p'_1,\ldots,p'_{n'}|S-1|p_1,\ldots,p_n> \\
    = &(-\mathrm{i})^{n+n'}\prod_{i=1}^{n'}(p'^2_i-m^2)\prod_{j=1}^n(p_j^2-m^2)\hat{G}^{(n+n')}(-p'_1,\ldots,-p'_{n'},p_1,\ldots,p_n)
\end{aligned}
$$
がわかる. それぞれの$(p^2-m^2)$の因子は, 一旦運動量がオンシェルに置かれれば消える. これが意味するのは, それぞれの外線に対するプロパゲーターを持つ$G^{(n)}(x_1,\ldots,x_n)$に寄与しているダイアグラムの非ゼロの解を正に得たということである. 

3章まででテキスト[^1]の一般論は終わりである. そのため$\cancel{\text{(生活習慣がぐちゃぐちゃのけちょんけちょんに崩壊しているのもあるが)}}$, 以後のDiracの式, Dirac場, 量子電磁気学について述べられる章は省く. 後々書くかも. 

## 計算

ただ計算を追ったPDFです. 本文の訳出や考察/気づき/疑問等をまとめたものではありません. 

<div class="iframely-embed"><div class="iframely-responsive" style="height: 140px; padding-bottom: 0;"><a href="https://github.com/kHigasa/hitorinkou/blob/main/tong-qft/tong-qft.pdf" data-iframely-url="//cdn.iframe.ly/ZUFTTag"></a></div></div><script async src="//cdn.iframe.ly/embed.js" charset="utf-8"></script>

[^1]:[David Tong: Lectures on Quantum Field Theory](http://www.damtp.cam.ac.uk/user/dt281/qft.html)