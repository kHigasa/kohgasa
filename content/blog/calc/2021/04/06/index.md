---
title: 今日の計算(160)
date: "2021-04-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.51
### (a)
$\bm{Question.}$
複素ベクトル$\bm{r}$がUnitary行列$U$を用いて複素ベクトル$\bm{r}'=U\bm{r}$に変換されるとき, 複素ベクトル$\bm{r}$の大きさがこの変換によって不変であることを示せ. 

$\bm{Proof.}$
$|\bm{r}'|^2=\bm{r}'^{\dagger}\bm{r}'=\bm{r}^{\dagger}U^{\dagger}U\bm{r}=\bm{r}^{\dagger}\bm{r}=|\bm{r}|^2$となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
逆に行列$U$が複素ベクトル$\bm{r}$に対する変換$\bm{r}'=U\bm{r}$で$\bm{r}$の大きさを不変に保つならば$U$はUnitary行列であることを示せ. 

$\bm{Proof.}$
$|\bm{r}'|^2=|\bm{r}|^2$より, $\bm{r}^{\dagger}U^{\dagger}U\bm{r}=\bm{r}^{\dagger}\bm{r}$が導かれ, これが成り立つには$U^{\dagger}U\bm{r}=I$でなければならない. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)