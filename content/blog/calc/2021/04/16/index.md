---
title: 今日の計算(170)
date: "2021-04-16T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.10

ベクトル$\bm{A}$の動径方向ベクトルを$\bm{A}_{\text{r}}$, 接線方向ベクトルを$\bm{A}_{\text{t}}$と表したとき, $\hat{\bm{e}}_{\text{r}}$を動径方向の単位ベクトルとして次を示せ. 

### (a)
$\bm{Question.}$
$$
\bm{A}_{\text{r}} = \hat{\bm{e}}_{\text{r}}(\bm{A}\cdot\hat{\bm{e}}_{\text{r}}). 
$$

$\bm{Proof.}$
勿論. <div class="qed" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$$
\bm{A}_{\text{t}} = -\hat{\bm{e}}_{\text{r}}\times(\hat{\bm{e}}_{\text{r}}\times\bm{A}). 
$$

$\bm{Proof.}$
勿論. <div class="qed" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)