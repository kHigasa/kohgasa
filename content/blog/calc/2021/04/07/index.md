---
title: 今日の計算(161)
date: "2021-04-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.1

$\bm{Question.}$
$xy$平面上のベクトル$\bm{P}=P_x\hat{\bm{e}}_x+P_y\hat{\bm{e}}_y$と$\bm{Q}=Q_x\hat{\bm{e}}_x+Q_y\hat{\bm{e}}_y$が平行でないとき, ベクトル$\bm{P}\times\bm{Q}$が$z$方向を向いていることを示せ. 

$\bm{Proof.}$
$\bm{P}\times\bm{Q}=(P_xQ_y-P_yQ_x)\hat{\bm{e}}_z$となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)