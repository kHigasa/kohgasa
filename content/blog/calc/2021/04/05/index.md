---
title: 今日の計算(159)
date: "2021-04-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.50

$\bm{Question.}$
極限$m\to 0$でDirac方程式がWeylの表示における独立な$2\times 2$のブロックに分かれることを示せ. 

$\bm{Proof.}$
極限$m\to 0$でDirac方程式は
$$
\left\lbrace\gamma^0E-c(\gamma^1p_1+\gamma^2p_2+\gamma^3p_3)\right\rbrace\Psi = 0
$$
となる. これより$\Psi$のベクトル成分$\psi_{\text{L}}$と$\psi_{\text{R}}$について独立な式が得られる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)