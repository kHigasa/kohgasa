---
title: 今日の計算(173)
date: "2021-04-19T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.13

$\bm{Question.}$
$$
(\bm{A}\times\bm{B})\cdot(\bm{C}\times\bm{D}) = (\bm{A}\cdot\bm{C})(\bm{B}\cdot\bm{D})-(\bm{A}\cdot\bm{D})(\bm{B}\cdot\bm{C})
$$
を示せ. 

$\bm{Proof.}$
幾何的に考えると, $(\bm{A}\times\bm{B})\cdot(\bm{C}\times\bm{D})=\lbrace(\bm{A}\times\bm{B})\rbrace\cdot\bm{D}$である. これより, 
$$
\begin{aligned}
  \lbrace(\bm{A}\times\bm{B})\rbrace\cdot\bm{D} &= (\bm{C}\bm{B}\bm{A}-\bm{C}\bm{A}\bm{B})\cdot\bm{D} \\
  &= \bm{C}\bm{A}\bm{B}\bm{D}-\bm{C}\bm{B}\bm{A}\bm{D} \\
  &= (\bm{A}\cdot\bm{C})(\bm{B}\cdot\bm{D})-(\bm{A}\cdot\bm{D})(\bm{B}\cdot\bm{C})
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)