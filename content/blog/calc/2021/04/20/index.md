---
title: 今日の計算(174)
date: "2021-04-20T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.14

$\bm{Question.}$
$$
(\bm{A}\times\bm{B})\times(\bm{C}\times\bm{D}) = (\bm{A}\cdot\bm{B}\times\bm{D})\bm{C}-(\bm{A}\cdot\bm{B}\times\bm{C})\bm{D}
$$
を示せ. 

$\bm{Proof.}$
$(\bm{C}\times\bm{D})$を頭の中であるベクトル$\bm{E}$などと見立ててベクトル三重積を展開すれば示される. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)