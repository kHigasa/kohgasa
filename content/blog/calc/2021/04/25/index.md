---
title: 今日の計算(179)
date: "2021-04-25T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.3.4

直交変換
$$
S = \begin{pmatrix}
    0.80 & 0.60 & 0.00 \\
    -0.48 & 0.64 & 0.60 \\
    0.36 & -0.48 & 0.80
\end{pmatrix}
$$
と2つの列ベクトル
$$
\bm{a} = \begin{pmatrix}
    1 \\
    0 \\
    1
\end{pmatrix}
\bm{b} = \begin{pmatrix}
    0 \\
    2 \\
    -1
\end{pmatrix}
$$
が与えられている. 

### (a)
$\bm{Question.}$
$\mathrm{det} S$を計算せよ. 

$\bm{Answer.}$
$1$. 

### (b)
$\bm{Question.}$
内積について$\bm{a}\cdot\bm{b}=(S\bm{a})\cdot(S\bm{b})$が成り立つことを確かめよ. 

$\bm{Answer.}$
O.K. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)