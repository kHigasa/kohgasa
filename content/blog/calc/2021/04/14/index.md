---
title: 今日の計算(168)
date: "2021-04-14T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.8
3つのベクトル
$$
\begin{aligned}
  \bm{A} &= \hat{\bm{e}}_x+\hat{\bm{e}}_y, 
  \bm{B} &= \hat{\bm{e}}_y+\hat{\bm{e}}_z, 
  \bm{C} &= \hat{\bm{e}}_x-\hat{\bm{e}}_z
\end{aligned}
$$
が与えられている. 

### (a)
$\bm{Question.}$
スカラー三重積$\bm{A}\cdot\bm{B}\times\bm{C}$を計算せよ. 

$\bm{Answer.}$
$1$. 

### (b)
$\bm{Question.}$
ベクトル三重積$\bm{A}\times(\bm{B}\times\bm{C})$を計算せよ. 

$\bm{Answer.}$
$-\hat{\bm{e}}_x+\hat{\bm{e}}_y+2\hat{\bm{e}}_z$. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)