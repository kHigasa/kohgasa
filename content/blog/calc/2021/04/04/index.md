---
title: 今日の計算(158)
date: "2021-04-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.49

$\bm{Question.}$
Dirac行列が
$$
\gamma^0 = \begin{pmatrix}
    0 & I_2 \\
    I_2 & 0
\end{pmatrix}\text{, }\gamma^i = \begin{pmatrix}
    0 & \sigma_i \\
    -\sigma_i & 0
\end{pmatrix}\ (i=1,2,3)
$$
と取られることを示せ(Weylの表示). 

$\bm{Proof.}$
$(\gamma^0)^2=I_4$, $(\gamma^i)^2=-I_4$, $\gamma^{\mu}\gamma^i+\gamma^i\gamma^{\mu}=0\ \mu\neq i$を満たすことより, Dirac行列の表示たりうることが示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)