---
title: 今日の計算(171)
date: "2021-04-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.11

$\bm{Question.}$
3つの非ゼロベクトル$\bm{A}$, $\bm{B}$, $\bm{C}$が共平面であるための必要十分条件は
$$
\bm{A}\cdot\bm{B}\times\bm{C} = 0
$$
であることを示せ. 

$\bm{Proof.}$
明らか. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)