---
title: 今日の計算(162)
date: "2021-04-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.2

$\bm{Question.}$
$$
(\bm{A}\times\bm{B})^2 = (AB)^2-(\bm{A}\cdot\bm{B})^2
$$
を示せ. 

$\bm{Proof.}$
$$
(\bm{A}\times\bm{B})^2 = (AB)^2\sin^2\theta = (AB)^2(1-\cos^2\theta) = (AB)^2-(\bm{A}\cdot\bm{B})^2
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)