---
title: 今日の計算(164)
date: "2021-04-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.4
### (a)
$\bm{Question.}$
ベクトル$\bm{U}=2\hat{\bm{e}}_x+\hat{\bm{e}}_y-\hat{\bm{e}}_z$とベクトル$\bm{V}=\hat{\bm{e}}_x-\hat{\bm{e}}_y+\hat{\bm{e}}_z$に垂直なベクトル$\bm{A}$を求めよ. 

$\bm{Answer.}$
例えば, $\bm{A}=\bm{U}\times\bm{V}=-3\hat{\bm{e}}_y-3\hat{\bm{e}}_z$. 

### (b)
$\bm{Question.}$
前問[(a)](#a)で求めたベクトル$\bm{A}$を規格化せよ. 

$\bm{Answer.}$
$-\displaystyle\frac{1}{\sqrt{2}}\hat{\bm{e}}_y-\frac{1}{\sqrt{2}}\hat{\bm{e}}_z$. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)