---
title: 今日の計算(169)
date: "2021-04-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.9

$\bm{Question.}$
ベクトル積に対するJacobiの恒等式
$$
\bm{a}\times(\bm{b}\times\bm{c})+\bm{b}\times(\bm{c}\times\bm{a})+\bm{c}\times(\bm{a}\times\bm{b}) = \bm{0}
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    &\bm{a}\times(\bm{b}\times\bm{c})+\bm{b}\times(\bm{c}\times\bm{a})+\bm{c}\times(\bm{a}\times\bm{b}) \\
    = &\lbrace(\bm{a}\cdot\bm{c})\cdot\bm{b}-(\bm{a}\cdot\bm{b})\cdot\bm{c})\rbrace+\lbrace(\bm{b}\cdot\bm{a})\cdot\bm{c}-(\bm{b}\cdot\bm{c})\cdot\bm{a})\rbrace+\lbrace(\bm{c}\cdot\bm{b})\cdot\bm{a}-(\bm{c}\cdot\bm{a})\cdot\bm{b})\rbrace \\
    = &0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)