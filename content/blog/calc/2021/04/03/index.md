---
title: 今日の計算(157)
date: "2021-04-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.48

$\bm{Question.}$
Dirac方程式
$$
\left\lbrace\gamma^0E-c(\gamma^1p_1+\gamma^2p_2+\gamma^3p_3)\right\rbrace\Psi = mc^2\Psi
$$
が
$$
\begin{pmatrix}
    mc^2-E & c(\sigma_1p_1+\sigma_2p_2+\sigma_3p_3) \\
    -c(\sigma_1p_1+\sigma_2p_2+\sigma_3p_3) & -mc^2-E
\end{pmatrix}
\begin{pmatrix}
    \psi_{\text{L}} \\
    \psi_{\text{S}}
\end{pmatrix} = 0
$$
のようにかけることを示せ. 

$\bm{Proof.}$
そのまま. <div class="QED" style="text-align: right">$\Box$</div>

#### コメント
問題文[^1]でthatが重複してる. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)