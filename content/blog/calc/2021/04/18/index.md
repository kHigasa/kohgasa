---
title: 今日の計算(172)
date: "2021-04-18T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.12

$\bm{Question.}$
3つのベクトル
$$
\begin{aligned}
  \bm{A} &= 3\hat{\bm{e}}_x-2\hat{\bm{e}}_y+2\hat{\bm{e}}_z, \\
  \bm{B} &= 6\hat{\bm{e}}_x+4\hat{\bm{e}}_y-2\hat{\bm{e}}_z, \\
  \bm{C} &= -3\hat{\bm{e}}_x-2\hat{\bm{e}}_y-4\hat{\bm{e}}_z
\end{aligned}
$$
が与えられたとき, スカラー三重積$\bm{A}\cdot\bm{B}\times\bm{C}$とベクトル三重積$\bm{A}\times(\bm{B}\times\bm{C})$, $\bm{B}\times(\bm{C}\times\bm{A})$, $\bm{C}\times(\bm{A}\times\bm{B})$を計算せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
  \bm{A}\cdot\bm{B}\times\bm{C} &= -120 \\
  \bm{A}\times(\bm{B}\times\bm{C}) &= -60\hat{\bm{e}}_x-40\hat{\bm{e}}_y+50\hat{\bm{e}}_z \\
  \bm{B}\times(\bm{C}\times\bm{A}) &= 24\hat{\bm{e}}_x+88\hat{\bm{e}}_y-62\hat{\bm{e}}_z \\
  \bm{C}\times(\bm{A}\times\bm{B}) &= 36\hat{\bm{e}}_x-48\hat{\bm{e}}_y+12\hat{\bm{e}}_z
\end{aligned}
$$
と計算された. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)