---
title: 今日の計算(163)
date: "2021-04-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.3

$\bm{Question.}$
3つのベクトル
$$
\begin{aligned}
  \bm{P} &= \cos\theta\hat{\bm{e}}_x+\sin\theta\hat{\bm{e}}_y \\
  \bm{Q} &= \cos\varphi\hat{\bm{e}}_x-\sin\varphi\hat{\bm{e}}_y \\
  \bm{R} &= \cos\varphi\hat{\bm{e}}_x+\sin\varphi\hat{\bm{e}}_y
\end{aligned}
$$
を用いて, 加法定理
$$
\begin{aligned}
  \sin(\theta+\varphi) &= \sin\theta\cos\varphi+\cos\theta\sin\varphi \\
  \cos(\theta+\varphi) &= \cos\theta\cos\varphi-\sin\theta\sin\varphi
\end{aligned}
$$
を示せ. 

$\bm{Proof.}$
代数的に$\bm{P}\cdot\bm{Q}=\cos\theta\cos\varphi-\sin\theta\sin\varphi$, $\bm{Q}\times\bm{P}=(\sin\theta\cos\varphi+\cos\theta\sin\varphi)\hat{\bm{e}}_z$, また幾何的に$\bm{P}\cdot\bm{Q}=\cos(\theta+\varphi)$, $\bm{Q}\times\bm{P}=\sin(\theta+\varphi)\hat{\bm{e}}_z$となることは明らかであるから2つの式を結んで示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)