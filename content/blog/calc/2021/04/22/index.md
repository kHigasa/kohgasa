---
title: 今日の計算(176)
date: "2021-04-22T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.3.1

$\bm{Question.}$
$z$軸周りに角度$\varphi_1$と$\varphi_2$の回転を連続して行ったとき, 
$$
\begin{aligned}
    \cos(\varphi_1+\varphi_2) &= \cos\varphi_1\cos\varphi_2-\sin\varphi_1\sin\varphi_2 \\
    \sin(\varphi_1+\varphi_2) &= \sin\varphi_1\cos\varphi_2+\cos\varphi_1\sin\varphi_2
\end{aligned}
$$
の関係を回転行列を用いて示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    &\begin{pmatrix}
        \cos(\varphi_1+\varphi_2) & \sin(\varphi_1+\varphi_2) \\
        -\sin(\varphi_1+\varphi_2) & \cos(\varphi_1+\varphi_2)
    \end{pmatrix} \\
    = &\begin{pmatrix}
        \cos\varphi_1 & \sin\varphi_1 \\
        -\sin\varphi_1 & \cos\varphi_1 \\
    \end{pmatrix}\begin{pmatrix}
        \cos\varphi_2 & \sin\varphi_2 \\
        -\sin\varphi_2 & \cos\varphi_2 \\
    \end{pmatrix} \\
    = &\begin{pmatrix}
        \cos\varphi_1\cos\varphi_2-\sin\varphi_1\sin\varphi_2 & \sin\varphi_1\cos\varphi_2+\cos\varphi_1\sin\varphi_2 \\
        -\sin\varphi_1\cos\varphi_2-\cos\varphi_1\sin\varphi_2 & \cos\varphi_1\cos\varphi_2-\sin\varphi_1\sin\varphi_2
    \end{pmatrix}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)