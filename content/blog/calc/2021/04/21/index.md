---
title: 今日の計算(175)
date: "2021-04-21T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.15

電荷$q_1$が速度$\bm{v}_1$を持って運動しているとき, 磁場
$$
\bm{B} = \frac{\mu_0}{4\pi}q_1\frac{\bm{v}_1\times\hat{\bm{r}}}{r^2}
$$
が誘導される(mks単位系). 

### (a)
$\bm{Question.}$
この磁場の影響を受けて, $\bm{v}_2$で運動している電荷$q_2$に働く力$\bm{F}_2$を求めよ. 

$\bm{Answer.}$
Lorentz力
$$
\bm{F}_2 = q_2\bm{v}_2\times\bm{B} = \frac{\mu_0}{4\pi}q_1q_2\frac{\bm{v}_2\times(\bm{v}_1\times\hat{\bm{r}})}{r^2}
$$
である. 

### (b)
$\bm{Question.}$
運動している電荷$q_2$によって電荷$q_1$が受ける力$\bm{F}_1$と[(a)](#a)で求めた力$\bm{F}_2$を比較せよ. 

$\bm{Answer.}$
$$
\bm{F}_1 = \frac{\mu_0}{4\pi}q_1q_2\frac{\bm{v}_1\times(\bm{v}_2\times\hat{\bm{r}})}{r^2}
$$
であるが, 同じベクトルを用いてそれを置換したベクトル三重積の間に一般的に成り立つ関係はないので, ベクトル$\bm{v}_1$と$\bm{v}_2$の性質によるとしか言えない. 

### (c)
$\bm{Question.}$
各電荷が互いに平行に同じ向きに運動しているとき, 各力間の関係を示せ. 

$\bm{Answer.}$
$\bm{F}_1=-\bm{F}_2$となり, 作用反作用則が成立している. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)