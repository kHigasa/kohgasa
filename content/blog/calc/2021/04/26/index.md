---
title: 今日の計算(180)
date: "2021-04-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.3.5

$$
S = \begin{pmatrix}
    0.60 & 0.00 & 0.80 \\
    -0.64 & -0.60 & 0.48 \\
    -0.48 & 0.80 & 0.36
\end{pmatrix}
$$
と3つの列ベクトル
$$
\bm{a} = \begin{pmatrix}
    1 \\
    0 \\
    1
\end{pmatrix}
\bm{b} = \begin{pmatrix}
    0 \\
    2 \\
    -1
\end{pmatrix}
\bm{c} = \begin{pmatrix}
    2 \\
    1 \\
    3
\end{pmatrix}
$$
が与えられている. 

### (a)
$\bm{Question.}$
$\mathrm{det}S$を計算せよ. 

$\bm{Answer.}$
$-1$. 

$\bm{Question.}$
Sに作用させて次の量を計算せよ. 
### (b)
$\bm{a}\times\bm{b}$

$\bm{Answer.}$
$$
(S\bm{a})\times(S\bm{b}) = \begin{pmatrix}
    -0.40 \\
    -1.64 \\
    -2.48
\end{pmatrix}\text{, }
S(\bm{a})\times\bm{b}) = \begin{pmatrix}
    0.40 \\
    1.64 \\
    2.48
\end{pmatrix}
$$

### (c)
$(\bm{a}\times\bm{b})\cdot\bm{c}$

$\bm{Answer.}$
$$
((S\bm{a})\times(S\bm{b}))\cdot(S\bm{c}) = 3\text{, }
(\bm{a}\times\bm{b})\cdot\bm{c}) = -3
$$

### (d)
$\bm{a}\times(\bm{b}\times\bm{c})$

$\bm{Answer.}$
$$
(S\bm{a})\times((S\bm{b})\times(S\bm{c})) = \begin{pmatrix}
    -0.40 \\
    -8.84 \\
    7.12
\end{pmatrix}\text{, }
S(\bm{a}(\times\bm{b}\times\bm{c})) = \begin{pmatrix}
    -0.40 \\
    -8.84 \\
    7.12
\end{pmatrix}
$$

$\bm{Answer.}$
### (e)
$\bm{Question.}$
小問[(b)](#b)-[(d)](#d)の量をスカラー・ベクトル・擬スカラー・擬ベクトルのいずれかに分類せよ. 

$\bm{Answer.}$
$\bm{a}\times\bm{b}$は擬ベクトル. $(\bm{a}\times\bm{b})\cdot\bm{c}$は擬スカラー. $\bm{a}\times(\bm{b}\times\bm{c})$はベクトル. 
### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)