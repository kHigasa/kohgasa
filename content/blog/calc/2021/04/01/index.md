---
title: 今日の計算(155)
date: "2021-04-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.46

$\bm{Question.}$
4次正方行列$A$が16個のDirac行列$\Gamma_i$の線型結合により
$$
A=\sum_{i=1}^{16}c_i\Gamma_i
$$
で与えられるとき, 
$$
c_i\sim\mathrm{Tr}(A\Gamma_i)
$$
を示せ. 

$\bm{Proof.}$
$\Gamma_i^2=I_4$より示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)