---
title: 今日の計算(178)
date: "2021-04-24T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.3.3

$\bm{Question.}$
行ベクトル$\bm{x}$, $\bm{y}$を直交変換$S$により$\bm{x}'=S\bm{x}$, $\bm{y}'=S\bm{y}$と変換する. このとき
$$
^t\bm{x}'\bm{y}' = ^t\bm{x}\bm{y}
$$
が成り立つことを示せ. 

$\bm{Proof.}$
定義 ($^tSS=\bm{1}$) より明らか. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)