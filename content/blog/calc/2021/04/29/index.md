---
title: 今日の計算(183)
date: "2021-04-29T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.4.3

$\bm{Question.}$
Eulerの回転行列
$$
\begin{pmatrix}
    \cos\gamma\cos\beta\cos\alpha-\sin\gamma\sin\alpha & \cos\gamma\cos\beta\sin\alpha+\sin\gamma\cos\alpha & -\cos\gamma\sin\beta \\
    -\sin\gamma\cos\beta\cos\alpha-\cos\gamma\sin\alpha & -\sin\gamma\cos\beta\sin\alpha+\cos\gamma\cos\alpha & \sin\gamma\sin\beta \\
    \sin\beta\cos\alpha & \sin\beta\sin\alpha & \cos\beta
\end{pmatrix}
$$
が, 変換
$$
\alpha\to\alpha+\pi\text{, }\beta\to-\beta\text{, }\gamma\to\gamma+\pi
$$
のもとで不変であることを確かめよ. 

$\bm{Answer.}$
O.K. 

#### コメント
左手系でも成り立つと. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)