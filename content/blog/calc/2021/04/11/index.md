---
title: 今日の計算(165)
date: "2021-04-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.5

$\bm{Question.}$
4つのベクトル$\bm{a}$, $\bm{b}$, $\bm{c}$, $\bm{d}$が全て同一平面上にあるとき, 
$$
(\bm{a}\times\bm{b})\times(\bm{c}\times\bm{d})=\bm{0}
$$
を示せ. 

$\bm{Proof.}$
明らかに$(\bm{a}\times\bm{b})\parallel(\bm{c}\times\bm{d})$であるから示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)