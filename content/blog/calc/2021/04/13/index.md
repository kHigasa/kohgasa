---
title: 今日の計算(167)
date: "2021-04-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.7

$\bm{Question.}$
Lorentz力に関する3つの実験結果
$$
\begin{aligned}
  \bm{v} &= \hat{\bm{e}}_x\text{ , }\frac{\bm{F}}{q} = 2\hat{\bm{e}}_z-4\hat{\bm{e}}_y, 
  \bm{v} &= \hat{\bm{e}}_y\text{ , }\frac{\bm{F}}{q} = 4\hat{\bm{e}}_x-\hat{\bm{e}}_z, 
  \bm{v} &= \hat{\bm{e}}_z\text{ , }\frac{\bm{F}}{q} = \hat{\bm{e}}_y-2\hat{\bm{e}}_x
\end{aligned}
$$
から誘導磁場$\bm{B}$を求めよ. 

$\bm{Answer.}$
Lorentz力は$\bm{F}=q\bm{v}\times\bm{B}$によって与えられるので, $\bm{B}=\hat{\bm{e}}_x+2\hat{\bm{e}}_y+4\hat{\bm{e}}_z$と求まる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)