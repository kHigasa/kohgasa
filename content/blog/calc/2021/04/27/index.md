---
title: 今日の計算(181)
date: "2021-04-27T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.4.1

$\bm{Question.}$
変換
$$
\begin{aligned}
    \varphi &= \alpha+\frac{\pi}{2}\text{ (about }x_3\text{-軸) } \\
    \theta &= \beta\text{ (about }x_1'\text{-軸) } \\
    \psi &= \gamma-\frac{\pi}{2}\text{ (about }x_3''\text{-軸) }
\end{aligned}
$$
がEuler変換と同等の変換になっていることを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    &\begin{pmatrix}
        \cos(\gamma-\frac{\pi}{2}) & \sin(\gamma-\frac{\pi}{2}) & 0 \\
        -\sin(\gamma-\frac{\pi}{2}) & \cos(\gamma-\frac{\pi}{2}) & 0 \\
        0 & 0 & 1
    \end{pmatrix}
    \begin{pmatrix}
        1 & 0 & 0 \\
        0 \cos\beta & -\sin\beta \\
        0 \sin\beta & \cos\beta
    \end{pmatrix}
    \begin{pmatrix}
        \cos(\alpha+\frac{\pi}{2}) & \sin(\alpha+\frac{\pi}{2}) & 0 \\
        -\sin(\alpha+\frac{\pi}{2}) & \cos(\alpha+\frac{\pi}{2}) & 0 \\
        0 & 0 & 1
    \end{pmatrix} \\
    = &\begin{pmatrix}
        \sin\gamma & -\cos\gamma & 0 \\
        \cos\gamma & \sin\gamma & 0 \\
        0 & 0 & 1
    \end{pmatrix}
    \begin{pmatrix}
        1 & 0 & 0 \\
        0 \cos\beta & -\sin\beta \\
        0 \sin\beta & \cos\beta
    \end{pmatrix}
    \begin{pmatrix}
        -\sin\alpha & \cos\alpha & 0 \\
        -\cos\alpha & \sin\alpha & 0 \\
        0 & 0 & 1
    \end{pmatrix} \\
    = &\begin{pmatrix}
        \cos\gamma\cos\beta\cos\alpha-\sin\gamma\sin\alpha & \cos\gamma\cos\beta\sin\alpha+\sin\gamma\cos\alpha & -\cos\gamma\sin\beta \\
        -\sin\gamma\cos\beta\cos\alpha-\cos\gamma\sin\alpha & -\sin\gamma\cos\beta\sin\alpha+\cos\gamma\cos\alpha & \sin\gamma\sin\beta \\
        \sin\beta\cos\alpha & \sin\beta\sin\alpha & \cos\beta
    \end{pmatrix}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)