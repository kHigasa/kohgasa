---
title: 今日の計算(166)
date: "2021-04-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.2.6

$\bm{Question.}$
正弦定理を導出せよ. 

$\bm{Answer.}$
三角形ABCについて, その面積は$|\bm{A}||\bm{B}|\sin c=|\bm{B}||\bm{C}|\sin a=|\bm{C}||\bm{A}|\sin b$で与えられ, これより正弦定理が導かれた. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)