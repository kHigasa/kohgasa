---
title: 今日の計算(156)
date: "2021-04-02T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.47

$\bm{Question.}$
電荷共軛行列$C=\mathrm{i}\bm{\gamma}^2\bm{\gamma}^0$について
$$
C\bm{\gamma}^{\mu}C^{-1} = -^t(\bm{\gamma}^{\mu})\ (\mu=0,1,2,3)
$$
が成り立つことを示せ. 

$\bm{Proof.}$
$C^{-1}=-C$より, 
$$
\begin{aligned}
    C\bm{\gamma}^0C^{-1} &= \mathrm{i}\bm{\gamma}^2\bm{\gamma}^0\bm{\gamma}^0(-\mathrm{i}\bm{\gamma}^0\bm{\gamma}^2) \\
    &= \bm{\gamma}^2\bm{\gamma}^0\bm{\gamma}^2 \\
    &= -\bm{\gamma}^0\bm{\gamma}^2\bm{\gamma}^2 \\
    &= -\bm{\gamma}^0 = -^t(\bm{\gamma}^0) \\
    C\bm{\gamma}^1C^{-1} &= -^t(\bm{\gamma}^1) \\
    C\bm{\gamma}^2C^{-1} &= -^t(\bm{\gamma}^2) \\
    C\bm{\gamma}^3C^{-1} &= -^t(\bm{\gamma}^3)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)