---
title: 今日の計算(177)
date: "2021-04-23T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.3.2

$\bm{Question.}$
コーナーリフレクターは, 相互に垂直な3つの反射面で形成されています. コーナーリフレクターに入射した光線 (3つの面すべてに当たる) は, 入射線に平行な線に沿って反射されることを示しなさい. 

$\bm{Proof.}$
入射光のベクトルは各平面での反射 (回転) を終えたあと, 結局各成分が反転した状態になっているため (実直に証明するのが面倒くさい) . <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)