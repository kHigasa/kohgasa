---
title: 今日の計算(184)
date: "2021-04-30T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.4.4

$\bm{Question.}$
Euler角回転行列$S(\alpha,\beta,\gamma)$について次の関係を示せ. 

### (a)
$$
S^{-1}(\alpha,\beta,\gamma) = ^tS(\alpha,\beta,\gamma). 
$$

$\bm{Proof.}$
$$
\begin{aligned}
    &^tS(\alpha,\beta,\gamma)S(\alpha,\beta,\gamma) \\
    = &\begin{pmatrix}
        \cos\alpha & -\sin\alpha & 0 \\
        \sin\alpha & \cos\alpha & 0 \\
        0 & 0 & 1
    \end{pmatrix}
    \begin{pmatrix}
        \cos\beta & 0 & \sin\beta \\
        0 & 1 & 0 \\
        -\sin\beta & 0 & \cos\beta
    \end{pmatrix}
    \begin{pmatrix}
        \cos\gamma & -\sin\gamma & 0 \\
        \sin\gamma & \cos\gamma & 0 \\
        0 & 0 & 1
    \end{pmatrix} \\
    &\begin{pmatrix}
        \cos\gamma & \sin\gamma & 0 \\
        -\sin\gamma & \cos\gamma & 0 \\
        0 & 0 & 1
    \end{pmatrix}
    \begin{pmatrix}
        \cos\beta & 0 & -\sin\beta \\
        0 & 1 & 0 \\
        \sin\beta & 0 & \cos\beta
    \end{pmatrix}
    \begin{pmatrix}
        \cos\alpha & \sin\alpha & 0 \\
        -\sin\alpha & \cos\alpha & 0 \\
        0 & 0 & 1
    \end{pmatrix} \\
    = &I
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$$
S^{-1}(\alpha,\beta,\gamma) = S(-\gamma,-\beta,-\alpha). 
$$

$\bm{Proof.}$
$$
\begin{aligned}
    &S(-\gamma,-\beta,-\alpha)\\
    = &\begin{pmatrix}
        \cos\gamma & -\sin\gamma & 0 \\
        \sin\gamma & \cos\gamma & 0 \\
        0 & 0 & 1
    \end{pmatrix}
    \begin{pmatrix}
        \cos\beta & 0 & \sin\beta \\
        0 & 1 & 0 \\
        -\sin\beta & 0 & \cos\beta
    \end{pmatrix}
    \begin{pmatrix}
        \cos\alpha & -\sin\alpha & 0 \\
        \sin\alpha & \cos\alpha & 0 \\
        0 & 0 & 1
    \end{pmatrix} \\
    = &^tS(\alpha,\beta,\gamma)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)