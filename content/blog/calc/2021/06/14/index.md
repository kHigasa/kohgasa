---
title: 今日の計算(229)
date: "2021-06-14T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.8

$\bm{Question.}$
Stokesの定理の別の形式
$$
\int_{\text{S}}(\mathrm{d}\bm{\sigma}\times\nabla)\times\bm{P} = \int_{\text{C}}\mathrm{d}\bm{\ell}\times\bm{P}
$$
を用いて, $xy$平面上のループCについての線積分
$$
\int_{\text{C}}\bm{r}\times\mathrm{d}\bm{r}
$$
を計算せよ. 

$\bm{Answer.}$
$\bm{P}=\bm{r}$として計算すれば, 
$$
\int_{\text{S}}(\mathrm{d}\bm{\sigma}\times\nabla)\times\bm{r} = \int_{\text{C}}\mathrm{d}\bm{\ell}\times\bm{r} = 2A
$$
と求まる. ここで$A$はループCの囲む面積である. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)