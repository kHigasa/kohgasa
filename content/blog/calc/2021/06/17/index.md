---
title: 今日の計算(232)
date: "2021-06-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.11

$\bm{Question.}$
$$
\int_{\partial V}\mathrm{d}\bm{\sigma}\times\bm{f} = \int_V\nabla\times\bm{f}\mathrm{d}V
$$
を示せ. 

$\bm{Proof.}$
ある定ベクトル$\bm{g}$を導入してGaussの定理より, 
$$
\begin{aligned}
    \int_{\partial V}(\bm{g}\times\bm{f})\cdot\mathrm{d}\bm{\sigma} &= \int_V\nabla\cdot(\bm{g}\times\bm{f})\mathrm{d}V \\
    \int_{\partial V}\bm{f}\times\mathrm{d}\bm{\sigma}\cdot\bm{g} &= \int_V\left\lbrace\bm{f}(\nabla\times\bm{g})-\bm{g}(\nabla\times\bm{f}))\right\rbrace\cdot\mathrm{d}V \\
    -\int_{\partial V}\bm{f}\times\mathrm{d}\bm{\sigma}\cdot\bm{g} &= \int_V\bm{g}(\nabla\times\bm{f})\cdot\mathrm{d}V \\
    -\bm{g}\cdot\int_{\partial V}\bm{f}\times\mathrm{d}\bm{\sigma} &= \bm{g}\cdot\int_V\nabla\times\bm{f}\cdot\mathrm{d}V \\
    \bm{g}\cdot\int_{\partial V}\mathrm{d}\bm{\sigma}\times\bm{f} &= \bm{g}\cdot\int_V\nabla\times\bm{f}\cdot\mathrm{d}V
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)