---
title: 今日の計算(244)
date: "2021-06-29T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.10

$\bm{Question.}$
Maxwell方程式を用いて, 定常電流の流れる系に対してベクトルポテンシャルが
$$
\nabla^2\bm{A} = -\mu\bm{J}
$$
をPoisson方程式を満たすことを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \nabla\times\bm{B} &= \mu\left(\frac{\partial\bm{D}}{\partial t}+\bm{J}\right) = \bm{J} \\
    \nabla\times\bm{B} &= \nabla\times(\nabla\times\bm{A}) \\
    &= -\nabla^2\bm{A}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)