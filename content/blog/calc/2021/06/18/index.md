---
title: 今日の計算(233)
date: "2021-06-18T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.12

$\bm{Question.}$
$$
\int_S\mathrm{d}\bm{\sigma}\times\nabla\varphi = \int_{\partial S}\varphi\mathrm{d}\bm{r}
$$
を示せ. 

$\bm{Proof.}$
ある定ベクトル$\bm{g}$を導入してStokesの定理より, 
$$
\begin{aligned}
    \int_S\nabla\times(\varphi\bm{g})\cdot\mathrm{d}\bm{\sigma} &= \int_{\partial S}\varphi\bm{g}\cdot\mathrm{d}\bm{r} \\
    \int_S(\nabla\varphi\times\bm{g})\cdot\mathrm{d}\bm{\sigma} &= \int_{\partial S}\bm{g}\varphi\cdot\mathrm{d}\bm{r} \\
    \int_S\bm{g}(\mathrm{d}\bm{\sigma}\times\nabla\varphi) &= \int_{\partial S}\bm{g}\varphi\cdot\mathrm{d}\bm{r}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)