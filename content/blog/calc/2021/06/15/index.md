---
title: 今日の計算(230)
date: "2021-06-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.9

$\bm{Question.}$
$$
\int_{\text{C}}u\nabla v\cdot\mathrm{d}\bm{\ell} = -\int_{\text{C}}v\nabla u\cdot\mathrm{d}\bm{\ell}
$$
を示せ. 

$\bm{Proof.}$
Stokesの定理より, 
$$
\int_{\text{C}}\nabla(uv)\mathrm{d}\cdot\bm{\ell} = \int_{\text{S}}\nabla\times\nabla(uv)\mathrm{d}\bm{\sigma} = 0
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)