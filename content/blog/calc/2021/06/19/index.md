---
title: 今日の計算(234)
date: "2021-06-19T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.13

$\bm{Question.}$
$$
\int_S(\mathrm{d}\bm{\sigma}\times\nabla)\times\bm{f} = \int_{\partial S}\mathrm{d}\bm{r}\times\bm{f}
$$
を示せ. 

$\bm{Proof.}$
定ベクトル$\bm{g}$を導入しStokesの定理より, 
$$
\begin{aligned}
    \int_S\left\lbrace\nabla\times(\bm{g}\times\bm{f})\right\rbrace\cdot\mathrm{d}\bm{\sigma} &= \int_{\partial S}(\bm{g}\times\bm{f})\cdot\mathrm{d}\bm{r} \\
    -\int_S\bm{g}\cdot\left\lbrace(\mathrm{d}\bm{\sigma}\times\nabla\times\bm{f})\right\rbrace\cdot\mathrm{d}\bm{\sigma} &= \int_{\partial S}(\bm{f}\times\mathrm{d}\bm{r})\cdot\bm{g} \\
    \int_S\bm{g}\cdot\left\lbrace(\mathrm{d}\bm{\sigma}\times\nabla\times\bm{f})\right\rbrace\cdot\mathrm{d}\bm{\sigma} &= \int_{\partial S}\bm{g}\cdot(\mathrm{d}\bm{r}\times\bm{f})
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)