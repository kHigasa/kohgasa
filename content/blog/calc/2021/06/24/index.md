---
title: 今日の計算(239)
date: "2021-06-24T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.5

$\bm{Question.}$
磁場$\bm{B}$が
$$
\bm{B} = \frac{\bm{r}}{r^3}
$$
で与えられているとき, ベクトルポテンシャル$\bm{A}$を求めよ. 

$\bm{Answer.}$
$\bm{B}=\nabla\times\bm{A}$より, 
$$
\bm{A} = \frac{z}{r(x^2+y^2)}\left(y,-x,0\right)
$$
がこれを満たす. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)