---
title: 今日の計算(235)
date: "2021-06-20T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.1

$\bm{Question.}$
力が
$$
\bm{F} = (x^2+y^2+z^2)^n(x\hat{\bm{e}}_x+y\hat{\bm{e}}_y+z\hat{\bm{e}}_z)
$$
で与えられているとき, 次の量を計算せよ. 

### (a)
$$
\nabla\cdot\bm{F}\text{. }
$$

$\bm{Answer.}$
$$
(2n+3)(x^2+y^2+z^2)^n\text{. }
$$

### (b)
$$
\nabla\times\bm{F}\text{. }
$$

$\bm{Answer.}$
$$
0\text{. }
$$

### (c)
$$
\bm{F} = -\nabla\varphi
$$
なるポテンシャル$\varphi(x,y,z)$. 

$\bm{Answer.}$
$$
-\frac{(x^2+y^2+z^2)^{n+1}}{2n+2}\text{. }
$$

### (d)
指数$n$のどんな値に対して, スカラーポテンシャルは原点と無限遠点で発散するか. 

$\bm{Answer.}$
$$
-1\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)