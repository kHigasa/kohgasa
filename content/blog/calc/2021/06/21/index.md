---
title: 今日の計算(236)
date: "2021-06-21T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.2

$\bm{Question.}$
一様に帯電した半径$a$の球がある. $0\leq r<\infty$に対する静電ポテンシャル$\varphi(r)$を求めよ. 

$\bm{Answer.}$
$$
\varphi(r) = \begin{cases}
    \frac{Q}{4\pi\epsilon_0a}\left(\frac{3}{2}-\frac{1}{2}\frac{r^2}{a^2}\right) &\text{for } 0\leq r< a \\
    \frac{Q}{4\pi\epsilon_0r} &\text{for } a\leq r< \infty
\end{cases}\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)