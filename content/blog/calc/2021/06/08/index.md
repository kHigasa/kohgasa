---
title: 今日の計算(223)
date: "2021-06-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.2

$\bm{Question.}$
閉曲面Sによって閉じ込められた領域の体積Vに対して, 
$$
\frac{1}{3}\int_{\text{S}}\bm{r}\cdot\mathrm{d}\bm{\sigma} = V
$$
が成り立つことを示せ. 

$\bm{Proof.}$
Gaussの発散定理より, 
$$
\text{(与式)} = \frac{1}{3}\int_{\text{V}}\nabla\cdot\bm{r}\mathrm{d}V = \frac{1}{3}\int_{\text{V}}\mathrm{d}V = V
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)