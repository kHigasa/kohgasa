---
title: 今日の計算(221)
date: "2021-06-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.7.5

$\bm{Question.}$
原点を1つの頂点とし, 正方向の$x$, $y$, $z$軸と$(1,1,1)$に頂点を持つボクセルに対して, 
$$
\frac{1}{3}\int_{\text{S}}\bm{r}\cdot\mathrm{d}\bm{\sigma}
$$
を計算せよ. 

$\bm{Answer.}$
$$
\text{(与式)} = \frac{x}{3}\int\mathrm{d}y\mathrm{d}z+\frac{y}{3}\int\mathrm{d}z\mathrm{d}x+\frac{z}{3}\int\mathrm{d}x\mathrm{d}y = 1\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)