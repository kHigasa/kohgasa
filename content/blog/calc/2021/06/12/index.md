---
title: 今日の計算(227)
date: "2021-06-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.6

$\bm{Question.}$
$xy$平面上のある連続した閉曲線Cに沿ったベクトル$\bm{t}=-y\hat{\bm{e}}_x+x\hat{\bm{e}}_y$の積分について, 
$$
\int_{\text{C}}\frac{1}{2}\bm{t}\cdot\mathrm{d}\bm{\ell} = \frac{1}{2}\int_{\text{C}}(x\mathrm{d}y-y\mathrm{d}x) = A
$$
を示せ. ここで$A$は閉曲線Cに囲まれた部分の面積である. 

$\bm{Proof.}$
Stokesの定理より, 
$$
\int_{\text{C}}\frac{1}{2}\bm{t}\cdot\mathrm{d}\bm{\ell} = \int_{\text{S}}\nabla\times\bm{t}\mathrm{d}\bm{sigma} = \int_{\text{S}}\hat{\bm{e}}_z\mathrm{d}\bm{\sigma} = A
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)