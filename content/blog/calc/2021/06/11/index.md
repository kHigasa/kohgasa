---
title: 今日の計算(226)
date: "2021-06-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.5

$\bm{Question.}$
特定の定常状態の電流密度$\bm{J}$は空間に局在しており, そこから十分離れた境界表面で$\bm{J}=0$となるように選べば, 
$$
\int\bm{J}\mathrm{d}V = 0
$$
となることを示せ. 

$\bm{Proof.}$
ベクトル$x\bm{J}$を考えると, 
$$
\int_{\text{V}}\nabla\cdot(x\bm{J})\mathrm{d}V = \int_{\text{V}}J_x\mathrm{d}V
$$
となり, 一方でGaussの定理と境界条件より
$$
\int_{\text{V}}\nabla\cdot(x\bm{J})\mathrm{d}V = \int_{\text{S}}x\bm{J}\mathrm{d}\bm{\sigma} = 0
$$
であるから, 同じことを$y$, $z$成分についても繰り返して題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)