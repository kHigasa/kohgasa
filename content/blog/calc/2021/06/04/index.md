---
title: 今日の計算(219)
date: "2021-06-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.7.3

$\bm{Question.}$
力
$$
\bm{F} = (x-y)\hat{\bm{e}}_x+(x+y)\hat{\bm{e}}_y
$$
でもって, 座標$(1,1)$から$(3,3)$へものを動かすのに必要とする仕事を計算せよ. 

$\bm{Answer.}$
例えば$(1,1)\to(3,1)\to(3,3)$と$x$軸, $y$軸方向に沿って動かした場合, 
$$
\int_1^3(x-1)\mathrm{d}x+\int_1^3(3+y)\mathrm{d}y = 12
$$
と求まる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)