---
title: 今日の計算(220)
date: "2021-06-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.7.4

$\bm{Question.}$
線績分
$$
\int_{\text{C}}\bm{r}\cdot\mathrm{d}\bm{r}
$$
を任意の閉じた経路Cに関して評価せよ. 

$\bm{Answer.}$
$0$. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)