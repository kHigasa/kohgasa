---
title: 今日の計算(218)
date: "2021-06-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.7.2

$\bm{Question.}$
力
$$
\bm{F} = -\frac{y}{x^2+y^2}\hat{\bm{e}}_x+\frac{x}{x^2+y^2}\hat{\bm{e}}_y
$$
でもって$xy$-平面の単位円周上を次のように動くときに必要な仕事$\displaystyle W=\int_{\text{C}}\bm{F}\cdot\mathrm{d}\bm{r}$を計算せよ. 

### (a)

反時計回りに$0\to\pi$へ. 

$\bm{Answer.}$
$$
W = \int_0^{\pi}(\sin^2\theta+\cos^2\theta)\mathrm{d}\theta = \pi\text{. }
$$

### (b)

時計回りに$0\to-\pi$へ. 

$\bm{Answer.}$
$$
W = -\pi\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)