---
title: 今日の計算(242)
date: "2021-06-27T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.8

$\bm{Question.}$
誘導磁場とベクトルポテンシャルの関係式$\bm{B}=\nabla\times\bm{A}$がゲージ変換$\bm{A}\to\bm{A}+\nabla\varphi$に対して不変であることを示せ. 

$\bm{Proof.}$
$$
\nabla\times(\bm{A}+\nabla\varphi) = \nabla\times\bm{A} = \bm{B}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)