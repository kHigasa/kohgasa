---
title: 今日の計算(216)
date: "2021-06-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.18

$\bm{Question.}$
Pauli行列により定義されたベクトル
$$
\bm{\sigma} \equiv \hat{\bm{e}}_x\bm{\sigma}_1+\hat{\bm{e}}_y\bm{\sigma}_2+\hat{\bm{e}}_z\bm{\sigma}_3
$$
が, 任意のベクトル$\bm{a}$, $\bm{b}$に対して
$$
(\bm{\sigma}\cdot\bm{a})(\bm{\sigma}\cdot\bm{b}) = (\bm{a}\cdot\bm{b})\bm{1}_2+\mathrm{i}\bm{\sigma}\cdot(\bm{a}\times\bm{b})
$$
を満たすことを確かめよ. 

$\bm{Answer.}$
OK. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)