---
title: 今日の計算(243)
date: "2021-06-28T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.9

$\bm{Question.}$
もし球面または球内に電荷が与えられていないならば, 任意の点$P$での静電ポテンシャル$\varphi$の値が$P$を中心とする任意の球面についてのポテンシャルの平均と等しいことを示せ. 

$\bm{Proof.}$
略. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)