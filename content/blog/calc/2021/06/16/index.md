---
title: 今日の計算(231)
date: "2021-06-16T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.10

$\bm{Question.}$
$$
`\int_{\text{C}}u\nabla v\cdot\mathrm{d}\bm{\ell} = \int_{\text{S}}(\nabla u)\times(\nabla v)\cdot\mathrm{d}\bm{\sigma}
$$
を示せ. 

$\bm{Proof.}$
Stokesの定理より, 
$$
\begin{aligned}
`    \int_{\text{C}}u\nabla v\cdot\mathrm{d}\bm{\ell} &= \int_{\text{S}}\nabla\times(u\nabla v)\cdot\mathrm{d}\bm{\sigma} \\
    &= \int_{\text{S}}\left[(\nabla u)\times(\nabla v)+u\nabla\times(\nabla v)\right]\cdot\mathrm{d}\bm{\sigma} \\
    &= \int_{\text{S}}(\nabla u)\times(\nabla v)\cdot\mathrm{d}\bm{\sigma}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)