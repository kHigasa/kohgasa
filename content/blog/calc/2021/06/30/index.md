---
title: 今日の計算(245)
date: "2021-06-30T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.11

$\bm{Question.}$
Lorentzゲージを仮定して, 
$$
\frac{1}{c^2}\frac{\partial^2\bm{A}}{\partial t^2}-\nabla^2\bm{A} = \mu_0\bm{J}
$$
を導け. 

$\bm{Answer.}$
$$
\begin{aligned}
    \nabla\times\bm{B} &= \frac{1}{c^2}\frac{\partial\bm{E}}{\partial t}+\mu_0\bm{J} \\
    &= -\frac{1}{c^2}\frac{\partial}{\partial t}\left(\nabla\varphi+\frac{\partial\bm{A}}{\partial t}\right) \\
    \nabla\times\bm{B} &= \nabla\times(\nabla\times\bm{A}) \\
    &= \nabla(\nabla\cdot\bm{A})-\nabla^2\bm{A} \\
    &= -\frac{1}{c^2}\nabla\frac{\partial\varphi}{\partial t}
\end{aligned}
$$
より, 
$$
\frac{1}{c^2}\frac{\partial^2\bm{A}}{\partial t^2}-\nabla^2\bm{A} = \mu_0\bm{J}
$$
が導かれた. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)