---
title: 今日の計算(225)
date: "2021-06-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.4

$\bm{Question.}$
点電荷$\rho$の作る電場$\bm{E}$, 静電ポテンシャル$\varphi$, 閉曲面$S$に囲まれた領域$V$に対して
$$
\int\rho\varphi\mathrm{d}V =  \epsilon_0\int E^2\mathrm{d}V
$$
が成り立つことを示せ. 

$\bm{Proof.}$
$$
\nabla(\varphi\bm{E}) = \nabla\varphi\cdot\bm{E}+\varphi\nabla\cdot\bm{E} = -\bm{E}^2+\frac{\rho\varphi}{\epsilon_0}
$$
であり, Gaussの発散定理より, 
$$
\int\nabla(\varphi\bm{E})\mathrm{d}V = \int_{\text{S}}\varphi\bm{E}\cdot\mathrm{d}\bm{\sigma} = 0
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)