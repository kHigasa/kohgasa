---
title: 今日の計算(222)
date: "2021-06-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.1

$\bm{Question.}$
Sを閉曲面としたとき
$$
\int_{\text{S}}\mathrm{d}\bm{\sigma} = 0
$$
を示せ. 

$\bm{Proof.}$
ベクトル$\bm{1}$に対してGaussの発散定理より, 
$$
\int_{\text{S}}\mathrm{d}\bm{\sigma} = \int_{\text{S}}\bm{1}\cdot\mathrm{d}\bm{\sigma} = \int_{\text{V}}\nabla\cdot\bm{1}\mathrm{d}V = 0
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)