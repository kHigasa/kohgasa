---
title: 今日の計算(238)
date: "2021-06-23T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.4

$\bm{Question.}$
電流$I$の流れている長いまっすぐな棒の作る誘導磁場$\bm{B}$は
$$
\bm{B} = \frac{\mu_0I}{2\pi}(-\frac{y}{x^2+y^2},\frac{x}{x^2+y^2},0)
$$
で与えられ, このときのベクトルポテンシャル$\bm{A}$を求めよ. 

$\bm{Answer.}$
$\bm{B}=\nabla\times\bm{A}$より, 
$$
\bm{A} = -\frac{\mu_0I}{4\pi}\ln(x^2+y^2)\hat{\bm{e}}_z
$$
というベクトルポテンシャルがこの関係を満たす. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)
