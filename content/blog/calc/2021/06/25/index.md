---
title: 今日の計算(240)
date: "2021-06-25T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.6

$\bm{Question.}$
いかなる定誘導磁場ベクトル$\bm{B}$に対しても, 
$$
\bm{A} = \frac{1}{2}(\bm{B}\times\bm{r})\text{, }\bm{B} = \nabla\times\bm{A}
$$
が満たされることを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \bm{B} &= \frac{1}{2}\nabla\times(\bm{B}\times\bm{r}) \\
    &= \frac{1}{2}(\bm{B}\nabla\cdot\bm{r}-\bm{B}\cdot\nabla\bm{r}) = \bm{B}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)