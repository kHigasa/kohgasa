---
title: 今日の計算(237)
date: "2021-06-22T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.3

$\bm{Question.}$
Cartesian座標系の原点を地球の中心にとって, 月が$z$軸上に$R$だけ離れたところにあるとき, 地球上の位置$\bm{r}$にある粒子に対する月による潮汐力は
$$
\bm{F} = -\frac{GMm}{R^3}\left(x, y, -2z\right)
$$
であたえられ, この力を生み出すポテンシャルを求めよ. 

$\bm{Answer.}$
$$
-\frac{GMm}{R^3}\left(\frac{x^2}{2}, \frac{y^2}{2}, -z^2\right)\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)