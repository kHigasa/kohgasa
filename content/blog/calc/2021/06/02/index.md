---
title: 今日の計算(217)
date: "2021-06-02T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.7.1

$\bm{Question.}$
原点$O$と3つのベクトル$\bm{A}$, $\bm{B}$, $\bm{C}$によりなる正四面体の表面積$S$を求めよ. 

$\bm{Answer.}$
$$
S = \frac{1}{2}\lbrace|bm{A}\times\bm{B}|+|\bm{B}\times\bm{C}|+|\bm{C}\times\bm{A}|+|(\bm{B}-\bm{A})\times(\bm{C}-\bm{A})|\rbrace
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)