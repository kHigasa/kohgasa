---
title: 今日の計算(241)
date: "2021-06-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.7

$\bm{Question.}$
スカラー関数$u$と$v$に対してベクトル$\bm{B}$が
$$
\bm{B} = (\nabla u)\times(\nabla v)
$$
を満たすとき次を示せ. 

### (a)
$\bm{B}$は管状である. 

$\bm{Proof.}$
$$
\begin{aligned}
    &\nabla\cdot\bm{B} \\
    = &(\nabla v)\cdot(\nabla\times(\nabla u))-(\nabla u)\cdot(\nabla\times(\nabla v)) \\
    = &0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{B}=\nabla\times\bm{A}$を満たすとき, 
$$
\bm{A} = \frac{1}{2}(u\nabla v-v\nabla u)
$$
である. 

$\bm{Proof.}$
$$
\begin{aligned}
    &\nabla\times\bm{A} \\
    = &\frac{1}{2}\left\lbrace(\nabla u)\times(\nabla u)-(\nabla v)\times(\nabla u)\right\rbrace \\
    = &(\nabla u)\times(\nabla u) = \bm{B}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)