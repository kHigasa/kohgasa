---
title: 今日の計算(228)
date: "2021-06-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.7

あるループC (囲まれる面積は$A$) によって作られる磁気モーメントを考える際に次の線績分が現れる: 
$$
\int_{\text{C}}\bm{r}\times\mathrm{d}\bm{r}\text{. }
$$

### (a)
$\bm{Question.}$
線積分の値を計算せよ. 

$\bm{Answer.}$
Stokesの定理より, 
$$
\int_{\text{C}}\bm{r}\times\mathrm{d}\bm{r} = \int_{\text{S}}\nabla\times\bm{r}\times\mathrm{d}\bm{r} = 2\int_{\text{S}}\hat{\bm{r}}\cdot\mathrm{d}\bm{\sigma} = 2A
$$
と計算される. 

### (b)
$\bm{Question.}$
ループが楕円$\bm{r}=a\cos\theta\hat{\bm{e}}_x+b\sin\theta\hat{\bm{e}}_y$で与えられるとき, 楕円の面積が$\pi ab$となることを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \int_{\text{C}}\bm{r}\times\mathrm{d}\bm{r} &= \int_{\text{C}}(a\cos\theta,b\sin\theta)\times(-a\sin\theta,b\cos\theta)\mathrm{d}\theta \\
    &= \int_0^{2\pi}ab(\cos^2\theta+\sin^2\theta)\hat{\bm{e}}_z\mathrm{d}\theta = 2\pi ab\hat{\bm{e}}_z
\end{aligned}
$$
となり, 前問[(a)](#a)の結果と合わせることにより示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)