---
title: 今日の計算(224)
date: "2021-06-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.8.3

$\bm{Question.}$
$\bm{B}=\nabla\times\bm{A}$であるとき, 任意の閉曲面Sに対して
$$
\int_{\text{S}}\bm{B}\cdot\mathrm{d}\bm{\sigma} = 0
$$
となることを示せ. 

$\bm{Proof.}$
Gaussの発散定理より, 
$$
\text{(与式)} = \int_{\text{V}}\nabla\cdot\bm{B}\mathrm{d}V = \int_{\text{V}}\nabla\cdot(\nabla\times\bm{A})\mathrm{d}V = 0
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)