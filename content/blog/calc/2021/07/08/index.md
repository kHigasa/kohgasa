---
title: 今日の計算(251)
date: "2021-07-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.7

$\bm{Question.}$

円筒座標成分を用いて, Cartesian座標系の単位ベクトルを表せ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \hat{\bm{e}}_x &= \cos\phi\hat{\bm{e}}_{\rho}-\sin\phi\hat{\bm{e}}_{\phi} \\
    \hat{\bm{e}}_y &= \sin\phi\hat{\bm{e}}_{\rho}+\cos\phi\hat{\bm{e}}_{\phi} \\
    \hat{\bm{e}}_z &= \hat{\bm{e}}_z\text{. }
\end{aligned}
$$


### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)