---
title: 今日の計算(257)
date: "2021-07-14T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.13

$\bm{Question.}$
動いている粒子の速度と加速度を円筒座標系で表せ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \bm{v} &= (\dot{\rho}, \rho\dot{\phi}, \dot{z})\text{, } \\
    \bm{a} &= (\ddot{\rho}-\rho\dot{\phi}^2, \rho\ddot{\phi}+2\dot{\rho}\dot{\phi}, \ddot{z})\text{. }
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)