---
title: 今日の計算(255)
date: "2021-07-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.11
### (a)
$\bm{Question.}$
固定された$xyz$軸に対する座標$(\rho,\phi,z)$でのパリティ操作が変換
$$
\rho\to\rho\text{, }\phi\to\phi\pm\pi\text{, }z\to-z
$$
からなることを示せ. 

$\bm{Proof.}$
$(x,y,z)\to(-x,-y,-z)$にパリティ変換することは$(\rho,\phi,z)\to(\rho,\phi\pm\pi,-z)$に対応する. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$\hat{\bm{e}}_{\rho}$, $\hat{\bm{e}}_{\phi}$が奇パリティを持ち, $\hat{\bm{e}}_z$が偶パリティを持つことを示せ. 

$\bm{Proof.}$
$\phi$を$\pi$だけ変化させると, パリティは題意の通りになる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)