---
title: 今日の計算(266)
date: "2021-07-23T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.22
### (a)
$\bm{Question.}$
3次元極座標単位ベクトルの3次元Cartesian直交座標成分表示を$r$, $\theta$, $\phi$について微分せよ. 

$\bm{Answer.}$
$\hat{\bm{e}}=(\hat{\bm{e}}_r, \hat{\bm{e}}_{\theta}, \hat{\bm{e}}_{\phi})$とおく. 
$$
\begin{aligned}
    \frac{\partial\hat{\bm{e}}}{\partial r} &= \hat{\bm{e}}_r \\
    \frac{\partial\hat{\bm{e}}}{\partial\theta} &= \hat{\bm{e}}_r(\cos\theta\cos\phi, \cos\theta\sin\phi, -\sin\theta) = \hat{\bm{e}}_{\theta} \\
    \frac{\partial\hat{\bm{e}}}{\partial\phi} &= \hat{\bm{e}}_r(-\sin\theta\sin\phi, \sin\theta\cos\phi, 0) = \sin\theta\hat{\bm{e}}_{\phi}
\end{aligned}
$$

### (b)
$\bm{Question.}$
$\nabla$が
$$
\nabla = \frac{\partial}{\partial r}\hat{\bm{e}}_r+\frac{1}{r}\frac{\partial}{\partial\theta}\hat{\bm{e}}_{\theta}+\frac{1}{r\sin\theta}\frac{\partial}{\partial\phi}\hat{\bm{e}}_{\theta}
$$
と与えられているとき, 小問[(a)](#a)の結果を用いて, 3次元極座標系のLaplacianの表式を求めよ. 

$\bm{Answer.}$
積の微分法と小問[(a)](#a)の結果を用いて, 
$$
\nabla\cdot\nabla = \frac{\partial^2}{\partial r^2}+\frac{2}{r}\frac{\partial}{\partial r}+\frac{1}{r^2}\frac{\partial^2}{\partial\theta^2}+\frac{\tan\theta}{r^2}\frac{\partial}{\partial\theta}+\frac{1}{r^2\sin^2\theta}\frac{\partial^2}{\partial\phi^2}
$$
と計算される. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)