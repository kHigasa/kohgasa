---
title: 今日の計算(260)
date: "2021-07-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.16
力が
$$
\bm{F} = \frac{-y}{x^2+y^2}\hat{\bm{e}}_x+\frac{x}{x^2+y^2}\hat{\bm{e}}_y
$$
で与えられている. 

### (a)
$\bm{Question.}$
$\bm{F}$を円筒座標系で表せ. 

$\bm{Answer.}$
$$
\bm{F} = \frac{1}{\rho}\hat{\bm{e}}_{\phi}\text{. }
$$

### (b)
$\bm{Question.}$
円筒座標系で$\nabla\times\bm{F}$を計算せよ. 

$\bm{Answer.}$
$$
\nabla\times\bm{F} = \bm{0}\text{. }
$$

### (c)
$\bm{Question.}$
単位円上を反時計回りに一周したときに$\bm{F}$によってなされる仕事を円筒座標系で計算せよ. 

$\bm{Answer.}$
$$
\int_0^{2\pi}\bm{F}\cdot\hat{\bm{e}}_{\phi}\rho\mathrm{d}\phi = 2\pi\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)