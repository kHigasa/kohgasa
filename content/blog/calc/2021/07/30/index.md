---
title: 今日の計算(273)
date: "2021-07-30T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.29

$\bm{Question.}$
$$
x\frac{\partial}{\partial y}-y\frac{\partial}{\partial x} = \frac{\partial}{\partial\phi}
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \text{LHS} = &r\cos\theta\left(\sin\theta\sin\phi\frac{\partial}{\partial r}+\frac{\cos\theta\sin\phi}{r}\frac{\partial}{\partial\theta}+\frac{\cos\phi}{r\sin\theta}\frac{\partial}{\partial\phi}\right) \\
    &-r\sin\theta\left(\sin\theta\cos\phi\frac{\partial}{\partial r}+\frac{\cos\theta\cos\phi}{r}\frac{\partial}{\partial\theta}-\frac{\sin\phi}{r\sin\theta}\frac{\partial}{\partial\phi}\right) \\
    = &\frac{\sin\theta}{\sin\theta}\frac{\partial}{\partial\phi} = \frac{\partial}{\partial\phi}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)