---
title: 今日の計算(252)
date: "2021-07-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.8

$\bm{Question.}$
$$
\frac{\partial\hat{\bm{e}}_{\rho}}{\partial\phi} = \hat{\bm{e}}_{\phi}\text{, }\frac{\partial\hat{\bm{e}}_{\phi}}{\partial\phi} = -\hat{\bm{e}}_{\rho}
$$
を確かめよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \hat{\bm{e}}_r &= \cos\phi\hat{\bm{e}}_x+\sin\phi\hat{\bm{e}}_y \\
    \hat{\bm{e}}_{\phi} &= -\sin\phi\hat{\bm{e}}_x+\cos\phi\hat{\bm{e}}_y
\end{aligned}
$$
より確かめられた. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)