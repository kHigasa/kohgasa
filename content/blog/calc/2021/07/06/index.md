---
title: 今日の計算(249)
date: "2021-07-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.5

$\bm{Question.}$
曲線座標系において直交単位ベクトルの集合が
$$
\hat{\bm{e}}_i = \frac{1}{h_i}\frac{\partial\bm{r}}{\partial q_i}
$$
によって定義されることを示せ. 

$\bm{Proof.}$
ベクトル$\bm{r}$の$q_i$方向への微分が成分$h_i$である. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)