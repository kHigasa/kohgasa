---
title: 今日の計算(262)
date: "2021-07-19T01:00:00.000Z"
description: "手元の雑多な計算集"
draft: true
---

## 3.10.18

$\bm{Question.}$
3次元極座標の単位ベクトルを, 3次元Cartesian座標系の単位ベクトルを用いて表せ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \hat{\bm{e}}_r &= \sin\theta\cos\phi\hat{\bm{e}}_x+\sin\theta\sin\phi\hat{\bm{e}}_y+\cos\theta\hat{\bm{e}}_z \\
    \hat{\bm{e}}_{\theta} &= \cos\theta\cos\phi\hat{\bm{e}}_x+\cos\theta\sin\phi\hat{\bm{e}}_y-\sin\theta\hat{\bm{e}}_z \\
    \hat{\bm{e}}_{\phi} &= -\sin\phi\hat{\bm{e}}_x+\cos\phi\hat{\bm{e}}_y\text{. }
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)