---
title: 今日の計算(248)
date: "2021-07-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.3

$\bm{Question.}$
直交座標系と同様に, $\mathbb{R}^3$において直交曲線座標系においてもドット積とクロス積が定義されることを議論せよ. 

$\bm{Answer.}$
直交曲線座標系のベクトル$\bm{a},\bm{b},\bm{c}$を直交座標系の基底$\bm{p},\bm{q},\bm{r}$で表すと, $\bm{a}=\bm{p}\bm{a}\cdot\bm{p}+\bm{q}\bm{a}\cdot\bm{q}+\bm{r}\bm{a}\cdot\bm{r}$等となるが, この基底の変換でドット積とクロス積は形を変えない. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)