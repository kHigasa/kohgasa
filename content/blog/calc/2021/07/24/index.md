---
title: 今日の計算(267)
date: "2021-07-24T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.23

$\bm{Question.}$
3次元極座標系で角速度ベクトル$\bm{\omega}$について次の量を計算せよ. 

### (a)
$$
\bm{v} = \bm{\omega}\times\bm{r}\text{. }
$$

$\bm{Answer.}$
$$
\bm{v} = \omega r\sin\theta\hat{\bm{e}}_{\phi}\text{. }
$$

### (b)
$$
\bm{\nabla}\times\bm{v}\text{. }
$$

$\bm{Answer.}$
$$
\nabla\times\bm{v} = 2\bm{\omega}\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)