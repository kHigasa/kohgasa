---
title: 今日の計算(248)
date: "2021-07-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.2

$\bm{Question.}$
楕円筒座標系は次の3つの面からなる: 
$$
\begin{aligned}
\frac{x^2}{a^2\cosh^2u}+\frac{y^2}{a^2\sinh^2u} &= 1 \\
\frac{x^2}{a^2\cos^2v}-\frac{y^2}{a^2\sin^2u} &= 1 \\
z &= z\text{. }
\end{aligned}
$$
単位ベクトル$\hat{\bm{e}}_u$, $\hat{\bm{e}}_v$を示せ. 

$\bm{Answer.}$
略. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)