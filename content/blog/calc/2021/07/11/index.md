---
title: 今日の計算(254)
date: "2021-07-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.10
### (a)
$\bm{Question.}$
円筒座標系において位置ベクトルが
$$
\bm{r} = \rho\hat{\bm{e}}_{\rho}+z\hat{\bm{e}}_z
$$
と表されることを示せ. 

$\bm{Proof.}$
勿論. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
円筒座標系において
$$
\nabla\cdot\bm{r} = 3\text{ and }\nabla\times\bm{r} = 0
$$
を示せ. 

$\bm{Proof.}$
$$
``\begin{aligned}
    \nabla\cdot\bm{r} &= \frac{1}{\rho}\frac{\partial \rho^2}{\partial\rho}+\frac{\partial z}{\partial z} \\
    &= 3 \\
    \nabla\times\bm{r} &= \frac{1}{\rho}\begin{vmatrix}
        \hat{\bm{e}}_{\rho} & \rho\hat{\bm{e}}_{\phi} & \hat{\bm{e}}_z \\
	\frac{\partial}{\partial\rho} & \frac{\partial}{\partial\phi} & \frac{\partial}{\partial z} \\
	\rho^2 & 0 & z
    \end{vmatrix} = 0
``\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)