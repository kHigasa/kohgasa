---
title: 今日の計算(261)
date: "2021-07-18T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.17

$\bm{Question.}$
誘導磁場が$\bm{B}=B_{\phi}(\rho)\hat{\bm{e}}_{\phi}$で与えられているとき, 
$$
(\bm{B}\cdot\nabla)\bm{B} = -\frac{B_{\phi}^2}{\rho}\hat{\bm{e}}_{\rho}
$$
を示せ. 

$\bm{Proof.}$
なります. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)