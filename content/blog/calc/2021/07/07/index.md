---
title: 今日の計算(250)
date: "2021-07-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.6

$\bm{Question.}$
Cartesian成分を用いて, 円筒座標系の単位ベクトルを表せ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \hat{\bm{e}}_r &= \cos\phi\hat{\bm{e}}_x+\sin\phi\hat{\bm{e}}_y \\
    \hat{\bm{e}}_{\phi} &= -\sin\phi\hat{\bm{e}}_x+\cos\phi\hat{\bm{e}}_y \\
    \hat{\bm{e}}_z &= \hat{\bm{e}}_z\text{. }
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)