---
title: 今日の計算(247)
date: "2021-07-02T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.1

$\bm{Question.}$
静電気学や流体力学においてしばしば用いられる直交$uvz$座標系は
$$
xy = u\text{, }x^2-y^2=v\text{, }z=z
$$
によって定義される. この系は右手系か左手系か答えよ. 

$\bm{Answer.}$
Jacobian
$$
\frac{\partial(u,v)}{\partial(x,y)} = -2(x^2+y^2) < 0
$$
より, $xyz$座標系が右手系であるので, $uvz$座標系は左手系であるとわかる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)