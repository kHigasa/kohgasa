---
title: 今日の計算(269)
date: "2021-07-26T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.25
### (a)
$\bm{Question.}$
座標$(r,\theta,\phi)$を固定された$xyz$-座標系に対して反転させたとき, 移った先の座標を答えよ. 

$\bm{Answer.}$
$$
(r,\theta-\pi,\phi\pm\pi)\text{. }
$$

### (b)
$\bm{Question.}$
$\hat{\bm{e}}_r$と$\hat{\bm{e}}_{\phi}$が奇パリティを持ち, $\hat{\bm{e}}_{\theta}$が偶パリティを持つことを示せ. 

$\bm{Proof.}$
小問[(a)](#a)の結果より, 座標系を考えれば解る. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)