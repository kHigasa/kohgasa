---
title: 今日の計算(253)
date: "2021-07-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.9

$\bm{Question.}$
円筒座標系に対するベクトル$\bm{V}$の勾配の表式と
$$
\nabla = \frac{\partial}{\partial\rho}\hat{\bm{e}}_{\rho}+\frac{1}{\rho}\frac{\partial}{\partial\phi}\hat{\bm{e}}_{\phi}+\frac{\partial}{\partial z}\hat{\bm{e}}_z
$$
をベクトル$\bm{V}$に作用させた表式を比べよ. 

$\bm{Answer.}$
略. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)