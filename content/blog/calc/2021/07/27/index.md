---
title: 今日の計算(270)
date: "2021-07-27T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.26

$\bm{A}$は任意のベクトルであるとき, 
$$
\bm{A}\cdot\nabla\bm{r} = \bm{A}
$$
が成り立っている. 

### (a)
$\bm{Question.}$
この関係をCartesian座標系で確かめよ. 

$\bm{Answer.}$
$$
\bm{A}\cdot\nabla\bm{r} = A_x\frac{\partial\bm{r}}{\partial x}+A_y\frac{\partial\bm{r}}{\partial y}+A_z\frac{\partial\bm{r}}{\partial z} = \bm{A}\text{. }
$$

### (b)
$\bm{Question.}$
この関係を極座標系で確かめよ. 

$\bm{Answer.}$
$$
\bm{A}\cdot\nabla\bm{r} = A_r\frac{\partial\bm{r}}{\partial r}+A_{\theta}\frac{1}{r}\frac{\partial\bm{r}}{\partial\theta}+A_{\phi}\frac{1}{r\sin\theta}\frac{\partial\bm{r}}{\partial\phi} = \bm{A}\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)