---
title: 今日の計算(264)
date: "2021-07-21T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.20

$\bm{Question.}$
3次元極座標系のベクトルを3次元Cartesian座標系のベクトルに変換する行列を求めよ. 

$\bm{Answer.}$
$$
\begin{pmatrix}
    \sin\theta\cos\phi & \sin\theta\sin\phi & \cos\theta \\
    \cos\theta\cos\phi & \cos\theta\sin\phi & -\sin\theta \\
    -\sin\phi & \cos\phi & 0
\end{pmatrix}\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)