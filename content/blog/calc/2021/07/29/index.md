---
title: 今日の計算(272)
date: "2021-07-29T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.28

$\bm{Question.}$
微分演算子$\nabla$を極座標系で表せ. 

$\bm{Answer.}$
$(x,y,z)=r(\sin\theta\cos\phi,\sin\theta\sin\phi,\cos\theta)$より, $(r,\theta,\phi)=\left(\sqrt{x^2+y^2+z^2,\tan^{-1}(y/x),\tan^{-1}(\sqrt{x^2+y^2}/z)}\right)$であるから, 
$$
\begin{aligned}
    \frac{\partial}{\partial x} &= \frac{\partial}{\partial r}\frac{\partial r}{\partial x}+\frac{\partial}{\partial\theta}\frac{\partial\theta}{\partial x}+\frac{\partial}{\partial\phi}\frac{\partial\phi}{\partial x} \\
    &= \sin\theta\cos\phi\frac{\partial}{\partial r}+\frac{\cos\theta\cos\phi}{r}\frac{\partial}{\partial\theta}-\frac{\sin\phi}{r\sin\theta}\frac{\partial}{\partial\phi} \\
    \frac{\partial}{\partial y} &= \sin\theta\sin\phi\frac{\partial}{\partial r}+\frac{\cos\theta\sin\phi}{r}\frac{\partial}{\partial\theta}+\frac{\cos\phi}{r\sin\theta}\frac{\partial}{\partial\phi} \\
    \frac{\partial}{\partial z} &= \cos\theta\frac{\partial}{\partial r}-\frac{\sin\theta}{r}\frac{\partial}{\partial\theta}
\end{aligned}
$$
となる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)