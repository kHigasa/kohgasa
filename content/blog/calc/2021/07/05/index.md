---
title: 今日の計算(248)
date: "2021-07-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.4

$\bm{Question.}$
$\hat{\bm{e}}_1$が曲線座標系の第一成分$q_1$が増加する方向の単位ベクトルであるとき次を示せ. 

### (a)
$$
\nabla\cdot\hat{\bm{e}}_1 = \frac{1}{h_1h_2h_3}\frac{\partial(h_2h_3)}{\partial q_1}
$$

$\bm{Proof.}$
ベクトル$\hat{\bm{e}}_1$の第二, 三成分が$0$であるのでこのようになる. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$$
\nabla\times\hat{\bm{e}}_1 = \frac{1}{h_1}\left[\hat{\bm{e}}_2\frac{1}{h_3}\frac{\partial h_1}{q_3}-\hat{\bm{e}}_3\frac{1}{h_2}\frac{\partial h_1}{q_2}\right]
$$

$\bm{Proof.}$
ベクトル$\hat{\bm{e}}_1$の第二, 三成分が$0$であるのでこのようになる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)