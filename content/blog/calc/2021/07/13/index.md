---
title: 今日の計算(256)
date: "2021-07-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.12

$\bm{Question.}$
一定の角速度ベクトル$\bm{\omega}$で剛体が回転しているとる. 円筒座標系で次の量を計算せよ. 

### (a)
$$
\bm{v} = \bm{\omega}\times\bm{r}\text{. }
$$

$\bm{Answer.}$
$\bm{r}=(\rho,0,0)$, $\bm{\omega}=(0,0,\omega)$であるので, 
$$
\bm{v} = \omega\rho\hat{\bm{e}}_{\phi}
$$
と計算される. 

### (b)
$$
\nabla\times\bm{v}\text{. }
$$

$\bm{Answer.}$
$$
\bm{v} = 2\omega\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)