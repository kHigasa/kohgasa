---
title: 今日の計算(268)
date: "2021-07-25T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.24

$\bm{Question.}$
$r$成分がなく, 回転の$\theta$成分もないベクトル$\bm{V}$があるとき, $\bm{V}$の$\theta$成分の$r$依存性についてどのようなことが示唆されるか答えよ. 

$\bm{Answer.}$
情報量が少なくて何も言えない. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)