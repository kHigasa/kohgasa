---
title: 今日の計算(263)
date: "2021-07-20T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.19

$\bm{Question.}$
3次元Cartesian座標系の単位ベクトルを, 3次元極座標系の単位ベクトルを用いて表せ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \hat{\bm{e}}_x &= \sin\theta\cos\phi\hat{\bm{e}}_r+\cos\theta\sin\phi\hat{\bm{e}}_{\theta}-\sin\phi\hat{\bm{e}}_{\phi} \\
    \hat{\bm{e}}_y &= \sin\theta\sin\phi\hat{\bm{e}}_r+\cos\theta\sin\phi\hat{\bm{e}}_{\theta}+\cos\phi\hat{\bm{e}}_{\phi} \\
    \hat{\bm{e}}_z &= \cos\theta\hat{\bm{e}}_r-\sin\theta\hat{\bm{e}}_{\theta}\text{. }
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)