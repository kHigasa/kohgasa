---
title: 今日の計算(259)
date: "2021-07-16T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.15

$\bm{Question.}$
$z$軸に沿って伸びる導線に電流$I$が流れているとき, ベクトルポテンシャル
$$
\bm{A} = \frac{\mu I}{2\pi}\ln\left(\frac{1}{\rho}\right)\hat{\bm{e}}_z
$$
が生じている. このとき誘導される磁場を求めよ. 

$\bm{Answer.}$
$$
\bm{B} = \nabla\times\bm{A} = \frac{\mu I}{2\pi\rho}\hat{\bm{e}}_{\phi}\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)