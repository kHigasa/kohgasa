---
title: 今日の計算(258)
date: "2021-07-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.14

$\bm{Question.}$
円筒座標系において, 
$$
\bm{V} = V_{\rho}(\rho,\phi)\hat{\bm{e}}_{\rho}+V_{\phi}(\rho,\phi)\hat{\bm{e}}_{\phi}
$$
と表されるベクトルについての回転を取ると$z$成分のみであることを確かめよ. 

$\bm{Answer.}$
当然. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)