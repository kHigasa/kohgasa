---
title: 今日の計算(271)
date: "2021-07-28T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.27

$\bm{Question.}$
運動している粒子の速度と加速度を極座標系で表せ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \bm{v} &= (\dot{r},r\dot{\theta},r\sin\theta\dot{\phi}) \\
    \bm{a} &= (\ddot{r}-r\dot{\theta}^2-r\sin^2\theta\dot{\phi}^2,r\ddot{\theta}+2\dot{r}\dot{\theta}-r\sin\theta\cos\theta\dot{\phi}^2,r\sin\theta\ddot{\phi}+2\dot{r}\sin\theta\dot{\phi}+2r\cos\theta\dot{\theta}\dot{\phi})\text{. }
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)