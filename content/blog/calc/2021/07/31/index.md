---
title: 今日の計算(274)
date: "2021-07-31T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.30

$\bm{Question.}$
軌道角運動量演算子$\bm{L}=-\mathrm{i}(\bm{r}\times\bm{\nabla})$に対して以下のことを示せ. 

### (a)
$$
L_x+\mathrm{i}L_y = \mathrm{e}^{\mathrm{i}\phi}\left(\frac{\partial}{\partial\theta}+\mathrm{i}\cot\theta\frac{\partial}{\partial\phi}\right)\text{. }
$$

$\bm{Proof.}$
なります. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$$
L_x-\mathrm{i}L_y = -\mathrm{e}^{-\mathrm{i}\phi}\left(\frac{\partial}{\partial\theta}-\mathrm{i}\cot\theta\frac{\partial}{\partial\phi}\right)\text{. }
$$

$\bm{Proof.}$
なります. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)