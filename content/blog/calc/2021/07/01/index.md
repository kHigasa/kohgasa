---
title: 今日の計算(246)
date: "2021-07-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.9.12

$\bm{Question.}$
任意の管状ベクトル$\bm{B}$が
$$
\bm{A} = -\hat{\bm{e}_x}\int_{y_0}^yB_z(x,y,z)\mathrm{d}y-\hat{\bm{e}}_z\left[\int_{x_0}^xB_y(x,y_0,z)\mathrm{d}x-\int_{y_0}^yB_x(x,y,z)\mathrm{d}y\right]
$$
を用いて, $\bm{B}=\nabla\times\bm{A}$と表されることを示せ. 

$\bm{Proof.}$
計算すると, $\nabla\times\bm{A}=\bm{B}$となり, このとき$\nabla\cdot\bm{B}=0$となるから示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)