---
title: 今日の計算(150)
date: "2021-03-27T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.41

$\bm{Question.}$
Diracの$\gamma$行列$\gamma^{\mu}\ (\mu=0,1,2,3)$が
$$
(\gamma^0)^2 = I_4\text{, }(\gamma^i)^2=-I_4\text{, }\gamma^{\mu}\gamma^i+\gamma^i\gamma^{\mu} = 0\ (\mu\neq i)
$$
を満たすことを示せ. 

$\bm{Proof.}$
$\gamma^0=\sigma_3\otimes I_2$より, 
$$
(\gamma^0)^2 = \sigma_3^2\otimes I_2^2 = I_2\otimes I_2 = I_4
$$
となる. また$\gamma^i=\gamma\otimes\sigma_i\ (i=1,2,3)$, 
$$
\gamma = \begin{pmatrix}
    0 & 1 \\
    -1 & 0
\end{pmatrix}
$$
より, 
$$
\begin{aligned}
    (\gamma^i)^2 &= \gamma^2\otimes\sigma_i^2=-I_2\otimes I_2 = -I_4\ (i=1,2,3) \\
    \gamma^{\mu}\gamma^i+\gamma^i\gamma^{\mu} &= \gamma^2\otimes\sigma_{\mu}\sigma_i+\gamma^2\otimes\sigma_i\sigma_{\mu} \\
    &= \gamma^2\otimes\sigma_{\mu}\sigma_i-\gamma^2\otimes\sigma_{\mu}\sigma_i =0\ (\mu\neq i)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)