---
title: 今日の計算(133)
date: "2021-03-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.24

人口動態を調べる. $n$個の地域の始めの人口分布をベクトル$\bm{P}$で表し, ある時刻におけるある地域からある地域への人口移動を$n$次正方形の確率論的行列$T$で表す. またこれによる終わりの人口分布をベクトル$\bm{Q}=T\bm{P}$で表す. 定義より$\sum_{i=1}^nP_i=1$である. 

### (a)
人口が保存するためには, 
$$
\sum_{i=1}^nT_{ij} = 1\text{ , }j=1,2,\ldots ,n
$$
という条件が必要であることを示せ. 

$\bm{Proof.}$
地域$i\text{ , }1,\ldots ,n$から地域$j$へ人口が移る確率を足し合わせたものであるから, 全確率の法則より$1$になる. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
条件
$$
\sum_{i=1}^nQ_i = 1
$$
が人口の保存を維持していることを示せ. 

$\bm{Proof.}$
$$
\sum_{i=1}^nQ_i = \sum_{i,j=1}^nT_{ij}P_j = \sum_{j=1}^nP_j = 1
$$
より示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)