---
title: 今日の計算(148)
date: "2021-03-25T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.39

$\bm{Question.}$
2つのUnitary行列の積によって得られる行列はUnitaryであることを示せ. 

$\bm{Proof.}$
2つのUnitary行列を$U_1$, $U_2$とおくと, 
$$
(U_1U_2)^{\dagger}(U_1U_2) = U_2^{\dagger}U_1^{\dagger}U_1U_2 = \bm{1}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### コメント
問題文[^1]の*direct product*という言い方は紛らわしいのでやめてほしい. 

### References

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)