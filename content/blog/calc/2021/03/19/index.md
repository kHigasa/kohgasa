---
title: 今日の計算(142)
date: "2021-03-19T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.33

$\bm{Question.}$
$S$がゼロ行列でなければ, 行列$C=S^{\dagger}S$のトレースは正定値であることを示せ. 

$\bm{Proof.}$
行列$C$の定義より全ての対角成分はある複素数とその共軛複素数との積の和によって得られるため, 示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)