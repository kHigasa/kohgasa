---
title: 今日の計算(130)
date: "2021-03-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.21

$\bm{Question.}$
行列$A$に$M_L$を左から作用させることによって, 以下の各小問に示すような要素を持つ行列になるとき, $M_L$を求めよ. 

### (a)

第$i$行が$k$倍される. 

$\bm{Answer.}$
$M_L$は$i$行$i$列目の要素だけ$k$でそれ以外は単位行列であるような行列. 

### (b)

$i$行$j$列の要素が, $i$行$j$列の要素から$m$行$j$列の要素の$k$倍を引いたものになる. 

$\bm{Answer.}$
$M_L$は$i$行$m$列目の要素だけ$-k$でそれ以外は単位行列であるような行列. 

### (c)

第$i$行と第$m$行が交換する. 

$\bm{Answer.}$
$M_L$は第$i$行と第$j$行を交換した単位行列. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)