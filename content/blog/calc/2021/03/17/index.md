---
title: 今日の計算(140)
date: "2021-03-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.31

$\bm{Question.}$
3つの角運動量行列は交換関係$[J_x,J_y]=\mathrm{i}J_z$を満たす. これらの行列のうち2つが実行列であるとき, 3つ目の行列は純虚数を要素に持つ行列であることを示せ. 

$\bm{Proof.}$
交換関係より明らか. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)