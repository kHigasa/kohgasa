---
title: 今日の計算(132)
date: "2021-03-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.23

$\bm{Question.}$
$$
A = \begin{pmatrix}
    3 & 2 & 1 \\
    2 & 2 & 1 \\
    1 & 1 & 4
\end{pmatrix}
$$
の逆行列を求めよ. 

$\bm{Answer.}$
$$
A^{-1} = \frac{1}{7}\begin{pmatrix}
    7 & -7 & 0 \\
    -7 & 11 & -1 \\
    0 & -1 & 2
\end{pmatrix}. 
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)