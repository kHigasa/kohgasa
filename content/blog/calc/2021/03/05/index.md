---
title: 今日の計算(128)
date: "2021-03-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.19
$\bm{Question.}$
2つの非特異な行列$A$,$B$が反交換するとき, それぞれの行列のトレースがゼロとなることを示せ. 

$\bm{Proof.}$
仮定より, それぞれの行列には逆行列が存在し, 
$$
A^{-1}(AB) = -A^{-1}(BA)\Rightarrow B = -A^{-1}BA
$$
となるから, 
$$
\begin{aligned}
    \mathrm{Tr}(B) &= -\mathrm{Tr}(-A^{-1}BA) = -\mathrm{Tr}((BA)A^{-1}) = -\mathrm{Tr}(B) \\
    \therefore\ \mathrm{Tr}(B) &= 0
\end{aligned}
$$
となり, $A$についても同様に議論できるため示された. <div class="QED" style="text-align: right">$\Box$</div>

### コメント
テキスト[^1]では上述の問題を小問(a)とし, 小問(b)として$n$次正方行列$A$,$B$の$n$の偶奇によって小問(a)の結果に矛盾が生じることを示せというものがあるが, 矛盾は生じず小問(a)の主張は成り立つので省略. 

### References

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)