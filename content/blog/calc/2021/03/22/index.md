---
title: 今日の計算(145)
date: "2021-03-22T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.36

$\bm{Question.}$
$A$と$B$が非可換なHermite行列であるとき, 
$$
AB-BA=\mathrm{i}C
$$
で定義される$C$がHermite行列であることを示せ. 

$\bm{Proof.}$
$$
C^{\dagger} = \lbrace\mathrm{i}(BA-AB)\rbrace^{\dagger} = \mathrm{i}(B^{\dagger}A^{\dagger}-A^{\dagger}B^{\dagger}) = \mathrm{i}(BA-AB) = C
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)