---
title: 今日の計算(153)
date: "2021-03-30T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.44

$\bm{Question.}$
$$
M=\frac{1}{2}(I_4+\bm{\gamma}^5)\text{, }\bm{\gamma}^5=\begin{pmatrix}
    0 & I_2 \\
    I_2 & 0
\end{pmatrix}
$$
とおくとき, $M^2=M$を示せ. 

$\bm{Proof.}$
$$
M^2=\frac{1}{4}(I_4+2\bm{\gamma}^5+I_4)=\frac{1}{2}(I_4+\bm{\gamma}^5)=M
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)