---
title: 今日の計算(127)
date: "2021-03-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.18

$\bm{Question.}$
行列$A$と$B$が反交換し, $A^2=B^2=1$を満たすとき, $\mathrm{Tr}(A)=\mathrm{Tr}(B)=0$が成り立つことを示せ. 

$\bm{Proof.}$
仮定より, 
$$
A(AB) = -A(BA)\ \Rightarrow B = -ABA
$$
となるから, 
$$
\begin{aligned}
    \mathrm{Tr}(B) &= -\mathrm{Tr}(ABA) = -\mathrm{Tr}(A(AB)) = -\mathrm{Tr}(B) \\
    \therefore\ \mathrm{Tr}(B) &= 0
\end{aligned}
$$
となり, $A$についても同様に議論できるため, 示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)