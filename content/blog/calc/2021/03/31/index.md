---
title: 今日の計算(154)
date: "2021-03-31T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.45

$\bm{Question.}$
4次元時空における16個のDiracの$\gamma$行列は線型独立な集合となっていることを示せ. 

$\bm{Proof.}$
$\gamma^0$, $\mathrm{i}\gamma^1$, $\mathrm{i}\gamma^2$, $\mathrm{i}\gamma^3$, $\gamma^0\gamma^1$, $\gamma^0\gamma^2$, $\gamma^0\gamma^3$, $\mathrm{i}\gamma^1\gamma^2$, $\mathrm{i}\gamma^2\gamma^3$, $\mathrm{i}\gamma^3\gamma^1$, $\gamma^1\gamma^2\gamma^3$, $\mathrm{i}\gamma^0\gamma^2\gamma^3$, $\mathrm{i}\gamma^0\gamma^1\gamma^3$, $\mathrm{i}\gamma^0\gamma^1\gamma^2$, $\gamma^5=\mathrm{i}\gamma^0\gamma^1\gamma^2\gamma^3$であり, 明らかにどれも他の線型結合によって表されないことがわかる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)