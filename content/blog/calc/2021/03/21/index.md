---
title: 今日の計算(144)
date: "2021-03-21T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.35

$\bm{Question.}$
$C$がHermite行列でないとき, $C+C^{\dagger}$と$\mathrm{i}(C-C^{\dagger})$がHermite行列であることを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    (C+C^{\dagger})^{\dagger} &= (C^{\dagger}+C)\text{, } \\
    \lbrace\mathrm{i}(C-C^{\dagger})\rbrace^{\dagger} &= -\mathrm{i}(C^{\dagger}-C) = \mathrm{i}(C-C^{\dagger})
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)