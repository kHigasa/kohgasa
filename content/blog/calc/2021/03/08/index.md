---
title: 今日の計算(131)
date: "2021-03-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.22

$\bm{Question.}$
行列$A$に$M_R$を右から作用させることによって, 以下の各小問に示すような要素を持つ行列になるとき, $M_R$を求めよ. 

### (a)

第$i$列が$k$倍される. 

$\bm{Answer.}$
$M_R$は$i$行$i$列目の要素だけ$k$でそれ以外は単位行列であるような行列. 

### (b)

$j$行$i$列の要素が, $j$行$i$列の要素から$j$行$m$列の要素の$k$倍を引いたものになる. 

$\bm{Answer.}$
$M_R$は$m$行$i$列目の要素だけ$-k$でそれ以外は単位行列であるような行列. 

### (c)

第$i$列と第$m$列が交換する. 

$\bm{Answer.}$
$M_R$は第$i$列と第$j$列を交換した単位行列. 


### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)