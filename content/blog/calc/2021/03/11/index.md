---
title: 今日の計算(134)
date: "2021-03-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.25

$\bm{Question.}$
6次正方行列
$$
A = \begin{pmatrix}
    1 & 1/2 & 1/4 & 1/8 & 1/16 & 1/32 \\
    1/2 & 1 & 1/2 & 1/4 & 1/8 & 1/16 \\
    1/4 & 1/2 & 1 & 1/2 & 1/4 & 1/8 \\
    1/8 & 1/4 & 1/2 & 1 & 1/2 & 1/4 \\
    1/16 & 1/8 & 1/4 & 1/2 & 1 & 1/2 \\
    1/32 & 1/16 & 1/8 & 1/4 & 1/2 & 1
\end{pmatrix}
$$
の逆行列を求めよ. 

$\bm{Answer.}$
$$
A^{-1} = \frac{1}{3}\begin{pmatrix}
    4 & -2 & 0 & 0 & 0 & 0 \\
    -2 & 5 & -2 & 0 & 0 & 0 \\
    0 & -2 & 5 & -2 & 0 & 0 \\
    0 & 0 & -2 & 5 & -2 & 0 \\
    0 & 0 & 0 & -2 & 5 & -2 \\
    0 & 0 & 0 & 0 & -2 & 4
\end{pmatrix}. 
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)