---
title: 今日の計算(135)
date: "2021-03-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.26

$\bm{Question.}$
2つの直交行列の積は直交行列であることを示せ. 

$\bm{Proof.}$
2つの直交行列$U_1$と$U_2$の積は
$$
^t(U_1U_2)U_1U_2 =\ ^tU_2^tU_1U_1U_2 =\ ^tU_2U_2 = 1
$$
となり, 直交行列であることが示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)