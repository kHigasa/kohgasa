---
title: 今日の計算(147)
date: "2021-03-24T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.38

$\bm{Question.}$
任意のUnitary行列の逆行列はUnitaryであることを示せ. 

$\bm{Proof.}$
あるUnitary行列を$U$とすれば$U^{\dagger}U=\bm{1}$が成り立っており, $U^{-1}=U^{\dagger}$である. $U^{\dagger}$は勿論Unitary行列であるので, $U^{-1}$はUnitary行列である. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)