---
title: 今日の計算(136)
date: "2021-03-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.27

$\bm{Question.}$
$A$が直交行列であるとき, その行列式が$\pm 1$となることを示せ. 

$\bm{Proof.}$
$\mathrm{det}(^tAA)=1$と$\mathrm{det}\ ^tA=\mathrm{det}A$より, $(\mathrm{det}A)^2=1$となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)