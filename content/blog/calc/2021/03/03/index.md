---
title: 今日の計算(126)
date: "2021-03-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.17

$\bm{Question.}$
角運動量行列は交換関係
$$
[M_i,M_j] = \mathrm{i}M_k\text{ , }i, j, k\text{はサイクリックに変わる}
$$
を満たす. 角運動量行列それぞれのトレースがゼロになることを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \mathrm{i}\ \mathrm{Tr}(M_k) &= \mathrm{Tr}([M_i,M_j]) \\
    &= \mathrm{Tr}(M_iM_j)-\mathrm{Tr}(M_jM_i) \\
    &= 0\ \because\text{一般に, }\mathrm{Tr}(AB)=\mathrm{Tr}(BA)
\end{aligned}
$$
より示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)