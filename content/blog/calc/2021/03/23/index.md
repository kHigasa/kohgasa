---
title: 今日の計算(146)
date: "2021-03-23T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.37

$\bm{Question.}$
$A$と$B$がHermite行列であるとき, $AB$がHermite行列であるための必要十分条件を求めよ. 

$\bm{Answer.}$
$AB$がHermite行列である $\Leftrightarrow$ $AB=(AB)^{\dagger}=B^{\dagger}A^{\dagger}=BA$. したがって$[A,B]=0$が求める条件である. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)