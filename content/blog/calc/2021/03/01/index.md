---
title: 今日の計算(124)
date: "2021-03-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.15

$\bm{Question.}$
$A$と$B$が対角行列であるとき, 行列$A$と$B$は交換することを示せ. 

$\bm{Proof.}$
$AB=BA=\mathrm{diag}(A_{11}B_{11},A_{22}B_{22},\ldots ,A_{nn}B_{nn})$となることは明らか. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)