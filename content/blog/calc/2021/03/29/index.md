---
title: 今日の計算(152)
date: "2021-03-29T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.43
$g_{\mu\nu}=g^{\mu\nu}$を
$$
g_{00}=1\text{, }g_{kk}=-1\ (k=1,2,3)\text{, }g_{\mu\nu}=0\ (\mu\neq\nu)
$$
によって, また$\gamma_{\mu}$を
$$
\gamma_{\mu} = \sum_{\mu=0}^3g_{\mu\nu}\gamma^{\mu}
$$
によって定義する. 次の関係を示せ. 

### (a)
$\bm{Question.}$
$$
\sum_{\mu=0}^3\gamma_{\mu}\gamma^{\alpha}\gamma^{\mu} = -2\gamma^{\alpha}. 
$$

$\bm{Proof.}$
OK. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$$
\sum_{\mu=0}^3\gamma_{\mu}\gamma^{\alpha}\gamma^{\beta}\gamma^{\mu} = 4g^{\alpha\beta}. 
$$

$\bm{Proof.}$
OK. <div class="QED" style="text-align: right">$\Box$</div>

### (c)
$\bm{Question.}$
$$
\sum_{\mu=0}^3\gamma_{\mu}\gamma^{\alpha}\gamma^{\beta}\gamma^{\nu}\gamma^{\mu} = -2\gamma^{\nu}\gamma^{\beta}\gamma^{\alpha}. 
$$

$\bm{Proof.}$
OK. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)