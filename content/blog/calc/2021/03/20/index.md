---
title: 今日の計算(143)
date: "2021-03-20T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.34

$\bm{Question.}$
$A$と$B$がHermite行列であるとき, $(AB+BA)$と$\mathrm{i}(AB-BA)$もまたHermite行列であることを示せ. 

$\bm{Proof.}$
$A=A^{\dagger}$と$B=B^{\dagger}$より, 
$$
\begin{aligned}
    (AB+BA)^{\dagger} &= (B^{\dagger}A^{\dagger}+A^{\dagger}B^{\dagger}) = (BA+AB)\text{, } \\
    \lbrace\mathrm{i}(AB-BA)\rbrace^{\dagger} &= -\mathrm{i}(B^{\dagger}A^{\dagger}-A^{\dagger}B^{\dagger}) = \mathrm{i}(AB-BA)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)