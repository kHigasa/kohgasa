---
title: 今日の計算(151)
date: "2021-03-28T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.42

$\bm{Question.}$
$$
\bm{\gamma}^5 = \begin{pmatrix}
    0 & I_2 \\
    I_2 & 0
\end{pmatrix}
$$
が$\bm{\gamma}^{\mu}\ (\mu=0,1,2,3)$と反交換することを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \bm{\gamma}^0 = \begin{pmatrix}
        I_2 & 0 \\
        0 & -I_2
    \end{pmatrix}\text{, }
    \bm{\gamma}^1 &= \begin{pmatrix}
        0 & 0 & 0 & 1 \\
        0 & 0 & 1 & 0 \\
        0 & -1 & 0 & 0 \\
        -1 & 0 & 0 & 0
    \end{pmatrix}\text{, } \\
    \bm{\gamma}^2 = \begin{pmatrix}
        0 & 0 & 0 & -\mathrm{i} \\
        0 & 0 & \mathrm{i} & 0 \\
        0 & \mathrm{i} & 0 & 0 \\
        -\mathrm{i} & 0 & 0 & 0
    \end{pmatrix}\text{, }
    \bm{\gamma}^3 &= \begin{pmatrix}
        0 & 0 & 1 & 0 \\
        0 & 0 & 0 & -1 \\
        -1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0
    \end{pmatrix}
\end{aligned}
$$
から直接の計算により$\lbrace\bm{\gamma}^5,\bm{\gamma}^{\mu}\rbrace=0\ (\mu=0,1,2,3)$となる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)