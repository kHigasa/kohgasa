---
title: 今日の計算(129)
date: "2021-03-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.20

$\bm{Question.}$
$C_{ij}$を行列$A$の$ji$要素の余因子行列として, 行列$B$の$ij$要素が
$$
B_{ij} = \frac{C_{ji}}{|A|}
$$
であるとき, 
$$
BA = 1
$$
を示せ. 

$\bm{Proof.}$
$A$が正則行列であれば, 要素$B_{ij}$を持つ行列$B$は逆行列となるので示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)