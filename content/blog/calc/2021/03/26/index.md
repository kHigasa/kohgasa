---
title: 今日の計算(149)
date: "2021-03-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.40

$\bm{Question.}$
$\sigma_i$をPauli行列として$\bm{\sigma}=\sigma_1\bm{e}_1+\sigma_2\bm{e}_2+\sigma_3\bm{e}_3$というベクトルを定義し, $\bm{p}$が任意の2次元ベクトルであるとき, 
$$
(\bm{\sigma}\cdot\bm{p})^2=\bm{p}^2\bm{1}_2
$$
を示せ. 

$\bm{Proof.}$
$\bm{p}=p_1\bm{e}_1+p_2\bm{e}_2+p_3\bm{e}_3$とおくと, 
$$
\begin{aligned}
    (\bm{\sigma}\cdot\bm{p})^2 &= (\sigma_1p_1+\sigma_2p_2+\sigma_3p_3)^2 \\
    &= \sigma_1^2p_1^2+\sigma_2^2p_2^2+\sigma_3^2p_3^2+(\sigma_1\sigma_2+\sigma_2\sigma_1)p_1p_2+(\sigma_2\sigma_3+\sigma_3\sigma_2)p_2p_3+(\sigma_3\sigma_1+\sigma_1\sigma_3)p_3p_1 \\
    &= \sigma_1^2p_1^2+\sigma_2^2p_2^2+\sigma_3^2p_3^2 \\
    &= \bm{p}^2\bm{1}_2
\end{aligned}
$$
示された. <div class="QED" style="text-align: right">$\Box$</div>

### コメント
非公式解答[^2]は展開部分に誤りがある. 

### References

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)