---
title: 今日の計算(138)
date: "2021-03-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.29

$\bm{Question.}$
2次正方形の直交行列$A$の一般形を示せ. 

$\bm{Answer.}$
$$
A = \begin{pmatrix}
    a & b \\
    c & d
\end{pmatrix}
$$
とおく. $^tA=A^{-1}$, $|A|=1$より, $a=d$, $b=-c$であり, $|A|=a^2+b^2=1$となるから, 一般形は
$$
A = \begin{pmatrix}
    a & b \\
    -b & a
\end{pmatrix}
= \begin{pmatrix}
    \cos\theta & \sin\theta \\
    -\sin\theta & \cos\theta
\end{pmatrix}
$$
となる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)