---
title: 今日の計算(139)
date: "2021-03-16T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.30

$\bm{Question.}$
$$
\mathrm{det}(A^*) = (\mathrm{det}\ A)^* = \mathrm{det}(A^{\dagger})
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \mathrm{det}(A^{\dagger}) = \mathrm{det}(A^*) &= \sum_{\sigma\in S_n}\left(\mathrm{sgn}\ \sigma\cdot\prod_{i=1}^na_{i\sigma(i)}^*\right) \\
    &= \left\lbrace\sum_{\sigma\in S_n}\left(\mathrm{sgn}\ \sigma\cdot\prod_{i=1}^na_{i\sigma(i)}\right)\right\rbrace^* = (\mathrm{det}\ A)^*
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)