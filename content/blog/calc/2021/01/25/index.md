---
title: 今日の計算(90)
date: "2021-01-25T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.10

$\bm{Question.}$
$$
\int_0^{\infty}\left(\frac{\arctan x}{x}\right)^2\mathrm{d}x = \pi\log 2
$$
を示せ. 

$\bm{Proof.}$
部分分数分解して, 
$$
\begin{aligned}
    \int_0^{\infty}\left(\frac{\arctan x}{x}\right)^2\mathrm{d}x &= \left[-\frac{\arctan^2 x}{x}\right]_0^{\infty}+\int_0^{\infty}\frac{2\arctan x}{x(1+x^2)}\mathrm{d}x \\
    &= 2\int_0^{\infty}\frac{\arctan x}{x(1+x^2)}\mathrm{d}x
\end{aligned}
$$
を得る. 実数定数$a>0$を用いて, 
$$
I(a) = 2\int_0^{\infty}\frac{\arctan ax}{x(1+x^2)}\mathrm{d}x
$$
とおくと, 元々の積分は$\displaystyle\lim_{a\to 1}I(a)$と書き改められる. これを$a$について微分して計算を進めると, 
$$
\begin{aligned}
    I'(a) &= 2\int_0^{\infty}\frac{\mathrm{d}x}{(1+x^2)(1+a^2x^2)} \\
    &= \frac{2}{1-a^2}\int_0^{\infty}\left(\frac{1}{1+x^2}-\frac{a^2}{1+a^2x^2}\right)\mathrm{d}x \\
    &= \frac{2}{1-a^2}\left[\arctan x-a\arctan ax\right]_0^{\infty} \\
    &= \frac{2}{1-a^2}\left(\frac{\pi}{2}-\frac{\pi a}{2}\right) \\
    &= \frac{\pi(1-a)}{1-a^2} = \frac{\pi}{1+a}
\end{aligned}
$$
となる. これを$a$について積分すると, 積分の下限を$c\in\mathbb{R}$にとって, 
$$
\begin{aligned}
    I(a) &= \int_c^aI'(a')\mathrm{d}a' \\
    &= \pi\int_c^a\frac{\mathrm{d}a'}{1+a'} \\
    &= \pi\log(1+a)+\text{const.}
\end{aligned}
$$
を得る. ここで$c=0$に選べば, $\text{const.}=0$となり, 
$$
\int_0^{\infty}\left(\frac{\arctan x}{x}\right)^2\mathrm{d}x = \lim_{a\to 1}\pi\log(1+a) = \pi\log 2
$$
を得る. 以上により題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### コメント
部分分数分解後, 被積分函数の分母の$x$が邪魔であることより生まれる発想. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)