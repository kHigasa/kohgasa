---
title: 今日の計算(72)
date: "2021-01-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.5

$\bm{Question.}$
三角関数と双曲線関数が複素引数に対して適切な冪級数によって定義されているとして, 
$$
\begin{aligned}
    \mathrm{i}\sin z &= \sinh\mathrm{i}z\ ,\ \sin\mathrm{i}z=\mathrm{i}\sinh z \\
    \cos z &= \cosh\mathrm{i}z\ ,\ \cos\mathrm{i}z=\cosh z
\end{aligned}
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \sinh\mathrm{i}z &= \sum_{n=0}^{\infty}\frac{(\mathrm{i}z)^{2n+1}}{(2n+1)!} \\
    &= \mathrm{i}\sum_{n=0}^{\infty}(-1)^n\frac{z^{2n+1}}{(2n+1)!} = \mathrm{i}\sin z \\
    \sin\mathrm{i}z &= \sum_{n=0}^{\infty}(-1)^n\frac{(\mathrm{i}z)^{2n+1}}{(2n+1)!} \\
    &= \mathrm{i}\sum_{n=0}^{\infty}(-1)^{2n}\frac{z^{2n+1}}{(2n+1)!} \\
    &= \mathrm{i}\sum_{n=0}^{\infty}\frac{z^{2n+1}}{(2n+1)!} = \mathrm{i}\sinh z \\
    \cosh\mathrm{i}z &= \sum_{n=0}^{\infty}\frac{(\mathrm{i}z)^{2n}}{(2n)!} \\
    &= \sum_{n=0}^{\infty}(-1)^n\frac{z^{2n}}{(2n)!} = \cos z \\
    \cos\mathrm{i}z &= \sum_{n=0}^{\infty}(-1)^n\frac{(\mathrm{i}z)^{2n}}{(2n)!} \\
    &= \sum_{n=0}^{\infty}(-1)^{2n}\frac{z^{2n}}{(2n)!} \\
    &= \sum_{n=0}^{\infty}\frac{z^{2n}}{(2n)!} = \cosh z \\
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)