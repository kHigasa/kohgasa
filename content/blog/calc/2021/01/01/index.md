---
title: 今日の計算(66)
date: "2021-01-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.10

$\bm{Question.}$
cosine定理を示せ. 

$\bm{Proof.}$
三角形ABCの各辺に一般的な名前付けをして, ベクトル$\bm{B}$と$\bm{C}$の成す角を$\theta$とすれば, 
$$
A^2 = \bm{A}^2 = (\bm{B}-\bm{C})^2 = B^2+C^2-2BC\cos\theta
$$
となり, $B^2$,$C^2$に対しても同様にすればよいので示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)