---
title: 今日の計算(78)
date: "2021-01-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.11

$\bm{Question.}$
以下の全ての値に対する極形式を求めよ. 

### (a) $(1+\mathrm{i})^3$

$\bm{Answer.}$
$$
\sqrt{2}^3\mathrm{e}^{\mathrm{i}3\pi/4}\text{ .}
$$

### (b) $(-1)^{1/5}$

$\bm{Answer.}$
$$
\mathrm{e}^{-\mathrm{i}\pi/5}\text{ and }\mathrm{e}^{-\mathrm{i}3\pi/5}\text{ and }\mathrm{e}^{-\mathrm{i}\pi}\text{ and }\mathrm{e}^{-\mathrm{i}7\pi/5}\text{ and }\mathrm{e}^{-\mathrm{i}9\pi/5}\text{ .}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)