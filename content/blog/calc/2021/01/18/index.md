---
title: 今日の計算(83)
date: "2021-01-18T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.3

$\bm{Question.}$
$$
\int_0^{\infty}\frac{\mathrm{d}x}{\cosh x}\text{ .}
$$

$\bm{Answer.}$
$$
\begin{aligned}
    \int_0^{\infty}\frac{\mathrm{d}x}{\cosh x} &= \int_0^{\infty}\frac{2}{\mathrm{e}^x+\mathrm{e}^{-x}}\mathrm{d}x \\
    &= 2\int_0^{\infty}\frac{\mathrm{e}^{-x}}{1+\mathrm{e}^{-2x}}\mathrm{d}x \\
    \mathrm{e}^{-x}&=y\text{と置換} \\
    &= 2\int_1^0\frac{-\mathrm{d}u}{1+y^2} \\
    &= 2\int_0^1\frac{\mathrm{d}y}{1+y^2} \\
    &= 2\left[\arctan y\right]_0^1 \\
    &= 2\left(\frac{\pi}{4}-0\right) = \frac{\pi}{2}\text{ .}
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)