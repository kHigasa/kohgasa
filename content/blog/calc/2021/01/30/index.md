---
title: 今日の計算(94)
date: "2021-01-30T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.3

$\bm{Question.}$
級数和に関するFejerの方法は函数
$$
\delta_n(t) = \frac{1}{2\pi n}\left(\frac{\sin(nt/2)}{\sin(t/2)}\right)^2
$$
に関連している. 
$$
\lim_{n\to\infty}\int_{-\infty}^{\infty}f(t)\delta_n(t)\mathrm{d}t = f(0)
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \lim_{n\to\infty}\int_{-\infty}^{\infty}f(t)\delta_n(t)\mathrm{d}t &= \lim_{n\to\infty}\int_{-\infty}^{\infty}f(t)\frac{1}{2\pi n}\left(\frac{\sin(nt/2)}{\sin(t/2)}\right)^2\mathrm{d}t \\
    &= \lim_{n\to\infty}\int_{-\infty}^{\infty}f(t)\frac{1}{2\pi n}\frac{1-\cos nt}{1-\cos t}\mathrm{d}t \\
    nt&=t'\text{と置換} \\
    &= \lim_{n\to\infty}\frac{1}{2\pi n}\int_{-\infty}^{\infty}f\left(\frac{t'}{n}\right)\frac{1-\cos t'}{1-\cos(t'/n)}\frac{\mathrm{d}t'}{n} \\
    &\approx \lim_{n\to\infty}\frac{1}{2\pi n}\int_{-\infty}^{\infty}f\left(\frac{t'}{n}\right)\frac{t'}{t'/n}\frac{\mathrm{d}t'}{n} \\
    &= \lim_{n\to\infty}\frac{1}{2\pi n}\int_{-\infty}^{\infty}f\left(\frac{t'}{n}\right)\mathrm{d}t' \\
    &= \frac{1}{2\pi}\int_{-\infty}^{\infty}f(t)\mathrm{d}t \\
    &= F(k=0) = F(0)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)