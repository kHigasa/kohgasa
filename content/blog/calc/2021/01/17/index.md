---
title: 今日の計算(82)
date: "2021-01-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.2

$\bm{Question.}$
$$
\int_0^{\infty}\frac{\sin x}{x}\mathrm{d}x
$$

$\bm{Answer.}$
$\mathrm{e}^{-ax}(a\geq 0)$を掛けたものを$I(a)$とおく, 
$$
I(a) = \int_0^{\infty}\mathrm{e}^{-ax}\frac{\sin x}{x}\mathrm{d}x\text{ .}
$$
これを$a$について微分しそれを$x$について積分すると, 
$$
\begin{aligned}
    I'(a) &= -\int_0^{\infty}\mathrm{e}^{-ax}\sin x\mathrm{d}x \\
    &= -\left(\left[-\frac{\mathrm{e}^{-ax}}{a}\sin x\right]_0^{\infty}+\int_0^{\infty}\frac{\mathrm{e}^{-ax}}{a}\cos x\mathrm{d}x\right) \\
    &= -\frac{1}{a}\left(\left[-\frac{\mathrm{e}^{-ax}}{a}\cos x\right]-\int_0^{\infty}\frac{\mathrm{e}^{-ax}}{a}\sin x\mathrm{d}x\right) \\
    &= -\frac{1}{a}\left(\frac{1}{a}+\frac{1}{a}I'(a)\right) \\
    \therefore\ I'(a) &= -\frac{1}{a^2+1}
\end{aligned}
$$
となる. これを$a$について積分すると, 
$$
I(a) = -\int_c^a\frac{1}{a'^2+1}\mathrm{d}a' = \int_a^c\frac{1}{a'^2+1}\mathrm{d}a' = -\arctan a+\arctan c
$$
を得, $a\to 0$の極限を取り, 積分の上端を$c=\infty$に取ることで, 
$$
\begin{aligned}
    \lim_{a\to 0}I(a) = -\arctan 0+\arctan\infty = \frac{\pi}{2} = \int_0^{\infty}\frac{\sin x}{x}\mathrm{d}x
\end{aligned}
$$
を得る. 

### コメント
この計算手続きは間違っていないが, 正しくない気がするので嫌いだ. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)