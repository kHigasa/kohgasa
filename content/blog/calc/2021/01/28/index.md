---
title: 今日の計算(92)
date: "2021-01-28T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.1

$\bm{Question.}$
$$
\delta_n(x) = \begin{cases}
    0 &\text{when }x<-\displaystyle\frac{1}{2n} \\
    n &\text{when }-\displaystyle\frac{1}{2n}<x<\frac{1}{2n} \\
    0 &\text{when }x>\displaystyle\frac{1}{2n}
\end{cases}
$$
とおいて, $f(x)$が$x=0$で連続であることを仮定して, 
$$
\lim_{n\to\infty}\int_{-\infty}^{\infty}f(x)\delta_n(x)\mathrm{d}x = f(0)
$$
が成り立つことを示せ. 

$\bm{Proof.}$
積分は領域$-1/2n<x<1/2n$でのみ有効で, $n\to\infty$を取るために, 微小区間の積分と考え積分を長方形の面積$f(x)/n$に近似すると, 
$$
\lim_{n\to\infty}\int_{-1/2n}^{1/2n}nf(x)\mathrm{d}x = \lim_{n\to\infty}n\cdot\frac{f(x)}{n} = \lim_{n\to\infty}f(x) = f(0)
$$
を得る. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)