---
title: 今日の計算(76)
date: "2021-01-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.9

$\bm{Question.}$
冪級数展開を比較することによって, 
$$
\arctan x = \frac{\mathrm{i}}{2}\log\left(\frac{1-\mathrm{i}x}{1+\mathrm{i}x}\right)
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \arctan x &= x-\frac{x^3}{3}+\frac{x^5}{5}-\cdots \\
    \log(1-\mathrm{i}x) &= -\mathrm{i}x-\frac{(\mathrm{i}x)^2}{2}-\frac{(\mathrm{i}x)^3}{3}-\cdots \\
    \log(1+\mathrm{i}x) &= \mathrm{i}x-\frac{(\mathrm{i}x)^2}{2}+\frac{(\mathrm{i}x)^3}{3}-\cdots
\end{aligned}
$$
と展開されるので, 
$$
\begin{aligned}
    \text{RHS} &= \frac{\mathrm{i}}{2}\left\lbrace\left(-\mathrm{i}x-\frac{(\mathrm{i}x)^2}{2}-\frac{(\mathrm{i}x)^3}{3}-\cdots\right)-\left(\mathrm{i}x-\frac{(\mathrm{i}x)^2}{2}+\frac{(\mathrm{i}x)^3}{3}-\cdots\right)\right\rbrace \\
    &= \frac{\mathrm{i}}{2}\cdot(-2\mathrm{i})\left(x-\frac{x^3}{3}+\frac{x^5}{5}-\cdots\right) \\
    &= x-\frac{x^3}{3}+\frac{x^5}{5}-\cdots = \text{LHS}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)