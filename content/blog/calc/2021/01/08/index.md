---
title: 今日の計算(73)
date: "2021-01-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.6

$\bm{Question.}$
次を示せ. 

### (a)

$$
\begin{aligned}
    \sin(x+\mathrm{i}y) &= \sin x\cosh y+\mathrm{i}\cos x\sinh y \\
    \cos(x+\mathrm{i}y) &= \cos x\cosh y-\mathrm{i}\sin x\sinh y
\end{aligned}
$$

$\bm{Proof.}$
問1.8.5[^1]の結果を用いて, 
$$
\begin{aligned}
    \sin x\cosh y+\mathrm{i}\cos x\sinh y &= \sin x\cos\mathrm{i}y+\cos x\sin\mathrm{i}y \\
    &= \sin(x+\mathrm{i}y) \\
    \cos x\cosh y-\mathrm{i}\sin x\sinh y &= \cos x\cos\mathrm{i}y-\sin x\sin\mathrm{i}y \\
    &= \cos(x+\mathrm{i}y)
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### (b)

$$
\begin{aligned}
    |\sin(x+\mathrm{i}y)|^2 &= \sin^2x+\sinh^2y \\
    |\cos(x+\mathrm{i}y)|^2 &= \cos^2x+\sinh^2y
\end{aligned}
$$

$\bm{Proof.}$
小問[(a)](#a)の結果を用いて, 
$$
\begin{aligned}
    |\sin(x+\mathrm{i}y)|^2 &= (\sin x\cosh y+\mathrm{i}\cos x\sinh y)(\sin x\cosh y-\mathrm{i}\cos x\sinh y) \\
    &= \sin^2x\cosh^2y+\cos^2x\sinh^2y \\
    &= \sin^2x\cosh^2y+(1-\sin^2x)\sinh^2y \\
    &= \sin^2x(\cosh^2y-\sinh^2y)+\sinh^2y \\
    &= \sin^2x+\sinh^2y \\
    |\cos(x+\mathrm{i}y)|^2 &= (\cos x\cosh y-\mathrm{i}\sin x\sinh y)(\cos x\cosh y+\mathrm{i}\sin x\sinh y) \\
    &= \cos^2x\cosh^2y+\sin^2x\sinh^2y \\
    &= \cos^2x\cosh^2y+(1-\cos^2x)\sinh^2y \\
    &= \cos^2x(\cosh^2y-\sinh^2y)+\sinh^2y \\
    &= \cos^2x+\sinh^2y
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)