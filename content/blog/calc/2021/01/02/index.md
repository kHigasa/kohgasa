---
title: 今日の計算(67)
date: "2021-01-02T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.11

$\bm{Question.}$
次の3つのベクトルの中で, 垂直な組と平行な組をそれぞれ求めよ, 
$$
\begin{aligned}
    \bm{P} &= 3\hat{\bm{e}}_x+2\hat{\bm{e}}_y-\hat{\bm{e}}_z \\
    \bm{Q} &= -6\hat{\bm{e}}_x-4\hat{\bm{e}}_y+2\hat{\bm{e}}_z \\
    \bm{R} &= \hat{\bm{e}}_x-2\hat{\bm{e}}_y-\hat{\bm{e}}_z
\end{aligned}
$$

$\bm{Answer.}$
$$
\bm{P}\parallel\bm{Q}\ ,\ \bm{R}\perp\bm{P}\ ,\ \bm{R}\perp\bm{Q}\ .
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)