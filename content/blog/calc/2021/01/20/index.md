---
title: 今日の計算(85)
date: "2021-01-20T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.5

$\bm{Question.}$
$$
\int_{\pi}^{\infty}\frac{\sin x}{x^2}\mathrm{d}x
$$

$\bm{Answer.}$
部分積分して, 
$$
\begin{aligned}
    \int_{\pi}^{\infty}\frac{\sin x}{x^2}\mathrm{d}x &= \left[-\frac{\sin x}{x}\right]_{\pi}^{\infty}+\int_{\pi}^{\infty}\frac{\cos x}{x}\mathrm{d}x \\
    &= \int_{\pi}^{\infty}\frac{\cos x}{x}\mathrm{d}x
\end{aligned}
$$
となる. 被積分関数は可積分でないのでこの積分の値は定義できない. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)