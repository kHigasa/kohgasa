---
title: 今日の計算(84)
date: "2021-01-19T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.4

$\bm{Question.}$
$$
\int_0^{\infty}\frac{\mathrm{d}x}{\mathrm{e}^{ax}+1}\text{ , for }a>0\text{ .}
$$

$\bm{Answer.}$
$$
\begin{aligned}
    \int_0^{\infty}\frac{\mathrm{d}x}{\mathrm{e}^{ax}+1} &= \int_0^{\infty}\frac{\mathrm{e}^{-ax}}{1+\mathrm{e}^{-ax}}\mathrm{d}x \\
    &\mathrm{e}^{-ax}=y\text{と置換} \\
    &= \int_1^0\frac{1}{1+y}\left(\frac{\mathrm{d}y}{-a}\right) \\
    &= \frac{1}{a}\int_0^1\frac{\mathrm{d}y}{y+1} \\
    &= \frac{1}{a}\left[\log|y+1|\right]_0^1 \\
    &= \frac{\log 2}{a}\text{ .}
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)