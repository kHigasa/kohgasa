---
title: 今日の計算(95)
date: "2021-01-31T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.4

$\bm{Question.}$
$$
\delta(a(x-x_1)) = \frac{1}{a}\delta(x-x_1)
$$
を示せ. 

$\bm{Proof.}$
ある函数$f(x)$を掛けて, $[-\infty,\infty]$に渡って積分すると, 
$$
\begin{aligned}
    &\int_{-\infty}^{\infty}f(x)\delta(a(x-x_1))\mathrm{d}x \\
    a&(x-x_1)=y\text{とおく} \\
    = &\frac{1}{a}\int_{-\infty}^{\infty}f\left(\frac{y}{a}+x_1\right)\delta(y)\mathrm{d}y \\
    = &\frac{1}{a}f(x_1) \\
    = &\frac{1}{a}\int_{-\infty}^{\infty}f(x)\delta(x-x_1)\mathrm{d}x \\
    \therefore\ &\delta(a(x-x_1)) = \frac{1}{a}\delta(x-x_1)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)