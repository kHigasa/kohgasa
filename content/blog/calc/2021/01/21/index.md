---
title: 今日の計算(86)
date: "2021-01-21T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.6

$\bm{Question.}$
$$
\int_0^{\infty}\frac{\mathrm{e}^{-x}\sin x}{x}\mathrm{d}x\text{ .}
$$

$\bm{Answer.}$
小問1.10.2[^1]の計算を用いて, 
$$
\text{(与式)} = -\arctan 1+\arctan\infty = -\frac{\pi}{4}+\frac{\pi}{2}=\frac{\pi}{4}
$$
と求められる. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)