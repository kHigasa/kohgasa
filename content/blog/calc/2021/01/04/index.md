---
title: 今日の計算(69)
date: "2021-01-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.2

$\bm{Question.}$
複素数が2乗根を持ち, 2乗根によって複素数平面にマップされることを示せ. また虚数単位$\mathrm{i}$の2乗根を求めよ. 

$\bm{Proof.}$
複素数を極形式で$z=r\mathrm{e}^{\mathrm{i}\theta}\ ,\ r\geq 0\ ,\ 0\leq\theta<2\pi$と表すと, 
$$
\sqrt{z} = \sqrt{r}\mathrm{e}^{\mathrm{i}\theta/2} \in \mathbb{C}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>
また, 
$$
\sqrt{\mathrm{i}} = \begin{cases}
    \mathrm{e}^{\mathrm{i}\pi/4} &= \displaystyle\frac{1+\mathrm{i}}{\sqrt{2}} \\
    \mathrm{e}^{-\mathrm{i}3\pi/4} &= \displaystyle\frac{-1-\mathrm{i}}{\sqrt{2}}
\end{cases}
$$
である. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)