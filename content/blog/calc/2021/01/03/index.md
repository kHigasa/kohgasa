---
title: 今日の計算(68)
date: "2021-01-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.1

$\bm{Question.}$
$x+\mathrm{i}y$の逆数を求める作業を極形式を用いて取り組んで, 結果はCartesian形式で表わせ. 

$\bm{Answer.}$
$x+\mathrm{i}y=r\mathrm{e}^{\mathrm{i}\theta}\ ,\ r=\sqrt{x^2+y^2}\ ,\ 0\leq\theta<2\pi$と表されるから, 
$$
\begin{aligned}
    \frac{1}{x+\mathrm{i}y} &= \frac{\mathrm{e}^{-\mathrm{i}\theta}}{r} \\
    &= \frac{x-\mathrm{i}y}{r^2} \\
    &= \frac{x-\mathrm{i}y}{x^2+y^2}
\end{aligned}
$$
となる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)