---
title: 今日の計算(92)
date: "2021-01-27T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.12

$\bm{Question.}$
$y=1/2$で単位円を2分し, 面積の小さい側の面積$A$を求めよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    A &= 2\int_{1/2}^{1}\sqrt{1-y^2}\mathrm{d}y \\
    y&=\cos\theta\text{と置換} \\
    &= 2\int_{\pi/3}^{0}\sin\theta(-\sin\theta)\mathrm{d}\theta \\
    &= 2\int_0^{\pi/3}\sin^2\theta\mathrm{d}\theta \\
    &= \int_0^{\pi/3}(1-\cos 2\theta)\mathrm{d}\theta \\
    &= \left[\theta-\frac{\sin 2\theta}{2}\right]_0^{\pi/3} \\
    &= \frac{\pi}{3}-\frac{\sqrt{3}}{4}\text{ .}
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)