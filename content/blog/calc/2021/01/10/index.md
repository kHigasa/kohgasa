---
title: 今日の計算(75)
date: "2021-01-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.8

$\bm{Question.}$
次を示せ. 

### (a)

$$
\tanh\frac{x+\mathrm{i}y}{2} = \frac{\sinh x+\mathrm{i}\sin y}{\cosh x+\cos y}
$$

$\bm{Proof.}$
問1.8.7[^1]の結果を用いて, 
$$
\begin{aligned}
    \tanh(x+\mathrm{i}y) &= \frac{\sinh(x+\mathrm{i}y)}{\cosh(x+\mathrm{i}y)} \\
    &= \frac{\sinh x\cos y+\mathrm{i}\cosh x\sin y}{\cosh x\cos y+\mathrm{i}\sinh x\sin y} \\
    &= \frac{(\sinh x\cos y+\mathrm{i}\cosh x\sin y)(\cosh x\cos y-\mathrm{i}\sinh x\sin y)}{\cosh^2x\cos^2y+\sinh^2x\sin^2y} \\
    &= \frac{\sinh x\cosh x(\cos^2y+\sin^2y)+\mathrm{i}\sin y\cos y(\cosh^2x-\sinh^2x)}{\cosh^2x(1-\sin^2y)+\sinh^2x\sin^2y} \\
    &= \frac{\sinh x\cosh x+\mathrm{i}\sin y\cos y}{\cosh^2x-\sin^2y} \\
    &= \frac{1}{2}\frac{\sinh 2x+\mathrm{i}\sin 2y}{\cosh^2x-\sin^2y} \\
    &= \frac{\sinh 2x+\mathrm{i}\sin 2y}{(1+\cosh 2x)-(1-\cos 2y)} \\
    &= \frac{\sinh 2x+\mathrm{i}\sin 2y}{\cosh 2x+\cos 2y} \\
    \therefore\ \tanh\frac{x+\mathrm{i}y}{2} &= \frac{\sinh x+\mathrm{i}\sin y}{\cosh x+\cos y}
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### (b)

$$
\coth\frac{x+\mathrm{i}y}{2} = \frac{\sinh x-\mathrm{i}\sin y}{\cosh x-\cos y}
$$

$\bm{Proof.}$
問[(a)](#a)と同様にして, 
$$
\begin{aligned}
    \coth(x+\mathrm{i}y) &= \frac{\cosh(x+\mathrm{i}y)}{\sinh(x+\mathrm{i}y)} \\
    &= \frac{\cosh x\cos y+\mathrm{i}\sinh x\sin y}{\sinh x\cos y+\mathrm{i}\cosh x\sin y} \\
    &= \frac{(\cosh x\cos y+\mathrm{i}\sinh x\sin y)(\sinh x\cos y-\mathrm{i}\cosh x\sin y)}{\sinh^2x\cos^2y+\cosh^2x\sin^2y} \\
    &= \frac{\sinh x\cosh x(\cos^2y+\sin^2y)-\mathrm{i}\sin y\cos y(\cosh^2x-\sinh^2x)}{\sinh^2x\cos^2y+\cosh^2x(1-\cos^2y)} \\
    &= \frac{\sinh x\cosh x-\mathrm{i}\sin y\cos y}{\cosh^2x-\cos^2y} \\
    &= \frac{1}{2}\frac{\sinh 2x-\mathrm{i}\sin 2y}{\cosh^2x-\cos^2y} \\
    &= \frac{\sinh 2x-\mathrm{i}\sin 2x}{(1+\cosh 2x)-(\cos 2y+1)} \\
    &= \frac{\sinh 2x-\mathrm{i}\sin 2x}{\cosh 2x-\cos 2y} \\
    \therefore\ \coth\frac{x+\mathrm{i}y}{2} &= \frac{\sinh x-\mathrm{i}\sin y}{\cosh x-\cos y}
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### References

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)