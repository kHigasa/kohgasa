---
title: 今日の計算(89)
date: "2021-01-24T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.9

$\bm{Question.}$
$$
\int_0^{\infty}\frac{\mathrm{e}^{-x}}{x+1}\mathrm{d}x\text{ .}
$$

$\bm{Answer.}$
$x+1=y$と置換すると, 
$$
\begin{aligned}
    \text{与式} &= \int_1^{\infty}\frac{\mathrm{e}^{1-y}}{y}\mathrm{d}y \\
    &= \mathrm{e}\int_1^{\infty}\frac{\mathrm{e}^{-y}}{y}\mathrm{d}y \\
    &= eE_1(1)
\end{aligned}
$$
を得る. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)