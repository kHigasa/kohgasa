---
title: 今日の計算(79)
date: "2021-01-14T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.9.1

$\bm{Question.}$
2変数函数のMaclaurin展開を与えよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    f(x,y) = &\sum_{n=0}^{\infty}\sum_{m=0}^{\infty}\frac{1}{n!}x^n\frac{1}{m!}y^m\left(\frac{\partial^n}{\partial x^n}\frac{\partial^m}{\partial y^m}f(x,y)\right)\Bigg|_{x=0,y=0} \\
    = &\sum_{n=0}^{\infty}\sum_{m=0}^{\infty}\frac{1}{(m+n)!}\binom{m+n}{n}x^ny^m\left(\frac{\partial^n}{\partial x^n}\frac{\partial^m}{\partial y^m}f(x,y)\right)\Bigg|_{x=0,y=0} \\
    = &f(0,0)+x\frac{\partial f}{\partial x}\Big|_{x=y=0}+y\frac{\partial f}{\partial y}\Big|_{x=y=0} \\
    &+ \frac{1}{2!}\left\lbrace\binom{2}{0}x^2\frac{\partial^2f}{\partial x^2}\Big|_{x=y=0}+\binom{2}{1}xy\frac{\partial^2f}{\partial xy}\Big|_{x=y=0}+\binom{2}{2}y^2\frac{\partial^2f}{\partial y^2}\Big|_{x=y=0}\right\rbrace \\
    &+\cdots\text{ .}
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)