---
title: 今日の計算(70)
date: "2021-01-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.3

$\bm{Question.}$
次を示せ. 

### (a)

$$
\cos n\theta = \cos^n\theta-\binom{n}{2}\cos^{n-2}\theta\sin^2\theta+\binom{n}{4}\cos^{n-4}\theta\sin^4\theta-\cdots
$$

$\bm{Proof.}$
$$
\begin{aligned}
    \cos n\theta &= \Re(\mathrm{e}^{\mathrm{i}\theta})^n = \Re(\cos\theta+\mathrm{i}\sin\theta)^n \\
    &= \cos^n\theta-\binom{n}{2}\cos^{n-2}\theta\sin^2\theta+\binom{n}{4}\cos^{n-4}\theta\sin^4\theta-\cdots
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### (b)

$$
\sin n\theta = \binom{n}{1}\cos^{n-1}\theta\sin\theta-\binom{n}{3}\cos^{n-3}\theta\sin^3\theta+\cdots
$$

$\bm{Proof.}$
$$
\begin{aligned}
    \sin n\theta &= \Im(\mathrm{e}^{\mathrm{i}\theta})^n = \Im(\cos\theta+\mathrm{i}\sin\theta)^n \\
    &= \binom{n}{1}\cos^{n-1}\theta\sin\theta-\binom{n}{3}\cos^{n-3}\theta\sin^3\theta+\cdots
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)