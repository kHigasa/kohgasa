---
title: 今日の計算(91)
date: "2021-01-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.11

$\bm{Question.}$
$$
\frac{x^2}{a^2}+\frac{y^2}{b^2} = 1
$$
によって定義される楕円の囲む領域の面積$A$を求めよ. 

$\bm{Answer.}$
2次元Descartes座標系の第一象限のみの面積を考えて4倍する. 
$$
\begin{aligned}
    A &= 4\int_0^a\left(\int_0^{b\sqrt{a^2-x^2}/a}\mathrm{d}y\right)\mathrm{d}x \\
    &= \frac{4b}{a}\int_0^a\sqrt{a^2-x^2}\mathrm{d}x \\
    &= \frac{4b}{a}\frac{\pi a^2}{4} \\
    &= \pi ab\text{ .}
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)