---
title: 今日の計算(93)
date: "2021-01-29T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.2

$\bm{Question.}$
$$
\delta_n(x) = \frac{n}{\pi}\frac{1}{1+n^2x^2}
$$
に対して, 
$$
\int_{-\infty}^{\infty}\delta_n(x)\mathrm{d}x = 1
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \int_{-\infty}^{\infty}\delta_n(x)\mathrm{d}x &= \int_{-\infty}^{\infty}\frac{n}{\pi}\frac{1}{1+n^2x^2}\mathrm{d}x \\
    &= \frac{n}{\pi}\left[\frac{\arctan nx}{n}\right]_{-\infty}^{\infty} \\
    &= \frac{1}{\pi}\left(\frac{\pi}{2}-\left(-\frac{\pi}{2}\right)\right) \\
    &= \frac{1}{\pi}\cdot\pi = 1
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)