---
title: 今日の計算(74)
date: "2021-01-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.7

$\bm{Question.}$
次を示せ. 

### (a)

$$
\begin{aligned}
    \sinh(x+\mathrm{i}y) &= \sinh x\cos y+\mathrm{i}\cosh x\sin y \\
    \cosh(x+\mathrm{i}y) &= \cosh x\cos y+\mathrm{i}\sinh x\sin y
\end{aligned}
$$

$\bm{Proof.}$
問1.8.5[^1]の結果を用いて, 
$$
\begin{aligned}
    \sinh(x+\mathrm{i}y) &= -\mathrm{i}\sin\mathrm{i}(x+\mathrm{i}y) \\
    &= -\mathrm{i}(\sin\mathrm{i}x\cos(-y)+\cos\mathrm{i}x\sin(-y)) \\
    &= -\mathrm{i}(\mathrm{i}\sinh x\cos y-\cosh x\sin y) \\
    &= \sinh x\cos y+\mathrm{i}\cosh x\sin y \\
    \cosh(x+\mathrm{i}y) &= \cos\mathrm{i}(x+\mathrm{i}y) \\
    &= \cos\mathrm{i}x\cos(-y)-\sin\mathrm{i}x\sin(-y) \\
    &= \cos\mathrm{i}x\cos y+\sin\mathrm{i}x\sin y \\
    &= \cosh x\cos y+\mathrm{i}\sinh x\sin y
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### (b)

$$
\begin{aligned}
    |\sinh(x+\mathrm{i}y)|^2 &= \sinh^2x+\sin^2y \\
    |\cosh(x+\mathrm{i}y)|^2 &= \cosh^2x-\sin^2y
\end{aligned}
$$

$\bm{Proof.}$
小問[(a)](#a)の結果を用いて, 
$$
\begin{aligned}
    |\sin(x+\mathrm{i}y)|^2 &= (\sinh x\cos y+\mathrm{i}\cosh x\sin y)(\sinh x\cos y-\mathrm{i}\cosh x\sin y) \\
    &= \sinh^2x\cos^2y+\cosh^2x\sin^2y \\
    &= \sinh^2x(1-\sin^2y)+\cosh^2x\sin^2y \\
    &= \sinh^2x+\sin^2y(\cosh^2x-\sinh^2x) \\
    &= \sinh^2x+\sin^2y \\
    |\cos(x+\mathrm{i}y)|^2 &= (\cosh x\cos y+\mathrm{i}\sinh x\sin y)(\cosh x\cos y-\mathrm{i}\sinh x\sin y) \\
    &= \cosh^2x\cos^2y+\sinh^2x\sin^2y \\
    &= \cosh^2x(1-\sin^2y)+\sinh^2x\sin^2y \\
    &= \cosh^2x-(\cosh^2x-\sinh^2x)\sin^2y \\
    &= \cosh^2x-\sin^2y
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### コメント
小問[(b)](#b)はテキスト[^1]における$|\cosh(x+\mathrm{i}y)|^2$の右辺第2項の符号が逆であったため, 修正して証明した. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)