---
title: 今日の計算(81)
date: "2021-01-16T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.1

$\bm{Question.}$
再帰的な方法を用い, 全ての正の整数$n$に対して, $\Gamma(n)=(n-1)!$を示せ. 

$\bm{Proof.}$
部分分数分解により, 
$$
\begin{aligned}
    \Gamma(n) &= \int_0^{\infty}t^{n-1}\mathrm{e}^{-t}\mathrm{d}t \\
    &= \left[\frac{t^n}{n}\mathrm{e}^{-t}\right]_0^{\infty}+\int_0^{\infty}\frac{t^n}{n}\mathrm{e}^{-t}\mathrm{d}t \\
    &= \frac{\Gamma(n+1)}{n}
\end{aligned}
$$
を得る. これを繰り返し用いれば, 
$$
\Gamma(n)=(n-1)\Gamma(n-1)=\cdots=(n-1)!
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)