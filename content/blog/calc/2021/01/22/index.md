---
title: 今日の計算(87)
date: "2021-01-22T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.7

$\bm{Question.}$
$$
\int_0^x\mathrm{erf}(t)\mathrm{d}t= \int_0^x\left(\frac{2}{\sqrt{\pi}}\int_0^t\mathrm{e}^{-u^2}\mathrm{d}u\right)\mathrm{d}t \text{ .}
$$

$\bm{Answer.}$
積分の順序を交換すると, 
$$
\begin{aligned}
    \text{与式} &= \frac{2}{\sqrt{\pi}}\int_0^x\left(\int_u^x\mathrm{e}^{-u^2}\mathrm{d}t\right)\mathrm{d}u \\
    &= \frac{2}{\sqrt{\pi}}\int_0^x(x-u)\mathrm{e}^{-u^2}\mathrm{d}u \\
    &= x\mathrm{erf}(x)-\frac{2}{\sqrt{\pi}}\int_0^xu\mathrm{e}^{-u^2}\mathrm{d}u \\
    &= x\mathrm{erf}(x)+\frac{2}{\sqrt{\pi}}\left[\frac{\mathrm{e}^{-u^2}}{2}\right]_0^x \\
    &= x\mathrm{erf}(x)+\frac{1}{\sqrt{\pi}}(\mathrm{e}^{-u^2}-1)
\end{aligned}
$$
を得る. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)