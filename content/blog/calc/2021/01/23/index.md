---
title: 今日の計算(88)
date: "2021-01-23T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.10.8

$\bm{Question.}$
$$
\int_1^xE_1(t)\mathrm{d}t = \int_1^x\left(\int_t^{\infty}\frac{\mathrm{e}^{-u}}{u}\mathrm{d}u\right)\mathrm{d}t\text{ .}
$$

$\bm{Answer.}$
積分の順序を変更して, 
$$
\begin{aligned}
    \text{与式} &= \int_1^{\infty}\left(\int_1^x\frac{\mathrm{e}^{-u}}{u}\mathrm{d}t\right)\mathrm{d}u \\
    &= \int_1^x\left(\int_1^x\frac{\mathrm{e}^{-u}}{u}\mathrm{d}t\right)\mathrm{d}u + \int_x^{\infty}\left(\int_1^x\frac{\mathrm{e}^{-u}}{u}\mathrm{d}t\right)\mathrm{d}u \\
    &= \int_1^x(u-1)\frac{\mathrm{e}^{-u}}{u}\mathrm{d}u+\int_x^{\infty}(x-1)\frac{\mathrm{e}^{-u}}{u}\mathrm{d}u \\
    &= \int_1^x\mathrm{e}^{-u}\mathrm{d}u-\int_1^x\frac{\mathrm{e}^{-u}}{u}\mathrm{d}u+(x-1)\int_x^{\infty}\frac{\mathrm{e}^{-u}}{u}\mathrm{d}u \\
    &= \mathrm{e}^{-1}-\mathrm{e}^{-x}-\int_1^{\infty}\frac{\mathrm{e}^{-u}}{u}\mathrm{d}u+x\int_x^{\infty}\frac{\mathrm{e}^{-u}}{u}\mathrm{d}u \\
    &= \mathrm{e}^{-1}-\mathrm{e}^{-x}-E_1(1)+xE_1(x)
\end{aligned}
$$
という, 指数積分に対する式が得られた. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)