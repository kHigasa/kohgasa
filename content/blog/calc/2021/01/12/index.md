---
title: 今日の計算(77)
date: "2021-01-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.10

$\bm{Question.}$
以下の全ての値に対するCartesian形式を求めよ. 

### (a) $(-8)^{1/3}$

$\bm{Answer.}$
$$
\begin{aligned}
    2\cdot(-1)&\text{ and }2\cdot\mathrm{e}^{\mathrm{i}\pi/3}\text{ and }2\cdot\mathrm{e}^{-\mathrm{i}\pi/3} \\
    \therefore\ -2&\text{ and }1+\mathrm{i}\sqrt{3}\text{ and }1-\mathrm{i}\sqrt{3}
\end{aligned}
$$

### (b) $\mathrm{i}^{1/4}$

$\bm{Answer.}$
$$
\begin{aligned}
    \mathrm{e}^{\mathrm{i}\pi/8}&\text{ and }\mathrm{e}^{\mathrm{i}5\pi/8}\text{ and }\mathrm{e}^{-\mathrm{i}3\pi/8}\text{ and }\mathrm{e}^{-\mathrm{i}7\pi/8} \\
    \therefore\ \frac{\sqrt{2+\sqrt{2}}}{2}+\mathrm{i}\frac{\sqrt{2-\sqrt{2}}}{2}&\text{ and }\frac{\sqrt{2-\sqrt{2}}}{2}+\mathrm{i}\frac{\sqrt{2+\sqrt{2}}}{2} \\
    \text{ and }\frac{\sqrt{2-\sqrt{2}}}{2}-\mathrm{i}\frac{\sqrt{2+\sqrt{2}}}{2}&\text{ and }-\frac{\sqrt{2+\sqrt{2}}}{2}-\mathrm{i}\frac{\sqrt{2-\sqrt{2}}}{2}
\end{aligned}
$$

### (c) $\mathrm{e}^{\mathrm{i}\pi/4}$

$\bm{Answer.}$
$$
\frac{1+\mathrm{i}}{\sqrt{2}}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)