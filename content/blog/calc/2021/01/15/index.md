---
title: 今日の計算(80)
date: "2021-01-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.9.2

$\bm{Question.}$
$m$変数函数のMaclaurin展開を与えよ. 

$\bm{Answer.}$
$$
    f(x,y) = \sum_{n_1=0}^{\infty}\cdots\sum_{n_m=0}^{\infty}\frac{1}{n_1!}x_1^{n_1}\cdots\frac{1}{n_m!}x_m^{n_m}\left(\frac{\partial^{n_1}}{\partial x_1^{n_1}}\cdots\frac{\partial^{n_m}}{\partial x_m^{n_m}}f(x_1,\ldots,x_m)\right)\Bigg|_{x_1=0,\ldots,x_m=0}\text{ .}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)