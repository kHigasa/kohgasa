---
title: 今日の計算(71)
date: "2021-01-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.8.4

$\bm{Question.}$
次を示せ. 

### (a)

$$
\sum_{n=0}^{N-1}\cos nx = \frac{\sin(Nx/2)}{\sin(x/2)}\cos\frac{(N-1)x}{2}
$$

$\bm{Proof.}$
$$
\begin{aligned}
    \Re\sum_{n=0}^{N-1}(\mathrm{e}^{\mathrm{i}x})^n &= \Re\frac{1-\mathrm{e}^{\mathrm{i}Nx}}{1-\mathrm{e}^{\mathrm{i}x}} \\
    &= \Re\frac{\mathrm{e}^{-\mathrm{i}(Nx/2)}-\mathrm{e}^{\mathrm{i}(Nx/2)}}{\mathrm{e}^{-\mathrm{i}(x/2)}-\mathrm{e}^{\mathrm{i}(x/2)}}\frac{\mathrm{e}^{\mathrm{i}(Nx/2)}}{\mathrm{e}^{\mathrm{i}(x/2)}} \\
    &= \frac{\sin(Nx/2)}{\sin(x/2)}\cos\frac{(N-1)x}{2}
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### (b)

$$
\sum_{n=0}^{N-1}\sin nx = \frac{\sin(Nx/2)}{\sin(x/2)}\sin\frac{(N-1)x}{2}
$$

$\bm{Proof.}$
$$
\begin{aligned}
    \Im\sum_{n=0}^{N-1}(\mathrm{e}^{\mathrm{i}x})^n &= \Im\frac{1-\mathrm{e}^{\mathrm{i}Nx}}{1-\mathrm{e}^{\mathrm{i}x}} \\
    &= \Im\frac{\mathrm{e}^{-\mathrm{i}(Nx/2)}-\mathrm{e}^{\mathrm{i}(Nx/2)}}{\mathrm{e}^{-\mathrm{i}(x/2)}-\mathrm{e}^{\mathrm{i}(x/2)}}\frac{\mathrm{e}^{\mathrm{i}(Nx/2)}}{\mathrm{e}^{\mathrm{i}(x/2)}} \\
    &= \frac{\sin(Nx/2)}{\sin(x/2)}\sin\frac{(N-1)x}{2}
\end{aligned}
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)