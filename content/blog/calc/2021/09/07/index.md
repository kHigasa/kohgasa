---
title: 今日の計算(312)
date: "2021-09-07T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.4.1

$u$と$v$は微分可能な関数とする. 

### (a)
$\bm{Question.}$
$u(x,y,z)$と$v(x,y,z)$がある関数$f(u,v)=0$によって関係づけられることが$(\nabla u)\times(\nabla v)=0$であることの必要十分条件であることを示せ. 

$\bm{Proof.}$
ある関数$f(u,v)$の勾配を取ると, 
$$
\nabla f(u,v) = \frac{\partial f}{\partial u}\nabla u+\frac{\partial f}{\partial v}\nabla v = 0
$$
となり, $\nabla u$と$\nabla v$が平行と判るので$(\nabla u)\times(\nabla v)=0$となり必要条件であることが示された. 逆に, $(\nabla u)\times(\nabla v)=0$であれば, 議論をそのまま逆にたどることで十分条件も示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$u(x,y)$と$v(x,y)$に対して, 条件$(\nabla u)\times(\nabla v)=0$により二次元Jacobianが
$$
J = \frac{\partial(u,v)}{\partial(x,y)} = \begin{vmatrix}
    \frac{\partial{u}}{\partial{x}} & \frac{\partial{u}}{\partial{y}} \\
    \frac{\partial{v}}{\partial{x}} & \frac{\partial{v}}{\partial{y}}
\end{vmatrix} = 0
$$
となることを示せ. 

$\bm{Proof.}$
そのまま. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)