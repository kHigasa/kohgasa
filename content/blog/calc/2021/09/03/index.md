---
title: 今日の計算(308)
date: "2021-09-03T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.3.9

$\bm{Question.}$
前問の$\Gamma^k_{ij}$を用いて, 回転円筒座標系におけるベクトル$\bm{V}$の共変微分$V^i_{;j}$を書き下せ. 

$\bm{Answer.}$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)