---
title: 今日の計算(309)
date: "2021-09-04T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.3.10

$\bm{Question.}$
計量テンソルに対して, 
$$
g_{ij;k} = g^{ij}_{;k} = 0
$$
を示せ. 

$\bm{Proof.}$
. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)