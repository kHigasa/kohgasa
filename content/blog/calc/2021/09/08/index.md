---
title: 今日の計算(313)
date: "2021-09-08T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.4.2

$\bm{Question.}$
二次元直交座標系の座標が$q_1$と$q_2$によって与えられているとき, Jacobianが恒等式
$$
J\equiv\frac{\partial(x,y)}{\partial(q_1,q_2)} = \frac{\partial x}{\partial q_1}\frac{\partial y}{\partial q_2}-\frac{\partial x}{\partial q_2}\frac{\partial y}{\partial q_1} = h_1h_2
$$
を満たすことを示せ. 

$\bm{Proof.}$
そのまま. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)