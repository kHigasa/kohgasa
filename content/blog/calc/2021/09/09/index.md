---
title: 今日の計算(314)
date: "2021-09-09T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.4.3

$\bm{Question.}$
変換$u=x+y, v=x/y(x,y\geq 0)$に対してJacobian$\frac{\partial(x,y)}{\partial(u,v)}$を求めよ. 

### (a)
直接計算せよ. 

$\bm{Answer.}$
$x=uv/(v+1), y=u/(v+1)$より, 
$$
\begin{aligned}
    J &= \begin{vmatrix}
        \frac{v}{v+1} & \frac{u}{(v+1)^2} \\
	\frac{1}{v+1} & \frac{-u}{(v+1)^2}
    \end{vmatrix} \\
    &= -\frac{u}{(v+1)^2}
\end{aligned}
$$
と求まる. 

### (b)
$J^{-1}$を求め, それを使い計算せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    J^{-1} &= \begin{vmatrix}
        1 & 1 \\
	\frac{1}{y} & \frac{-x}{y^2}
    \end{vmatrix} \\
    &= -\frac{x+y}{y^2} \\
    \therefore\ J &= -\frac{y^2}{x+y} \\
    &= -\frac{u}{(v+1)^2}
\end{aligned}
$$
と求まる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)