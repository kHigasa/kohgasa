---
title: 今日の計算(306)
date: "2021-09-01T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.3.7

$\bm{Question.}$
$$
\frac{\partial V_i}{\partial q^j}-V_k\Gamma^k_{ij} = g_{ik}\left[\frac{\partial V^k}{\partial q^j}+V^m\Gamma^k_{mj}\right]
$$
を示すことにより
$$
V_{i;j} = g_{ik}V^k_{ij}
$$
を確かめよ. 

$\bm{Answer.}$
. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)