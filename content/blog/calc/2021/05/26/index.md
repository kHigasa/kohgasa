---
title: 今日の計算(210)
date: "2021-05-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.12

$\bm{Question.}$
流体力学のNavier-Stokes方程式の含む非線形項$(\bm{v}`\cdot\nabla)\bm{v}$の回転が$-\nabla\times\lbrace\bm{v}\times(\nabla\times\bm{v})\rbrace$と書き直せることを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    -\nabla\times\lbrace\bm{v}\times(\nabla\times\bm{v})\rbrace &= -\nabla\times\left\lbrace\frac{1}{2}\nabla(\bm{v}^2)-(\bm{v}\cdot\nabla)\bm{v}\right\rbrace \\
    &= \nabla\times(\bm{v}\cdot\nabla)\bm{v}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)