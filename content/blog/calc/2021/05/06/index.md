---
title: 今日の計算(190)
date: "2021-05-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.5

$\bm{Question.}$
スカラー関数$u$と$v$について, 
$$
\nabla(uv) = v\nabla u+u\nabla v
$$
が成り立つことを示せ. 

$\bm{Proof.}$
各座標についての積の微分則より従う. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)