---
title: 今日の計算(189)
date: "2021-05-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.4

$\bm{Question.}$
ベクトル関数$\bm{F}(x,y,z,t)$の全微分が
$$
\mathrm{d}\bm{F} = (\mathrm{d}\bm{r}\cdot\nabla)\bm{F}+\frac{\partial\bm{F}}{\partial t}\mathrm{d}t
$$
によって与えられることを示せ. 

$\bm{Proof.}$
全微分の定義より, 
$$
\begin{aligned}
    \mathrm{d}\bm{F} &= \frac{\partial\bm{F}}{\partial x}\mathrm{d}x+\frac{\partial\bm{F}}{\partial y}\mathrm{d}y+\frac{\partial\bm{F}}{\partial z}\mathrm{d}z+\frac{\partial\bm{F}}{\partial t}\mathrm{d}t \\
    &= (\mathrm{d}\bm{r}\cdot\nabla)\bm{F}+\frac{\partial\bm{F}}{\partial t}\mathrm{d}t
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)