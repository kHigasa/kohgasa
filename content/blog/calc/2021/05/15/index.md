---
title: 今日の計算(199)
date: "2021-05-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.1

$\bm{Question.}$
ベクトル場$\bm{u}$と$`\bm{v}$が渦なし i.e. $\nabla\times\bm{u}=\bm{0}$であるとき, ベクトル場$\bm{u}\times\bm{v}$が管状 i.e. $\nabla\cdot(\bm{u}\times\bm{v})=0$であることを示せ. 

$\bm{Proof.}$
$$
\nabla\cdot(\bm{u}\times\bm{v}) = \bm{v}\cdot(\nabla\times\bm{u})-\bm{u}\cdot(\nabla\times\bm{v})=0
$$
より示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)