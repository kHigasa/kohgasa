---
title: 今日の計算(213)
date: "2021-05-29T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.15

$\bm{Question.}$
方程式
$$
\nabla\times(\nabla\times\bm{A})-k^2\bm{A}=0
$$
の解$\bm{A}$は, Helmholtzの方程式
$$
\nabla^2\bm{A}+k^2\bm{A}=0
$$
と管状の条件
$$
\nabla\cdot\bm{A}=0
$$
を満たしていることを示せ. 

$\bm{Proof.}$
$$
\nabla\times(\nabla\times\bm{A})-k^2\bm{A}=\nabla(\nabla\cdot\bm{A})-(\nabla\cdot\nabla\bm{A}+k^2\bm{A})=0
$$
の左から$\nabla\cdot$を作用させることにより示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)