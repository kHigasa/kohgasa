---
title: 今日の計算(191)
date: "2021-05-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.6
円軌道$\bm{r}=r(\cos\omega t\hat{\bm{e}}_x+\sin\omega t\hat{\bm{e}}_y)$を運動している粒子がある. 

### (a)
$\bm{Question.}$
$\bm{r}\times\dot{\bm{r}}$を求めよ. 

$\bm{Answer.}$
$$
\bm{r}\times\dot{\bm{r}} = \omega r^2\hat{\bm{e}}_z. 
$$

### (b)
$\bm{Question.}$
$\ddot{\bm{r}}+\omega^2\bm{r}=0$を示せ. 

$\bm{Proof.}$
円運動でしゅから. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)