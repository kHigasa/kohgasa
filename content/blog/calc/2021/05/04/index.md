---
title: 今日の計算(188)
date: "2021-05-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.3

$\bm{Question.}$
ベクトル$\bm{r}_{12}=(x_1-x_2)\hat{\bm{e}}_x+(y_1-y_2)\hat{\bm{e}}_y+(z_1-z_2)\hat{\bm{e}}_z$が与えられたとき, $\nabla_1r_{12}$が$\bm{r}_{12}$方向の単位ベクトルになっていることを示せ. ここで$\nabla_1$は添字$1$のついた座標に関する微分演算子で$r_{12}=|\bm{r}_{12}|$である. 

$\bm{Proof.}$
$r_{12}=\sqrt{(x_1-x_2)^2+(y_1-y_2)^2+(z_1-z_2)^2}$より, 
$$
\nabla_1r_{12} = \frac{\bm{r}_{12}}{r_{12}}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)