---
title: 今日の計算(185)
date: "2021-05-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.4.5

座標系$(x,y,z)$におけるベクトル$\bm{r}$を単位ベクトル$\hat{\bm{n}}$によって定義されるある軸に関して反時計回りに角度$\Phi$だけ回転し, 新たな座標系$(x',y',z')$におけるベクトル$\bm{r}'$とする. このとき
$$
\bm{r}' = \bm{r}\cos\Phi+(\bm{r}\times\hat{\bm{n}})\sin\Phi+\hat{\bm{n}}(\hat{\bm{n}}\cdot\bm{r})(1-\cos\Phi)
$$
とかける. 

### (a)
$\bm{Question.}$
幾何的な考察よりこの式を示せ. 

$\bm{Proof.}$
ベクトル$\bm{r}$, $\hat{\bm{n}}$, $\bm{r}\times\hat{\bm{n}}$によってはられる空間について回転$\Phi$を考えれば確かに上の式を得る. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$\hat{\bm{n}}=\hat{\bm{e}}_z$と選んで回転行列を得よ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \bm{r}' &= (x\cos\Phi+y\sin\Phi)\hat{\bm{e}}_x+(-x\sin\Phi+y\cos\Phi)\hat{\bm{e}}_y+z\hat{\bm{e}}_z \\
    &= \begin{pmatrix}
        x\cos\Phi & -x\sin\Phi & 0 \\
        y\sin\Phi & y\cos\Phi & 0 \\
        0 & 0 & z
    \end{pmatrix}
    \begin{pmatrix}
        \hat{\bm{e}}_x \\
        \hat{\bm{e}}_y \\
        \hat{\bm{e}}_z
    \end{pmatrix}
\end{aligned}. 
$$

### (c)
$\bm{Question.}$
$r'^2=r^2$を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    r'^2 = &r^2\cos^2\Phi+(\bm{r}\times\hat{\bm{n}})^2\sin^2\Phi \\
    &+(\hat{\bm{n}}\cdot\bm{r})^2(1+\cos^2\Phi-2\cos\Phi)+2(\bm{r}\cdot\hat{\bm{n}})^2\cos\Phi(1-\cos\Phi) \\
    = &r^2\cos^2\Phi+\left\lbrace r^2-(\bm{r}\cdot\hat{\bm{n}})^2\right\rbrace\sin^2\Phi+(\hat{\bm{n}}\cdot\bm{r})^2(1-\cos^2\Phi) \\
    = &r^2(\cos^2\Phi+\sin^2\Phi) = r^2
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)