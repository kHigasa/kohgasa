---
title: 今日の計算(194)
date: "2021-05-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.9

$\bm{Question.}$
$$
\nabla\cdot(\bm{a}\times\bm{b}) = \bm{b}\cdot(\nabla\times\bm{a})-\bm{a}\cdot(\nabla\times\bm{b})
$$
を示せ. 

$\bm{Proof.}$
$n$個の成分を持つ一般的なベクトルに対して愚直に計算してまとめるとこうなりますよね. <div class="QED" style="text-align: right">$\Box$</div>

#### コメント
スカラー三重積の性質から証明する方法? をとるときに微分作用素$\nabla$を謎に分解しているのを観るけどあれはなに. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)