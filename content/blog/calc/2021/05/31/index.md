---
title: 今日の計算(215)
date: "2021-05-31T03:00:00.000Z"
description: "手元の雑多な計算集"
draft: true
---

## 3.6.17

$\bm{Question.}$
3つの行列
$$
M_x = \begin{pmatrix}
   0 & 0 & 0 \\
   0 & 0 & -\mathrm{i} \\
   0 & \mathrm{i} & 0
\end{pmatrix}\text{, }
M_y = \begin{pmatrix}
   0 & 0 & \mathrm{i} \\
   0 & 0 & 0 \\
   -\mathrm{i} & 0 & 0
\end{pmatrix}\text{}, 
M_z = \begin{pmatrix}
   0 & -\mathrm{i} & 0 \\
   \mathrm{i} & 0 & 0 \\
   0 & 0 & 0
\end{pmatrix}
$$
と列ベクトル
$$
\bm{\psi} = \begin{pmatrix}
   B_x-\mathrm{i}E_x/c \\
   B_y-\mathrm{i}E_y/c \\
   B_z-\mathrm{i}E_z/c
\end{pmatrix}
$$
が与えられたとき, 
$$
\left(M\cdot\nabla+\bm{1}_3\frac{1}{c}\frac{\partial}{\partial t}\right)\bm{\psi} = 0
$$
が真空中のMaxwell方程式を再現することを確かめよ. 

$\bm{Answer.}$
Faraday則とAmpere則 (変位電流のみ) が得られる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)