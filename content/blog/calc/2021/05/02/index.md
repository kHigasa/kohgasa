---
title: 今日の計算(186)
date: "2021-05-02T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.1
$S(x,y,z)=(x^2+y^2+z^2)^{-3/2}$のとき, 次を求めよ. 

### (a)
$\bm{Question.}$
位置$(1,2,3)$における勾配$\nabla S$. 

$\bm{Answer.}$
勾配
$$
\nabla S = -3(x^2+y^2+z^2)^{-5/2}(x,y,z)
$$
より, $\nabla S(1,2,3)=-3(14)^{-5/2}(1,2,3)$である. 

### (b)
$\bm{Question.}$
位置$(1,2,3)$における勾配の大きさ$\left|\nabla S\right|$. 

$\bm{Answer.}$
$$
\frac{3}{14^2}. 
$$

### (c)
$\bm{Question.}$
位置$(1,2,3)$における勾配$\nabla S$の方向余弦. 

$\bm{Answer.}$
このとき方向余弦は
$$
\begin{aligned}
    (\cos a,\cos b, \cos c) &= \left(\frac{\nabla S\cdot\hat{\bm{e}}_x}{\left|\nabla S\right|}, \frac{\nabla S\cdot\hat{\bm{e}}_y}{\left|\nabla S\right|}, \frac{\nabla S\cdot\hat{\bm{e}}_z}{\left|\nabla S\right|}\right) \\
    &= -\frac{3(x^2+y^2+z^2)^{-5/2}}{\left|\nabla S\right|}(x,y,z)
\end{aligned}
$$
であるので, $(1,2,3)$において
$$
(\cos a,\cos b, \cos c) = -\frac{1}{\sqrt{14}}(1,2,3)
$$
である. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)