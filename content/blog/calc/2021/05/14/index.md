---
title: 今日の計算(198)
date: "2021-05-14T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.13

$\bm{Question.}$
ベクトル$\bm{b}=(b_x,b_y,b_z)=(-y,x,0)$の流線を求めよ. 

$\bm{Answer.}$
流線の方程式より, 
$$
\frac{\mathrm{d}y}{\mathrm{d}x} = \frac{b_y}{b_x}
$$
であるから, 
$$
\begin{aligned}
    \frac{\mathrm{d}y}{\mathrm{d}x} &= \frac{x}{-y} \\
    x\mathrm{d}x+y\mathrm{d}x &= 0 \\
    \frac{x^2}{2}+\frac{y^2}{2} &= \text{const. }
\end{aligned}
$$
という円を描くことがわかり, ベクトル$\bm{b}$の向きより反時計回りに描くことがわかる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)