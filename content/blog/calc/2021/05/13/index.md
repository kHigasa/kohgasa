---
title: 今日の計算(197)
date: "2021-05-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.12

$\bm{Question.}$
2つのベクトル演算子$\bm{a}$, $\bm{b}$と軌道角運動量演算子$\bm{L}$に対して, 交換関係$[\bm{a},\bm{L}]=[\bm{b},\bm{L}](=[\bm{a},\bm{b}])=0$が成り立つとき, 
$$
[\bm{a}\cdot\bm{L},\bm{b}\cdot\bm{L}] = \mathrm{i}(\bm{a}\times\bm{b})\cdot\bm{L}
$$
を示せ. 

$\bm{Proof.}$
$$
a_i[L_i,L_j]b_j = \mathrm{i}\hbar\epsilon_{ijk}a_ib_jL_k
$$
となる. これより題意が示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)