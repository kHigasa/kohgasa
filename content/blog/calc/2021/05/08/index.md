---
title: 今日の計算(192)
date: "2021-05-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.7

$\bm{Question.}$
$\bm{A}$がベクトルであるとき, $\mathrm{d}\bm{A}/\mathrm{d}t$もベクトルであることを示せ. 

$\bm{Proof.}$
位置座標の変換と同様の変換が$\mathrm{d}\bm{A}/\mathrm{d}t$でも同様に成り立つか, という問題であるが, 勿論成り立つ. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)