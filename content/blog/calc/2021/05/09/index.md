---
title: 今日の計算(193)
date: "2021-05-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.8
次を示せ. 

### (a)
$\bm{Question.}$
$$
\frac{\mathrm{d}}{\mathrm{d}t}(\bm{A}\cdot\bm{B}) = \frac{\mathrm{d}\bm{A}}{\mathrm{d}t}\cdot\bm{B}+\bm{A}\cdot\frac{\mathrm{d}\bm{B}}{\mathrm{d}t}. 
$$

$\bm{Proof.}$
積の微分則より従う. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$$
\frac{\mathrm{d}}{\mathrm{d}t}(\bm{A}\times\bm{B}) = \frac{\mathrm{d}\bm{A}}{\mathrm{d}t}\times\bm{B}+\bm{A}\times\frac{\mathrm{d}\bm{B}}{\mathrm{d}t}. 
$$

$\bm{Proof.}$
積の微分則より従う. <div class="QED" style="text-align: right">$\Box$</div>

#### コメント
積の微分則に従うことも定義からすぐわかるのでこれでいいよね. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)