---
title: 今日の計算(201)
date: "2021-05-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.3

$\bm{Question.}$
剛体が一定の角速度$\bm{\omega}$で回転しているとき, 線形速度$\bm{v}$が管状であることを示せ. 

$\bm{Proof.}$
$\bm{v}=\bm{\omega}\times\bm{r}$であるので, 
$$
\nabla\cdot\bm{v} = \nabla\cdot(\bm{\omega}\times\bm{r}) = \bm{r}\cdot(\nabla\times\bm{\omega})-\bm{\omega}\cdot(\nabla\times\bm{r}) = 0
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)