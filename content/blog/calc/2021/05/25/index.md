---
title: 今日の計算(209)
date: "2021-05-25T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.11

$\bm{Question.}$
$\nabla\times\bm{F}=\nabla\times\bm{G}$のとき次を示せ. 

### (a)

$\bm{F}$と$\bm{G}$が定数分だけ異なっても上式が成り立つ. 

$\bm{Proof.}$
付加された定数分は$\nabla\times$を作用させるとゼロになる. <div class="QED" style="text-align: right">$\Box$</div>

### (b)

$\bm{F}$と$\bm{G}$があるスカラー関数$f$の勾配$\nabla f$だけ異なっても上式が成り立つ. 

$\bm{Proof.}$
$\nabla\times(\nabla f)=0$より示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)