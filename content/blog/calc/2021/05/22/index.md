---
title: 今日の計算(206)
date: "2021-05-22T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.8

$\bm{Question.}$
$\bm{A}$, $\bm{B}$が定ベクトルであるとき, 
$$
\nabla(\bm{A}\cdot\bm{B}\times\bm{r}) = \bm{A}\times\bm{B}
$$
を示せ. 

$\bm{Proof.}$
スカラー三重積の性質より, 
$$
\nabla(\bm{A}\cdot(\bm{B}\times\bm{r})) = \nabla((\bm{A}\times\bm{B})\cdot\bm{r}) = \bm{A}\times\bm{B}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)