---
title: 今日の計算(203)
date: "2021-05-19T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.5

$\bm{Question.}$
恒等式
$$
\nabla\times(\bm{A}\times\bm{B}) = (\bm{B}\cdot\nabla)\bm{A}-(\bm{A}\cdot\nabla)\bm{B}-\bm{B}(\nabla\cdot\bm{A})+\bm{A}(\nabla\cdot\bm{B})
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \nabla\times(\bm{A}\times\bm{B}) &= (\nabla\cdot\bm{B})\bm{A}-(\nabla\cdot\bm{A})\bm{B} \\
    &= (\bm{B}\cdot\nabla)\bm{A}-(\bm{A}\cdot\nabla)\bm{B}-\bm{B}(\nabla\cdot\bm{A})+\bm{A}(\nabla\cdot\bm{B})
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)