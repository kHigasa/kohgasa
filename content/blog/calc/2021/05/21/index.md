---
title: 今日の計算(205)
date: "2021-05-21T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.7

$\bm{Question.}$
恒等式
$$
\bm{A}\times(\nabla\times\bm{A}) = \frac{1}{2}\nabla(\bm{A}^2)-(\bm{A}\cdot\nabla)\bm{A}
$$
を示せ. 

$\bm{Proof.}$
ベクトル三重積の展開より
$$
\bm{A}\times(\nabla\times\bm{A}) = \frac{1}{2}\nabla(\bm{A}^2)-(\bm{A}\cdot\nabla)\bm{A}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)