---
title: 今日の計算(204)
date: "2021-05-20T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.6

$\bm{Question.}$
恒等式
$$
\nabla(\bm{A}\cdot\bm{B}) = (\bm{A}\times\nabla)\times\bm{B}+(\bm{B}\times\nabla)\times\bm{A}+\bm{A}(\nabla\cdot\bm{B})+\bm{B}(\nabla\cdot\bm{A})
$$
を示せ. 

$\bm{Proof.}$
3次元成分で具体的に示すしか... <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)