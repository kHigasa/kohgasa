---
title: 今日の計算(187)
date: "2021-05-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.2
### (a)
$\bm{Question.}$
位置$(1,1,1)$で面$x^2+y^2+z^2=3$に垂直な単位ベクトルを求めよ. 

$\bm{Answer.}$
$S(x,y,z)=x^2+y^2+z^2-3=0$とおく. 求める法線ベクトルは
$$
\frac{\nabla S(1,1,1)}{\left|\nabla S(1,1,1)\right|} = \frac{1}{\sqrt{3}}\left(\hat{\bm{e}}_x+\hat{\bm{e}}_y+\hat{\bm{e}}_z\right)
$$
と求まる. 

### (b)
$\bm{Question.}$
位置$(1,1,1)$で面$x^2+y^2+z^2=3$に接する平面を求めよ. 

$\bm{Answer.}$
接平面上の点$(x,y,z)$は$\nabla S(1,1,1)\cdot((x,y,z)-(1,1,1))=0$を満たすので, 
$$
x+y+z=3
$$
が求める平面となる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)