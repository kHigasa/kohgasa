---
title: 今日の計算(196)
date: "2021-05-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.11

$\bm{Question.}$
軌道角運動量演算子$L_x$, $L_y$, $L_z$に対する交換関係を与えよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    [L_x, L_y] &= \mathrm{i}L_z \\
    [L_y, L_z] &= \mathrm{i}L_x \\
    [L_z, L_x] &= \mathrm{i}L_y \\
    \therefore\ \bm{L}\times\bm{L} &= \mathrm{i}\bm{L}. 
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)