---
title: 今日の計算(195)
date: "2021-05-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.5.10

$\bm{Question.}$
古典的な軌道角運動量の定義$\bm{L}=\bm{r}\times\bm{p}$より, $\bm{p}\to-\mathrm{i}\nabla$ (自然単位系) と置き換えて量子力学的な角運動量の3次元直交成分を得よ. 

$\bm{Answer.}$
$$
\bm{L} = -\mathrm{i}\left(y\frac{\partial}{\partial z}-z\frac{\partial}{\partial y}, z\frac{\partial}{\partial x}-x\frac{\partial}{\partial z}, x\frac{\partial}{\partial y}-y\frac{\partial}{\partial x}\right). 
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)