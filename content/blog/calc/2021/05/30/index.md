---
title: 今日の計算(214)
date: "2021-05-30T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.16

$\bm{Question.}$
Laplace方程式$\nabla^2\Phi=0$を満たすポテンシャル$\Phi$に対して, 熱伝導方程式
$$
\nabla^2\Psi-k|\nabla\Phi|^2=0
$$
が成り立つとき, $\Psi=k\Phi^2/2$であることを示せ. 

$\bm{Proof.}$
$\Psi=k\Phi^2/2$を代入して計算してみると, 
$$
\begin{aligned}
    \nabla^2\Psi &= \frac{k}{2}\nabla^2\Phi^2 \\
    &= k(\Phi\nabla^2\Phi+|\nabla\Phi|^2) \\
    &= k|\nabla\Phi|^2\ \because\nabla^2\Phi=0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)