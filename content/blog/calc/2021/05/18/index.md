---
title: 今日の計算(202)
date: "2021-05-18T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.4

$\bm{Question.}$
ベクトル関数$\bm{V}(x,y,z)$が渦ありで, あるスカラー関数$g(x,y,z)$との積$g\bm{V}$は渦なしになるようなとき, 
$$
\bm{V}\cdot\nabla\times\bm{V} = 0
$$
となることを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    0 = \bm{V}\cdot(\nabla\times(g\cdot\bm{V})) &= \bm{V}(g\cdot\nabla\times\bm{V}+\nabla g\times\bm{V}) \\
    &= \bm{V}g\cdot\nabla\times\bm{V} \\
    &= g\bm{V}\cdot\nabla\times\bm{V} \\
    \therefore\ &\bm{V}\cdot\nabla\times\bm{V} = 0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)