---
title: 今日の計算(211)
date: "2021-05-27T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.13

$\bm{Question.}$
$u$と$v$が微分可能なスカラー関数であるとき, 
$$
\nabla\cdot\lbrace(\nabla u)\times(\nabla v)\rbrace = 0
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    &\nabla\cdot\lbrace(\nabla u)\times(\nabla v)\rbrace \\
    = &(\nabla v)\lbrace\nabla\times(\nabla u)\rbrace-(\nabla u)\lbrace\nabla\times(\nabla v)\rbrace \\
    = &0-0=0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)