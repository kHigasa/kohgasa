---
title: 今日の計算(212)
date: "2021-05-28T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.6.14

$\bm{Question.}$
あるスカラー関数$\varphi$がLaplace方程式$\nabla^2\varphi=0$の解であるとき, $\nabla\varphi$が管状かつ渦なしであることを示せ. 

$\bm{Proof.}$
Laplace方程式の解となっていることより管状であり, 一般にスカラー関数の勾配の回転はゼロであることから渦なしである. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)