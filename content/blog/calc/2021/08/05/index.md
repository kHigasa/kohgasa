---
title: 今日の計算(279)
date: "2021-08-05T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.35

ある力が
$$
\bm{F} = \frac{2P\cos\theta}{r^3}\hat{\bm{e}}_r+\frac{P\sin\theta}{r^3}\hat{\bm{e}}_{\theta}\text{, }r\geq P/2
$$
と与えられている. 

### (a)
$\bm{Question.}$
ポテンシャルの存在を確かめるために, $\nabla\times\bm{F}=\bm{0}$を計算せよ. 

$\bm{Answer.}$
$$
\nabla\times\bm{F}=\bm{0}\text{. }
$$

### (b)
$\bm{Question.}$
$\theta=\pi/2$における単位円に対して$\oint\bm{F}\cdot\mathrm{d}\bm{r}$を計算せよ. 

$\bm{Answer.}$
$$
\oint\bm{F}\cdot\mathrm{d}\bm{r}=0\text{. }
$$

### (c)
$\bm{Question.}$
ポテンシャル$V$があるとしてそれを求めよ. 

$\bm{Answer.}$
$$
V = \frac{P\cos\theta}{r^2}\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)