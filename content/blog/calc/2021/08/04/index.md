---
title: 今日の計算(278)
date: "2021-08-04T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.34

$\bm{Question.}$
球面座標系における$\nabla^2\psi(r)$の次の三つの表式
$$
\frac{1}{r^2}\frac{\mathrm{d}}{\mathrm{d}r}\left[r^2\frac{\mathrm{d}\psi(r)}{\mathrm{d}r}\right]\text{, }\frac{1}{r}\frac{\mathrm{d}^2}{\mathrm{d}r^2}[r\psi(r)]\text{, }\frac{\mathrm{d}^2\psi(r)}{\mathrm{d}r^2}+\frac{2}{r}\frac{\mathrm{d}\psi(r)}{\mathrm{d}r}
$$
が等価であることを示せ. 

$\bm{Proof.}$
全て等しくなることは見れば判る. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)