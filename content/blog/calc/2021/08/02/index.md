---
title: 今日の計算(276)
date: "2021-08-02T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.32
### (a)
$\bm{Question.}$
$$
\bm{L} = -\mathrm{i}(\bm{r}\times\bm{\nabla}) = \mathrm{i}\left(\frac{1}{\sin\theta}\frac{\partial}{\partial\phi}\hat{\bm{e}}_{\theta}-\frac{\partial}{\partial\theta}\hat{\bm{e}}_{\phi}\right)
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \bm{L} &= -\mathrm{i}(\bm{r}\times\bm{\nabla}) \\
    &= -\mathrm{i}\left[r\hat{\bm{e}}_r\times\left(\frac{\partial}{\partial r}\hat{\bm{e}}_r+\frac{1}{r}\frac{\partial}{\partial\theta}\hat{\bm{e}}_{\theta}+\frac{1}{r\sin\theta}\frac{\partial}{\partial\phi}\right)\right] \\
    &= -\mathrm{i}(\bm{r}\times\bm{\nabla}) = \mathrm{i}\left(\frac{1}{\sin\theta}\frac{\partial}{\partial\phi}\hat{\bm{e}}_{\theta}-\frac{\partial}{\partial\theta}\hat{\bm{e}}_{\phi}\right)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$\bm{L}$をCartesian座標系で表せ. 

$\bm{Answer.}$
$$
\bm{L} = (L_x,L_y,L_z) = \mathrm{i}\left(\sin\phi\frac{\partial}{\partial\theta}+\cot\theta\cos\phi\frac{\partial}{\partial\phi},-\cos\phi\frac{\partial}{\partial\theta}+\cot\theta\sin\phi`\frac{\partial}{\partial\phi}, -\frac{\partial}{\partial\phi}\right)\text{. }
$$

### (c)
$\bm{Question.}$
$$
\bm{L}^2 = -\frac{1}{\sin\theta}\frac{\partial}{\partial\theta}\left(\sin\theta\frac{\partial}{\partial\theta}\right)-\frac{1}{\sin^2\theta}\frac{\partial^2}{\partial\phi^2}
$$
を示せ. 

$\bm{Proof.}$
前問[(b)](#b)の結果を用いて, 
$$
\bm{L}^2 = L_x^2+L_y^2+L_z^2 = -\frac{1}{\sin\theta}\frac{\partial}{\partial\theta}\left(\sin\theta\frac{\partial}{\partial\theta}\right)-\frac{1}{\sin^2\theta}\frac{\partial^2}{\partial\phi^2}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)