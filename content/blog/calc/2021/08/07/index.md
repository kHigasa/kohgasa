---
title: 今日の計算(281)
date: "2021-08-07T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.37

$\bm{Question.}$
原点に置かれた電気双極子モーメント$\bm{p}$が位置$\bm{r}$に作る電気ポテンシャルは
$$
\psi(\bm{r}) = \frac{\bm{p}\cdot\bm{r}}{4\pi\varepsilon_0r^3}
$$
で与えられ, このとき$\bm{r}$における電場を求めよ. 

$\bm{Answer.}$
$$
\bm{E} = -\nabla\psi(\bm{r}) = \frac{3\hat{\bm{e}}_r(\bm{p}\cdot\hat{\bm{e}}_r)-\bm{p}}{4\pi\varepsilon_0r^3}\text{. }
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)