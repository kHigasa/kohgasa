---
title: 今日の計算(287)
date: "2021-08-13T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.6

$\bm{Question.}$
$T_{iklm}$が全てのインデックスのペアに関して反対称的であるとき, 3次元空間で独立な成分は幾つあるか答えよ. 

$\bm{Answer.}$
$0$. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)