---
title: 今日の計算(286)
date: "2021-08-12T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.5

$\bm{Question.}$
一般相対論の四次元四階Riemann-Christoffel曲率テンソル$R_{iklm}$は
$$
R_{iklm} = -R_{ikml} = -R_{kilm}
$$
という関係式を満たす. インデックスを$0$から$3$まで走らせると, 独立な成分の数は$256$から$36$へ減少し, 条件$R_{iklm}=R_{lmik}$により更に$21$にまで減少することを示せ. 最後に恒等式$R_{iklm}+R_{ilmk}+R_{imkl}=0$を満たすとき, $20$に減少することを示せ. 

$\bm{Proof.}$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)