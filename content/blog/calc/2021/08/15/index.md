---
title: 今日の計算(289)
date: "2021-08-15T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.8

$\bm{Question.}$
Cartesian座標系において$T_{ijk\ldots}$が$n$階のテンソルであるとき, $\sum_j\partial T_{ijk\ldots}/\partial x^j$が$n-1$階のテンソルであることを示せ. 

$\bm{Proof.}$
階数は微分演算により$1$増え, 縮約により$2$減るので$n-1$階となる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)