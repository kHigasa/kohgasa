---
title: 今日の計算(299)
date: "2021-08-25T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.2.7

$\bm{Question.}$
反対称な$B^{ij}=-B^{ji}$に対し$A_k=\frac{1}{2}\varepsilon_{ijk}B^{ij}$が与えられているとき, 
$$
B^{mn} = \varepsilon^{mnk}A_k
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    2\varepsilon^{mnk}A_k &= \varepsilon^{mnk}\varepsilon^{ijk} \\
    &= (\delta^{mi}\delta^{nj}-\delta^{mj}\delta^{ni})B^{ij} \\
    &= B^{mn}-B^{nm} = 2B^{mn}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)