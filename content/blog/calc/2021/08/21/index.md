---
title: 今日の計算(295)
date: "2021-08-21T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.2.3

$\bm{Question.}$
$\mathcal{R}^3$においてテンソル記法で$\nabla\cdot\nabla\times\bm{A}$と$\nabla\times\nabla\phi$を書き下せ. 

$\bm{Answer.}$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)