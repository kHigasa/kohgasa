---
title: 今日の計算(296)
date: "2021-08-22T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.2.4

$\bm{Question.}$
次の三つの四階テンソルが等方的であることを確かめよ. 

### (a)
$$
A^{ik}_{jl}=\delta^i_j\delta^k_l\text{. }
$$

### (b)
$$
B^{ij}_{kl}=\delta^i_k\delta^j_l+\delta^i_l\delta^j_k\text{. }
$$

### (c)
$$
C^{ij}_{kl}=\delta^i_k\delta^j_l-\delta^i_l\delta^j_k\text{. }
$$

$\bm{Answer.}$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)