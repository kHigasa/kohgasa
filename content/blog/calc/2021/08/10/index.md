---
title: 今日の計算(284)
date: "2021-08-10T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.3

$\bm{Question.}$
四次元ベクトルの最後の三成分が二つの基準座標系それぞれにおいて消えており, 一方の基準座標系が他方の基準座標系の軸$x_0$に関する単なる回転になっていないとき, 全ての基準座標系の零番目の成分が消えることを示せ. 

$\bm{Proof.}$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)