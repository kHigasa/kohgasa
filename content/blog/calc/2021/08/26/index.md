---
title: 今日の計算(300)
date: "2021-08-26T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.3.1

$\bm{Question.}$
右手系をなしている次の三次元空間に対して, 
$$
\varepsilon^i = \frac{\varepsilon_j\times\varepsilon_k}{\varepsilon_j\times\varepsilon_k\cdot\varepsilon_i}\ i,j,k=1,2,3\ \text{and cyclic順列}
$$
を示せ. 

$\bm{Proof.}$
. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)