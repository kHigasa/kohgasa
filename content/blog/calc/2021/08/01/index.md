---
title: 今日の計算(275)
date: "2021-08-01T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.31

$\bm{Question.}$
軌道角運動量演算子$\bm{L}=-\mathrm{i}(\bm{r}\times\bm{\nabla})$に対して$\bm{L}\times\bm{L}=\mathrm{i}\bm{L}$を確かめよ. 

$\bm{Answer.}$
OK. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)