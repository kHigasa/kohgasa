---
title: 今日の計算(282)
date: "2021-08-08T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.1

$\bm{Question.}$
一つの座標系で任意の階数の任意のテンソルのすべての成分が消えるならば, 他の全ての座標系においても消えることを示せ. 

$\bm{Proof.}$
座標系を変更したことによるテンソルの変換を$0$に施しても$0$となることより示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)