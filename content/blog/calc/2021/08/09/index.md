---
title: 今日の計算(283)
date: "2021-08-09T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.2

$\bm{Question.}$
ある座標系においてテンソル$A$の成分とテンソル$B$のそれに対応する成分が一致しているとき, 全ての座標系においてこれらは一致することを示せ. 

$\bm{Proof.}$
座標系を変更したことによるテンソルの変換を同じ対称に施すことになるので当然一致する. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)