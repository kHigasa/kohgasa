---
title: 今日の計算(298)
date: "2021-08-24T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.2.6

$\bm{Question.}$
$\varepsilon_{ij}$を$2\times 2$行列により表せ. 又, $\varepsilon_{ij}$が直交相似変換の下で不変であることを示せ. 

$\bm{Answer.}$
$$
\varepsilon_{ij} = \begin{pmatrix}
    0 & 1 \\
    -1 & 0
\end{pmatrix}\text{. }
$$
$$
\begin{pmatrix}
    \cos\phi & \sin\phi \\
    -\sin\phi & \cos\phi
\end{pmatrix}
\begin{pmatrix}
    0 & 1 \\
    -1 & 0
\end{pmatrix}
\begin{pmatrix}
    \cos\phi & -\sin\phi \\
    \sin\phi & \cos\phi
\end{pmatrix} = 
\begin{pmatrix}
    0 & 1 \\
    -1 & 0
\end{pmatrix}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)