---
title: 今日の計算(293)
date: "2021-08-19T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.2.1

$\bm{Question.}$
$(C_1,C_2,C_3)$を擬ベクトルとして, 反対称平方列が
$$
\begin{pmatrix}
    0 & C_3 & -C_2 \\
    -C_3 & 0 & C_1 \\
    C_2 & -C_1 & 0
\end{pmatrix} = 
\begin{pmatrix}
    0 & C^{12} & C^{13} \\
    -C^{12} & 0 & C^{23} \\
    -C^{13} & -C^{23} & 0
\end{pmatrix}
$$
と与えられているとき, 
$$
C_i = \frac{1}{2!}\varepsilon_{ijk}C^{jk}
$$
という関係式を仮定すれば, $C^{jk}$がテンソルであることを示せ. 

$\bm{Proof.}$
. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)