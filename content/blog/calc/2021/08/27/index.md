---
title: 今日の計算(301)
date: "2021-08-27T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.3.2

$\bm{Question.}$
共変ベクトル$\bm{\varepsilon}_i$が直交しているとき, 次を示せ. 

### (a)
$g_{ij}$は対角行列である. 

$\bm{Proof.}$
. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$$
g^{ii} = \frac{1}{g_{ii}}\text{. }
$$

$\bm{Proof.}$
. <div class="QED" style="text-align: right">$\Box$</div>

### (c)
$$
|\bm{\varepsilon}^i| = \frac{1}{|\bm{\varepsilon}_i|}\text{. }
$$

$\bm{Proof.}$
. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)