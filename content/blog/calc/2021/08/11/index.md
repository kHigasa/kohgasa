---
title: 今日の計算(285)
date: "2021-08-11T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.4

$\bm{Question.}$
座標軸に関する一般の二階テンソルの$90^{\circ}$, $180^{\circ}$回転の振る舞いの解析から, 三次元空間における等方二階テンソルは$\delta^i_{\ j}$の積でなければならないことを示せ. 

$\bm{Proof.}$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)