---
title: 今日の計算(288)
date: "2021-08-14T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.7

$\bm{Question.}$
Cartesian座標系において, $T_{\ldots i}$が$n$階のテンソルであるとき, $\partial T_{\ldots i}/\partial x^j$が$n+1$階のテンソルであることを示せ. 

$\bm{Proof.}$
$n$階テンソルの各成分に対して微分演算子を施すことになり階数は$1$増える. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)