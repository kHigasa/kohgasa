---
title: 今日の計算(290)
date: "2021-08-16T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 4.1.9

$\bm{Question.}$
d'Alembertian
$$
\sum_{i=1}^4\frac{\partial^2}{\partial x_i^2}
$$
がスカラー演算子であることを示せ. 

$\bm{Proof.}$
Lorentz変換により不変であるのでd'Alembertianはスカラー演算子である. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)