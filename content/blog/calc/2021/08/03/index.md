---
title: 今日の計算(277)
date: "2021-08-03T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.33

$\bm{Question.}$
軌道角運動量演算子$\bm{L}=-\mathrm{i}\bm{r}\times\nabla$に対して以下の恒等式を確かめよ. 

### (a)
$$
\nabla = \frac{\partial}{\partial r}\hat{\bm{e}}_r-\mathrm{i}\frac{\bm{r}\times\bm{L}}{r^2}\text{. }
$$

$\bm{Answer.}$
OK. 

### (b)
$$
r\nabla^2-\nabla\left(1+r\frac{\partial}{\partial r}\right) = \mathrm{i}\nabla\times\bm{L}\text{. }
$$

$\bm{Answer.}$
OK. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)