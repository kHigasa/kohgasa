---
title: 今日の計算(280)
date: "2021-08-06T01:00:00.000Z"
description: "手元の雑多な計算集"
---

## 3.10.36
### (a)
$\bm{Question.}$
$\bm{A}=-\hat{\bm{e}}_{\phi}\cot\theta/r$が$\nabla\times\bm{A}=\hat{\bm{e}}_r/r^2$の解であることを示せ. 

$\bm{Proof.}$
$$
\nabla\times\bm{A}=\frac{1}{r}\frac{\partial}{\partial\theta} \left(-\frac{\cot\theta}{r}\right)\hat{\bm{e}}_r = \frac{1}{r^2}\hat{\bm{e}}_r
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
前問 [(a)](#a)のベクトル$\bm{A}$をCartesian座標系で表わせ. 

$\bm{Answer.}$
$$
\bm{A} = \frac{yz}{r(x^2+y^2}\hat{\bm{e}}_x-\frac{xz}{r(x^2+y^2)}\hat{\bm{e}}_y\text{. }
$$

### (c)
$\bm{Question.}$
$\bm{A}=-\hat{\bm{e}}_{\theta}\phi\sin\theta/r$も$\nabla\times\bm{A}=\hat{\bm{e}}_r/r^2$の解であることを示せ. 

$\bm{Proof.}$
$$
\nabla\times\bm{A}=\frac{1}{r\sin\theta}\frac{\partial}{\partial\phi}\left(-\frac{\phi\sin\theta}{r}\right)\hat{\bm{e}}_r = \frac{1}{r^2}\hat{\bm{e}}_r
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)