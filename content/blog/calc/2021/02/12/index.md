---
title: 今日の計算(107)
date: "2021-02-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.7

$\bm{Question.}$
次の6元連立方程式の結果を小数第5位まで与えよ. 

$\bm{Answer.}$
Gaussの消去法を使って計算機にやらせましょう. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)