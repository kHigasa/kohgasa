---
title: 今日の計算(104)
date: "2021-02-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.4

$c_{ij}$が行列$A$の要素$a_{ij}$の余因子であるとき次を示せ. 

### (a)

$\bm{Question.}$
$$
\sum_ia_{ij}C_{ij} = \sum_ia_{ji}C_{ji} = |A|
$$

$\bm{Proof.}$
行と列に関する余因子展開の証明. 行列式の多重線形性より, 関心のある行(列)について和の形に分解し, それぞれの列(行)交換を行えば示される. <div class="QED" style="text-align: right">$\Box$</div>

### (b)

$\bm{Question.}$
$$
\sum_ia_{ij}C_{ik} = \sum_ia_{ji}C_{ki} = 0\text{ , }j\neq k
$$

$\bm{Proof.}$
このとき展開される行(列)と同じ行(列)を含んだ行列式を計算していることになるので, $|A|=-|A|$より$|A|=0$となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)