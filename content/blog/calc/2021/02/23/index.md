---
title: 今日の計算(118)
date: "2021-02-23T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.9

行列が
$$
i = \begin{pmatrix}
    0 & 1 & 0 & 0 \\
    -1 & 0 & 0 & 0 \\
    0 & 0 & 0 & 1 \\
    0 & 0 & -1 & 0
\end{pmatrix}\text{ , }
j = \begin{pmatrix}
    0 & 0 & 0 & -1 \\
    0 & 0 & -1 & 0 \\
    0 & 1 & 0 & 0 \\
    1 & 0 & 0 & 0
\end{pmatrix}\text{ , }
k = \begin{pmatrix}
    0 & 0 & -1 & 0 \\
    0 & 0 & 0 & 1 \\
    1 & 0 & 0 & 0 \\
    0 & -1 & 0 & 0
\end{pmatrix}
$$
で与えられるとき, 4次単位行列$\bm{I}$と合わせてこれらはクォータニオンの基底をなす. これらについて次の関係式を確かめよ. 

### (a)
$\bm{Question.}$
$$
i^2=j^2=k^2=-\bm{1}.
$$

$\bm{Answer.}$
OK. 

### (b)
$\bm{Question.}$
$$
ij=-ji=k\text{ , }jk=-kj=i\text{ , }ki=-ik=j.
$$

$\bm{Answer.}$
OK. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)