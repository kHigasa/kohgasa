---
title: 今日の計算(112)
date: "2021-02-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.3
### (a)
$\bm{Question.}$
複素数$a+\mathrm{i}b\text{ , }a,b\in\mathbb{R}$が$2\times 2$行列によって表示されることを示せ, 
$$
a+\mathrm{i}b \leftrightarrow \begin{pmatrix}
   a & b \\
   -b & a
\end{pmatrix}. 
$$

$\bm{Proof.}$
$a,b,c,d\in\mathbb{R}$. まず加法について, 
$$
(a+c)+\mathrm{i}(b+d) \leftrightarrow \begin{pmatrix}
   a+c & b+d \\
   -(b+d) & a+c
\end{pmatrix}. 
$$
となる. 次に乗法について, 
$$
(ac-bd)+\mathrm{i}(ad+bc) \leftrightarrow \begin{pmatrix}
   ac-bd & ad+bc \\
   -(ad+bc) & ac-bd
\end{pmatrix}. 
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$(a+\mathrm{i}b)^{-1}$に対応する行列表示を与えよ. 

$\bm{Answer.}$
$$
\frac{1}{a^2+b^2}\begin{pmatrix}
   a & -b \\
   b & a
\end{pmatrix}. 
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)