---
title: 今日の計算(114)
date: "2021-02-19T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.5
### (a)
$\bm{Question.}$
2乗がゼロとなる$2\times 2$行列は最も一般に
$$
\begin{pmatrix}
   ab & b^2 \\
   -a^2 & -ab
\end{pmatrix}\text{ , }a,b\in\mathbb{C}
$$
の形でかけることを示せ. 

$\bm{Proof.}$
$a,b,c,d\in\mathbb{C}$として, 
$$
\begin{pmatrix}
   a & b \\
   c & d
\end{pmatrix}
\begin{pmatrix}
   a & b \\
   c & d
\end{pmatrix}
\begin{pmatrix}
   a^2+bc & b(a+d) \\
   c(a+d) & bc+c^2
\end{pmatrix} = 0
$$
となるためには, $a^2=d^2=-bc$, $a=-d$が必要十分である. これより示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$C=A+B$のとき, 一般に
$$
\mathrm{det}C\neq\mathrm{det}A+\mathrm{det}B
$$
である. 例を与えよ. 

$\bm{Answer.}$
略. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)