---
title: 今日の計算(115)
date: "2021-02-20T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.6

$\bm{Question.}$
$$
K = \begin{pmatrix}
    0 & 0 & \mathrm{i} \\
    -\mathrm{i} & 0 & 0 \\
    0 & -1 & 0
\end{pmatrix}
$$
のとき, $K^n=I_3$(3次単位行列)となるような$n$を見つけよ. 

$\bm{Answer.}$
$n=6$($n=3$で対角行列となる). 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)