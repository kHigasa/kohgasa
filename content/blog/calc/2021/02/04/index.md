---
title: 今日の計算(99)
date: "2021-02-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.8

$\bm{Question.}$
$f(x_0)=0$を満たすような$x_0$に対して, 
$$
\delta(f(x)) = \left(\frac{\mathrm{d}f(x)}{\mathrm{d}x}\right)^{-1}\delta(x-x_0)
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \int_{-\infty}^{\infty}\delta(f(x))f(x)\mathrm{d}f &= f(x_0) \\
    &= \int_{-\infty}^{\infty}\delta(x-x_0)f(x)\mathrm{d}x \\
    &= \int_{-\infty}^{\infty}\delta(x-x_0)f(x)\left(\frac{\mathrm{d}f(x)}{\mathrm{d}x}\right)^{-1}\mathrm{d}f \\
    \therefore\ \delta(f(x)) &= \left(\frac{\mathrm{d}f(x)}{\mathrm{d}x}\right)^{-1}\delta(x-x_0)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### コメント
問題文[^1]では函数$f(x)$の$x=x_0$における微分係数を用いており, 実質それで何の問題もないのだが, ここでは導関数$f'(x)$を採用した. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)