---
title: 今日の計算(111)
date: "2021-02-16T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.2

$\bm{Question.}$
$A$と$B$が交換する$[A,B]=0$とき, またこのときに限り
$$
(A+B)(A-B) = A^2-B^2
$$
が成り立つことをを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    (A+B)(A-B) &= A^2-B^2-(AB-BA) \\
    &= A^2-B^2\ \because [A,B]=0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)