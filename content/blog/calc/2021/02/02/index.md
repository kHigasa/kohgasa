---
title: 今日の計算(97)
date: "2021-02-02T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.6

$\bm{Question.}$
Gauss誤差曲線$\delta$数列
$$
\delta_n(x) = \frac{n}{\sqrt{\pi}}\mathrm{e}^{-n^2x^2}
$$
を用いて, 
$$
x\frac{\mathrm{d}\delta(x)}{\mathrm{d}x} = -\delta(x)
$$
を示せ. 

$\bm{Proof.}$
ある函数$f(x)$を掛けて, $[-\infty,\infty]$に渡って$\displaystyle\lim_{n\to\infty}x\delta_n'(x)$を積分すると, 
$$
\begin{aligned}
    &\lim_{n\to\infty}\int_{-\infty}^{\infty}x\frac{\mathrm{d}\delta_n(x)}{\mathrm{d}x}f(x)\mathrm{d}x \\
    = &\lim_{n\to\infty}\left[\delta_n(x)xf(x)\right]_{-\infty}^{\infty}-\lim_{n\to\infty}\int_{-\infty}^{\infty}\delta_n(x)f(x)\mathrm{d}x-\lim_{n\to\infty}\int_{-\infty}^{\infty}\delta_n(x)xf'(x)\mathrm{d}x \\
    = &-\lim_{n\to\infty}\int_{-\infty}^{\infty}\delta_n(x)f(x)\mathrm{d}x \\
    \therefore\ &\lim_{n\to\infty}x\frac{\mathrm{d}\delta_n(x)}{\mathrm{d}x}=-\lim_{n\to\infty}\delta_n(x) \\
    \therefore\ &x\frac{\mathrm{d}\delta(x)}{\mathrm{d}x}=-\delta(x)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)