---
title: 今日の計算(109)
date: "2021-02-14T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.9

$\bm{Question.}$
3次元空間において, 
$$
\sum_k\epsilon_{ijk}\epsilon_{pqk} = \delta_{ip}\delta_{jq}-\delta_{iq}\delta_{jp}
$$
を示せ. $\delta_{ij}$はKroneckerのデルタ, $\epsilon_{ijk}$はLevi-Civita記号である. 

$\bm{Answer.}$
ある$k$が与えられたとき, 左辺が非ゼロとなる$i,j,p,q$は自然に決まる. このときそれぞれは$k$と異なる値を持ち, $i\neq j$, $p\neq q$を満たす. $i=p$かつ$j=q$のとき両辺$1$, $i=q$かつ$j=p$のとき両辺$-1$となる. 他の$k$についての項は全てゼロであるので, 両辺はこのまま同じ値となる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)