---
title: 今日の計算(102)
date: "2021-02-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.2

$\bm{Question.}$
次の3本の線形同次方程式
$$
\begin{cases}
    x+3y+3z=0 \\
    x-y+z=0 \\
    2x+y+3z=0
\end{cases}
$$
が, 非自明な解を持つかどうかを確かめよ. いずれの場合においてもこの3元連立方程式の解を求めよ. 

$\bm{Answer.}$
この3本の方程式は3次正方行列$A$と3次元ベクトル$\bm{x}=\ ^{\text{t}}(x,y,z)$を用いて, $A\bm{x}=\bm{0}$とかける. ここで$A$の行列式を計算すると, 
$$
\mathrm{det}A = \begin{vmatrix}
   1 & 3 & 3 \\
   1 & -1 & 1 \\
   2 & 1 & 3
\end{vmatrix} = 2
$$
となる. したがってこの3本の連立方程式は非自明解を持たず, その解は自明解$\bm{x}=\bm{0}=\ ^{\text{t}}(0,0,0)$のみである. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)