---
title: 今日の計算(123)
date: "2021-02-28T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.14

$\bm{Question.}$
$A$が全ての対角要素が異なる対角行列であり, $A$と$B$が交換するとき, $B$が対角行列であることを示せ. 

$\bm{Proof.}$
行列$AB$と$BA$の要素$(i,j)\text{ , }i\neq j$を考える. 行列$A$を$\mathrm{diag}(\lambda_1,\ldots ,\lambda_n)$とおく. 仮定より$AB=BA$なので$(AB)_{ij}=(BA)_{ij}$なので, 
$$
\begin{aligned}
    (AB)_{ij} &= \lambda_iB_{ij} \\
    = (BA)_{ij} &= B_{ij}\lambda_{j}
\end{aligned}
$$
となり, いま仮定より$i\neq j$で$\lambda_i\neq\lambda_j$であるから, $i\neq j$のとき$B_{ij}=0$となり題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)