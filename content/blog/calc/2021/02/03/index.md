---
title: 今日の計算(98)
date: "2021-02-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.7

$\bm{Question.}$
$f'(x)$が$x=0$で連続であると仮定して, 
$$
\int_{-\infty}^{\infty}\delta'(x)f(x)\mathrm{d}x = -f'(0)
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    &\int_{-\infty}^{\infty}\delta'(x)f(x)\mathrm{d}x \\
    = &\left[\delta(x)f(x)\right]_{-\infty}^{\infty}-\int_{-\infty}^{\infty}\delta(x)f'(x)\mathrm{d}x \\
    = &-f'(0)
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)