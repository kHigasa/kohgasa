---
title: 今日の計算(120)
date: "2021-02-25T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.11

3つのPauliスピン行列は
$$
\sigma_1 = \begin{pmatrix}
    0 & 1 \\
    1 & 0
\end{pmatrix}\text{ , }
\sigma_2 = \begin{pmatrix}
    0 & -\mathrm{i} \\
    \mathrm{i} & 0
\end{pmatrix}\text{ , }
\sigma_3 = \begin{pmatrix}
    1 & 0 \\
    0 & -1
\end{pmatrix}
$$
で与えられる. 

### (a)
$\bm{Question.}$
$$
(\sigma_i)^2 = I_2
$$
を確かめよ. 

$\bm{Answer.}$
OK. 

### (b)
$\bm{Question.}$
$$
\sigma_i\sigma_j = i\sigma_k\text{ , }(i,j,k)=(1,2,3)\text{ または これのサイクリックな入れ替え}
$$
を確かめよ. 

$\bm{Answer.}$
OK. 

### (c)
$\bm{Question.}$
$$
\sigma_i\sigma_j+\sigma_j\sigma_i = 2\delta_{ij}I_2
$$
を確かめよ. 

$\bm{Answer.}$
OK. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)