---
title: 今日の計算(110)
date: "2021-02-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.1

$\bm{Question.}$
行列における積の結合則$(AB)C=A(BC)$を示せ. 

$\bm{Proof.}$
$(i,j)$行列成分は, 
$$
\begin{aligned}
    [(AB)C]_{ij} &= \sum_q\left(\sum_pa_{ip}b_{pq}\right)c_{qj} \\
    &= \sum_{pq}a_{ip}b_{pq}c_{qj} \\
    &= \sum_pa_{ip}\left(\sum_qb_{pq}c_{qj}\right) \\
    &= [A(BC)]_{ij}
\end{aligned}
$$
となって等しい. ここで複素数や実数の積の結合法則が成り立つことを用いた. よって示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)