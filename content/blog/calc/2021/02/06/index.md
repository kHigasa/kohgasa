---
title: 今日の計算(101)
date: "2021-02-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.1

次の行列式を計算せよ. 

### (a)
$\bm{Question.}$
$$
\begin{vmatrix}
   1 & 0 & 1 \\
   0 & 1 & 0 \\
   1 & 0 & 0
\end{vmatrix}
$$

$\bm{Answer.}$
$$
-1.
$$

### (b)
$\bm{Question.}$
$$
\begin{vmatrix}
   1 & 2 & 0 \\
   3 & 1 & 2 \\
   0 & 3 & 1
\end{vmatrix}
$$

$\bm{Answer.}$
$$
-11.
$$

### (c)
$\bm{Question.}$
$$
\frac{1}{\sqrt{2}}\begin{vmatrix}
   0 & \sqrt{3} & 0 & 0 \\
   \sqrt{3} & 0 & 2 & 0 \\
   0 & 2 & 0 & \sqrt{3} \\
   0 & 0 & \sqrt{3} & 0
\end{vmatrix}
$$

$\bm{Answer.}$
第1列に関して展開して, 
$$
\text{与式} = \frac{1}{\sqrt{2}}(-1)^{2+1}\sqrt{3}\begin{vmatrix}
    \sqrt{3} & 0 & 0 \\
    2 & 0 & \sqrt{3} \\
    0 & \sqrt{3} & 0
\end{vmatrix} = \frac{9}{\sqrt{2}}
$$
と求まる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)