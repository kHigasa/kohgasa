---
title: 今日の計算(116)
date: "2021-02-21T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.7

$\bm{Question.}$
Jacobi恒等式
$$
\left[A,[B,C]\right]=\left[B,[A,C]\right]-\left[C,[A,B]\right]
$$
が成り立つことを確かめよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \text{LHS} &= A(BC-CB)-(BC-CB)A \\
    &= ABC+CBA-ACB-BCA
\end{aligned}
$$
$$
\begin{aligned}
    \text{RHS} &= B(AC-CA)-(AC-CA)B-C(AB-BA)+(AB-BA)C \\
    &= ABC+CBA-ACB-BCA
\end{aligned}
$$
となり確かめられた. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)