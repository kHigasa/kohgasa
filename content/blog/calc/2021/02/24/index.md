---
title: 今日の計算(119)
date: "2021-02-24T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.10

$\bm{Question.}$

2つの上三角行列の積は上三角行列となることを示せ. 

$\bm{Answer.}$
上三角行列の積$AB$の$(i,j)$成分を考えると, $(AB)_{ij}=\sum_ka_{ik}b_{kj}$であり, これは$i\gt j$のときゼロと成るから示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)