---
title: 今日の計算(106)
date: "2021-02-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.6

$\bm{Question.}$
線形従属なベクトルから成る行列の行列式の値はゼロであることを示せ. 

$\bm{Proof.}$
あるベクトルが他のベクトルの線型結合で表されているので, 多重線形性を利用して同じベクトルを持つ複数の行列式の和に分割され, 個々の行列式はゼロであるので示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)