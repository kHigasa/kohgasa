---
title: 今日の計算(100)
date: "2021-02-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.9

### (a)
$\bm{Question.}$
$\delta$数列を$\delta_n(x)=n/2\cosh^2(nx)$で定義するとき, $n$に無関係に
$$
\int_{-\infty}^{\infty}\delta_n(x)\mathrm{d}x = 1
$$
が成り立つことを示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \int_{-\infty}^{\infty}\delta_n(x)\mathrm{d}x &= \int_{-\infty}^{\infty}\frac{n}{2\cosh^2(nx)}\mathrm{d}x \\
    nx&=t\text{と置換} \\
    &= \int_{-\infty}^{\infty}\frac{1}{2\cosh^2t}\mathrm{d}t \\
    &= \frac{1}{2}\left[\tanh t\right]_{-\infty}^{\infty} \\
    &= \frac{1}{2}(1-(-1)) = 1
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
またこの$\delta$数列について, 
$$
\int_{-\infty}^x\delta_n(x')\mathrm{d}x' = \frac{1}{2}(1+\tanh nx) \equiv u_n(x)
$$
と
$$
\lim_{n\to\infty}u_n(x) = \begin{cases}
    1 &\text{for }x>0 \\
    0 &\text{for }x<0
\end{cases}
$$
を示せ. 

$\bm{Proof.}$
最初の式については, 小問[(a)](#a)と同様にして, 
$$
\begin{aligned}
    \int_{-\infty}^{\infty}\delta_n(x)\mathrm{d}x &= \frac{1}{2}\left[\tanh t\right]_{-\infty}^{nx} \\
    &= \frac{1}{2}(\tanh nx+1)
\end{aligned}
$$
となり示された. また, $\displaystyle\lim_{n\to\infty}u_n(x)$がHeavisideの階段関数となるのは双曲線函数の定義よりそのまま従う. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)