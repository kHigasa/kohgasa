---
title: 今日の計算(113)
date: "2021-02-18T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.4

$\bm{Question.}$
$A$が$n\times n$行列であるとき, 
$$
\mathrm{det}(-A) = (-1)^n\mathrm{det}A
$$
が成り立つことを示せ. 

$\bm{Proof.}$
行列式の多重線型性より従う. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)