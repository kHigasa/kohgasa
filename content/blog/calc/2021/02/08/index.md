---
title: 今日の計算(103)
date: "2021-02-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.3

次の2本の方程式が与えられている, 
$$
\begin{cases}
    x+2y&=3& \\
    2x+4y&=6&.
\end{cases}
$$

### (a)
$\bm{Question.}$
係数の行列式がゼロとなることを示せ. 

### (b)
$\bm{Question.}$
Cramerの公式の分子の行列式がゼロとなることを示せ. 

### (c)
$\bm{Question.}$
解を少なくとも2つ求めよ. 

### コメント
簡単な問題をやる気力がないので全て略. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)