---
title: 今日の計算(105)
date: "2021-02-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.5

$\bm{Question.}$
Hilbert行列式を計算せよ. 

### (a)

$\bm{Answer.}$
1次, 
$$
\begin{vmatrix}
   1
\end{vmatrix} = 1.
$$
2次, 
$$
\begin{vmatrix}
   1 & 1/2 \\
   1/2 & 1/3
\end{vmatrix} = \frac{1}{12}.
$$
3次, 
$$
\begin{vmatrix}
   1 & 1/2 & 1/3 \\
   1/2 & 1/3 & 1/4 \\
   1/3 & 1/4 & 1/5
\end{vmatrix} = \frac{1}{720}.
$$

### (b)
数値計算? 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)