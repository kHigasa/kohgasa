---
title: 今日の計算(108)
date: "2021-02-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.1.8

3次元空間において, 以下を示せ. $\delta_{ij}$はKroneckerのデルタ, $\epsilon_{ijk}$はLevi-Civita記号である. 

### (a)
$\bm{Question.}$
$$
\sum_i\delta_{ii} = 3\text{ .}
$$

$\bm{Proof.}$
3次元空間なので$i=1,2,3$であり, $\delta_{ii}=1$より示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$$
\sum_{ij}\delta_{ij}\epsilon_{ijk} = 0\text{ .}
$$

$\bm{Proof.}$
Kroneckerのデルタは$i=j$のときのみ非ゼロだが, 一方Levi-Civita記号は$i=j$のとき$k$に関わらずゼロであるので示された. <div class="QED" style="text-align: right">$\Box$</div>

### (c)
$\bm{Question.}$
$$
\sum_{pq}\epsilon_{ipq}\epsilon_{jpq} = 2\delta_{ij}\text{ .}
$$

$\bm{Proof.}$
$i=j$のとき, 左辺は$1\cdot 1+(-1)\cdot (-1)=2$より右辺に一致する. $i\neq j$のときは一方のLevi-Civita記号が必ず$0$となるので, 左辺は右辺に一致する. よって示された. <div class="QED" style="text-align: right">$\Box$</div>

### (d)
$\bm{Question.}$
$$
\sum_{ijk}\epsilon_{ijk}\epsilon_{ijk} = 6\text{ .}
$$

$\bm{Proof.}$
$i,j,k$が$1,2,3$の内すべて異なる値をとるのは6通りである. そしてその時$\epsilon^2$が取る値は$1^2$か$(-1)^2$ですべて$1$であるので, 与式が示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)