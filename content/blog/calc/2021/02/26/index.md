---
title: 今日の計算(121)
date: "2021-02-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.12
スピン1を持つ粒子は行列
$$
M_x = \frac{1}{\sqrt{2}}\begin{pmatrix}
    0 & 1 & 0 \\
    1 & 0 & 1 \\
    0 & 1 & 0
\end{pmatrix}\text{ , }
M_y = \frac{1}{\sqrt{2}}\begin{pmatrix}
    0 & -\mathrm{i} & 0 \\
    \mathrm{i} & 0 & -\mathrm{i} \\
    0 & \mathrm{i} & 0
\end{pmatrix}\text{ , }
M_z = \begin{pmatrix}
    1 & 0 & 0 \\
    0 & 0 & 0 \\
    0 & 0 & -1
\end{pmatrix}
$$
によって記述される. 

### (a)
$\bm{Question.}$
$$
[M_i,M_j] = \mathrm{i}\sum_k\epsilon_{ijk}M_k
$$
となることを確かめよ. ここで$\epsilon_{ijk}$はLevi-Civita記号である. 

$\bm{Answer.}$
OK. 

### (b)
$\bm{Question.}$
$$
M^2 \equiv M_x^2+M_y^2+M_z^2 = 2I_3
$$
となることを確かめよ. ここで$I_3$は3次単位行列である. 

$\bm{Answer.}$
OK. 

### (c)
$\bm{Question.}$
$$
[M^2,M_i]=0\text{ , }[M_z,L^+]=L^+\text{ , }[L^+,L^-]=2M_z
$$
となることを確かめよ. ここで$L^+\equiv M_x+\mathrm{i}M_y\text{ , }L^-\equiv M_x-\mathrm{i}M_y$である. 

$\bm{Answer.}$
OK. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)