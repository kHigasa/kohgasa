---
title: 今日の計算(96)
date: "2021-02-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.11.5

$\bm{Question.}$
$$
\delta((x-x_1)(x-x_2)) = \frac{1}{|x_1-x_2|}\lbrace\delta(x-x_1)+\delta(x-x_2)\rbrace
$$
を示せ. 

$\bm{Proof.}$
$x_1$と$x_2$がどの範囲にあっても全く良いのだが, ここでは最も簡単なものとして, $x_1\in[0,\infty]$, $x_2\in[-\infty,0]$にある場合を証明する. ある函数$f(x)$を掛けて, $[-\infty,\infty]$に渡って証明すべき左辺の函数を積分すると, 
$$
\begin{aligned}
    &\int_{-\infty}^{\infty}f(x)\delta((x-x_1)(x-x_2))\mathrm{d}x \\
    = &\int_{0}^{\infty}f(x)\delta((x-x_1)(x-x_2))\mathrm{d}x+\int_{-\infty}^{0}f(x)\delta((x-x_1)(x-x_2))\mathrm{d}x \\
    = &\int_{0}^{\infty}f(x)\delta((x_1-x_2)(x-x_1))\mathrm{d}x+\int_{-\infty}^{0}f(x)\delta((x_2-x_1)(x-x_2))\mathrm{d}x \\
    = &\frac{1}{x_1-x_2}f(x_1)+\frac{1}{x_2-x_1}f(x_2) \\
    = &\frac{1}{|x_1-x_2|}(f(x_1)+f(x_2)) \\
    = &\frac{1}{|x_1-x_2|}\int_{-\infty}^{\infty}f(x)\lbrace\delta(x-x_1)+\delta(x-x_2)\rbrace\mathrm{d}x \\
    \therefore\ &\delta((x-x_1)(x-x_2)) = \frac{1}{|x_1-x_2|}\lbrace\delta(x-x_1)+\delta(x-x_2)\rbrace
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)