---
title: 今日の計算(117)
date: "2021-02-22T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 2.2.8

$\bm{Question.}$
行列が
$$
A = \begin{pmatrix}
    0 & 1 & 0 \\
    0 & 0 & 0 \\
    0 & 0 & 0
\end{pmatrix}\text{ , }
B = \begin{pmatrix}
    0 & 0 & 0 \\
    0 & 0 & 1 \\
    0 & 0 & 0
\end{pmatrix}\text{ , }
C = \begin{pmatrix}
    0 & 0 & 1 \\
    0 & 0 & 0 \\
    0 & 0 & 0
\end{pmatrix}
$$
で与えられるとき, 交換関係
$$
[A,B]=C\text{ , }[A,C]=0\text{ , }[B,C]=0
$$
を満たすことを確かめよ. 

$\bm{Answer.}$
OK. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)