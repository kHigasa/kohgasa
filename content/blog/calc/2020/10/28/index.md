---
title: 今日の計算(1)
date: "2020-10-28T02:59:59.999Z"
description: "手元の雑多な計算集"
---

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)

## 1.1.1

### (a)

$\bm{Question.}$
極限$\displaystyle\lim_{n\to\infty}n^pu_n = A < \infty ,\ p>1$が存在し, $u_n$が単調増加数列であるとき, 無限級数$\displaystyle\sum_{n=1}^{\infty}u_n$が収束することを示せ. 

$\bm{Proof.}$
$n\to\infty$で$u_n=A/n^p$であり, 数列$u_n$が単調増加であるので, 
$$
u_n<\frac{A}{n^p}\ (1<n<\infty)
$$
である. したがって不等式
$$
\sum_{n=1}^{\infty}u_n < \sum_{n=1}^{\infty}\frac{A}{n^p}
$$
を得る. いま数列$u_n$は振動せず, 各項は正の値で考えているので$-\infty$への発散も考えなくてよく, $\displaystyle\sum_{n=1}^{\infty}(A/n^p)$が収束することだけ示せばよい. 次に示す積分は
$$
\begin{aligned}
\int_1^{\infty}\frac{A}{x^p}\mathrm{d}x &= A\left[(1-p)x^{1-p}\right]_1^{\infty} = 0 \\
\int_1^{\infty}\frac{A}{x^p}\mathrm{d}x + u_1 &= u_1 < \infty
\end{aligned}
$$
となり, 共に有限値を取る. したがってCauchy-Maclaurinの積分判定法より$\displaystyle\sum_{n=1}^{\infty}(A/n^p)$が収束することが示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)

$\bm{Question.}$
極限$\displaystyle\lim_{n\to\infty}nu_n = A > 0$が存在するとき, 無限級数$\displaystyle\sum_{n=1}^{\infty}u_n$が発散する条件を求めよ. 

$\bm{Answer.}$
$n\to\infty$で$u_n=A/n$である. ここで調和級数$\displaystyle\sum_{n=1}^{\infty}1/n$を考えると, これはExample1.1.2[^1]に載っているように部分和が発散するため発散する. したがって, 無限級数$\displaystyle\sum_{n=1}^{\infty}u_n$が発散するためには, 
$$
\frac{A}{n} < u_n\ \text{for}\ \forall n
$$
と評価されれば良い. これがいま求めたい発散条件である. 

#### コメント

問題文[^1]ではhogehogeのとき収束・発散を示せとなっているのに, 解答にて追加で収束・発散条件を考えなければならないのは問題として不親切に思われた. そのため問(a)では数列が単調増加である条件を追加し, 問(b)は発散条件を求める問題に変更した. 