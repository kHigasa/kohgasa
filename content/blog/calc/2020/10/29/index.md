---
title: 今日の計算(2)
date: "2020-10-29T23:59:59.999Z"
description: "手元の雑多な計算集"
---

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)

## 1.1.2

$\bm{Question.}$
$\displaystyle 0<\lim_{n\to\infty}(b_n/a_n)=K<\infty$. このとき, $\displaystyle\sum_na_n$が収束or発散で$\displaystyle\sum_nb_n$はどうなるか. 

$\bm{Answer.}$
$n\to\infty$で$b_n=Ka_n$である. $\displaystyle\sum_na_n$が収束(発散)するなら, $\displaystyle\sum_nKa_n$も収束(発散)する. したがって, $\displaystyle\sum_na_n$の収束性は$\displaystyle\sum_nb_n$の収束性と一致する. 

#### コメント

解答[^2]は当てにならない. 