---
title: 今日の計算(4)
date: "2020-10-31T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.4

$\bm{Question.}$
いま数列の第n項とn+1項の比が次の形で与えられている, 
$$
\frac{u_n}{u_{n+1}}=\frac{n^2+a_1n+a_0}{n^2+b_1n+b_0}.
$$
このとき, パラメータ$a_1$や$b_1$がどんな値を持てば無限級数$\displaystyle\sum_nu_n$は収束/発散するか. 

$\bm{Answer.}$
$$
\begin{aligned}
    \frac{u_n}{u_{n+1}} &= 1+\frac{(a_1-b_1)n}{n^2+b_1n+b_0}+\frac{a_0-b_0}{n^2+b_1n+b_0} \\
    &= 1+\frac{(a_1-b_1)n}{n^2}\frac{n^2}{n^2+b_1n+b_0}+\frac{a_0-b_0}{n^2}\frac{n^2}{n^2+b_1n+b_0} \\
    &= 1+\frac{a_1-b_1}{n}\frac{1}{1+\frac{b_1}{n}+\frac{b_0}{n^2}}+\frac{1}{n^2}\frac{a_0-b_0}{1+\frac{b_1}{n}+\frac{b_0}{n^2}}
\end{aligned}
$$
いま最右辺第3項の$\frac{a_0-b_0}{1+b_1/n+b_0/n^2}$の部分が十分に大きな$n$で有界となるようにパラメータ$a_0,b_0$が選ばれているとする. そうするとこのときGaussの判定法より, パラメータ$a_1,b_1$の選び方によって, 
$$
a_1-b_1 = \begin{cases}
   > 1 &\Rightarrow\sum_nu_n\text{は収束} \\
   \leq 1 &\Rightarrow\sum_nu_n\text{は発散}
\end{cases}
$$
という結論を得る. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)