---
title: 今日の計算(3)
date: "2020-10-30T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.3

### (a)

$\bm{Question.}$
無限級数$\displaystyle \sum_{n=2}^{\infty}\frac{1}{n(\mathrm{log}n)^2}$が収束することを示せ.  

$\bm{Proof.}$
いま函数$1/x(\mathrm{log}x)^2$は単調減少であり, 次の積分
$$
\begin{aligned}
\int_2^{\infty}\frac{1}{x(\mathrm{log}x)^2}\mathrm{d}x &= \left[-\frac{1}{\mathrm{log}x}\right]_2^{\infty} = \frac{1}{\mathrm{log}2} \\
\int_2^{\infty}\frac{1}{x(\mathrm{log}x)^2}\mathrm{d}x + \frac{1}{2(\mathrm{log}2)^2} &= \frac{1}{\mathrm{log}2}+\frac{1}{2(\mathrm{log}2)^2}
\end{aligned}
$$
はどちらも有限であるので, Cauchy-Maclaurinの積分判定法より与えられた級数は収束することが示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)

$\bm{Question.}$
この級数を有効数字5桁で値を見積もれ. 

$\bm{Answer.}$
問(a)より, 
$$
\frac{1}{\mathrm{log}2}\leq\sum_{n=2}^{\infty}\frac{1}{n(\mathrm{log}n)^2}\leq\frac{1}{\mathrm{log}2}+\frac{1}{2(\mathrm{log}2)^2}
$$
と評価された. したがって, 
$$
3.3219\leq\sum_{n=2}^{\infty}\frac{1}{n(\mathrm{log}n)^2}\leq 8.8395
$$
と見積もられる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)