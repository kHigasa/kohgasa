---
title: 今日の計算(35)
date: "2020-12-01T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.4

$\bm{Question.}$
$y=\mathrm{arcsinh}\ x$を$x$について冪級数に展開せよ. 

$\bm{Answer.}$
$\mathrm{sinh}\ y=x$より, これを$y$について解いて$y=\mathrm{log}(x+\sqrt{x^2+1})$を得る. この導関数を$x$について冪級数展開すると, 
$$
\begin{aligned}
    y' &= (1+x^2)^{-1} \\
    &= \sum_{n=0}^{\infty}\binom{-\frac{1}{2}}{n}x^{2n} = \sum_{n=0}^{\infty}\frac{\left(-\frac{2n-1}{2}\right)_n}{n!}x^{2n} \\
    &= \sum_{n=0}^{\infty}\frac{1}{n!}\left(-\frac{1}{2}\right)\cdots\left(-\frac{2n-1}{2}\right)x^{2n} \\
    &= \sum_{n=0}^{\infty}(-1)^n\frac{(2n-1)!!}{(2n)!!}x^{2n} \\
    &= 1-\frac{x^2}{2}+\frac{3}{4}x^4-\cdots
\end{aligned}
$$
となる. これを$x$について$0$から$y$まで積分すると, 
$$
\begin{aligned}
    \int_0^yy'\mathrm{d}x &= \int_0^y\left(1-\frac{x^2}{2}+\frac{3}{4}x^4-\cdots\right)\mathrm{d}x \\
    &= \left[x-\frac{1}{2}\frac{x^3}{3}+\frac{1\cdot3}{2\cdot4}\frac{x^5}{5}-\cdots\right]_0^y \\
    &= y-\frac{1}{2}\frac{y^3}{3}+\frac{1\cdot3}{2\cdot4}\frac{y^5}{5}-\cdots
\end{aligned}
$$
と$y=\mathrm{arcsinh}\ x$の$x$についての冪級数展開が求められた. 

### コメント
テキスト[^1]で指示されていた展開のやり方"inversion of the series for $\mathrm{sinh}\ y$と, direct Maclaurin expansionがよくわからなかったので, ここでは$y$を初等関数で表しその導関数を積分する方法により解答を与えた. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)