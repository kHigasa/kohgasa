---
title: 今日の計算(48)
date: "2020-12-14T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.17

$\bm{Question.}$
中性子が質量$A$を持つ原子核に弾性衝突することによって, 中性子が失うエネルギーは, 
$$
\xi_1 = 1+\frac{(A-1)^2}{2A}\mathrm{log}\frac{A-1}{A+1}
$$
によって記述される. 十分大きな$A$に対しては
$$
\xi_2 = \frac{2}{A+2/3}
$$
と近似される. パラメータ$\xi_1$と$\xi_2$を$A^{-1}$の冪級数に展開せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \xi_1 &= 1+\frac{(A-1)^2}{2A}\left\lbrace\mathrm{log}\left(1-\frac{1}{A}\right)-\mathrm{log}\left(1+\frac{1}{A}\right)\right\rbrace \\
    &= 1+\frac{(A-1)^2}{2A}\left\lbrace -\left(\frac{1}{A}+\frac{1}{2A^2}+\frac{1}{3A^3}+\cdots\right)-\left(\frac{1}{A}-\frac{1}{2A^2}+\frac{1}{3A^3}-\cdots\right)\right\rbrace \\
    &= 1+\frac{(A-1)^2}{2A}\left(-\frac{2}{A}-\frac{2}{3A^3}-\cdots\right) \\
    &= -\left(A-2+\frac{1}{A}\right)\left(\frac{1}{A}+\frac{1}{3A^3}-\cdots\right) \\
    &= 2A^{-1}-\frac{4}{3}A^{-2}+\frac{2}{3}A^{-3}+O(A^{-4}) \\
    \xi_2 &= \frac{2}{A}\frac{1}{1+2/(3A)} \\
    &= \frac{2}{A}\left\lbrace 1-\frac{2}{3A}+\left(\frac{2}{3A}\right)^2-O(A^{-3})\right\rbrace \\
    &= 2A^{-1}-\frac{4}{3}A^{-2}+\frac{8}{9}A^{-3}+O(A^{-4})
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)