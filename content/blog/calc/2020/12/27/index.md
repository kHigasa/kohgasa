---
title: 今日の計算(61)
date: "2020-12-27T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.4

$\bm{Question.}$
Hubbleの法則によれば, 遠方銀河団は我々の観測地である地球からの距離に比例した速度で後退している. $i$番目の銀河に対して後退速度は, $\bm{v}_i=H_0\bm{r}_i$で与えられる. この我々からの銀河の後退が, 我々が宇宙の中心に居るのではないと示唆していることを示せ. 

$\bm{Proof.}$
つまり, 地球ではない新たな始点を取ったベクトルで定義した銀河の位置が未だHubbleの法則に則っていることを示せば良い. ベクトル算数なので, 宇宙空間の新たな座標を取って銀河の位置が$\bm{r}_i\to\bm{r}'_i=\bm{r}_i-\bm{r}_{new\ origin}$と変換できるとする. すると後退速度も同様の変換を受け(微分せよ), $\bm{v}_i\to\bm{v}'_i=\bm{v}_i-\bm{v}_{new\ origin}$となる. このとき, 地球を原点とした座標系で成り立っていたHubbleの法則より, 
$$
\begin{aligned}
    H_0\bm{r}'_i &= H_0\bm{r}_i-H_0\bm{r}_{new\ origin} \\
    &= H_0\bm{v}_i-H_0\bm{v}_{new\ origin} \\
    &= \bm{v}'_i
\end{aligned}
$$
となり, 新たな座標系でもHubbleの法則が成り立つことがわかる. 以上により題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### コメント
ベクトル場なので当然. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)