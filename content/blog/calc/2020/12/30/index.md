---
title: 今日の計算(64)
date: "2020-12-30T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.8

$\bm{Question.}$
速度ベクトル$(1,2,3)$でもって自由飛行しているロケットから, 位置$(2,1,3)$にいる観測者への最短距離を求めよ. そのロケットは時刻$t=0$に位置$(1,1,1)$から飛び立ったものとする. 

$\bm{Answer.}$
最短距離となる時刻を$t=t'>0$とおくと, 
$$
(t'+1,2t'+1,3t'+1)\cdot\begin{pmatrix}
   t'-1 \\
   2t' \\
   3t'-2
\end{pmatrix} = 0
$$
が成り立つ. したがって, 時刻$t'=1/2$において最短距離
$$
\left|\left(\frac{3}{2},2,\frac{5}{2}\right)-(2,1,3)\right| = \frac{1}{2}
$$
を取る. 

### コメント
問1.7.7[^1]はなんかうざいので飛ばした. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)