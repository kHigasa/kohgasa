---
title: 今日の計算(49)
date: "2020-12-15T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.18

$\bm{Question.}$
次の2つの積分がCatalan数
$$
\beta(2) = \sum_{n=0}^{\infty}\frac{(-1)^n}{(2n+1)^2}
$$
に等しいことを示せ. 

### (a)
$$
\int_0^1\mathrm{arctan}\ t\frac{\mathrm{d}t}{t}
$$

$\bm{Proof.}$
$\mathrm{arctan}\ t$を$t$について冪級数展開して, 
$$
\begin{aligned}
    \int_0^1\mathrm{arctan}\ t\frac{\mathrm{d}t}{t} &= \int_0^1\sum_{n=0}^{\infty}\frac{(-1)^n}{2n+1}t^{2n+1}\frac{\mathrm{d}t}{t} \\
    &= \int_0^1\sum_{n=0}^{\infty}\frac{(-1)^n}{2n+1}t^{2n}\mathrm{d}t \\
    &= \left[\sum_{n=0}^{\infty}\frac{(-1)^n}{(2n+1)^2}t^{2n+1}\right]_0^1 \\
    &= \sum_{n=0}^{\infty}\frac{(-1)^n}{(2n+1)^2}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$$
-\int_0^1\mathrm{log}\ x\frac{\mathrm{d}x}{1+x^2}
$$

$\bm{Proof.}$
部分積分して[(a)](#a)の結果を用いれば, 
$$
\begin{aligned}
    -\int_0^1\mathrm{log}\ x\frac{\mathrm{d}x}{1+x^2} &= -\left[\mathrm{log}\ x\cdot\mathrm{arctan}\ x\right]_0^1+\int_0^1\frac{\mathrm{arctan}\ x}{x}\mathrm{d}x \\
    &= \int_0^1\frac{\mathrm{arctan}\ x}{x}\mathrm{d}x \\
    &= \sum_{n=0}^{\infty}\frac{(-1)^n}{(2n+1)^2}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)