---
title: 今日の計算(42)
date: "2020-12-08T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.11

$\bm{Question.}$
Diracの相対性理論を使うことにより, 原子分光学の微細構造式が
$$
E = mc^2\left\lbrace 1+\frac{\gamma^2}{(s+n-|k|)^2}\right\rbrace^{-\frac{1}{2}}
$$
where
$$
\begin{aligned}
    s &= (|k|^2-\gamma^2)^{\frac{1}{2}}\ ,\ k=\pm 1,\pm 2,\pm 3,\ldots \\
    \gamma^2 &= \frac{Ze^2}{4\pi\epsilon_0\hbar c}
\end{aligned}
$$
によって得られる. $\gamma^4$まで$\gamma^2$について冪級数展開せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    E &= mc^2\left[1+\frac{\gamma^2}{\left\lbrace |k|\displaystyle\left(1-\frac{1}{2}\frac{\gamma^2}{|k|^2}-\cdots\right)+n-|k|\right\rbrace^2}\right]^{\displaystyle-\frac{1}{2}} \\
    &= mc^2\left\lbrace 1+\frac{1}{n^2}\frac{\gamma^2}{\displaystyle\left(1-\frac{1}{2}\frac{\gamma^2}{|k|}-\cdots\right)^2}\right\rbrace^{\displaystyle-\frac{1}{2}} \\
    &= mc^2\left(1+\frac{1}{n^2}\frac{\gamma^2}{\displaystyle 1-\frac{\gamma^2}{n|k|}-\cdots}\right)^{\displaystyle-\frac{1}{2}} \\
    &= mc^2\left\lbrace 1+\frac{1}{n^2}\left(\gamma^2+\frac{\gamma^4}{n|k|}+\cdots\right)\right\rbrace^{\displaystyle-\frac{1}{2}} \\
    &= mc^2\left(1-\frac{\gamma^2}{2n^2}-\frac{\gamma^4}{2n^3|k|}+\frac{3}{8}\frac{\gamma^4}{n^2}+\cdots\right) \\
    &= mc^2\left\lbrace 1-\frac{\gamma^2}{2n^2}-\frac{\gamma^4}{2n^4}\left(\frac{n}{|k|}-\frac{3}{4}\right)+\cdots\right\rbrace .
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)