---
title: 今日の計算(47)
date: "2020-12-13T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.16

$\bm{Question.}$
電子による陽子散乱に対するKlein-Nishinaの式は次の形の項を含む, 
$$
f(\epsilon) = \frac{1+\epsilon}{\epsilon^2}\left\lbrace\frac{2+2\epsilon}{1+2\epsilon}-\frac{\mathrm{log}(1+2\epsilon)}{\epsilon}\right\rbrace\ ,\ \epsilon = \frac{h\nu}{mc^2} .
$$
極限$\displaystyle\lim_{\epsilon\to 0}f(\epsilon)$を求めよ. 

$\bm{Answer.}$
$|2\epsilon|<1$で, 
$$
\begin{aligned}
    f(\epsilon) &= \frac{1+\epsilon}{\epsilon^2}\left\lbrace(2+2\epsilon)(1-2\epsilon+(2\epsilon)^2-\cdots)-\frac{1}{\epsilon}\left(2\epsilon-\frac{(2\epsilon)^2}{2}+\frac{(2\epsilon)^3}{3}-\cdots\right)\right\rbrace \\
    &= \frac{1+\epsilon}{\epsilon^2}\left\lbrace2+2\epsilon-2\epsilon(2+2\epsilon)+2(2\epsilon)^2-\left(2-2\epsilon+\frac{8\epsilon^2}{3}\right)+O(\epsilon^3)\right\rbrace \\
    &= \frac{1+\epsilon}{\epsilon^2}\left(\frac{4\epsilon^2}{3}+O(\epsilon^3)\right) \\
    &= (1+\epsilon)\left(\frac{4}{3}+O(\epsilon)\right)
\end{aligned}
$$
となる. したがって求める極限は
$$
\lim_{\epsilon\to 0}f(\epsilon) = \frac{4}{3}
$$
と求まる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)