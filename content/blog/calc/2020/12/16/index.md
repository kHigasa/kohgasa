---
title: 今日の計算(50)
date: "2020-12-16T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.4.1

$\bm{Question.}$
$$
S(n) = \sum_{j=1}^nj^4 = \frac{n}{30}(2n+1)(n+1)(3n^2+3n-1)
$$
を数学的帰納法によって示せ. 

$\bm{Proof.}$
$n=1$のとき成り立つのはあたりまえ. $n=k$のとき成り立つと仮定すると, $n=k+1$のとき, 
$$
\begin{aligned}
    S(k+1) &= S(k)+(k+1)^4 \\
    &= \frac{k}{30}(2k+1)(k+1)(3k^2+3k-1)+(k+1)^4 \\
    &= \frac{(k+1)}{30}\left\lbrace k(2k+1)(3k^2+3k-1)+30(k+1)^3\right\rbrace \\
    &= \frac{(k+1)}{30}(6k^4+39k^3+91k^2+89k+30) \\
    &= \frac{(k+1)}{30}(k+2)(6k^3+27k^2+37k+15) \\
    &= \frac{(k+1)}{30}(2k+3)(k+2)(3k^2+9k+5) \\
    &= \frac{(k+1)}{30}(2k+3)(k+2)\lbrace 3(k+1)^2+3(k+1)-1\rbrace
\end{aligned}
$$
となり成り立つ. 以上により題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)