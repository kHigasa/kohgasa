---
title: 今日の計算(45)
date: "2020-12-11T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.14
### (a)
$\bm{Question.}$
調和振動子のPlankの法則によりエネルギーの期待値は, 
$$
<\epsilon> = \frac{\displaystyle\sum_{n=1}^{\infty}n\epsilon_0\mathrm{exp}\left(-n\frac{\epsilon_0}{kT}\right)}{\displaystyle\sum_{n=0}^{\infty}\mathrm{exp}\left(-n\frac{\epsilon_0}{kT}\right)}
$$
で与えられる. 分子と分母をそれぞれ二項展開と見て
$$
<\epsilon> = \frac{\epsilon_0}{\mathrm{exp}(\epsilon_0/kT)-1}
$$
と改められることを示せ. 

$\bm{Proof.}$
$x=\mathrm{exp(-\epsilon_0/kT)}$とおいて, 分子/分母は, 
$$
\begin{aligned}
    \text{numerator} &= \epsilon_0\sum_{n=1}^{\infty}nx^2 = \epsilon_0\frac{1}{(1-x)^2} \\
    \text{denominator} &= \sum_{n=0}^{\infty}x^n = \frac{1}{1-x}
\end{aligned}
$$
と改められる. これを用いてエネルギーの期待値は, 
$$
<\epsilon> = \frac{\epsilon_0x}{1-x}=\frac{\epsilon_0}{1/x-1}=\frac{\epsilon_0}{\mathrm{exp}(\epsilon_0/kT)-1}
$$
となり題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$kT\gg\epsilon_0$の極限で$<\epsilon>\sim kT$と簡約化されることを示せ. 

$\bm{Proof.}$
$$
<\epsilon> \sim \frac{\epsilon_0}{\epsilon_0/kT} = kT
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)