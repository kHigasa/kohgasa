---
title: 今日の計算(36)
date: "2020-12-02T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.5

$\bm{Question.}$
$0$以上の整数$n$に対して, $\displaystyle\frac{1}{(1-x)^{n+1}}=\sum_{m=n}^{\infty}\binom{m}{n}x^{m-n}$を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \frac{1}{(1-x)^{n+1}} &= \sum_{p=0}^{\infty}\binom{-(n+1)}{p}(-x)^p \\
    &= \sum_{p=0}^{\infty}(-1)^p\frac{(n+1)\cdots(n+p)}{p!}(-x)^p \\
    &= \sum_{p=0}^{\infty}\frac{(n+1)\cdots(n+p)}{p!}x^p \\
    &p=m-n\text{とおく} \\
    &= \sum_{m=n}^{\infty}\frac{(n+1)\cdots m}{(m-n)!}x^{m-n} \\
    &= \sum_{m=n}^{\infty}\frac{m!}{n!(m-n)!}x^{m-n} \\
    &= \sum_{m=n}^{\infty}\binom{m}{n}x^{m-n} \\
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)