---
title: 今日の計算(62)
date: "2020-12-28T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.5

$\bm{Question.}$
3次元Cartesian座標系でその原点を始点に持つベクトルを考え, 単位体積の立方体の角のベクトルを求めよ. また長さ$\sqrt{2},\sqrt{3}$を持つベクトルを求めよ. 

$\bm{Answer.}$
- 角
$$
(0,0,0),(1,0,0),(0,1,0),(0,0,1),(1,1,0),(0,1,1),(1,0,1),(1,1,1)
$$

- $\sqrt{2}$
$$
(1,1,0),(0,1,1),(1,0,1),(1,-1,0),(0,1,-1),(1,0,-1)
$$

- $\sqrt{3}$
$$
(1,1,1),(-1,1,1),(1,-1,1),(1,1,-1)
$$

### コメント
始点のとり方に応じた符号反転の任意性は残されている. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)