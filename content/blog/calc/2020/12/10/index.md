---
title: 今日の計算(44)
date: "2020-12-10T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.13

$\bm{Question.}$
以下の2つの二項展開を足し合わせて, 無限級数の関係式$\displaystyle\sum_{n=-\infty}^{\infty}x^n = 0$を得よ, 
$$
\frac{x}{1-x}=\sum_{n=1}^{\infty}x^n\ ,\ \frac{x}{x-1}=\frac{1}{1-x^{-1}}=\sum_{n=0}^{\infty}x^{-n}.
$$
またこの式は明らかにナンセンスであるが, どうして誤りなのか説明せよ. 

$\bm{Answer.}$
問にしたがって, 
$$
\begin{aligned}
    \sum_{n=-\infty}^{\infty}x^n &= \sum_{n=0}^{\infty}x^{-n}+\sum_{n=1}^{\infty}x^n \\
    &= \frac{x}{x-1}+\frac{x}{1-x} = 0
\end{aligned}
$$
を得る. これが間違っているのは, 与えられた2つの無限級数の収束半径に共通部分がないからである. 最初の無限級数の収束半径は, $|x|<1\Rightarrow -1<x<1$であるが, 2つ目の無限級数の収束半径は, $|1/x|<1\Rightarrow x>1$となっている. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)