---
title: 今日の計算(57)
date: "2020-12-23T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.6.1

$\bm{Question.}$
$$
\mathrm{log}\left(\frac{1+x}{1-x}\right) = 2\left(x+\frac{x^3}{3}+\frac{x^5}{5}+\cdots\right)\ ,\ |x|<1
$$
を示せ. 

$\bm{Proof.}$
$|x|<1$で, Maclaurin展開すれば, 
$$
\begin{aligned}
    \mathrm{log}\left(\frac{1+x}{1-x}\right) &= \mathrm{log}(1+x)-\mathrm{log}(1-x) \\
    &= \sum_{n=1}^{\infty}(-1)^{n-1}\frac{x^n}{n}-\left(-\sum_{n=1}^{\infty}\frac{x^n}{n}\right) \\
    &= \sum_{n=1}^{\infty}\lbrace (-1)^{n-1}+1\rbrace\frac{x^n}{n} \\
    &= 2\left(x+\frac{x^3}{3}+\frac{x^5}{5}+\cdots\right)
\end{aligned}
$$
となる. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)