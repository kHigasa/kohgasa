---
title: 今日の計算(38)
date: "2020-12-04T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.7

$\bm{Question.}$
2項定理を用いて以下の3つのDopplerシフトの式を比較せよ. 

$\bm{Answer.}$
### (a) 波源が動いている場合
$$
\begin{aligned}
    \nu' &= \nu\left(1\mp\frac{\nu}{c}\right)^{-1} \\
    &= \nu\left(1\pm\frac{\nu}{c}+\frac{\nu^2}{c^2}+\cdots\right)
\end{aligned}
$$

### (b) 観測者が動いている場合
$$
\nu' = \nu\left(1\pm\frac{\nu}{c}\right)
$$

### (c) 相対論的Dopplerシフト
$$
\begin{aligned}
    \nu' &= \nu\left(1\pm\frac{\nu}{c}\right)\left(1-\frac{\nu^2}{c^2}\right)^{-\frac{1}{2}} \\
    &= \nu\left(1\pm\frac{\nu}{c}\right)\left(1+\frac{1}{2}\frac{\nu^2}{c^2}+\cdots\right) \\
    &= \nu\left(1\pm\frac{\nu}{c}+\frac{1}{2}\frac{\nu^2}{c^2}+\cdots\right)
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)