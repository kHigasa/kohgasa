---
title: 今日の計算(60)
date: "2020-12-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.3

$\bm{r}_1=(x_1,y_1,z_1)$を中心とした半径$a$の球がある. 

### (a)

$\bm{Question.}$
球の代数方程式を書き下せ. 

$\bm{Answer.}$
球面上の点を表すベクトルを$\bm{r}=(x,y,z)$とすれば, 
$$
\begin{aligned}
    |\bm{r}-\bm{r}_1| &= a \\
    |\bm{r}-\bm{r}_1|^2 &= a^2 \\
    (x-x_1)^2+(y-y_1)^2+(z-z_1)^2 &= a^2
\end{aligned}
$$
とかける. 

### (b)

### コメント
[(b)](#b)は球に対するベクトル方程式を与えよという問題であり, テキスト内の解答[^1]が$\bm{r}=\bm{r}_1+\bm{a}$となっているがこれは無理があるだろう. 答えるとすれば, [(a)](#a)の第一式であるが, この式が解答の式と等価である筈もない. 検定を受けていない教科書に載っていそうな忖度を要求されるアレである($\xcancel{\text{検定済の教科書にもあるが}}$). そのため省略した. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)