---
title: 今日の計算(65)
date: "2020-12-31T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.9

$\bm{Question.}$
三角形の中線がその長さの$2/3$の位置で交わることを示せ. 

$\bm{Proof.}$
三角形の3頂点を$\bm{a},\bm{b},\bm{c}$とおくと, 各中線は頂点に対応して中線A,B,Cと名付けて順に, 
$$
\bm{a}-\frac{\bm{b}}{2}-\frac{\bm{c}}{2}, \bm{b}-\frac{\bm{c}}{2}-\frac{\bm{a}}{2}, \bm{c}-\frac{\bm{a}}{2}-\frac{\bm{b}}{2}
$$
とかける. 三角形の異なる中線2本は必ず中線を持つので, いま例えば中線AとBの交点を$\bm{x}$とおくと, $\bm{a}-\bm{x}\parallel\text{中線A}$であるから, 定数$\alpha$を用いて, $\text{中線A}=\alpha(\bm{a}-\bm{x})$とかける. 中線Bに関しても同様に, $\text{中線B}=\beta(\bm{b}-\bm{x})$とかける. 以下この2式を$\bm{x}$について解いて, 係数比較して定数を決定し, $\bm{x}$を求める. 
$$
\begin{cases}
   \alpha(\bm{a}-\bm{x}) &= \bm{a}-\displaystyle\frac{\bm{b}}{2}-\frac{\bm{c}}{2} \\
   &\  \\
   \beta(\bm{b}-\bm{x}) &= \bm{b}-\displaystyle\frac{\bm{c}}{2}-\frac{\bm{a}}{2}
\end{cases}
$$
$$
\begin{aligned}
   \Rightarrow \alpha &= \beta=\frac{3}{2} \\
   \therefore\ \bm{x} &= \frac{1}{3}(\bm{a}+\bm{b}+\bm{c})\ .
\end{aligned}
$$
最後に$(\bm{b}-\bm{a})/2$を通るベクトル$\bm{x}-\bm{b}/2+\bm{a}/2$が中線Cを通るかどうかを調べればよいのだが, これは当該ベクトルが$(\bm{c}-\bm{a}/2-\bm{b}/2)/3\parallel\text{中線C}$となることより従う. 他の選び方で最初の交点を選んでも証明の仕方に何ら変更は生じない. 以上により題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)