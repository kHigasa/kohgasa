---
title: 今日の計算(53)
date: "2020-12-19T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.5.2

$\bm{Question.}$
$p$を正の整数として, 次の部分分数分解の式を示せ, 
$$
\frac{1}{n(n+1)\cdots(n+p)} = \frac{1}{p!}\left\lbrace\binom{p}{0}\frac{1}{n}-\binom{p}{1}\frac{1}{n+1}+\cdots +(-1)^p\binom{p}{p}\frac{1}{n+p}\right\rbrace .
$$
次の二項係数の関係式は既知として使って良い, 
$$
\begin{aligned}
    \frac{p+1}{p+1-j}\binom{p}{j} &= \binom{p+1}{j}\ ,\  \\
    \sum_{j=1}^{p+1}(-1)^{j-1}\binom{p+1}{j} &= 1\ .
\end{aligned}
$$

$\bm{Proof.}$
$p=1$のとき成り立つ. $p$のとき成り立つと仮定すると, $p+1$のとき, 
$$
\begin{aligned}
    &\frac{1}{p!}\sum_{j=0}^p(-1)^j\binom{p}{j}\frac{1}{n+j}\frac{1}{n+p+1} \\
    = &\frac{1}{p!}\sum_{j=0}^p(-1)^j\binom{p}{j}\frac{1}{p+1-j}\left(\frac{1}{n+j}-\frac{1}{n+p+1}\right) \\
    = &\frac{1}{(p+1)!}\sum_{j=0}^p(-1)^j\binom{p}{j}\frac{p+1}{p+1-j}\left(\frac{1}{n+j}-\frac{1}{n+p+1}\right) \\
    &\text{与えられた関係式を利用して} \\
    = &\frac{1}{(p+1)!}\sum_{j=0}^p(-1)^j\binom{p+1}{j}\frac{1}{n+j} \\
    &+\frac{1}{(p+1)!}\sum_{j=0}^p(-1)^{j-1}\binom{p+1}{j}\frac{1}{n+p+1} \\
    = &\frac{1}{(p+1)!}\sum_{j=0}^p(-1)^j\binom{p+1}{j}\frac{1}{n+j} \\
    &+\frac{1}{(p+1)!}\sum_{j=1}^{p+1}(-1)^{j-1}\binom{p+1}{j}\frac{1}{n+p+1} \\
    &+\frac{1}{(p+1)!}(-1)^{-1}\binom{p+1}{0}\frac{1}{n+p+1} \\
    &-\frac{1}{(p+1)!}(-1)^{p}\binom{p+1}{p+1}\frac{1}{n+p+1} \\
    = &\frac{1}{(p+1)!}\sum_{j=0}^p(-1)^j\binom{p+1}{j}\frac{1}{n+j} \\
    &\text{与えられた関係式を利用して} \\
    &+\frac{1}{(p+1)!}\frac{1}{n+p+1}-\frac{1}{(p+1)!}\frac{1}{n+p+1} \\
    &+\frac{1}{(p+1)!}(-1)^{p+1}\frac{1}{n+p+1} \\
    = &\frac{1}{(p+1)!}\sum_{j=0}^{p+1}(-1)^j\binom{p+1}{j}\frac{1}{n+j}
\end{aligned}
$$
となり成り立つ. 数学的帰納法により題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)