---
title: 今日の計算(41)
date: "2020-12-07T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.10

$\bm{Question.}$
$x$軸に沿った定数の大きさの力から生じる静止質量$m_0$を持つ1粒子の変位$x$は相対論的効果を考慮して, 
$$
x=\frac{c^2}{g}\left[\left\lbrace 1+\left(g\frac{t}{c}\right)^2\right\rbrace^{\frac{1}{2}}-1\right]
$$
で与えられる. これを時間$t$で冪級数展開せよ. 古典的な結果は$x=(1/2)gt^2$である. 

$\bm{Answer.}$
$$
\begin{aligned}
    x &= \frac{c^2}{g}\left\lbrace 1+\frac{1}{2}\left(g\frac{t}{c}\right)^2+\frac{1}{2!}\frac{1}{2}\left(-\frac{1}{2}\right)\left(g\frac{t}{c}\right)^4+\frac{1}{2!}\frac{1}{2}\left(-\frac{1}{2}\right)\left(-\frac{3}{2}\right)\left(g\frac{t}{c}\right)^6+\cdots-1\right\rbrace \\
    &= \frac{1}{2}gt^2-\frac{1}{8}\left(\frac{g^3}{c^2}\right)t^4+\frac{1}{16}\left(\frac{g^5}{c^4}\right)t^6-\cdots .
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)