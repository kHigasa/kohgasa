---
title: 今日の計算(37)
date: "2020-12-03T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.6

$\bm{Question.}$
$m=1,2,3,\ldots$に対して, 
$$
(1+x)^{-\frac{m}{2}} = \sum_{n=0}^{\infty}(-1)^n\frac{(m+2n-2)!!}{2^nn!(m-2)!!}
$$
を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \text{LHS} &= \sum_{n=0}^{\infty}\binom{-\frac{m}{2}}{n}x^n \\
    &= \sum_{n=0}^{\infty}\frac{1}{n!}\left(-\frac{m}{2}\right)\left(-\frac{m}{2}-1\right)\cdots\left(-\frac{m}{2}-n+1\right)x^n \\
    &= \sum_{n=0}^{\infty}(-1)^n\frac{m(m+2)\cdots(m+2n-2)}{2^nn!}x^n \\
    &= \sum_{n=0}^{\infty}(-1)^n\frac{(m+2n-2)!!}{2^nn!(m-2)!!}x^n = \text{RHS}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)