---
title: 今日の計算(55)
date: "2020-12-21T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.5.4

$\bm{Question.}$
Euler変換[^1]を確かめよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    f(x) &= \frac{1}{1+x}\sum_{n=0}^{\infty}(-1)^na_n\left(\frac{x}{1+x}\right)^n \\
    &= \frac{1}{1+x}\sum_{n=0}^{\infty}(-1)^n\sum_{j=0}^n(-1)^j\binom{n}{j}c_{n-j}\left(\frac{x}{1+x}\right)^n \\
    &n-j=k\text{とおく} \\
    &= \frac{1}{1+x}\sum_{k=0}^{\infty}(-1)^{k+j}\sum_{j=0}^{k+j}(-1)^j\binom{j+k}{j}c_k\left(\frac{x}{1+x}\right)^{j+k} \\
    &= \frac{1}{1+x}\sum_{k=0}^{\infty}(-1)^kc_k\left(\frac{x}{1+x}\right)^k\sum_{j=0}^{\infty}\binom{k+j}{j}\left(\frac{x}{1+x}\right)^j \\
    &= \sum_{k=0}^{\infty}(-1)^kc_k\frac{x^k}{(1+x)^{k+1}}\sum_{j=0}^{\infty}(-1)^j\binom{-k-1}{j}\left(\frac{x}{1+x}\right)^j \\
    &= \sum_{k=0}^{\infty}(-1)^kc_k\frac{x^k}{(1+x)^{k+1}}\sum_{j=0}^{\infty}\binom{-k-1}{j}\left(-\frac{x}{1+x}\right)^j \\
    &= \sum_{k=0}^{\infty}(-1)^kc_k\frac{x^k}{(1+x)^{k+1}}\left(1-\frac{x}{1+x}\right)^{-k-1} \\
    &= \sum_{k=0}^{\infty}(-1)^kc_k\frac{x^k}{(1+x)^{k+1}}\left(\frac{1}{1+x}\right)^{-k-1} \\
    &k\text{を}n\text{に書き直す} \\
    &= \sum_{n=0}^{\infty}(-1)^nc_nx^n\ .
\end{aligned}
$$

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)