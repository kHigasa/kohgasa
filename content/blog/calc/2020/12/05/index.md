---
title: 今日の計算(39)
date: "2020-12-05T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.8

$\bm{Question.}$
一般相対性理論において, 銀河の後退速度をその赤方偏移と結びつける様々な式がある. Milneのモデル(運動学的相対性理論)によると次の[(a)](#a),[(b)](#b),[(c)](#c)の3つの式が与えられる. 展開せよ. 

$\bm{Answer.}$
### (a)
$$
\nu_1 = c\delta\left(1+\frac{1}{2}\delta\right)
$$

### (b)
$$
\begin{aligned}
    \nu_2 &= c\delta\left(1+\frac{1}{2}\delta\right)(1+\delta)^{-2} \\
    &= c\delta\left(1+\frac{1}{2}\delta\right)\left(1-2\delta-\cdots\right) \\
    &= c\delta\left(1-\frac{3}{2}\delta-\cdots\right)
\end{aligned}
$$

### (c)
$$
\begin{aligned}
    1+\delta &= \left(1+2\frac{\nu_3}{c}+2\frac{\nu_3^2}{c^2}+\cdots\right)^{\frac{1}{2}} \\
    &= 1+\frac{\nu_3}{c}+\frac{1}{2}\frac{\nu_3^2}{c^2}+\cdots \\
    \frac{\nu_3^2}{c^2}+2\frac{\nu_3}{c}-2\delta &= 0 \\
    \frac{\nu_3}{c} &= -1+(1+2\delta)^{\frac{1}{2}}\ \because\ \text{LHS>0} \\
    &= -1+\left(1+\delta-\frac{1}{2}\delta^2+\cdots\right) \\
    \therefore\ \nu_3 &= c\delta\left(1-\frac{1}{2}\delta+\cdots\right)
\end{aligned}
$$

### コメント
テキスト[^1]の問を改めて展開のみとした. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)