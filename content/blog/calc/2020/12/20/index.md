---
title: 今日の計算(54)
date: "2020-12-20T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.5.3

$\bm{Question.}$
$$
u_n(p) = \frac{1}{n(n+1)\cdots(n+p)}
$$
について, 
$$
u_n(p) = \frac{u_n(p-1)-u_{n+1}(p-1)}{p}\ ,\ \sum_{n=1}^{\infty}u_n(p)=\frac{1}{pp!}
$$
の2式を示せ. 

$\bm{Proof.}$
まず最初の式は, 
$$
\begin{aligned}
    \text{RHS} &= \frac{1}{p}\left\lbrace\frac{1}{n(n+1)\cdots(n+p-1)}-\frac{1}{(n+1)(n+2)\cdots(n+p)}\right\rbrace \\
    &= \frac{1}{p}\frac{n+p-n}{n(n+1)\cdots(n+p-1)(n+p)} \\
    &= \frac{1}{n(n+1)\cdots(n+p)} = \text{LHS}
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

次に最後の式は最初の式を用いて, 
$$
\begin{aligned}
    \sum_{n=1}^{\infty}u_n(p) &= \sum_{n=1}^{\infty}\frac{u_n(p-1)-u_{n+1}(p-1)}{p} \\
    &= \frac{u_1(p-1)}{p}\ \because\ \lim_{n\to\infty}u_n(p-1)=0 \\
    &= \frac{1}{p}\frac{1}{1\cdot 2\cdots p} = \frac{1}{pp!}
\end{aligned}
$$
と示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)