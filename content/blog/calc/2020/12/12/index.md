---
title: 今日の計算(46)
date: "2020-12-12T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.15

$\bm{Question.}$
$y=\mathrm{tan}^{-1}x$を積分に直して, 被積分関数を二項定理を用いて展開せよ. それを項別積分して$y=\mathrm{tan}^{-1}x$に対するGregory級数を得よ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \mathrm{tan}^{-1}x &= \int_0^x\frac{\mathrm{d}t}{1+t^2} \\
    &= \int_0^x(1-t^2+t^4-t^6+\cdots)\mathrm{d}t \\
    &= x-\frac{x^3}{3}+\frac{x^5}{5}-\frac{x^7}{7}+\cdots \\
    &= \sum_{n=0}^{\infty}(-1)^n\frac{x^{2n+1}}{2n+1}\ ,\ |x|\leq 1
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)