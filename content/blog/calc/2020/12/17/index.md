---
title: 今日の計算(51)
date: "2020-12-17T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.4.2

$\bm{Question.}$
数学的帰納法を用いてLeibnizルール
$$
\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^n(f(x)g(x)) = \sum_{j=0}^n\binom{n}{j}\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^jf(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{n-j}g(x)\right\rbrace
$$
を示せ. 

$\bm{Proof.}$
$n=1$で成り立つ. $n=k$で成り立つと仮定して, $n=k+1$のときも
$$
\begin{aligned}
    \left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k+1}(f(x)g(x)) = &\frac{\mathrm{d}}{\mathrm{d}x}\sum_{j=0}^k\binom{k}{j}\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^jf(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j}g(x)\right\rbrace \\
    = &\sum_{j=0}^k\binom{k}{j}\left[\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{j+1}f(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j}g(x)\right\rbrace\right. \\
    &+\left.\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^jf(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j+1}g(x)\right\rbrace\right] \\
    j=j'+1&\text{とおく} \\
    = &\sum_{j'=-1}^k\binom{k}{j'+1}\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{j'}f(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j'+1}g(x)\right\rbrace \\
    &+\sum_{j=0}^k\binom{k}{j}\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{j}f(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j+1}g(x)\right\rbrace \\
    j'=-1&\text{の項は導関数が定義できないので消えるから} \\
    = &\sum_{j=0}^k\left\lbrace\binom{k}{j+1}+\binom{k}{j} \right\rbrace \\
    &\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{j}f(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j+1}g(x)\right\rbrace \\
    = &\sum_{j=0}^k\frac{(k+1)!}{(j+1)!(k-j)!} \\
    &\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{j}f(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j+1}g(x)\right\rbrace \\
    = &\sum_{j=0}^k\binom{k+1}{j}\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{j}f(x)\right\rbrace\left\lbrace\left(\frac{\mathrm{d}}{\mathrm{d}x}\right)^{k-j+1}g(x)\right\rbrace
\end{aligned}
$$
となり成り立つ. 以上により題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)