---
title: 今日の計算(63)
date: "2020-12-29T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.6

次の2つの場合にベクトル$\bm{r}$によって掃かれる表面を求めよ. 

### (a)
$\bm{Question.}$
$(\bm{r}-\bm{a})\cdot\bm{a}=0$. 

$\bm{Answer.}$
ベクトル$\bm{r}$はベクトル$\bm{a}$に垂直な平面を描く. 

### (b)
$\bm{Question.}$
$(\bm{r}-\bm{a})\cdot\bm{r}=0$. 

$\bm{Answer.}$
$$
(\bm{r}-\bm{a})\cdot\bm{r} = \left(\bm{r}-\frac{\bm{a}}{2}\right)^2-\left(\frac{\bm{a}}{2}\right)^2 = 0
$$
ベクトル$\bm{r}$はベクトル$\bm{a}/2$を中心とする半径$|\bm{a}/2|$の球面を描く. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)