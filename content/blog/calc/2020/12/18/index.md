---
title: 今日の計算(52)
date: "2020-12-18T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.5.1

$\bm{Question.}$
$0<x<1$に対して, $\displaystyle\int_{-x}^x\frac{\mathrm{d}t}{1-t^2}$を計算せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \int_{-x}^x\frac{\mathrm{d}t}{1-t^2} &= 2\int_0^x\frac{\mathrm{d}t}{1-t^2} \\
    &= \int_0^x\left(\frac{1}{t+1}-\frac{1}{t-1}\right)\mathrm{d}t \\
    &= \left[\mathrm{log}\left(\frac{t+1}{t-1}\right)\right]_0^x \\
    &= \mathrm{log}\left(\frac{x+1}{x-1}\right) .
\end{aligned}
$$

### コメント
問[^1]を積分計算に改めた. 

### References

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)