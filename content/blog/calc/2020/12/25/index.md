---
title: 今日の計算(59)
date: "2020-12-25T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.2

$\bm{Question.}$
ある三角形は始点を原点とする3つのベクトル$\bm{A},\bm{B},\bm{C}$によって定義される. $\bm{AB}+\bm{BC}+\bm{CA}=\bm{0}$となることを示せ. 

$\bm{Proof.}$
$$
\bm{AB}+\bm{BC}+\bm{CA} = (\bm{B}-\bm{A})+(\bm{C}-\bm{B})+(\bm{A}-\bm{C}) = \bm{0}\ .
$$
$ $<div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)