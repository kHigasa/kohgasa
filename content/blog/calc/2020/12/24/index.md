---
title: 今日の計算(58)
date: "2020-12-24T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.7.1

$\bm{Question.}$
大きさが$1.732$のベクトル$\bm{A}$の3次元直交座標成分$A_x,A_y,A_z$を求めよ. 

$\bm{Answer.}$
$$
A_x=A_y=A_z=1\ .
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)