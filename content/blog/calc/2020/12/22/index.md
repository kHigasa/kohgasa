---
title: 今日の計算(56)
date: "2020-12-22T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.5.5

$\bm{Question.}$
$$
\mathrm{arctan}\ x = \sum_{n=0}^{\infty}(-1)^n\frac{x^{2n+1}}{2n+1} = \sum_{n=0}^{\infty}(-1)^nc_nx^{2n+1}
$$
に対してEuler変換を実行せよ. また, $\mathrm{arctan}(1), \mathrm{arctan}(1/\sqrt{3})$を計算せよ. 

$\bm{Answer.}$
まずEuler変換して係数を第5項まで求めると, 
$$
\begin{aligned}
    \mathrm{arctan}\ x = &\frac{1}{1+x}\sum_{n=0}^{\infty}(-1)^n\sum_{j=0}^n(-1)^j\binom{n}{j}c_{n-j}\left(\frac{x}{1+x}\right)^n \\
    = &\frac{1}{1+x}\sum_{n=0}^{\infty}(-1)^na_n\left(\frac{x}{1+x}\right)^n \\
    \simeq &\frac{1}{1+x}\sum_{n=0}^4(-1)^na_n\left(\frac{x}{1+x}\right)^n \\
    = &\frac{1}{1+x}\Bigg\lbrace c_0-(c_1-c_0)\left(\frac{x}{1+x}\right) \\
    &+(c_2-2c_1+c_0)\left(\frac{x}{1+x}\right)^2 \\
    &-(c_3-3c_2+3c_1-c_0)\left(\frac{x}{1+x}\right)^3 \\
    &\left.+(c_4-4c_3+6c_2-4c_1+c_0)\left(\frac{x}{1+x}\right)^4\right\rbrace \\
    = &\frac{1}{1+x}\left\lbrace 1+\frac{2}{3}\left(\frac{x}{1+x}\right)+\frac{8}{15}\left(\frac{x}{1+x}\right)^2\right. \\
    &\left. +\frac{16}{35}\left(\frac{x}{1+x}\right)^3+\frac{128}{315}\left(\frac{x}{1+x}\right)^4\right\rbrace
\end{aligned}
$$
となる. また, 
$$
\begin{aligned}
    \mathrm{arctan}(1) &\approx 0.7746031 \\
    \mathrm{arctan}\left(\frac{1}{\sqrt{3}}\right) &\approx 1.1532861 \\
\end{aligned}
$$
と計算される. 

### コメント
Euler変換間違ってそう. 近似が酷すぎるので. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)