---
title: 今日の計算(43)
date: "2020-12-09T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.12

$\bm{Question.}$
陽子同士の正面衝突において, 系の質量中心の運動エネルギーの入射陽子の運動エネルギー$E_k$に対する比率は, 
$$
R = \frac{\sqrt{2mc^2(E_k+2mc^2)}-2mc^2}{E_k}
$$
で与えられる. この比率$R$を[(a)](#a),[(b)](#b)の2つの極限において評価せよ. 

### (a)
$\bm{Condition.}$
$E_k\ll mc^2$(非相対論的極限). 

$\bm{Answer.}$
$$
\begin{aligned}
    R &= \frac{\sqrt{(2mc^2)^2(1+E_k/2mc^2)}-2mc^2}{E_k} \\
    &= \frac{2mc^2\sqrt{1+E_k/2mc^2}-2mc^2}{E_k} \\
    &= \frac{2mc^2(1+E_k/4mc^2-\cdots)-2mc^2}{E_k} \\
    &= \frac{E_k/2-\cdots}{E_k} \\
    &\approx \frac{1}{2} .
\end{aligned}
$$

### (b)
$\bm{Condition.}$
$E_k\gg mc^2$(相対論的極限). 

$\bm{Answer.}$
$$
\begin{aligned}
    R &= \frac{\sqrt{2mc^2E_k(1+2mc^2/E_k)}-2mc^2}{E_k} \\
    &\simeq \sqrt{\frac{2mc^2}{E_k}}-\frac{2mc^2}{E_k} \\
    &\approx 0 .
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)