---
title: 今日の計算(40)
date: "2020-12-06T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.9

$\bm{Question.}$
同じ方向における2つの速度$u$と$v$の相対論的和は, $v/c=u/c=1-\alpha$のとき, 
$$
\frac{w}{c} = \frac{u/c+v/c}{1+uv/c^2}
$$
で与えられる. $0\leq\alpha\leq 1$のとき, $w/c$を$\alpha$の3次まで展開せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    \frac{w}{c} &= \frac{2(1-\alpha)}{1+(1-\alpha)^2} = (1-\alpha)\left\lbrace 1-\left(\alpha-\frac{\alpha}{2}\right)^2\right\rbrace^{-1} \\
    &= (1-\alpha)\left\lbrace 1+\left(\alpha -\frac{\alpha^2}{2}\right)+\left(\alpha -\frac{\alpha^2}{2}\right)^2+\left(\alpha -\frac{\alpha^2}{2}\right)^2+\cdots\right\rbrace \\
    &= 1-\alpha+\alpha-\alpha^2-\frac{\alpha^2}{2}+\frac{\alpha^3}{2}+\alpha^2-\alpha^3-\alpha^3+\alpha^3+\cdots \\
    &\simeq 1-\frac{\alpha^2}{2}-\frac{\alpha^3}{2}.
\end{aligned}
$$

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)