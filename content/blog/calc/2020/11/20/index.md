---
title: 今日の計算(24)
date: "2020-11-20T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.9

$\bm{Question.}$
$\mathrm{cot}\ x$を$x$の冪乗に展開(Laurent展開)せよ. 

$\bm{Answer.}$
$\mathrm{cos}\ x$と$\mathrm{sin}\ x$とのMaclaurin展開の比を取れば, 
$$
\begin{aligned}
   \mathrm{cot}\ x &= \frac{\mathrm{cos}\ x}{\mathrm{sin}\ x} = \frac{\displaystyle\sum_{n=0}^{\infty}(-1)^n\frac{x^{2n}}{(2n)!}}{\displaystyle\sum_{n=0}^{\infty}(-1)^n\frac{x^{2n+1}}{(2n+1)!}} \\
   &= \frac{\displaystyle 1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots}{\displaystyle x-\frac{x^3}{3!}+\frac{x^5}{5!}-\frac{x^7}{7!}+\cdots} \\
   &= \frac{1}{x}\left(\frac{\displaystyle 1-\frac{x^2}{2!}+\frac{x^4}{4!}-\frac{x^6}{6!}+\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right) \\
   &= \frac{1}{x}\left(1+\frac{\displaystyle -\frac{x^2}{2!}+\frac{x^2}{3!}+\frac{x^4}{4!}-\frac{x^4}{5!}-\frac{x^6}{6!}+\frac{x^6}{7!}+\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right) \\
   &= \frac{1}{x}\left(1+\frac{\displaystyle -\frac{x^2}{3}+\frac{x^4}{30}-\frac{x^6}{840}\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right) \\
   &= \frac{1}{x}\left\lbrace 1-\frac{x^2}{3}\left(\frac{\displaystyle 1-\frac{x^2}{10}+\frac{x^4}{280}\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right)\right\rbrace \\
   &= \frac{1}{x}\left\lbrace 1-\frac{x^2}{3}\left(1+\frac{\displaystyle -\frac{x^2}{10}+\frac{x^2}{3!}+\frac{x^4}{280}-\frac{x^4}{5!}\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right)\right\rbrace \\
   &= \frac{1}{x}\left\lbrace 1-\frac{x^2}{3}\left(1+\frac{\displaystyle \frac{x^2}{15}-\frac{x^4}{210}\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right)\right\rbrace \\
   &= \frac{1}{x}\left[ 1-\frac{x^2}{3}\left\lbrace 1+\frac{x^2}{15}\left(\frac{\displaystyle 1-\frac{x^2}{14}\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right)\right\rbrace\right] \\
   &= \frac{1}{x}\left[ 1-\frac{x^2}{3}\left\lbrace 1+\frac{x^2}{15}\left(1+\frac{\displaystyle 1-\frac{x^2}{14}+\frac{x^2}{3!}\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right)\right\rbrace\right] \\
   &= \frac{1}{x}\left[ 1-\frac{x^2}{3}\left\lbrace 1+\frac{x^2}{15}\left(1+\frac{\displaystyle \frac{2x^2}{21}\cdots}{\displaystyle 1-\frac{x^2}{3!}+\frac{x^4}{5!}-\frac{x^6}{7!}+\cdots}\right)\right\rbrace\right] \\
   &= \frac{1}{x}-\frac{x}{3}-\frac{x^3}{45}-\frac{2x^5}{945}-\cdots
\end{aligned}
$$
と計算される. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)