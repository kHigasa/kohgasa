---
title: 今日の計算(25)
date: "2020-11-21T11:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.10

$\bm{Question.}$
$$
\frac{1}{2}\mathrm{log}\frac{\eta_0+1}{\eta_0-1}=\mathrm{Arccoth}\ \eta_0\ ,\ |\eta_0|>1
$$
を示せ. 

$\bm{Proof.}$
$y=\text{RHS}$とおいてこれを解けば左辺が得られることはすぐにわかる. <div class="QED" style="text-align: right">$\Box$</div>

### コメント
これを級数展開によって`示せ'[^1]というのは意味が不明だゾ. どうせ右辺を級数展開するときに左辺を導いてその微分係数を使うのだから. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)