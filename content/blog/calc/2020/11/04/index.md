---
title: 今日の計算(8)
date: "2020-11-04T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.8

$\bm{Question.}$
値$\displaystyle\sum_{n=1}^{1,000}n^{-1}=7.485470\ldots$が与えられたとき, $n=1,000$まででEuler-Mascheroniの定数を見積もれ. 

$\bm{Answer.}$
$$
\lim_{n\to 1,000}\left\lparen\sum_{m=1}^nm^{-1}-\mathrm{log}n\right\rparen = 7.485470-6.907755 = 0.577715
$$
と見積もられる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)