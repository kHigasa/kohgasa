---
title: 今日の計算(9)
date: "2020-11-05T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.10

$\bm{Question.}$
次の無限級数の収束性を判定せよ. 
$$
\sum_{n=1}^{\infty}\left\lbrace\frac{1\cdot 3\cdots (2n-1)}{2\cdot 4\cdots (2n)}\right\rbrace^2
$$

$\bm{Answer.}$
数列の第n項と第n+1項の比は, 
$$
\begin{aligned}
\frac{u_n}{u_{n+1}} &= \left\lbrace\frac{1\cdot 3\cdots (2n-1)}{2\cdot 4\cdots (2n)}\right\rbrace^2\cdot\left\lbrace\frac{2\cdot 4\cdots (2n)\cdot (2n+2)}{1\cdot 3\cdots (2n-1)\cdot (2n+1)}\right\rbrace^2 \\
&= \left\lparen\frac{2n+2}{2n+1}\right\rparen^2 = \left\lparen\frac{1+1/n}{1+1/2n}\right\rparen^2 \\
&\simeq \left(1+\frac{1}{n}\right)\left(1-\frac{1}{n}\right)\ \text{for large}\ n \\
&= 1-\frac{1}{n}-\frac{1}{n^2}
\end{aligned}
$$
となる. $n^{-2}$の係数は有界で, なおかつ$n^{-1}$の係数が1以下であるので, Gaussの判定法によりこの無限級数は発散する. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)