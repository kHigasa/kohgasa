---
title: 今日の計算(23)
date: "2020-11-19T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.8
次を示せ. 

### (a)

$\bm{Question.}$
$\ \displaystyle\mathrm{sin}\ x=\sum_{n=0}^{\infty}(-1)^n\frac{x^{2n+1}}{(2n+1)!}$

$\bm{Proof.}$
$\mathrm{sin}\ x$の導関数は4階で周期的に変化する. つまり$k$を$0$以上の整数とすると, 
$$
\begin{aligned}
   f^{(4k)}(x) &= \mathrm{sin}\ x \\
   f^{(4k+1)}(x) &= \mathrm{cos}\ x \\
   f^{(4k+2)}(x) &= -\mathrm{sin}\ x \\
   f^{(4k+3)}(x) &= -\mathrm{cos}\ x
\end{aligned}
$$
となり, 
$$
\begin{aligned}
   &\sum_{k=0}^{\infty}\left\lbrace f^{(4k)}(0)\frac{x^{4k}}{(4k)!}+f^{(4k+1)}(0)\frac{x^{4k+1}}{(4k+1)!}+f^{(4k+2)}(0)\frac{x^{4k+2}}{(4k+2)!}+f^{(4k+3)}(0)\frac{x^{4k+3}}{(4k+3)!}\right\rbrace \\
   = &\sum_{k=0}^{\infty}\left\lbrace \frac{x^{4k+1}}{(4k+1)!}-\frac{x^{4k+3}}{(4k+3)!}\right\rbrace \\
   = &\sum_{n=0}^{\infty}(-1)^n\frac{x^{2n+1}}{(2n+1)!}
\end{aligned}
$$
とMaclaurin展開されることが示される. <div class="QED" style="text-align: right">$\Box$</div>

### (b)

$\bm{Question.}$
$\ \displaystyle\mathrm{cos}\ x=\sum_{n=0}^{\infty}(-1)^n\frac{x^{2n}}{(2n)!}$

$\bm{Proof.}$
$\mathrm{cos}\ x$の導関数は4階で周期的に変化する. つまり$k$を$0$以上の整数とすると, 
$$
\begin{aligned}
   f^{(4k)}(x) &= \mathrm{cos}\ x \\
   f^{(4k+1)}(x) &= -\mathrm{sin}\ x \\
   f^{(4k+2)}(x) &= -\mathrm{cos}\ x \\
   f^{(4k+3)}(x) &= \mathrm{sin}\ x
\end{aligned}
$$
となり, 
$$
\begin{aligned}
   &\sum_{k=0}^{\infty}\left\lbrace f^{(4k)}(0)\frac{x^{4k}}{(4k)!}+f^{(4k+1)}(0)\frac{x^{4k+1}}{(4k+1)!}+f^{(4k+2)}(0)\frac{x^{4k+2}}{(4k+2)!}+f^{(4k+3)}(0)\frac{x^{4k+3}}{(4k+3)!}\right\rbrace \\
   = &\sum_{k=0}^{\infty}\left\lbrace \frac{x^{4k}}{(4k)!}-\frac{x^{4k+2}}{(4k+2)!}\right\rbrace \\
   = &\sum_{n=0}^{\infty}(-1)^n\frac{x^{2n}}{(2n)!}
\end{aligned}
$$
とMaclaurin展開されることが示される. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)