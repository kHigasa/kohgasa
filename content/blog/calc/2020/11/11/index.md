---
title: 今日の計算(15)
date: "2020-11-11T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.16

Example1.1.11[^1]の収束性向上は$\alpha_2$より対称的な形
$$
\alpha_2' = \sum_{n=2}^{\infty}\frac{1}{(n-1)n(n+1)} = \frac{1}{4}
$$
を用いればより改善される. 

### (a)

$\bm{Question.}$
$\zeta(3)$と$\alpha_2'$を組み合わせて$n^{-5}$の速さで収束する無限級数を得よ. 

$\bm{Answer.}$
$a$を定数とする. 
$$
\begin{aligned}
\zeta(3)+a\alpha_2' &= 1+\sum_{n=2}^{\infty}\left(\frac{1}{n^3}+\frac{a}{(n-1)n(n+1)}\right) \\
&= 1+\sum_{n=2}^{\infty}\frac{(a+1)n^2-1}{(n-1)n^3(n+1)}.
\end{aligned}
$$
$a=-1$と選べば, 
$$
\begin{aligned}
\zeta(3)-\frac{1}{4} &= 1-\sum_{n=2}^{\infty}\frac{1}{(n-1)n^3(n+1)} \\
\therefore \zeta(3) &= \frac{5}{4}-\sum_{n=2}^{\infty}\frac{1}{(n-1)n^3(n+1)}
\end{aligned}
$$
と求めたい無限級数を得た. 

### (b)

$\bm{Question.}$
$\zeta(3)$と$\alpha_2',$
$$
\alpha_4' = \sum_{n=3}^{\infty}\frac{1}{(n-2)(n-1)n(n+1)(n+2)}
$$
を組み合わせて$n^{-7}$の速さで収束する無限級数を得よ. 

$\bm{Answer.}$
$a,b$を定数とする. 
$$
\begin{aligned}
&\zeta(3)+a\alpha_2'+b\alpha_4' \\
= &1+\frac{1}{8}+\frac{1}{6}+\sum_{n=3}^{\infty}\left(\frac{1}{n^3}+\frac{a}{(n-1)n(n+1)}+\frac{b}{(n-2)(n-1)n(n+1)(n+2)}\right) \\
= &\frac{31}{24}+\sum_{n=3}^{\infty}\frac{(a+1)n^4+(-4a+b-5)n^2+4}{(n-2)(n-1)n^3(n+1)(n+2)}.
\end{aligned}
$$
$a=-1,b=1$と選べば, 
$$
\begin{aligned}
\zeta(3)-\frac{1}{4}+\frac{1}{96} &= \frac{31}{24}+\sum_{n=3}^{\infty}\frac{4}{(n-2)(n-1)n^3(n+1)(n+2)} \\
\therefore \zeta(3) &= \frac{147}{96}+\sum_{n=3}^{\infty}\frac{4}{(n-2)(n-1)n^3(n+1)(n+2)}
\end{aligned}
$$
と求めたい無限級数を得た. 

### (c)

$\bm{Question.}$
$\zeta(3)$が小数第6位まで誤差$5\times 10^{-7}$以内で正確に計算されるのに
1. $\displaystyle\sum_{n=1}^{\infty}\frac{1}{n^3}$
1. $\displaystyle\frac{5}{4}-\sum_{n=2}^{\infty}\frac{1}{(n-1)n^3(n+1)}$
1. $\displaystyle\frac{147}{96}+\sum_{n=3}^{\infty}\frac{4}{(n-2)(n-1)n^3(n+1)(n+2)}$

の各場合の無限級数は何項まで考える必要があるか. 

$\bm{Answer.}$
1. 126
1. 26
1. 11

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)