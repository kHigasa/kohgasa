---
title: 今日の計算(5)
date: "2020-11-01T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.5

以下に示す無限級数の収束性を判定せよ. 

### (a)

$\bm{Question.}$
$\displaystyle \sum_{n=2}^{\infty}\frac{1}{\mathrm{log}n}$

$\bm{Answer.}$
$n=2, 3, \cdots$において$n>\mathrm{log}n$であるから, 
$$
0<\sum_{n=2}^{\infty}\frac{1}{n} < \sum_{n=2}^{\infty}\frac{1}{\mathrm{log}n}
$$
である. 左辺は調和級数から1を引いたものなので発散する. したがって右辺の無限級数は発散する. 

### (b)

$\bm{Question.}$
$\displaystyle \sum_{n=1}^{\infty}\frac{n!}{10^n}$

$\bm{Answer.}$
第$n$項と第$n+1$項の比の極限を考えると, 
$$
\lim_{n\to\infty}\frac{(n+1)!}{10^{n+1}}\frac{10^n}{n!} = \lim_{n\to\infty}\frac{n+1}{10} > 1
$$
となる. したがってD'Alembert-Cauchyの比判定法より与えられた無限級数が発散することがわかる. 

### (c)

$\bm{Question.}$
$\displaystyle \sum_{n=1}^{\infty}\frac{1}{2n(2n+1)}$

$\bm{Answer.}$
正の整数において, 
$$
\frac{1}{2n(2n+1)} < \frac{1}{2n\cdot 2n} = \frac{1}{4n^2}
$$
が成り立つ. これより, 
$$
0<\sum_{n=1}^{\infty}\frac{1}{2n(2n+1)} < \sum_{n=1}^{\infty}\frac{1}{4n^2} = \frac{1}{4}\zeta(2)
$$
教科書[^1]Example1.1.5で述べられているRiemannの$\zeta$関数の収束性を用いればこれは収束することがわかる. 

### (d)

$\bm{Question.}$
$\displaystyle \sum_{n=1}^{\infty}\frac{1}{\sqrt{n(n+1)}}$

$\bm{Answer.}$
正の整数において, 
$$
\frac{1}{\sqrt{n(n+1)}} > \frac{1}{\sqrt{(n+1)(n+1)}} = \frac{1}{n+1} \geq \frac{1}{n+n} = \frac{1}{2n}
$$
が成り立つ. したがって, 
$$
\sum_{n=1}^{\infty}\frac{1}{2n} < \sum_{n=1}^{\infty}\frac{1}{\sqrt{n(n+1)}}
$$
となり, 左辺は調和級数の定数倍ゆえ右辺は発散する. 

### (e)

$\bm{Question.}$
$\displaystyle \sum_{n=0}^{\infty}\frac{1}{2n+1}$

$\bm{Answer.}$
$$
\frac{1}{2n+1} > \frac{1}{2n+2} = \frac{1}{2(n+1)}\ \text{for}\ n\in\mathbb{N}
$$
$$
\therefore \sum_{n=0}^{\infty}\frac{1}{2(n+1)} < \sum_{n=0}^{\infty}\frac{1}{2n+1}
$$
左辺は調和級数の定数倍ゆえ右辺は発散する. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)