---
title: 今日の計算(18)
date: "2020-11-14T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.3

無限級数$\displaystyle\sum_{n=0}^{\infty}\frac{1}{1+x^n}$が以下のようになるのは, 正の値$x$がどのような範囲にあるときか. 

### (a)

$\bm{Question.}$
収束する. 

$\bm{Answer.}$
$0\leq 1$で発散するのは明らか. $x>1$で函数$1/(1+x^n)$は単調減少で, 次の極限は
$$
\lim_{n\to\infty}\frac{1+x^n}{1+x^{n+1}} = \lim_{n\to\infty}\frac{1+1/x^n}{x+1/x^n} = \frac{1}{x} < 1
$$
となる. したがってD'Alembert-Cauchyの比判定法より$x\in(1,\infty)$で与えられた無限級数は収束する. 

### (b)

$\bm{Question.}$
一様収束する. 

$\bm{Answer.}$
小問[(a)](#a)の結果より$x\in(1,\infty)$の範囲で一様収束性を考える. このとき$y>1$を満たす最小の$y$を選ぶと
$$
\frac{1}{1+y^n} \geq \left|\frac{1}{1+x^n}\right|
$$
であるので, Weierstrassの優級数判定法より$x\in(1,\infty)$で与えられた無限級数は一様収束することがわかる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)