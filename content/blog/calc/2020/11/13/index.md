---
title: 今日の計算(17)
date: "2020-11-13T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.2

$\bm{Question.}$
$x$がどんな範囲にあるとき無限級数$\displaystyle\sum_{n=0}^{\infty}x^n$は一様収束するか. 

$\bm{Answer.}$
この無限級数は$|x|<1$のとき$1/(1-x)$へ収束するので, この範囲で一様収束性を考える. このとき
$$
|S(x)-s_n(x)| = \left|\frac{1}{1-x}-\frac{1-x^n}{1-x}\right| = \left|\frac{x^n}{1-x}\right| < \epsilon
$$
となり, いかなる小さな$\epsilon > 0$に対してもこの不等式を満たすある$n$が存在するので, 与えられた無限級数は$|x|<1$で一様収束する. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)