---
title: 今日の計算(14)
date: "2020-11-10T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.15

次の式を示せ. 

### (a)

$\bm{Question.}$
$\displaystyle\sum_{n=2}^{\infty}\left(\zeta(n)-1\right)=1$

$\bm{Proof.}$
$$
\sum_{n=2}^{\infty}\left(\zeta(n)-1\right) = \sum_{n=2}^{\infty}\sum_{m=2}^{\infty}\frac{1}{m^n}
$$
この無限級数は絶対収束するので, 和を取る順序を入れ換えてよく, 
$$
\begin{aligned}
\sum_{n=2}^{\infty}\left(\zeta(n)-1\right) &= \sum_{m=2}^{\infty}\sum_{n=2}^{\infty}\frac{1}{m^n} \\
&= \sum_{m=2}^{\infty}\left(\frac{m^{-1}}{1-m^{-1}}-\frac{1}{m}\right) \\
&= \sum_{m=2}^{\infty}\frac{1}{m(m-1)} \\
&= \sum_{m=1}^{\infty}\frac{1}{m(m+1)} = 1
\end{aligned}
$$
と示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)

$\bm{Question.}$
$\displaystyle\sum_{n=2}^{\infty}(-1)^n\left(\zeta(n)-1\right)=\frac{1}{2}$

$\bm{Proof.}$
小問[(a)](#a)と同様に和を取る順序を途中で入れ換えて, 
$$
\begin{aligned}
\sum_{n=2}^{\infty}(-1)^n\left(\zeta(n)-1\right) &= \sum_{k=1}^{\infty}\left\lbrace(-1)^{2k}\left(\zeta(2k)-1\right)+(-1)^{2k+1}\left(\zeta(2k-1)-1\right)\right\rbrace \\
&= \sum_{k=1}^{\infty}\left\lbrace\left(\zeta(2k)-1\right)-\left(\zeta(2k+1)-1\right)\right\rbrace \\
&= \sum_{k=1}^{\infty}\sum_{m=2}^{\infty}\left(\frac{1}{m^{2k}}-\frac{1}{m^{2k+1}}\right) \\
&= \sum_{m=2}^{\infty}\sum_{k=1}^{\infty}\left(\frac{1}{m^{2k}}-\frac{1}{m^{2k+1}}\right) \\
&= \sum_{m=2}^{\infty}\left(\frac{m^{-2}}{1-m^{-2}}-\frac{m^{-3}}{1-m^{-2}}\right) \\
&= \sum_{m=2}^{\infty}\frac{m-1}{m(m^2-1)} = \sum_{m=2}^{\infty}\frac{1}{m(m+1)} \\
&= \sum_{m=1}^{\infty}\frac{1}{m(m+1)} - \frac{1}{2} \\
&= 1-\frac{1}{2} = \frac{1}{2}
\end{aligned}
$$
と示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)