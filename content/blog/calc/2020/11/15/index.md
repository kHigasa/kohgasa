---
title: 今日の計算(19)
date: "2020-11-15T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.4

$\bm{Question.}$
係数の無限級数$\sum a_n, \sum b_n$が絶対収束するとき, Fourier級数$\sum(a_n\mathrm{cos}nx+b_n\mathrm{sin}nx)$が$-\infty < x < \infty$で一様収束することを示せ. 

$\bm{Proof.}$
$-\infty < x < \infty$で
$$
|a_n|+|b_n|\geq|a_n\mathrm{cos}nx+b_n\mathrm{sin}nx|
$$
である. 係数の無限級数$\sum a_n, \sum b_n$が絶対収束するので, Weierstrassの優級数判定法より題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)