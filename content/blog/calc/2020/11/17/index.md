---
title: 今日の計算(21)
date: "2020-11-17T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.6

$\bm{Question.}$
Chebyshev方程式の解は無限級数$\displaystyle\sum_{n=0}^{\infty}u_n(x)x^n$で表され, その第$j$項と第$j+2$項の比は, 
$$
\frac{u_{j+2}(x)}{u_j(x)} = \frac{(k+j)^2-n^2}{(k+j+1)(k+j+2)}x^2\ \text{for}\ k=0\ \text{or}\ 1
$$
で与えられる. この無限級数が$x=\pm 1$で収束するか判定せよ. 

$\bm{Answer.}$
この比の$j\to\infty$における極限を考える. 
- $k=0$のとき
$$
\begin{aligned}
\lim_{j\to\infty}\frac{u_{j+2}(x)}{u_j(x)} &= \lim_{j\to\infty}\frac{j^2-n^2}{(j+1)(j+2)}x^2 \\
&= \lim_{j\to\infty}\frac{1-(n/j)^2}{(1+1/j)(1+2/j)}x^2
\end{aligned}
$$
- $k=1$のとき
$$
\begin{aligned}
\lim_{j\to\infty}\frac{u_{j+2}(x)}{u_j(x)} &= \lim_{j\to\infty}\frac{(j+1)^2-n^2}{(j+2)(j+3)}x^2 \\
&= \lim_{j\to\infty}\frac{1-(n/(j+1))^2}{(1+1/(j+1))(1+2/(j+1))}x^2
\end{aligned}
$$
$x=\pm 1$でどちらの場合も極限は1より小さくなる[^1]. したがってD'Alembert-Cauchyの比判定法より, 無限級数$\displaystyle\sum_nu_n(x)x^n$は$x=\pm 1$で収束する. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)

[^1]:シビアに見ればわかる. 