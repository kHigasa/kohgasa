---
title: 今日の計算(11)
date: "2020-11-07T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.12

$\bm{Question.}$
Catalanの定数は, 
$$
\beta(2) = \sum_{k=0}^{\infty}(-1)^k\frac{1}{(2k+1)^2}
$$
によって定義される. この値を有効数字6桁で見積もれ. 

$\bm{Answer.}$
$$
\begin{aligned}
\beta(2) &= \frac{1}{1^2}-\left(\frac{1}{3^2}-\frac{1}{5^2}\right)-\left(\frac{1}{7^2}-\frac{1}{9^2}\right)-\cdots \\
&= \frac{1}{1^2}-\sum_{k=1}^{\infty}\left\lbrace\frac{1}{(4k-1)^2}-\frac{1}{(4k+1)^2}\right\rbrace \\
&= \frac{1}{1^2}-\sum_{k=1}^{\infty}\frac{16k}{(16k^2-1)^2}
\end{aligned}
$$
最終辺の和をおおよそ積分で評価すると, 
$$
\begin{aligned}
\int_1^{\infty}\frac{16x}{(16x^2-1)^2}\mathrm{d}x &= \left[-\frac{1}{2}\frac{32x}{16x-1}\right]_1^{\infty} \\
&= \left[-\frac{16x}{16x-1}\right]_1^{\infty} \\
&= \left[-\frac{1}{1-1/16x}\right]_1^{\infty} \\
&= -1+\frac{1}{1-1/16} = \frac{1}{15}
\end{aligned}
$$
となる. したがって, 
$$
\beta(2) = 1-\frac{1}{15} = \frac{14}{15} \fallingdotseq 0.933333\ldots
$$
と見積もられる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)