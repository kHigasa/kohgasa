---
title: 今日の計算(16)
date: "2020-11-12T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.1

次の無限級数が一様収束する$x$の範囲を求めよ. 

### (a)

$\bm{Question.}$
$\displaystyle\eta(x)=\sum_{n=1}^{\infty}\frac{(-1)^{n-1}}{n^x}$

$\bm{Answer.}$
極限$\displaystyle\lim_{n\to\infty}1/n^x$を考えるとLeibniz基準より, 無限級数$\eta(x)$は$x\in(0,\infty)$で収束することがわかる. したがってこの範囲で一様収束性を考える. 
$$
|S(x)-s_n(x)| < |u_{n+1}(x)| = \frac{1}{(n+1)^x}
$$
となり, 定義より無限級数$\eta(x)$は$x\in(0,\infty)$で一様収束することがわかる. 

### (b)

$\bm{Question.}$
$\displaystyle\zeta(x)=\sum_{n=1}^{\infty}\frac{1}{n^x}$

$\bm{Answer.}$
無限級数$\zeta(x)$はこれまでに判定したとおり, $x\in(1,\infty)$で収束するので, この範囲で一様収束性を考える. このとき$y>1$を満たす最小の$y$を選ぶと
$$
\frac{1}{n^y} \geq \left|\frac{1}{n^x}\right|
$$
であるので, WeierstrassのMテスト(優級数判定法)より$x\in(1,\infty)$で無限級数$\zeta(x)$は一様収束することがわかる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)