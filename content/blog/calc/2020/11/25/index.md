---
title: 今日の計算(29)
date: "2020-11-25T02:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.14

$\bm{Question.}$
数値解析において次の近似はしばしば便利である, 
$$
\psi^{(2)}(x) \approx \frac{1}{h^2}\lbrace\psi(x+h)-2\psi(x)+\psi(x-h)\rbrace .
$$
この近似における誤差を求めよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    &\psi(x+h)-2\psi(x)+\psi(x-h) \\
    = &\left(\psi(x)+h\psi^{(1)}(x)+\frac{h^2}{2!}\psi^{(2)}(x)+\frac{h^3}{3!}\psi^{(3)}(x)+\frac{h^4}{4!}\psi^{(4)}(x)+\cdots\right) \\
    &-2\psi(x) \\
    &+\left(\psi(x)-h\psi^{(1)}(x)+\frac{h^2}{2!}\psi^{(2)}(x)-\frac{h^3}{3!}\psi^{(3)}(x)-\frac{h^4}{4!}\psi^{(4)}(x)+\cdots\right) \\
    = &h^2\psi^{(2)}(x)+\frac{h^4}{12}\psi^{(4)}(x)+\cdots \\
    \therefore\ &\frac{1}{h^2}\lbrace\psi(x+h)-2\psi(x)+\psi(x-h)\rbrace\approx\psi^{(2)}(x)+\frac{h^2}{12}\psi^{(4)}(x)
\end{aligned}
$$
したがってこの近似における誤差は
$$
\frac{h^2}{12}\psi^{(4)}(x)
$$
程度である. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)