---
title: 今日の計算(30)
date: "2020-11-26T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.2.15

$\bm{Question.}$
次の極限
$$
\lim_{x\to 0}\frac{\mathrm{sin}(\mathrm{tan}\ x)-\mathrm{tan}(\mathrm{sin}\ x)}{x^7}
$$
を評価せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    (\text{与式}) = &\lim_{x\to 0}\frac{1}{x^7}\left\lbrace\mathrm{sin}\left(x+\frac{1}{3}x^3+\frac{2}{15}x^5+\frac{17}{315}x^7+\cdots\right)\right. \\
    &\left.-\mathrm{tan}\left(x-\frac{1}{3!}x^3+\frac{1}{5!}x^5-\frac{1}{7!}x^7+\cdots\right)\right\rbrace \\
    = &\lim_{x\to 0}\frac{1}{x^7}\left[\left\lbrace\left(x+\frac{1}{3}x^3+\frac{2}{15}x^5+\frac{17}{315}x^7+\cdots\right)\right.\right. \\
    &\ -\frac{1}{3!}\left(x+\frac{1}{3}x^3+\frac{2}{15}x^5+\cdots\right)^3 \\
    &\ +\left.\frac{1}{5!}\left(x+\frac{1}{3}x^3+\cdots\right)^5-\frac{1}{7!}(x+\cdots)^7+\cdots\right\rbrace \\
    &-\left\lbrace\left(x-\frac{1}{3!}x^3+\frac{1}{5!}x^5-\frac{1}{7!}x^7+\cdots\right)\right. \\
    &\ +\frac{1}{3}\left(x-\frac{1}{3!}x^3+\frac{1}{5!}x^5-\cdots\right)^3 \\
    &\ \left.\left.+\frac{2}{15}\left(x-\frac{1}{3!}x^3+\right)^5+\frac{17}{315}(x+\cdots)^7+\cdots\right\rbrace\right] \\
    = &\lim_{x\to 0}\frac{1}{x^7}\left[\left(x+\frac{1}{3}x^3+\frac{2}{15}x^5+\frac{17}{315}x^7\right)\right. \\
    &\ -\frac{1}{3!}\left(x^3+x^5+\left(\frac{1}{3}+\frac{2}{5}\right)x^7\right) \\
    &\ +\frac{1}{5!}\left(x^5+\frac{5}{3}x^7\right)-\frac{1}{7!}x^7 \\
    &-\left\lbrace\left(x-\frac{1}{3!}x^3+\frac{1}{5!}x^5-\frac{1}{7!}x^7\right)\right. \\
    &\ +\frac{1}{3}\left(x^3-\frac{1}{2!}x^5+\left(\frac{1}{3!2!}+\frac{3}{5!}\right)x^7\right) \\
    &\ \left.+\frac{2}{15}\left(x^5-\frac{5}{3!}x^7\right)+\frac{17}{315}x^7\right\rbrace \\
    + &\lim_{x\to 0}x\text{の1次以上の項} \\
    = &\lim_{x\to 0}\frac{1}{x^7}\left(-\frac{1}{3!3}-\frac{2}{3!5}+\frac{5}{5!3}-\frac{1}{3!2!3}-\frac{3}{5!3}+\frac{10}{3!15}\right)x^7 \\
    = &-\frac{1}{30}
\end{aligned}
$$
と評価された. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)