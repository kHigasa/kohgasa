---
title: 今日の計算(27)
date: "2020-11-23T02:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.12

$\bm{Question.}$
l'H$\text{\^{o}}$pitalの定理を示せ. 

$\bm{Proof.}$
$$
\begin{aligned}
    \lim_{x\to x_0}\frac{f(x)}{g(x)} &= \lim_{x\to x_0}\frac{f(x+(x_0-x))}{g(x+(x_0-x))} \\
    &= \lim_{x\to x_0}\frac{f(x)+f'(x)(x_0-x)+\cdots}{g(x)+g'(x)(x_0-x)+\cdots} \\
    &= \lim_{x\to x_0}\frac{f'(x)(x_0-x)+\cdots}{g'(x)(x_0-x)+\cdots}\ \because\ 0/0\text{の不定形} \\
    &= \lim_{x\to x_0}\frac{f'(x)}{g'(x)}
\end{aligned}
$$
より示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)