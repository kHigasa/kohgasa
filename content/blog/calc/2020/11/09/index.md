---
title: 今日の計算(13)
date: "2020-11-09T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.14

$\bm{Question.}$
$\displaystyle\lambda(3)=\sum_{n=0}^{\infty}\frac{1}{(2n+1)^3}$に関して少なくとも$n^{-8}$の速さで収束する計算法を与えよ. また, その値を少数第6位まで見積もれ. 

$\bm{Answer.}$
$$
\begin{aligned}
\lambda(3)+\sum_{n=1}^{\infty}\frac{1}{(2n)^3} &= \zeta(3) \\
\therefore \lambda(3) &= \frac{7}{8}\zeta(3)
\end{aligned}
$$
である. 
ここで$a,b,c,d,e,f$を定数とすると, 
$$
\begin{aligned}
&\zeta(3)+a\alpha_1+b\alpha_2+c\alpha_3+d\alpha_4+e\alpha_5+f\zeta(2) \\
= &\sum_{n=1}^{\infty}\left\lbrace\frac{1}{n^3}+\frac{a}{n(n+1)}+\frac{b}{n(n+1)(n+2)}+\frac{c}{n(n+1)(n+2)(n+3)}\right. \\
+ &\left.\frac{d}{n(n+1)(n+2)(n+3)(n+4)}+\frac{e}{n(n+1)(n+2)(n+3)(n+4)(n+5)}+\frac{f}{n^2}\right\rbrace \\
= &\sum_{n=1}^{\infty}\frac{(a+f)n^6+(14a+b+15f+1)n^5+(71a+12b+c+85f+15)n^4}{n^2(n+1)(n+2)} \\
&\frac{(154a+47b+9c+d+225f+85)n^3+(120a+60b+20c+5d+e+274f+225)n^2}{} \\
&\frac{(120f+274)n+120}{}
\end{aligned}
$$
より, $n$について1次以上の項を消すように定数$a,b,c,d,e,f$を選べば, 
$$
\begin{cases}
   a = \frac{137}{60} & b = \frac{77}{60} \\
   c = \frac{47}{30} & d = \frac{27}{10} \\
   e = \frac{24}{5} & f = -\frac{137}{60}
\end{cases}
$$
となるから, 
$$
\begin{aligned}
\zeta(3) = &-\frac{137}{60}\alpha_1-\frac{77}{60}\alpha_2-\frac{47}{30}\alpha_3-\frac{27}{10}\alpha_4-\frac{24}{5}\alpha_5+\frac{137}{60}\zeta(2) \\
&+\sum_{n=1}^{\infty}\frac{120}{n^3(n+1)(n+2)(n+3)(n+4)(n+5)} \\
= &-\frac{137}{60}\cdot 1-\frac{77}{60}\cdot\frac{1}{4}-\frac{47}{30}\cdot\frac{1}{18}-\frac{27}{10}\cdot\frac{1}{96}-\frac{24}{5}\cdot\frac{1}{600}+\frac{137}{60}\frac{\pi^2}{6} \\
&+\sum_{n=1}^{\infty}\frac{120}{n^3(n+1)(n+2)(n+3)(n+4)(n+5)} \\
= &\frac{137\pi^2}{360}-\frac{427103}{216000}+O(n^{-8}) \\
\end{aligned}
$$
を得る. したがって, 
$$
\lambda(3) = \frac{7}{8}\left(\frac{137\pi^2}{360}-\frac{427103}{216000}+\sum_{n=1}^{\infty}\frac{120}{n^3(n+1)(n+2)(n+3)(n+4)(n+5)}\right)
$$
と$n^{-8}$の速さで収束する$\lambda(3)$の表示を得る. またその値は, 
$$
\begin{aligned}
\lambda(3) &\simeq \frac{7}{8}\left(\frac{137\pi^2}{360}-\frac{427103}{216000}\right) \\
&\fallingdotseq 1.778604
\end{aligned}
$$
と見積もられる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)