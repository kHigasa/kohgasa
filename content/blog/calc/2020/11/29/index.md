---
title: 今日の計算(33)
date: "2020-11-29T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.2

$\bm{Question.}$
$$
\int_0^1\frac{\mathrm{d}x}{1+x^2} = \mathrm{arctan}\ x\Big|_0^1 = \frac{\pi}{4}.
$$
被積分関数を冪級数展開し, 項別に積分することにより, 次の冪級数を得る, 
$$
\frac{\pi}{4} = \sum_{n=0}^{\infty}(-1)^n\frac{1}{2n+1}.
$$
ここで$x=1$での被積分関数の無限級数とそれを積分した無限級数の収束性を比較せよ. 

$\bm{Answer.}$
$|x|<1$で被積分関数は, 
$$
(1+x^2)^{-1} = \sum_{n=0}^{\infty}\binom{-1}{n}x^{2n}
$$
と冪級数に展開される. これは$x=1$で, 
$$
(1+x^2)^{-1} = 1-1+1-1+\cdots
$$
となり, 振動することがわかる. 一方, 積分した無限級数は$\pi/4$に収束する. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)