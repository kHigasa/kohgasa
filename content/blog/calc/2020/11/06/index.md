---
title: 今日の計算(10)
date: "2020-11-06T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.11

次の無限級数が収束するか判定せよ. また, もし収束するならば絶対収束するかどうかも調べよ. 

### (a)

$\bm{Question.}$
$\displaystyle\sum_{n=2}^{\infty}(-1)^n\frac{\mathrm{log}n}{n}$

$\bm{Answer.}$
$\mathrm{log}n/n$は$n$について単調減少な函数である. この函数の次の極限は
$$
\lim_{n\to\infty}\frac{\mathrm{log}n}{n} = 0
$$
となるので, Leibniz基準により与えられた無限級数は収束する. また次の積分は
$$
\int_2^{\infty}\frac{\mathrm{log}x}{x}\mathrm{d}x = \left[\mathrm{log}(\mathrm{log}x)\right]_n^{\infty}
$$
となり, Cauchy-Maclaurinの積分判定法により絶対収束はしないことがわかる. 

### (b)

$\bm{Question.}$
$\displaystyle\sum_{n=1}^{\infty}(-1)^{n+1}\frac{1}{n}$

$\bm{Answer.}$
$1/n$は$n$について単調減少な函数である. この函数の次の極限は
$$
\lim_{n\to\infty}\frac{1}{n} = 0
$$
となるので, Leibniz基準により与えられた無限級数は収束する. 一方, $\displaystyle\sum_{n=1}^{\infty}\frac{1}{n}$は調和級数ゆえ発散し, 絶対収束はしないことがわかる. 

### (c)

$\bm{Question.}$
$\displaystyle 1-\frac{1}{2}-\frac{1}{3}+\frac{1}{4}+\frac{1}{5}+\frac{1}{6}-\frac{1}{7}-\frac{1}{8}-\frac{1}{9}-\frac{1}{10}+\frac{1}{11}\cdots$

$\bm{Answer.}$
この無限級数をそのまま正負の項でまとめて改めて文字で置くと, 
$$
\begin{aligned}
&1-\frac{1}{2}-\frac{1}{3}+\frac{1}{4}+\frac{1}{5}+\frac{1}{6}-\frac{1}{7}-\frac{1}{8}-\frac{1}{9}-\frac{1}{10}+\frac{1}{11}+\cdots \\
= &1-\left(\frac{1}{2}+\frac{1}{3}\right)+\left(\frac{1}{4}+\frac{1}{5}+\frac{1}{6}\right)-\left(\frac{1}{7}+\frac{1}{8}+\frac{1}{9}+\frac{1}{10}\right)+\left(\frac{1}{11}+\cdots\right)-\cdots \\
= &s_1-s_2+s_3-s_4+\cdots \\
= &\sum_{n=1}^{\infty}(-1)^{n+1}s_n
\end{aligned}
$$
となる. $s_n$は$n$について単調減少な函数である. この函数の次の極限は
$$
\begin{aligned}
\lim_{n\to\infty}s_n &= \lim_{n\to\infty}\left(\frac{1}{n(n+1)/2+1}+\cdots +\frac{1}{n(n+1)/2+n}\right) \\
&= \lim_{n\to\infty}\left(\frac{2}{n^2+n+2}+\cdots +\frac{2}{n^2+3n}\right) \\
\therefore \lim_{n\to\infty}\frac{2n}{n^2+3n} &< \lim_{n\to\infty}s_n < \lim_{n\to\infty}\frac{2n}{n^2+n+2} \\
\therefore \lim_{n\to\infty}s_n &= 0
\end{aligned}
$$
となるので, Leibniz基準により与えられた無限級数は収束する. 一方, $\displaystyle\sum_{n=1}^{\infty}\frac{1}{n}$は調和級数ゆえ発散し, 絶対収束はしないことがわかる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)