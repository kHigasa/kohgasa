---
title: 今日の計算(32)
date: "2020-11-28T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.1

$\bm{Question.}$
常磁性の古典的なLangevinの法則によると分極$P(x)$は, 
$$
P(x) = \mathrm{c}\left(\frac{\mathrm{cosh}\ x}{\mathrm{sinh}\ x}-\frac{1}{x}\right)
$$
に従う. 小さな$x$(低磁場, 高温)に対して, $P(x)$を冪級数に展開せよ. 

$\bm{Answer.}$
$$
\begin{aligned}
    P(x) &= \mathrm{c}\left(\frac{\displaystyle 1+\frac{x^2}{2!}+\frac{x^4}{4!}+\cdots}{\displaystyle x+\frac{x^3}{3!}+\frac{x^5}{5!}+\cdots}-\frac{1}{x}\right) \\
    &= \mathrm{c}\left[\frac{1}{x}\left\lbrace 1+\frac{\displaystyle \left(\frac{x^2}{2!}+\frac{x^4}{4!}+\cdots\right)-\left(\frac{x^2}{3!}+\frac{x^4}{5!}+\cdots\right)}{\displaystyle 1+\frac{x^2}{3!}+\frac{x^4}{5!}+\cdots}\right\rbrace-\frac{1}{x}\right] \\
    &= \mathrm{c}\left\lbrace\frac{1}{x}\left(1+\frac{\displaystyle\frac{x^2}{3}+\frac{x^4}{30}+\cdots}{\displaystyle 1+\frac{x^2}{3!}+\frac{x^4}{5!}+\cdots}\right)-\frac{1}{x}\right\rbrace \\
    &= \mathrm{c}\frac{1}{x}\left(\frac{\displaystyle\frac{x^2}{3}+\frac{x^4}{30}+\cdots}{\displaystyle 1+\frac{x^2}{3!}+\frac{x^4}{5!}+\cdots}\right) \\
    &= \mathrm{c}\frac{1}{x}x^2\left[\frac{1}{3}+\frac{1}{3}\left\lbrace\frac{\displaystyle 1+\left(\frac{x^2}{10}+\cdots\right)-\left(\frac{x^2}{3!}+\cdots\right)}{\displaystyle 1+\frac{x^2}{3!}+\frac{x^4}{5!}+\cdots}\right\rbrace\right] \\
    &= \mathrm{c}x\left\lbrace\frac{1}{3}+\frac{1}{3}\left(\frac{\displaystyle -\frac{x^2}{15}+\cdots}{\displaystyle 1+\frac{x^2}{3!}+\frac{x^4}{5!}+\cdots}\right)\right\rbrace \\
    &= \mathrm{c}\left(\frac{x}{3}-\frac{x^3}{45}+\cdots\right)
\end{aligned}
$$
と冪級数に展開される. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)