---
title: 今日の計算(28)
date: "2020-11-24T02:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.13

$n>1$のとき次の不等式を示せ. 

### (a)

$\bm{Question.}$
$\displaystyle\frac{1}{n}-\mathrm{log}\left(\frac{n}{n-1}\right)<0$

$\bm{Proof.}$
$1/|n|<1$なので, Maclaurin展開を用いて, 
$$
\begin{aligned}
    \frac{1}{n}-\mathrm{log}\left(\frac{n}{n-1}\right) &= \frac{1}{n}-\left(\frac{1}{n}+\frac{1}{2n^2}+\frac{1}{3n^3}+\cdots\right) \\
    &= -\left(\frac{1}{2n^2}+\frac{1}{3n^3}+\cdots\right) < 0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$\bm{Question.}$
$\displaystyle\frac{1}{n}-\mathrm{log}\left(\frac{n+1}{n}\right)>0$

$\bm{Proof.}$
小問[(a)](#a)と同様にして, 
$$
\begin{aligned}
    \frac{1}{n}-\mathrm{log}\left(\frac{n+1}{n}\right) &= \frac{1}{n}-\mathrm{log}\left(1+\frac{1}{n}\right) \\
    &= \frac{1}{n}-\left(\frac{1}{n}-\frac{1}{2n^2}+\frac{1}{3n^3}-\cdots\right) \\
    &= \left(\frac{1}{2n^2}-\frac{1}{3n^3}\right)+\left(\frac{1}{4n^4}-\frac{1}{5n^5}\right)+\cdots > 0
\end{aligned}
$$
となり示された. <div class="QED" style="text-align: right">$\Box$</div>

### 補問
$\bm{Question.}$
小問[(a)](#a), [(b)](#b)の結果を用いて, Euler-Mascheroniの定数がどのような範囲に収まるか求めよ. 

$\bm{Answer.}$
小問[(a)](#a)の不等式を$n=2$から$\infty$まで和を取ると, 
$$
\begin{aligned}
    0 &> \sum_{n=2}^{\infty}\left\lbrace\frac{1}{n}-\mathrm{log}\left(\frac{n}{n-1}\right)\right\rbrace \\
    &= \sum_{n=2}^{\infty}\frac{1}{n}-\mathrm{log}\left(\frac{2}{1}\frac{3}{2}\cdots\right) \\
    &= \sum_{n=2}^{\infty}\frac{1}{n}-\lim_{n\to\infty}\mathrm{log}\ n \\
    &= \gamma -1 \\
    \therefore\ &\gamma<1
\end{aligned}
$$
を得る. また小問[(b)](#b)の不等式を$n=1$から$\infty$まで和を取ると, 
$$
\begin{aligned}
    0 &< \sum_{n=1}^{\infty}\left\lbrace\frac{1}{n}-\mathrm{log}\left(\frac{n+1}{n}\right)\right\rbrace \\
    &= \sum_{n=1}^{\infty}\frac{1}{n}-\mathrm{log}\left(\frac{2}{1}\frac{3}{2}\cdots\right) \\
    &= \sum_{n=1}^{\infty}\frac{1}{n}-\lim_{n\to\infty}\mathrm{log}\ n \\
    &= \gamma \\
    \therefore\ &\gamma>0
\end{aligned}
$$
を得る. したがって, Euler-Mascheroniの定数は
$$
0<\gamma<1
$$
の範囲にあると結論できる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)