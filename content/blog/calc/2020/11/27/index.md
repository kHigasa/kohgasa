---
title: 今日の計算(31)
date: "2020-11-27T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.2.16

$\bm{Question.}$
冪級数$\displaystyle\sum_{n=0}^{\infty}a_nx^n$の収束半径が$R$のとき, この冪級数の各項を微分/積分した無限級数も同じ収束半径を持つことを示せ. 

$\bm{Proof.}$
いま, 
$$
\lim_{n\to\infty}\left|\frac{a_{n+1}}{a_n}\right|=\frac{1}{R}
$$
が成り立っている. 微分した無限級数は$\displaystyle\sum_{n=0}^{\infty}(n+1)a_{n+1}x^n$となる. このとき第$n$項と第$n+1$項の係数の比に関する次の極限は
$$
\lim_{n\to\infty}\left|\frac{(n+2)a_{n+2}}{(n+1)a_{n+1}}\right|=\lim_{n\to\infty}\left|\left(1+\frac{1}{n+1}\right)\frac{a_{n+2}}{a_{n+1}}\right|=\frac{1}{R}
$$
となり, 収束半径$R$を持つことがわかる. 一方積分した無限級数は$\displaystyle\sum_{n=0}^{\infty}\frac{a_n}{n+1}x^{n+1}$となる. このとき第$n$項と第$n+1$項の係数の比に関する次の極限は
$$
\lim_{n\to\infty}\left|\frac{a_{n+1}}{n+2}\frac{n+1}{a_n}\right|=\lim_{n\to\infty}\left|\left(1-\frac{1}{n+2}\right)\frac{a_{n+1}}{a_n}\right|=\frac{1}{R}
$$
となり, こちらも収束半径$R$を持つことがわかる. 以上により題意は示された. <div class="QED" style="text-align: right">$\Box$</div>

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)