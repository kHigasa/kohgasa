---
title: 今日の計算(26)
date: "2020-11-22T11:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.11

$\bm{Question.}$
$f(x)=x^{1/2}.$

### (a)
$f(x)$がMaclaurin展開できないことを示せ. 

$\bm{Proof.}$
微分係数$f'(0)$は存在しないことより従う. <div class="QED" style="text-align: right">$\Box$</div>

### (b)
$f(x)$は$x=x_0\neq 0$を満たす任意の$x$でTaylor展開できることを示し, そのときの収束半径を求めよ. 

$\bm{Answer.}$
$f^{(n)}(x)\propto x^{1/2-n}$である. したがって, $\forall n((n\in\mathbb{Z}\land n\geq 0)\rightarrow\forall x((x=x_0\neq 0)\rightarrow f^{(n)}(x)))$. よって問の前半は示された. <div class="QED" style="text-align: right">$\Box$</div>

つまりこのとき$x=x_0$まわりでのTaylor展開は, 
$$
f(x) = \sum_{n=0}^{\infty}\frac{f^{(n)}(x)}{n!}(x-x_0)^n
$$
と為される. D'Alembert-Cauchyの比判定法より, このTaylor級数が収束する条件は, 
$$
\begin{aligned}
    &\lim_{n\to\infty}\left|\frac{f^{(n+1)}(x_0)(x-x_0)^{(n+1)}}{(n+1)!}\cdot\frac{n!}{f^{(n)}(x_0)(x-x_0)^n}\right| \\
    = &\lim_{n\to\infty}\left|\frac{x-x_0}{x_0}\cdot\frac{1}{n+1}\right| \\
    < &\left|\frac{x-x_0}{x_0}\right| < 1 \\
    \therefore &\ |x-x_0| < x_0
\end{aligned}
$$
となり, 収束半径は$x_0$と求まる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)