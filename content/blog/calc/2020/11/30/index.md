---
title: 今日の計算(34)
date: "2020-11-30T03:00:00.000Z"
description: "手元の雑多な計算集"
---

## 1.3.3

$\bm{Question.}$
不完全$\gamma$関数
$$
\gamma(n+1,x)\equiv\int_0^x\mathrm{e}^{-t}t^n\mathrm{d}t
$$
を$x$についての冪級数に展開せよ. またその無限級数の収束範囲を求めよ. 

$\bm{Answer.}$
被積分関数を冪級数に展開してそれを積分することにより, 
$$
\begin{aligned}
    \gamma(n+1,x) &= \int_0^xt^n\left(1-\frac{t}{1!}+\frac{t^2}{2!}-\frac{t^3}{3!}+\cdots\right)\mathrm{d}t \\
    &= \left[\frac{t^{n+1}}{n+1}-\frac{t^{n+2}}{1!(n+2)}+\frac{t^{n+3}}{2!(n+3)}-\frac{t^{n+4}}{3!(n+4)}+\cdots\right]_0^x \\
    &= x^{n+1}\left(\frac{1}{x+1}-\frac{x}{1!(n+2)}+\frac{x^2}{2!(n+3)}-\frac{x^3}{3!(n+4)}+\cdots\right) \\
    &= x^{n+1}\sum_{k=0}^{\infty}(-1)^k\frac{x^k}{k!(k+n+1)} = x^{n+1}\sum_{k=0}^{\infty}a_k
\end{aligned}
$$
を得る. 収束範囲はD'Alembert-Cauchyの比判定法を利用して, 
$$
\begin{aligned}
    \lim_{k\to\infty}\left|\frac{a_{k+1}}{a_k}\right| &= \lim_{k\to\infty}\left|\frac{(-1)^{k+1}x^{k+1}}{(k+1)!(k+n+2)}\frac{k!(k+n+1)}{(-1)^kx^k}\right| \\
    &= \lim_{k\to\infty}\left|\frac{-x}{k+1}\left(1-\frac{1}{k+n+2}\right)\right| \\
    &= \lim_{k\to\infty}\left|\frac{-x}{k+1}\right| < 1 \\
    \therefore &\ |-x| < \lim_{k\to\infty}(k+1)
\end{aligned}
$$
が上で求めた無限級数が収束する条件であるとわかる. 但し, $x\to -x$となっているので収束範囲を求めるのには注意が必要で, 答えは$0\leq x <\infty$となる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)