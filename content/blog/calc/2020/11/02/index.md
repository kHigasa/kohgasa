---
title: 今日の計算(6)
date: "2020-11-02T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.6

以下に示す無限級数の収束性を判定せよ. 

### (a)

$\bm{Question.}$
$\displaystyle \sum_{n=1}^{\infty}\frac{1}{n(n+1)}$

$\bm{Answer.}$
正の整数において, 
$$
\frac{1}{n(n+1)} < \frac{1}{n\cdot n} = \frac{1}{n^2}
$$
が成り立つ. したがって, 
$$
0<\sum_{n=1}^{\infty}\frac{1}{n(n+1)} < \sum_{n=1}^{\infty}\frac{1}{n^2} = \zeta(2)
$$
となり, Riemannの$\zeta$関数の収束性を用いればこれは収束することがわかる. 

### (b)

$\bm{Question.}$
$\displaystyle \sum_{n=2}^{\infty}\frac{1}{n\mathrm{log}n}$

$\bm{Answer.}$
次の積分は
$$
\int_2^{\infty}\frac{1}{x\mathrm{log}x}\mathrm{d}x = \left[\mathrm{log}(\mathrm{log}x)\right]_2^{\infty}
$$
となり無限大となるので, Cauchy-Maclaurinの積分判定法によりこれは発散することがわかる. 

### (c)

$\bm{Question.}$
$\displaystyle \sum_{n=1}^{\infty}\frac{1}{n2^n}$

$\bm{Answer.}$
第$n$項と第$n+1$項の比の極限を考えると, 
$$
\lim_{n\to\infty}\frac{n2^n}{(n+1)2^{n+1}} = \lim_{n\to\infty}\frac{1}{2}\left(1-\frac{1}{n+1}\right) = \frac{1}{2} < 1
$$
となる. したがってD'Alembert-Cauchyの比判定法より与えられた無限級数が収束することがわかる. 

### (d)

$\bm{Question.}$
$\displaystyle \sum_{n=1}^{\infty}\mathrm{log}\left(1+\frac{1}{n}\right)$

$\bm{Answer.}$
与えられた無限級数は, 
$$
\begin{aligned}
\sum_{n=1}^{\infty}\mathrm{log}\left(1+\frac{1}{n}\right) &= \sum_{n=1}^{\infty}\left\lbrace\mathrm{log}(n+1)-\mathrm{log}n\right\rbrace \\
&= (\mathrm{log}2-\mathrm{log}1) + (\mathrm{log}3-\mathrm{log}2) + \cdots \\
&= \lim_{n\to\infty}\mathrm{log}n
\end{aligned}
$$
と変形され, これは発散することがわかる. 

### (e)

$\bm{Question.}$
$\displaystyle \sum_{n=1}^{\infty}\frac{1}{n\cdot n^{1/n}}$

$\bm{Answer.}$
いま$n=2,3,\cdots$において$\mathrm{log}n>n^{1/n}$であるから, 
$$
\sum_{n=1}^{\infty}\frac{1}{n\cdot n^{1/n}} = 1+\sum_{n=2}^{\infty}\frac{1}{n\cdot n^{1/n}} > 1+\sum_{n=2}^{\infty}\frac{1}{\mathrm{log}n}
$$
となる. 右辺は小問[(b)](#b)の結果より発散するので, 左辺も発散することがわかる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)