---
title: 今日の計算(20)
date: "2020-11-16T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.5

$\bm{Question.}$
Legendre級数$\displaystyle\sum_{j=even\geq 0}u_j(x)$が収束する$x$の範囲を求めよ. ここにLegendre級数の各項は以下の漸化式を満足する, 
$$
\begin{aligned}
&u_{j+2}(x) = \frac{(j+1)(j+2)-l(l+1)}{(j+2)(j+3)}x^2u_j(x) \\
&j=\text{even variable}\geq 0,\ l=\text{some odd constant}>0.
\end{aligned}
$$

$\bm{Answer.}$
$j=2k, k\geq 0$とおいて新たな無限級数$\displaystyle\sum_{k=0}^{\infty}v_{2k}(x)$を作る. そうすると上の関係は, 
$$
v_{2(k+1)}(x) = \frac{(2k+1)(2k+2)-l(l+1)}{(2k+2)(2k+3)}x^2v_{2k}(x)
$$
となる. したがって次の極限
$$
\begin{aligned}
\lim_{k\to\infty}\frac{v_{2(k+1)}}{v_{2k}} &= \lim_{k\to\infty}\frac{(2k+1)(2k+2)-l(l+1)}{(2k+2)(2k+3)}x^2 \\
&= \lim_{k\to\infty}\left(\frac{2k+1}{2k+3}-\frac{l(l+1)}{(2k+2)(2k+3)}\right)x^2 \\
&= \lim_{k\to\infty}\left\lbrace 1-\frac{1}{k+3/2}-\frac{l(l+1)}{2}\left(\frac{1}{k+1}-\frac{1}{k+3/2}\right)\right\rbrace x^2 \\
&= x^2
\end{aligned}
$$
を得る. D'Alembert-Cauchyの比判定法より$\displaystyle\lim_{k\to\infty}v_{2(k+1)}/v_{2k}<1$で無限級数$\displaystyle\sum_kv_{2k}$が収束するので, Legendre級数$\displaystyle\sum_{j=even\geq 0}u_j(x)$の収束条件は, 
$$
x^2<1\ \therefore |x|<1
$$
と求まる. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)