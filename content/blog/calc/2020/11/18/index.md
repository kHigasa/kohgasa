---
title: 今日の計算(22)
date: "2020-11-18T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.2.7

$\bm{Question.}$
Gegenbauer函数(超球函数)により$x=\pm 1$のとき次の漸化式が導かれる, 
$$
a_{j+2}(\alpha) = a_j(\alpha)\frac{(k+j)(k+j+2\alpha)-n(n+2\alpha)}{(k+j+1)(k+j+2)}.
$$
これの作る無限級数$\displaystyle\sum_{j=0}^{\infty}a_j(\alpha)$は, パラメータ$\alpha$がどんな値のとき収束/発散するか調べよ. 

$\bm{Answer.}$
次の比を考える. 
$$
\begin{aligned}
\frac{a_j(\alpha)}{a_{j+2}(\alpha)} &= \frac{(k+j+1)(k+j+2)}{(k+j)(k+j+2\alpha)-n(n+2\alpha)} \\
&= \frac{(1+(k+1)/j)(1+(k+2)/j)}{(1+k/j)(1+(k+2\alpha)/j)-n(n+2\alpha)/j^2} \\
&= \frac{1+(2k+3)/j+(k+1)(k+2)/j^2}{1+2(k+\alpha)/j+\lbrace k(k+2\alpha)-n(n+2\alpha)\rbrace/j^2} \\
&\simeq \left\lbrace 1+\frac{2k+3}{j}+\frac{(k+1)(k+2)}{j^2}\right\rbrace\left\lbrace 1-\frac{2(k+\alpha)}{j}-\frac{\lbrace k(k+2\alpha)-n(n+2\alpha)\rbrace}{j^2}\right\rbrace \\
&\text{for}\ \text{sufficient large}\ n \\
&\simeq \left(1+\frac{2k+3}{j}\right)\left(1-\frac{2(k+\alpha)}{j}\right) \\
&= 1+\frac{3-2\alpha}{j}-\frac{2(2k+3)(k+\alpha)}{j^2}
\end{aligned}
$$
Gaussの判定法により$j^{-1}$の係数が$3-2\alpha>1\ \therefore\alpha<1$のとき無限級数$\displaystyle\sum_{j=0}^{\infty}a_j(\alpha)$は収束する. このとき$j^{-2}$の係数が有界となっていることも確認できる. したがって結局解答は, 
$$
\begin{cases}
   \text{収束} & \text{for}\ \alpha>1 \\
   \text{発散} & \text{for}\ \alpha\leq 1
\end{cases}
$$
となる. 

### コメント
テキスト[^1]の問題は時々おかしい. 

[^1]:[(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
[^2]:[(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)