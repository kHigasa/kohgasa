---
title: 今日の計算(7)
date: "2020-11-03T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.7

$\bm{Question.}$
無限級数$\displaystyle \sum_{n=2}^{\infty}\frac{1}{n^p(\mathrm{log}n)^q}$は, パラメータ$p,q$がどのように選ばれたとき収束/発散するか. 

$\bm{Answer.}$
小問1.1.6(b)の結果より, $p=q=1$で発散することを知っているのでこの値を基準にして考える. 
- $p=1\ \text{and}\ q>1$のとき
$$
\int_2^{\infty}\frac{1}{x(\mathrm{log}x)^q}\mathrm{d}x = \left[(\mathrm{log}x)^{1-q}\right]_2^{\infty} = -(\mathrm{log}2)^{1-q}
$$
と有限値になり, Cauchy-Maclaurinの積分判定法によりこのとき収束する. 
- $p=1\ \text{and}\ q\leq1$のとき
$$
\int_2^{\infty}\frac{1}{x(\mathrm{log}x)^q}\mathrm{d}x = \left[(\mathrm{log}x)^{1-q}\right]_2^{\infty}
$$
と無限大の値を得, Cauchy-Maclaurinの積分判定法によりこのとき発散する. 
- $p>1\ \text{and}\ q\geq 0$のとき
$$
0<\frac{1}{n^p(\mathrm{log}n)^q} < \frac{1}{n^p} = \zeta(p)
$$
となり, Riemannの$\zeta$関数の収束性より右辺は収束する. したがってこのとき収束する. 
- $p>1\ \text{and}\ q<0$のとき
パラメータ$p,q$の選び方によって収束/発散する. 
- $p<1\ \text{and}\ q\leq 0$のとき
$$
\frac{1}{n^p} = \zeta(p) < \frac{1}{n^p(\mathrm{log}n)^q}
$$
となり, Riemannの$\zeta$関数の収束性より左辺は発散する. したがってこのとき発散する. 
- $p<1\ \text{and}\ q>0$のとき
パラメータ$p,q$の選び方によって収束/発散する. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)