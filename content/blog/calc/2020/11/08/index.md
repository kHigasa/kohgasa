---
title: 今日の計算(12)
date: "2020-11-08T23:59:59.999Z"
description: "手元の雑多な計算集"
---

## 1.1.13

$\bm{Question.}$
$\zeta(2)$は$\displaystyle\alpha_1=\sum_{n=1}^{\infty}\frac{1}{n(n+1)}=1, \alpha_2=\sum_{n=1}^{\infty}\frac{1}{n(n+1)(n+2)}=\frac{1}{4}$とどのように組み合わせれば, $n^{-4}$の速さで収束する無限級数となるか. 

$\bm{Answer.}$
$a,b$を定数とする. 
$$
\begin{aligned}
\zeta(2)+a\alpha_1+b\alpha_2 &= \sum_{n=1}^{\infty}\left\lbrace\frac{1}{n^2}+\frac{a}{n(n+1)}+\frac{b}{n(n+1)(n+2)}\right\rbrace \\
&= \sum_{n=1}^{\infty}\frac{(a+1)n^2+(2a+b+3)n+2}{n^2(n+1)(n+2)}
\end{aligned}
$$
より, 
$$
\begin{aligned}
    &\begin{cases}
       a+1 &= 0 \\
       2a+b+3 &= 0
    \end{cases} \\
    \therefore &\begin{cases}
       a &= -1 \\
       b &= -1
    \end{cases}
\end{aligned}
$$
と定数$a,b$を選べば分子の$n$について1次以上の項は消える. したがって, 
$$
\begin{aligned}
\zeta(2) &= \alpha_1+\alpha_2+\sum_{n=1}^{\infty}\frac{2}{n^2(n+1)(n+2)} \\
&= \frac{5}{4}+\sum_{n=1}^{\infty}\frac{2}{n^2(n+1)(n+2)}
\end{aligned}
$$
と求めたい無限級数を得る. 

### References

1. [(PDF)[7th]Mathematical Methods for Physicists Arfken.pdf](https://www.academia.edu/32064399/_7th_Mathematical_Methods_for_Physicists_Arfken_pdf)
1. [(PDF)Mathematical Methods for Physicists 7th Ed Arfken solutions manual](https://www.academia.edu/24227145/Mathematical_Methods_for_Physicists_7th_Ed_Arfken_solutions_manual)